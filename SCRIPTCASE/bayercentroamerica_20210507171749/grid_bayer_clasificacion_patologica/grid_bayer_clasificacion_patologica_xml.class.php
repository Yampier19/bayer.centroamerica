<?php

class grid_bayer_clasificacion_patologica_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_bayer_clasificacion_patologica_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_grid_bayer_clasificacion_patologica";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "grid_bayer_clasificacion_patologica.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_clasificacion_patologica']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_clasificacion_patologica']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_clasificacion_patologica']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->c_id_clasificacion_patologica = $Busca_temp['c_id_clasificacion_patologica']; 
          $tmp_pos = strpos($this->c_id_clasificacion_patologica, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_id_clasificacion_patologica = substr($this->c_id_clasificacion_patologica, 0, $tmp_pos);
          }
          $this->c_id_clasificacion_patologica_2 = $Busca_temp['c_id_clasificacion_patologica_input_2']; 
          $this->c_nombre_referencia = $Busca_temp['c_nombre_referencia']; 
          $tmp_pos = strpos($this->c_nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_nombre_referencia = substr($this->c_nombre_referencia, 0, $tmp_pos);
          }
          $this->c_nombre_clasificacion = $Busca_temp['c_nombre_clasificacion']; 
          $tmp_pos = strpos($this->c_nombre_clasificacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_nombre_clasificacion = substr($this->c_nombre_clasificacion, 0, $tmp_pos);
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT c.ID_CLASIFICACION_PATOLOGICA as c_id_clasificacion_patologica, c.NOMBRE_REFERENCIA as c_nombre_referencia, c.NOMBRE_CLASIFICACION as c_nombre_clasificacion, p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion_1, p.USUARIO_CREACION as p_usuario_creacion_1 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<grid_bayer_clasificacion_patologica";
         $this->c_id_clasificacion_patologica = $rs->fields[0] ;  
         $this->c_id_clasificacion_patologica = (string)$this->c_id_clasificacion_patologica;
         $this->c_nombre_referencia = $rs->fields[1] ;  
         $this->c_nombre_clasificacion = $rs->fields[2] ;  
         $this->p_id_paciente = $rs->fields[3] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->p_estado_paciente = $rs->fields[4] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[5] ;  
         $this->p_fecha_retiro_paciente = $rs->fields[6] ;  
         $this->p_motivo_retiro_paciente = $rs->fields[7] ;  
         $this->p_observacion_motivo_retiro_paciente = $rs->fields[8] ;  
         $this->p_identificacion_paciente = $rs->fields[9] ;  
         $this->p_nombre_paciente = $rs->fields[10] ;  
         $this->p_apellido_paciente = $rs->fields[11] ;  
         $this->p_correo_paciente = $rs->fields[12] ;  
         $this->p_direccion_paciente = $rs->fields[13] ;  
         $this->p_barrio_paciente = $rs->fields[14] ;  
         $this->p_pais_paciente = $rs->fields[15] ;  
         $this->p_ciudad_paciente = $rs->fields[16] ;  
         $this->p_id_ultima_gestion = $rs->fields[17] ;  
         $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
         $this->p_usuario_creacion = $rs->fields[18] ;  
         $this->p_id_ultima_gestion_1 = $rs->fields[19] ;  
         $this->p_id_ultima_gestion_1 = (string)$this->p_id_ultima_gestion_1;
         $this->p_usuario_creacion_1 = $rs->fields[20] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- c_id_clasificacion_patologica
   function NM_export_c_id_clasificacion_patologica()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->c_id_clasificacion_patologica))
         {
             $this->c_id_clasificacion_patologica = sc_convert_encoding($this->c_id_clasificacion_patologica, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " c_id_clasificacion_patologica =\"" . $this->trata_dados($this->c_id_clasificacion_patologica) . "\"";
   }
   //----- c_nombre_referencia
   function NM_export_c_nombre_referencia()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->c_nombre_referencia))
         {
             $this->c_nombre_referencia = sc_convert_encoding($this->c_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " c_nombre_referencia =\"" . $this->trata_dados($this->c_nombre_referencia) . "\"";
   }
   //----- c_nombre_clasificacion
   function NM_export_c_nombre_clasificacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->c_nombre_clasificacion))
         {
             $this->c_nombre_clasificacion = sc_convert_encoding($this->c_nombre_clasificacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " c_nombre_clasificacion =\"" . $this->trata_dados($this->c_nombre_clasificacion) . "\"";
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_paciente =\"" . $this->trata_dados($this->p_id_paciente) . "\"";
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_estado_paciente))
         {
             $this->p_estado_paciente = sc_convert_encoding($this->p_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_estado_paciente =\"" . $this->trata_dados($this->p_estado_paciente) . "\"";
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_activacion_paciente =\"" . $this->trata_dados($this->p_fecha_activacion_paciente) . "\"";
   }
   //----- p_fecha_retiro_paciente
   function NM_export_p_fecha_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_retiro_paciente))
         {
             $this->p_fecha_retiro_paciente = sc_convert_encoding($this->p_fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_retiro_paciente =\"" . $this->trata_dados($this->p_fecha_retiro_paciente) . "\"";
   }
   //----- p_motivo_retiro_paciente
   function NM_export_p_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_motivo_retiro_paciente))
         {
             $this->p_motivo_retiro_paciente = sc_convert_encoding($this->p_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_motivo_retiro_paciente =\"" . $this->trata_dados($this->p_motivo_retiro_paciente) . "\"";
   }
   //----- p_observacion_motivo_retiro_paciente
   function NM_export_p_observacion_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_observacion_motivo_retiro_paciente))
         {
             $this->p_observacion_motivo_retiro_paciente = sc_convert_encoding($this->p_observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_observacion_motivo_retiro_paciente =\"" . $this->trata_dados($this->p_observacion_motivo_retiro_paciente) . "\"";
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_identificacion_paciente))
         {
             $this->p_identificacion_paciente = sc_convert_encoding($this->p_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_identificacion_paciente =\"" . $this->trata_dados($this->p_identificacion_paciente) . "\"";
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_nombre_paciente))
         {
             $this->p_nombre_paciente = sc_convert_encoding($this->p_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_nombre_paciente =\"" . $this->trata_dados($this->p_nombre_paciente) . "\"";
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_apellido_paciente))
         {
             $this->p_apellido_paciente = sc_convert_encoding($this->p_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_apellido_paciente =\"" . $this->trata_dados($this->p_apellido_paciente) . "\"";
   }
   //----- p_correo_paciente
   function NM_export_p_correo_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_correo_paciente))
         {
             $this->p_correo_paciente = sc_convert_encoding($this->p_correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_correo_paciente =\"" . $this->trata_dados($this->p_correo_paciente) . "\"";
   }
   //----- p_direccion_paciente
   function NM_export_p_direccion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_direccion_paciente))
         {
             $this->p_direccion_paciente = sc_convert_encoding($this->p_direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_direccion_paciente =\"" . $this->trata_dados($this->p_direccion_paciente) . "\"";
   }
   //----- p_barrio_paciente
   function NM_export_p_barrio_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_barrio_paciente))
         {
             $this->p_barrio_paciente = sc_convert_encoding($this->p_barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_barrio_paciente =\"" . $this->trata_dados($this->p_barrio_paciente) . "\"";
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_pais_paciente =\"" . $this->trata_dados($this->p_pais_paciente) . "\"";
   }
   //----- p_ciudad_paciente
   function NM_export_p_ciudad_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_ciudad_paciente))
         {
             $this->p_ciudad_paciente = sc_convert_encoding($this->p_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_ciudad_paciente =\"" . $this->trata_dados($this->p_ciudad_paciente) . "\"";
   }
   //----- p_id_ultima_gestion
   function NM_export_p_id_ultima_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_ultima_gestion))
         {
             $this->p_id_ultima_gestion = sc_convert_encoding($this->p_id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_ultima_gestion =\"" . $this->trata_dados($this->p_id_ultima_gestion) . "\"";
   }
   //----- p_usuario_creacion
   function NM_export_p_usuario_creacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_usuario_creacion))
         {
             $this->p_usuario_creacion = sc_convert_encoding($this->p_usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_usuario_creacion =\"" . $this->trata_dados($this->p_usuario_creacion) . "\"";
   }
   //----- p_id_ultima_gestion_1
   function NM_export_p_id_ultima_gestion_1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_ultima_gestion_1))
         {
             $this->p_id_ultima_gestion_1 = sc_convert_encoding($this->p_id_ultima_gestion_1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_ultima_gestion_1 =\"" . $this->trata_dados($this->p_id_ultima_gestion_1) . "\"";
   }
   //----- p_usuario_creacion_1
   function NM_export_p_usuario_creacion_1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_usuario_creacion_1))
         {
             $this->p_usuario_creacion_1 = sc_convert_encoding($this->p_usuario_creacion_1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_usuario_creacion_1 =\"" . $this->trata_dados($this->p_usuario_creacion_1) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_clasificacion_patologica'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Clasificación Patológica  :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_bayer_clasificacion_patologica_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_bayer_clasificacion_patologica"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
