<?php
   include_once('grid_bayer_paciente_tratamiento_session.php');
   session_start();
   if (!function_exists("NM_is_utf8"))
   {
       include_once("../_lib/lib/php/nm_utf8.php");
   }
    $Ord_Cmp = new grid_bayer_paciente_tratamiento_Ord_cmp(); 
    $Ord_Cmp->Ord_cmp_init();
   
class grid_bayer_paciente_tratamiento_Ord_cmp
{
function Ord_cmp_init()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos, $_POST, $_GET;
   if (isset($_POST['script_case_init']))
   {
       $sc_init    = $_POST['script_case_init'];
       $path_img   = $_POST['path_img'];
       $path_btn   = $_POST['path_btn'];
       $use_alias  = (isset($_POST['use_alias']))  ? $_POST['use_alias']  : "S";
       $fsel_ok    = (isset($_POST['fsel_ok']))    ? $_POST['fsel_ok']    : "";
       $campos_sel = (isset($_POST['campos_sel'])) ? $_POST['campos_sel'] : "";
       $sel_regra  = (isset($_POST['sel_regra']))  ? $_POST['sel_regra']  : "";
       $embbed     = isset($_POST['embbed_groupby']) && 'Y' == $_POST['embbed_groupby'];
       $tbar_pos   = isset($_POST['toolbar_pos']) ? $_POST['toolbar_pos'] : '';
   }
   elseif (isset($_GET['script_case_init']))
   {
       $sc_init    = $_GET['script_case_init'];
       $path_img   = $_GET['path_img'];
       $path_btn   = $_GET['path_btn'];
       $use_alias  = (isset($_GET['use_alias']))  ? $_GET['use_alias']  : "S";
       $fsel_ok    = (isset($_GET['fsel_ok']))    ? $_GET['fsel_ok']    : "";
       $campos_sel = (isset($_GET['campos_sel'])) ? $_GET['campos_sel'] : "";
       $sel_regra  = (isset($_GET['sel_regra']))  ? $_GET['sel_regra']  : "";
       $embbed     = isset($_GET['embbed_groupby']) && 'Y' == $_GET['embbed_groupby'];
       $tbar_pos   = isset($_GET['toolbar_pos']) ? $_GET['toolbar_pos'] : '';
   }
   $STR_lang    = (isset($_SESSION['scriptcase']['str_lang']) && !empty($_SESSION['scriptcase']['str_lang'])) ? $_SESSION['scriptcase']['str_lang'] : "es";
   $NM_arq_lang = "../_lib/lang/" . $STR_lang . ".lang.php";
   $this->Nm_lang = array();
   if (is_file($NM_arq_lang))
   {
       include_once($NM_arq_lang);
   }
   
   $tab_ger_campos = array();
   $tab_def_campos = array();
   $tab_labels     = array();
   $tab_ger_campos['p_id_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_id_paciente'] = "p_id_paciente";
       $tab_converte["p_id_paciente"]   = "p_id_paciente";
   }
   else
   {
       $tab_def_campos['p_id_paciente'] = "p.ID_PACIENTE";
       $tab_converte["p.ID_PACIENTE"]   = "p_id_paciente";
   }
   $tab_labels["p_id_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_id_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_id_paciente"] : "ID PACIENTE";
   $tab_ger_campos['p_estado_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_estado_paciente'] = "p_estado_paciente";
       $tab_converte["p_estado_paciente"]   = "p_estado_paciente";
   }
   else
   {
       $tab_def_campos['p_estado_paciente'] = "p.ESTADO_PACIENTE";
       $tab_converte["p.ESTADO_PACIENTE"]   = "p_estado_paciente";
   }
   $tab_labels["p_estado_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_estado_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_estado_paciente"] : "ESTADO PACIENTE";
   $tab_ger_campos['p_fecha_activacion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_fecha_activacion_paciente'] = "p_fecha_activacion_paciente";
       $tab_converte["p_fecha_activacion_paciente"]   = "p_fecha_activacion_paciente";
   }
   else
   {
       $tab_def_campos['p_fecha_activacion_paciente'] = "p.FECHA_ACTIVACION_PACIENTE";
       $tab_converte["p.FECHA_ACTIVACION_PACIENTE"]   = "p_fecha_activacion_paciente";
   }
   $tab_labels["p_fecha_activacion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_activacion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_activacion_paciente"] : "FECHA ACTIVACION PACIENTE";
   $tab_ger_campos['p_fecha_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_fecha_retiro_paciente'] = "p_fecha_retiro_paciente";
       $tab_converte["p_fecha_retiro_paciente"]   = "p_fecha_retiro_paciente";
   }
   else
   {
       $tab_def_campos['p_fecha_retiro_paciente'] = "p.FECHA_RETIRO_PACIENTE";
       $tab_converte["p.FECHA_RETIRO_PACIENTE"]   = "p_fecha_retiro_paciente";
   }
   $tab_labels["p_fecha_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_retiro_paciente"] : "FECHA RETIRO PACIENTE";
   $tab_ger_campos['p_motivo_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_motivo_retiro_paciente'] = "p_motivo_retiro_paciente";
       $tab_converte["p_motivo_retiro_paciente"]   = "p_motivo_retiro_paciente";
   }
   else
   {
       $tab_def_campos['p_motivo_retiro_paciente'] = "p.MOTIVO_RETIRO_PACIENTE";
       $tab_converte["p.MOTIVO_RETIRO_PACIENTE"]   = "p_motivo_retiro_paciente";
   }
   $tab_labels["p_motivo_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_motivo_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_motivo_retiro_paciente"] : "MOTIVO RETIRO PACIENTE";
   $tab_ger_campos['p_observacion_motivo_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_observacion_motivo_retiro_paciente'] = "cmp_maior_30_1";
       $tab_converte["cmp_maior_30_1"]   = "p_observacion_motivo_retiro_paciente";
   }
   else
   {
       $tab_def_campos['p_observacion_motivo_retiro_paciente'] = "p.OBSERVACION_MOTIVO_RETIRO_PACIENTE";
       $tab_converte["p.OBSERVACION_MOTIVO_RETIRO_PACIENTE"]   = "p_observacion_motivo_retiro_paciente";
   }
   $tab_labels["p_observacion_motivo_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_observacion_motivo_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_observacion_motivo_retiro_paciente"] : "OBSERVACION MOTIVO RETIRO PACIENTE";
   $tab_ger_campos['p_identificacion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_identificacion_paciente'] = "p_identificacion_paciente";
       $tab_converte["p_identificacion_paciente"]   = "p_identificacion_paciente";
   }
   else
   {
       $tab_def_campos['p_identificacion_paciente'] = "p.IDENTIFICACION_PACIENTE";
       $tab_converte["p.IDENTIFICACION_PACIENTE"]   = "p_identificacion_paciente";
   }
   $tab_labels["p_identificacion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_identificacion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_identificacion_paciente"] : "IDENTIFICACION PACIENTE";
   $tab_ger_campos['p_nombre_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_nombre_paciente'] = "p_nombre_paciente";
       $tab_converte["p_nombre_paciente"]   = "p_nombre_paciente";
   }
   else
   {
       $tab_def_campos['p_nombre_paciente'] = "p.NOMBRE_PACIENTE";
       $tab_converte["p.NOMBRE_PACIENTE"]   = "p_nombre_paciente";
   }
   $tab_labels["p_nombre_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_nombre_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_nombre_paciente"] : "NOMBRE PACIENTE";
   $tab_ger_campos['p_apellido_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_apellido_paciente'] = "p_apellido_paciente";
       $tab_converte["p_apellido_paciente"]   = "p_apellido_paciente";
   }
   else
   {
       $tab_def_campos['p_apellido_paciente'] = "p.APELLIDO_PACIENTE";
       $tab_converte["p.APELLIDO_PACIENTE"]   = "p_apellido_paciente";
   }
   $tab_labels["p_apellido_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_apellido_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_apellido_paciente"] : "APELLIDO PACIENTE";
   $tab_ger_campos['p_telefono_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_telefono_paciente'] = "p_telefono_paciente";
       $tab_converte["p_telefono_paciente"]   = "p_telefono_paciente";
   }
   else
   {
       $tab_def_campos['p_telefono_paciente'] = "p.TELEFONO_PACIENTE";
       $tab_converte["p.TELEFONO_PACIENTE"]   = "p_telefono_paciente";
   }
   $tab_labels["p_telefono_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono_paciente"] : "TELEFONO PACIENTE";
   $tab_ger_campos['p_telefono2_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_telefono2_paciente'] = "p_telefono2_paciente";
       $tab_converte["p_telefono2_paciente"]   = "p_telefono2_paciente";
   }
   else
   {
       $tab_def_campos['p_telefono2_paciente'] = "p.TELEFONO2_PACIENTE";
       $tab_converte["p.TELEFONO2_PACIENTE"]   = "p_telefono2_paciente";
   }
   $tab_labels["p_telefono2_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono2_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono2_paciente"] : "TELEFONO2 PACIENTE";
   $tab_ger_campos['p_telefono3_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_telefono3_paciente'] = "p_telefono3_paciente";
       $tab_converte["p_telefono3_paciente"]   = "p_telefono3_paciente";
   }
   else
   {
       $tab_def_campos['p_telefono3_paciente'] = "p.TELEFONO3_PACIENTE";
       $tab_converte["p.TELEFONO3_PACIENTE"]   = "p_telefono3_paciente";
   }
   $tab_labels["p_telefono3_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono3_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_telefono3_paciente"] : "TELEFONO3 PACIENTE";
   $tab_ger_campos['p_correo_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_correo_paciente'] = "p_correo_paciente";
       $tab_converte["p_correo_paciente"]   = "p_correo_paciente";
   }
   else
   {
       $tab_def_campos['p_correo_paciente'] = "p.CORREO_PACIENTE";
       $tab_converte["p.CORREO_PACIENTE"]   = "p_correo_paciente";
   }
   $tab_labels["p_correo_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_correo_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_correo_paciente"] : "CORREO PACIENTE";
   $tab_ger_campos['p_direccion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_direccion_paciente'] = "p_direccion_paciente";
       $tab_converte["p_direccion_paciente"]   = "p_direccion_paciente";
   }
   else
   {
       $tab_def_campos['p_direccion_paciente'] = "p.DIRECCION_PACIENTE";
       $tab_converte["p.DIRECCION_PACIENTE"]   = "p_direccion_paciente";
   }
   $tab_labels["p_direccion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_direccion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_direccion_paciente"] : "DIRECCION PACIENTE";
   $tab_ger_campos['p_genero_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_genero_paciente'] = "p_genero_paciente";
       $tab_converte["p_genero_paciente"]   = "p_genero_paciente";
   }
   else
   {
       $tab_def_campos['p_genero_paciente'] = "p.GENERO_PACIENTE";
       $tab_converte["p.GENERO_PACIENTE"]   = "p_genero_paciente";
   }
   $tab_labels["p_genero_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_genero_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_genero_paciente"] : "GENERO PACIENTE";
   $tab_ger_campos['p_fecha_nacimineto_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_fecha_nacimineto_paciente'] = "p_fecha_nacimineto_paciente";
       $tab_converte["p_fecha_nacimineto_paciente"]   = "p_fecha_nacimineto_paciente";
   }
   else
   {
       $tab_def_campos['p_fecha_nacimineto_paciente'] = "p.FECHA_NACIMINETO_PACIENTE";
       $tab_converte["p.FECHA_NACIMINETO_PACIENTE"]   = "p_fecha_nacimineto_paciente";
   }
   $tab_labels["p_fecha_nacimineto_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_nacimineto_paciente"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_fecha_nacimineto_paciente"] : "FECHA NACIMINETO PACIENTE";
   $tab_ger_campos['p_id_ultima_gestion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_id_ultima_gestion'] = "p_id_ultima_gestion";
       $tab_converte["p_id_ultima_gestion"]   = "p_id_ultima_gestion";
   }
   else
   {
       $tab_def_campos['p_id_ultima_gestion'] = "p.ID_ULTIMA_GESTION";
       $tab_converte["p.ID_ULTIMA_GESTION"]   = "p_id_ultima_gestion";
   }
   $tab_labels["p_id_ultima_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_id_ultima_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_id_ultima_gestion"] : "ID ULTIMA GESTION";
   $tab_ger_campos['t_id_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_id_tratamiento'] = "t_id_tratamiento";
       $tab_converte["t_id_tratamiento"]   = "t_id_tratamiento";
   }
   else
   {
       $tab_def_campos['t_id_tratamiento'] = "t.ID_TRATAMIENTO";
       $tab_converte["t.ID_TRATAMIENTO"]   = "t_id_tratamiento";
   }
   $tab_labels["t_id_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_id_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_id_tratamiento"] : "ID TRATAMIENTO";
   $tab_ger_campos['p_usuario_creacion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['p_usuario_creacion'] = "p_usuario_creacion";
       $tab_converte["p_usuario_creacion"]   = "p_usuario_creacion";
   }
   else
   {
       $tab_def_campos['p_usuario_creacion'] = "p.USUARIO_CREACION";
       $tab_converte["p.USUARIO_CREACION"]   = "p_usuario_creacion";
   }
   $tab_labels["p_usuario_creacion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_usuario_creacion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["p_usuario_creacion"] : "USUARIO CREACION";
   $tab_ger_campos['t_producto_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_producto_tratamiento'] = "t_producto_tratamiento";
       $tab_converte["t_producto_tratamiento"]   = "t_producto_tratamiento";
   }
   else
   {
       $tab_def_campos['t_producto_tratamiento'] = "t.PRODUCTO_TRATAMIENTO";
       $tab_converte["t.PRODUCTO_TRATAMIENTO"]   = "t_producto_tratamiento";
   }
   $tab_labels["t_producto_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_producto_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_producto_tratamiento"] : "PRODUCTO TRATAMIENTO";
   $tab_ger_campos['t_nombre_referencia'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_nombre_referencia'] = "t_nombre_referencia";
       $tab_converte["t_nombre_referencia"]   = "t_nombre_referencia";
   }
   else
   {
       $tab_def_campos['t_nombre_referencia'] = "t.NOMBRE_REFERENCIA";
       $tab_converte["t.NOMBRE_REFERENCIA"]   = "t_nombre_referencia";
   }
   $tab_labels["t_nombre_referencia"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_nombre_referencia"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_nombre_referencia"] : "NOMBRE REFERENCIA";
   $tab_ger_campos['t_clasificacion_patologica_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_clasificacion_patologica_tratamiento'] = "cmp_maior_30_2";
       $tab_converte["cmp_maior_30_2"]   = "t_clasificacion_patologica_tratamiento";
   }
   else
   {
       $tab_def_campos['t_clasificacion_patologica_tratamiento'] = "t.CLASIFICACION_PATOLOGICA_TRATAMIENTO";
       $tab_converte["t.CLASIFICACION_PATOLOGICA_TRATAMIENTO"]   = "t_clasificacion_patologica_tratamiento";
   }
   $tab_labels["t_clasificacion_patologica_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_clasificacion_patologica_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_clasificacion_patologica_tratamiento"] : "CLASIFICACION PATOLOGICA TRATAMIENTO";
   $tab_ger_campos['t_tratamiento_previo'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_tratamiento_previo'] = "t_tratamiento_previo";
       $tab_converte["t_tratamiento_previo"]   = "t_tratamiento_previo";
   }
   else
   {
       $tab_def_campos['t_tratamiento_previo'] = "t.TRATAMIENTO_PREVIO";
       $tab_converte["t.TRATAMIENTO_PREVIO"]   = "t_tratamiento_previo";
   }
   $tab_labels["t_tratamiento_previo"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_tratamiento_previo"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_tratamiento_previo"] : "TRATAMIENTO PREVIO";
   $tab_ger_campos['t_consentimiento_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_consentimiento_tratamiento'] = "t_consentimiento_tratamiento";
       $tab_converte["t_consentimiento_tratamiento"]   = "t_consentimiento_tratamiento";
   }
   else
   {
       $tab_def_campos['t_consentimiento_tratamiento'] = "t.CONSENTIMIENTO_TRATAMIENTO";
       $tab_converte["t.CONSENTIMIENTO_TRATAMIENTO"]   = "t_consentimiento_tratamiento";
   }
   $tab_labels["t_consentimiento_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_consentimiento_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_consentimiento_tratamiento"] : "CONSENTIMIENTO TRATAMIENTO";
   $tab_ger_campos['t_fecha_inicio_terapia_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_fecha_inicio_terapia_tratamiento'] = "cmp_maior_30_3";
       $tab_converte["cmp_maior_30_3"]   = "t_fecha_inicio_terapia_tratamiento";
   }
   else
   {
       $tab_def_campos['t_fecha_inicio_terapia_tratamiento'] = "t.FECHA_INICIO_TERAPIA_TRATAMIENTO";
       $tab_converte["t.FECHA_INICIO_TERAPIA_TRATAMIENTO"]   = "t_fecha_inicio_terapia_tratamiento";
   }
   $tab_labels["t_fecha_inicio_terapia_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_fecha_inicio_terapia_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_fecha_inicio_terapia_tratamiento"] : "FECHA INICIO TERAPIA TRATAMIENTO";
   $tab_ger_campos['t_regimen_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_regimen_tratamiento'] = "t_regimen_tratamiento";
       $tab_converte["t_regimen_tratamiento"]   = "t_regimen_tratamiento";
   }
   else
   {
       $tab_def_campos['t_regimen_tratamiento'] = "t.REGIMEN_TRATAMIENTO";
       $tab_converte["t.REGIMEN_TRATAMIENTO"]   = "t_regimen_tratamiento";
   }
   $tab_labels["t_regimen_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_regimen_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_regimen_tratamiento"] : "REGIMEN TRATAMIENTO";
   $tab_ger_campos['t_asegurador_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_asegurador_tratamiento'] = "t_asegurador_tratamiento";
       $tab_converte["t_asegurador_tratamiento"]   = "t_asegurador_tratamiento";
   }
   else
   {
       $tab_def_campos['t_asegurador_tratamiento'] = "t.ASEGURADOR_TRATAMIENTO";
       $tab_converte["t.ASEGURADOR_TRATAMIENTO"]   = "t_asegurador_tratamiento";
   }
   $tab_labels["t_asegurador_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_asegurador_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_asegurador_tratamiento"] : "ASEGURADOR TRATAMIENTO";
   $tab_ger_campos['t_operador_logistico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_operador_logistico_tratamiento'] = "cmp_maior_30_4";
       $tab_converte["cmp_maior_30_4"]   = "t_operador_logistico_tratamiento";
   }
   else
   {
       $tab_def_campos['t_operador_logistico_tratamiento'] = "t.OPERADOR_LOGISTICO_TRATAMIENTO";
       $tab_converte["t.OPERADOR_LOGISTICO_TRATAMIENTO"]   = "t_operador_logistico_tratamiento";
   }
   $tab_labels["t_operador_logistico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_operador_logistico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_operador_logistico_tratamiento"] : "OPERADOR LOGISTICO TRATAMIENTO";
   $tab_ger_campos['t_fecha_ultima_reclamacion_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_fecha_ultima_reclamacion_tratamiento'] = "cmp_maior_30_5";
       $tab_converte["cmp_maior_30_5"]   = "t_fecha_ultima_reclamacion_tratamiento";
   }
   else
   {
       $tab_def_campos['t_fecha_ultima_reclamacion_tratamiento'] = "t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO";
       $tab_converte["t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO"]   = "t_fecha_ultima_reclamacion_tratamiento";
   }
   $tab_labels["t_fecha_ultima_reclamacion_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_fecha_ultima_reclamacion_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_fecha_ultima_reclamacion_tratamiento"] : "FECHA ULTIMA RECLAMACION TRATAMIENTO";
   $tab_ger_campos['t_otros_operadores_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_otros_operadores_tratamiento'] = "t_otros_operadores_tratamiento";
       $tab_converte["t_otros_operadores_tratamiento"]   = "t_otros_operadores_tratamiento";
   }
   else
   {
       $tab_def_campos['t_otros_operadores_tratamiento'] = "t.OTROS_OPERADORES_TRATAMIENTO";
       $tab_converte["t.OTROS_OPERADORES_TRATAMIENTO"]   = "t_otros_operadores_tratamiento";
   }
   $tab_labels["t_otros_operadores_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_otros_operadores_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_otros_operadores_tratamiento"] : "OTROS OPERADORES TRATAMIENTO";
   $tab_ger_campos['t_medios_adquisicion_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_medios_adquisicion_tratamiento'] = "cmp_maior_30_6";
       $tab_converte["cmp_maior_30_6"]   = "t_medios_adquisicion_tratamiento";
   }
   else
   {
       $tab_def_campos['t_medios_adquisicion_tratamiento'] = "t.MEDIOS_ADQUISICION_TRATAMIENTO";
       $tab_converte["t.MEDIOS_ADQUISICION_TRATAMIENTO"]   = "t_medios_adquisicion_tratamiento";
   }
   $tab_labels["t_medios_adquisicion_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_medios_adquisicion_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_medios_adquisicion_tratamiento"] : "MEDIOS ADQUISICION TRATAMIENTO";
   $tab_ger_campos['t_ips_atiende_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_ips_atiende_tratamiento'] = "t_ips_atiende_tratamiento";
       $tab_converte["t_ips_atiende_tratamiento"]   = "t_ips_atiende_tratamiento";
   }
   else
   {
       $tab_def_campos['t_ips_atiende_tratamiento'] = "t.IPS_ATIENDE_TRATAMIENTO";
       $tab_converte["t.IPS_ATIENDE_TRATAMIENTO"]   = "t_ips_atiende_tratamiento";
   }
   $tab_labels["t_ips_atiende_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_ips_atiende_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_ips_atiende_tratamiento"] : "IPS ATIENDE TRATAMIENTO";
   $tab_ger_campos['t_medico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_medico_tratamiento'] = "t_medico_tratamiento";
       $tab_converte["t_medico_tratamiento"]   = "t_medico_tratamiento";
   }
   else
   {
       $tab_def_campos['t_medico_tratamiento'] = "t.MEDICO_TRATAMIENTO";
       $tab_converte["t.MEDICO_TRATAMIENTO"]   = "t_medico_tratamiento";
   }
   $tab_labels["t_medico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_medico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_medico_tratamiento"] : "MEDICO TRATAMIENTO";
   $tab_ger_campos['t_especialidad_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_especialidad_tratamiento'] = "t_especialidad_tratamiento";
       $tab_converte["t_especialidad_tratamiento"]   = "t_especialidad_tratamiento";
   }
   else
   {
       $tab_def_campos['t_especialidad_tratamiento'] = "t.ESPECIALIDAD_TRATAMIENTO";
       $tab_converte["t.ESPECIALIDAD_TRATAMIENTO"]   = "t_especialidad_tratamiento";
   }
   $tab_labels["t_especialidad_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_especialidad_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_especialidad_tratamiento"] : "ESPECIALIDAD TRATAMIENTO";
   $tab_ger_campos['t_paramedico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_paramedico_tratamiento'] = "t_paramedico_tratamiento";
       $tab_converte["t_paramedico_tratamiento"]   = "t_paramedico_tratamiento";
   }
   else
   {
       $tab_def_campos['t_paramedico_tratamiento'] = "t.PARAMEDICO_TRATAMIENTO";
       $tab_converte["t.PARAMEDICO_TRATAMIENTO"]   = "t_paramedico_tratamiento";
   }
   $tab_labels["t_paramedico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_paramedico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_paramedico_tratamiento"] : "PARAMEDICO TRATAMIENTO";
   $tab_ger_campos['t_ciudad_base_paramedico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_ciudad_base_paramedico_tratamiento'] = "cmp_maior_30_7";
       $tab_converte["cmp_maior_30_7"]   = "t_ciudad_base_paramedico_tratamiento";
   }
   else
   {
       $tab_def_campos['t_ciudad_base_paramedico_tratamiento'] = "t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO";
       $tab_converte["t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO"]   = "t_ciudad_base_paramedico_tratamiento";
   }
   $tab_labels["t_ciudad_base_paramedico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_ciudad_base_paramedico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_ciudad_base_paramedico_tratamiento"] : "CIUDAD BASE PARAMEDICO TRATAMIENTO";
   $tab_ger_campos['t_notas_adjuntos_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['t_notas_adjuntos_tratamiento'] = "t_notas_adjuntos_tratamiento";
       $tab_converte["t_notas_adjuntos_tratamiento"]   = "t_notas_adjuntos_tratamiento";
   }
   else
   {
       $tab_def_campos['t_notas_adjuntos_tratamiento'] = "t.NOTAS_ADJUNTOS_TRATAMIENTO";
       $tab_converte["t.NOTAS_ADJUNTOS_TRATAMIENTO"]   = "t_notas_adjuntos_tratamiento";
   }
   $tab_labels["t_notas_adjuntos_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_notas_adjuntos_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['labels']["t_notas_adjuntos_tratamiento"] : "NOTAS ADJUNTOS TRATAMIENTO";
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display']))
   {
       foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['php_cmp_sel']))
   {
       foreach ($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (!isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select']))
   {
       $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select'] = array();
   }
   
   if ($fsel_ok == "cmp")
   {
       $this->Sel_processa_out_sel($campos_sel);
   }
   else
   {
       if ($embbed)
       {
           ob_start();
           $this->Sel_processa_form();
           $Temp = ob_get_clean();
           echo NM_charset_to_utf8($Temp);
       }
       else
       {
           $this->Sel_processa_form();
       }
   }
   exit;
   
}
function Sel_processa_out_sel($campos_sel)
{
   global $tab_ger_campos, $sc_init, $tab_def_campos, $tab_converte, $embbed;
   $arr_temp = array();
   $campos_sel = explode("@?@", $campos_sel);
   $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select'] = array();
   $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_grid']   = "";
   $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_cmp']    = "";
   foreach ($campos_sel as $campo_sort)
   {
       $ordem = (substr($campo_sort, 0, 1) == "+") ? "asc" : "desc";
       $campo = substr($campo_sort, 1);
       if (isset($tab_converte[$campo]))
       {
           $_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select'][$campo] = $ordem;
       }
   }
?>
    <script language="javascript"> 
<?php
   if (!$embbed)
   {
?>
      self.parent.tb_remove(); 
      parent.nm_gp_submit_ajax('inicio', ''); 
<?php
   }
   else
   {
?>
      nm_gp_submit_ajax('inicio', ''); 
<?php
   }
?>
   </script>
<?php
}
   
function Sel_processa_form()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos;
   $size = 10;
   $_SESSION['scriptcase']['charset']  = (isset($this->Nm_lang['Nm_charset']) && !empty($this->Nm_lang['Nm_charset'])) ? $this->Nm_lang['Nm_charset'] : "ISO-8859-1";
   foreach ($this->Nm_lang as $ind => $dados)
   {
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($ind))
      {
          $ind = sc_convert_encoding($ind, $_SESSION['scriptcase']['charset'], "UTF-8");
          $this->Nm_lang[$ind] = $dados;
      }
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
      {
          $this->Nm_lang[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
   }
   $str_schema_all = (isset($_SESSION['scriptcase']['str_schema_all']) && !empty($_SESSION['scriptcase']['str_schema_all'])) ? $_SESSION['scriptcase']['str_schema_all'] : "Sc5_Green/Sc5_Green";
   include("../_lib/css/" . $str_schema_all . "_grid.php");
   $Str_btn_grid = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
   include("../_lib/buttons/" . $Str_btn_grid);
   if (!function_exists("nmButtonOutput"))
   {
       include_once("../_lib/lib/php/nm_gp_config_btn.php");
   }
   if (!$embbed)
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Paciente Tratamiento </TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $_SESSION['scriptcase']['css_btn_popup'] ?>" /> 
</HEAD>
<BODY class="scGridPage" style="margin: 0px; overflow-x: hidden">
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery_plugin/touch_punch/jquery.ui.touch-punch.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/tigra_color_picker/picker.js"></script>
<?php
   }
?>
<script language="javascript"> 
<?php
if ($embbed)
{
?>
  function scSubmitOrderCampos(sPos, sType) {
    $("#id_fsel_ok_sel_ord").val(sType);
    if(sType == 'cmp')
    {
       scPackSelectedOrd();
    }
   $.ajax({
    type: "POST",
    url: "grid_bayer_paciente_tratamiento_order_campos.php",
    data: {
     script_case_init: $("#id_script_case_init_sel_ord").val(),
     script_case_session: $("#id_script_case_session_sel_ord").val(),
     path_img: $("#id_path_img_sel_ord").val(),
     path_btn: $("#id_path_btn_sel_ord").val(),
     campos_sel: $("#id_campos_sel_sel_ord").val(),
     sel_regra: $("#id_sel_regra_sel_ord").val(),
     fsel_ok: $("#id_fsel_ok_sel_ord").val(),
     embbed_groupby: 'Y'
    }
   }).success(function(data) {
    $("#sc_id_order_campos_placeholder_" + sPos).find("td").html(data);
    scBtnOrderCamposHide(sPos);
   });
  }
<?php
}
?>
 // Submeter o formularior
 //-------------------------------------
 function submit_form_Fsel_ord()
 {
     scPackSelectedOrd();
      document.Fsel_ord.submit();
 }
 function scPackSelectedOrd() {
  var fieldList, fieldName, i, selectedFields = new Array;
 fieldList = $("#sc_id_fldord_selected").sortable("toArray");
 for (i = 0; i < fieldList.length; i++) {
  fieldName  = fieldList[i].substr(14);
  selectedFields.push($("#sc_id_class_" + fieldName).val() + fieldName);
 }
 $("#id_campos_sel_sel_ord").val( selectedFields.join("@?@") );
 }
 </script>
<FORM name="Fsel_ord" method="POST">
  <INPUT type="hidden" name="script_case_init"    id="id_script_case_init_sel_ord"    value="<?php echo NM_encode_input($sc_init); ?>"> 
  <INPUT type="hidden" name="script_case_session" id="id_script_case_session_sel_ord" value="<?php echo NM_encode_input(session_id()); ?>"> 
  <INPUT type="hidden" name="path_img"            id="id_path_img_sel_ord"            value="<?php echo NM_encode_input($path_img); ?>"> 
  <INPUT type="hidden" name="path_btn"            id="id_path_btn_sel_ord"            value="<?php echo NM_encode_input($path_btn); ?>"> 
  <INPUT type="hidden" name="fsel_ok"             id="id_fsel_ok_sel_ord"             value=""> 
<?php
if ($embbed)
{
    echo "<div class='scAppDivMoldura'>";
    echo "<table id=\"main_table\" style=\"width: 100%\" cellspacing=0 cellpadding=0>";
}
elseif ($_SESSION['scriptcase']['reg_conf']['html_dir'] == " DIR='RTL'")
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; right: 20px\">";
}
else
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; left: 20px\">";
}
?>
<?php
if (!$embbed)
{
?>
<tr>
<td>
<div class="scGridBorder">
<table width='100%' cellspacing=0 cellpadding=0>
<?php
}
?>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivHeader scAppDivHeaderText':'scGridLabelVert'; ?>">
   <?php echo $this->Nm_lang['lang_btns_sort_hint']; ?>
  </td>
 </tr>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivContent css_scAppDivContentText':'scGridTabelaTd'; ?>">
   <table class="<?php echo ($embbed)? '':'scGridTabela'; ?>" style="border-width: 0; border-collapse: collapse; width:100%;" cellspacing=0 cellpadding=0>
    <tr class="<?php echo ($embbed)? '':'scGridFieldOddVert'; ?>">
     <td style="vertical-align: top">
     <table>
   <tr><td style="vertical-align: top">
 <script language="javascript" type="text/javascript">
  $(function() {
   $(".sc_ui_litem").mouseover(function() {
    $(this).css("cursor", "all-scroll");
   });
   $("#sc_id_fldord_available").sortable({
    connectWith: ".sc_ui_fldord_selected",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).show();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   }).disableSelection();
   $("#sc_id_fldord_selected").sortable({
    connectWith: ".sc_ui_fldord_available",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).hide();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   });
   scUpdateListHeight();
  });
  function scUpdateListHeight() {
   $("#sc_id_fldord_available").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
   $("#sc_id_fldord_selected").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
  }
 </script>
 <style type="text/css">
  .sc_ui_sortable_ord {
   list-style-type: none;
   margin: 0;
   min-width: 225px;
  }
  .sc_ui_sortable_ord li {
   margin: 0 3px 3px 3px;
   padding: 1px 3px 1px 15px;
   min-height: 28px;
  }
  .sc_ui_sortable_ord li span {
   position: absolute;
   margin-left: -1.3em;
  }
 </style>
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_available scAppDivSelectFields" id="sc_id_fldord_available">
<?php
   foreach ($tab_ger_campos as $NM_cada_field => $NM_cada_opc)
   {
       if ($NM_cada_opc != "none")
       {
           if (!isset($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select'][$tab_def_campos[$NM_cada_field]]))
           {
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>">
      <?php echo $tab_labels[$NM_cada_field]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>" class="scAppDivToolbarInput" style="display: none">
       <option value="+">Asc</option>
       <option value="-">Desc</option>
      </select><br/>
     </li>
<?php
           }
       }
   }
?>
    </ul>
   </td>
   <td style="vertical-align: top">
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_selected scAppDivSelectFields" id="sc_id_fldord_selected">
<?php
   foreach ($_SESSION['sc_session'][$sc_init]['grid_bayer_paciente_tratamiento']['ordem_select'] as $NM_cada_field => $NM_cada_opc)
   {
       if (isset($tab_converte[$NM_cada_field]))
       {
           $sAscSelected  = " selected";
           $sDescSelected = "";
           if ($NM_cada_opc == "desc")
           {
               $sAscSelected  = "";
               $sDescSelected = " selected";
           }
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo $NM_cada_field; ?>">
      <?php echo $tab_labels[$tab_converte[$NM_cada_field]]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[ $tab_converte[$NM_cada_field] ]); ?>" class="scAppDivToolbarInput" onchange="$('#f_sel_sub').css('display', 'inline-block');">
       <option value="+"<?php echo $sAscSelected; ?>>Asc</option>
       <option value="-"<?php echo $sDescSelected; ?>>Desc</option>
      </select>
     </li>
<?php
       }
   }
?>
    </ul>
    <input type="hidden" name="campos_sel" id="id_campos_sel_sel_ord" value="">
   </td>
   </tr>
   </table>
   </td>
   </tr>
   </table>
  </td>
 </tr>
   <tr><td class="<?php echo ($embbed)? 'scAppDivToolbar':'scGridToolbar'; ?>">
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bok", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "f_sel_sub", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bapply", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "f_sel_sub", "", "", "display: none;", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
  &nbsp;&nbsp;&nbsp;
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bsair", "self.parent.tb_remove()", "self.parent.tb_remove()", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
   </td>
   </tr>
<?php
if (!$embbed)
{
?>
</table>
</div>
</td>
</tr>
<?php
}
?>
</table>
<?php
if ($embbed)
{
?>
    </div>
<?php
}
?>
</FORM>
<script language="javascript"> 
var bFixed = false;
function ajusta_window_Fsel_ord()
{
<?php
   if ($embbed)
   {
?>
  return false;
<?php
   }
?>
  var mt = $(document.getElementById("main_table"));
  if (0 == mt.width() || 0 == mt.height())
  {
    setTimeout("ajusta_window_Fsel_ord()", 50);
    return;
  }
  else if(!bFixed)
  {
    var oOrig = $(document.Fsel_ord.sel_orig),
        oDest = $(document.Fsel_ord.sel_dest),
        mHeight = Math.max(oOrig.height(), oDest.height()),
        mWidth = Math.max(oOrig.width() + 5, oDest.width() + 5);
    oOrig.height(mHeight);
    oOrig.width(mWidth);
    oDest.height(mHeight);
    oDest.width(mWidth + 15);
    bFixed = true;
    if (navigator.userAgent.indexOf("Chrome/") > 0)
    {
      strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
      self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
      setTimeout("ajusta_window_Fsel_ord()", 50);
      return;
    }
  }
  strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
  self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
}
$( document ).ready(function() {
  ajusta_window_Fsel_ord();
});
</script>
<script>
    ajusta_window_Fsel_ord();
</script>
</BODY>
</HTML>
<?php
}
}
