<?php

class listado_pacientes_total
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;

   var $nm_data;

   //----- 
   function listado_pacientes_total($sc_page)
   {
      $this->sc_page = $sc_page;
      $this->nm_data = new nm_data("es");
      if (isset($_SESSION['sc_session'][$this->sc_page]['listado_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->sc_page]['listado_pacientes']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
          $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_observacion_motivo_retiro_paciente = $Busca_temp['p_observacion_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_observacion_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_observacion_motivo_retiro_paciente = substr($this->p_observacion_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
          $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
          }
          $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
          $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
          }
          $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
          $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
          }
          $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
          $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
          }
          $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
          $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
          }
          $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
          $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
          }
          $this->p_correo_paciente = $Busca_temp['p_correo_paciente']; 
          $tmp_pos = strpos($this->p_correo_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_correo_paciente = substr($this->p_correo_paciente, 0, $tmp_pos);
          }
          $this->p_direccion_paciente = $Busca_temp['p_direccion_paciente']; 
          $tmp_pos = strpos($this->p_direccion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_direccion_paciente = substr($this->p_direccion_paciente, 0, $tmp_pos);
          }
          $this->p_barrio_paciente = $Busca_temp['p_barrio_paciente']; 
          $tmp_pos = strpos($this->p_barrio_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_barrio_paciente = substr($this->p_barrio_paciente, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
          $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
          }
          $this->p_genero_paciente = $Busca_temp['p_genero_paciente']; 
          $tmp_pos = strpos($this->p_genero_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_genero_paciente = substr($this->p_genero_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_nacimineto_paciente = $Busca_temp['p_fecha_nacimineto_paciente']; 
          $tmp_pos = strpos($this->p_fecha_nacimineto_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_nacimineto_paciente = substr($this->p_fecha_nacimineto_paciente, 0, $tmp_pos);
          }
          $this->p_edad_paciente = $Busca_temp['p_edad_paciente']; 
          $tmp_pos = strpos($this->p_edad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_edad_paciente = substr($this->p_edad_paciente, 0, $tmp_pos);
          }
          $p_edad_paciente_2 = $Busca_temp['p_edad_paciente_input_2']; 
          $this->p_edad_paciente_2 = $Busca_temp['p_edad_paciente_input_2']; 
          $this->p_acudiente_paciente = $Busca_temp['p_acudiente_paciente']; 
          $tmp_pos = strpos($this->p_acudiente_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_acudiente_paciente = substr($this->p_acudiente_paciente, 0, $tmp_pos);
          }
          $this->p_telefono_acudiente_paciente = $Busca_temp['p_telefono_acudiente_paciente']; 
          $tmp_pos = strpos($this->p_telefono_acudiente_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_acudiente_paciente = substr($this->p_telefono_acudiente_paciente, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
          $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
          }
          $p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
          $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
          $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
          $tmp_pos = strpos($this->p_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
          }
          $this->p_id_ultima_gestion = $Busca_temp['p_id_ultima_gestion']; 
          $tmp_pos = strpos($this->p_id_ultima_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_ultima_gestion = substr($this->p_id_ultima_gestion, 0, $tmp_pos);
          }
          $p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
          $this->p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
          $this->p_usuario_creacion = $Busca_temp['p_usuario_creacion']; 
          $tmp_pos = strpos($this->p_usuario_creacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_usuario_creacion = substr($this->p_usuario_creacion, 0, $tmp_pos);
          }
          $this->t_id_tratamiento = $Busca_temp['t_id_tratamiento']; 
          $tmp_pos = strpos($this->t_id_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_id_tratamiento = substr($this->t_id_tratamiento, 0, $tmp_pos);
          }
          $t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
          $this->t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
          $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
          $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
          }
          $this->t_nombre_referencia = $Busca_temp['t_nombre_referencia']; 
          $tmp_pos = strpos($this->t_nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_nombre_referencia = substr($this->t_nombre_referencia, 0, $tmp_pos);
          }
          $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
          $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
          }
          $this->t_tratamiento_previo = $Busca_temp['t_tratamiento_previo']; 
          $tmp_pos = strpos($this->t_tratamiento_previo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_tratamiento_previo = substr($this->t_tratamiento_previo, 0, $tmp_pos);
          }
          $this->t_consentimiento_tratamiento = $Busca_temp['t_consentimiento_tratamiento']; 
          $tmp_pos = strpos($this->t_consentimiento_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_consentimiento_tratamiento = substr($this->t_consentimiento_tratamiento, 0, $tmp_pos);
          }
          $this->t_fecha_inicio_terapia_tratamiento = $Busca_temp['t_fecha_inicio_terapia_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_inicio_terapia_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_inicio_terapia_tratamiento = substr($this->t_fecha_inicio_terapia_tratamiento, 0, $tmp_pos);
          }
          $this->t_regimen_tratamiento = $Busca_temp['t_regimen_tratamiento']; 
          $tmp_pos = strpos($this->t_regimen_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_regimen_tratamiento = substr($this->t_regimen_tratamiento, 0, $tmp_pos);
          }
          $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
          $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
          $tmp_pos = strpos($this->t_punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
          }
          $this->t_fecha_ultima_reclamacion_tratamiento = $Busca_temp['t_fecha_ultima_reclamacion_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_ultima_reclamacion_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_ultima_reclamacion_tratamiento = substr($this->t_fecha_ultima_reclamacion_tratamiento, 0, $tmp_pos);
          }
          $this->t_otros_operadores_tratamiento = $Busca_temp['t_otros_operadores_tratamiento']; 
          $tmp_pos = strpos($this->t_otros_operadores_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_otros_operadores_tratamiento = substr($this->t_otros_operadores_tratamiento, 0, $tmp_pos);
          }
          $this->t_medios_adquisicion_tratamiento = $Busca_temp['t_medios_adquisicion_tratamiento']; 
          $tmp_pos = strpos($this->t_medios_adquisicion_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medios_adquisicion_tratamiento = substr($this->t_medios_adquisicion_tratamiento, 0, $tmp_pos);
          }
          $this->t_ips_atiende_tratamiento = $Busca_temp['t_ips_atiende_tratamiento']; 
          $tmp_pos = strpos($this->t_ips_atiende_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_ips_atiende_tratamiento = substr($this->t_ips_atiende_tratamiento, 0, $tmp_pos);
          }
          $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
          $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
          }
          $this->t_especialidad_tratamiento = $Busca_temp['t_especialidad_tratamiento']; 
          $tmp_pos = strpos($this->t_especialidad_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_especialidad_tratamiento = substr($this->t_especialidad_tratamiento, 0, $tmp_pos);
          }
          $this->t_paramedico_tratamiento = $Busca_temp['t_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_paramedico_tratamiento = substr($this->t_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_zona_atencion_paramedico_tratamiento = $Busca_temp['t_zona_atencion_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_zona_atencion_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_zona_atencion_paramedico_tratamiento = substr($this->t_zona_atencion_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_ciudad_base_paramedico_tratamiento = $Busca_temp['t_ciudad_base_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_ciudad_base_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_ciudad_base_paramedico_tratamiento = substr($this->t_ciudad_base_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_notas_adjuntos_tratamiento = $Busca_temp['t_notas_adjuntos_tratamiento']; 
          $tmp_pos = strpos($this->t_notas_adjuntos_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_notas_adjuntos_tratamiento = substr($this->t_notas_adjuntos_tratamiento, 0, $tmp_pos);
          }
      } 
   }

   //---- 
   function quebra_geral()
   {
      global $nada, $nm_lang ;
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['contr_total_geral'] == "OK") 
      { 
          return; 
      } 
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['tot_geral'] = array() ;  
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "select count(*) from " . $this->Ini->nm_tabela . " " . $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq']; 
      } 
      else 
      { 
          $nm_comando = "select count(*) from " . $this->Ini->nm_tabela . " " . $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq']; 
      } 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
      if (!$rt = $this->Db->Execute($nm_comando)) 
      { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['tot_geral'][0] = "" . $this->Ini->Nm_lang['lang_msgs_totl'] . ""; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['tot_geral'][1] = $rt->fields[0] ; 
      $rt->Close(); 
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['contr_total_geral'] = "OK";
   } 

   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
