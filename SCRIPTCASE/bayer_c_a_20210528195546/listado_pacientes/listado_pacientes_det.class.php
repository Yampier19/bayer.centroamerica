<?php
//--- 
class listado_pacientes_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $p_id_paciente;
   var $p_estado_paciente;
   var $p_fecha_activacion_paciente;
   var $p_fecha_retiro_paciente;
   var $p_motivo_retiro_paciente;
   var $p_observacion_motivo_retiro_paciente;
   var $p_identificacion_paciente;
   var $p_nombre_paciente;
   var $p_apellido_paciente;
   var $p_telefono_paciente;
   var $p_telefono2_paciente;
   var $p_telefono3_paciente;
   var $p_correo_paciente;
   var $p_direccion_paciente;
   var $p_barrio_paciente;
   var $p_pais_paciente;
   var $p_ciudad_paciente;
   var $p_genero_paciente;
   var $p_fecha_nacimineto_paciente;
   var $p_edad_paciente;
   var $p_acudiente_paciente;
   var $p_telefono_acudiente_paciente;
   var $p_codigo_xofigo;
   var $p_status_paciente;
   var $p_id_ultima_gestion;
   var $p_usuario_creacion;
   var $t_id_tratamiento;
   var $t_producto_tratamiento;
   var $t_nombre_referencia;
   var $t_clasificacion_patologica_tratamiento;
   var $t_tratamiento_previo;
   var $t_consentimiento_tratamiento;
   var $t_fecha_inicio_terapia_tratamiento;
   var $t_regimen_tratamiento;
   var $t_asegurador_tratamiento;
   var $t_operador_logistico_tratamiento;
   var $t_punto_entrega;
   var $t_fecha_ultima_reclamacion_tratamiento;
   var $t_otros_operadores_tratamiento;
   var $t_medios_adquisicion_tratamiento;
   var $t_ips_atiende_tratamiento;
   var $t_medico_tratamiento;
   var $t_especialidad_tratamiento;
   var $t_paramedico_tratamiento;
   var $t_zona_atencion_paramedico_tratamiento;
   var $t_ciudad_base_paramedico_tratamiento;
   var $t_notas_adjuntos_tratamiento;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
        $tmp_pos = strpos($this->p_id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
        }
        $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
        $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
        $tmp_pos = strpos($this->p_estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
        $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
        $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
        }
        $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
        $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
        }
        $this->p_observacion_motivo_retiro_paciente = $Busca_temp['p_observacion_motivo_retiro_paciente']; 
        $tmp_pos = strpos($this->p_observacion_motivo_retiro_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_observacion_motivo_retiro_paciente = substr($this->p_observacion_motivo_retiro_paciente, 0, $tmp_pos);
        }
        $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
        $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
        }
        $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
        $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
        }
        $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
        $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
        }
        $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
        $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
        }
        $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
        $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
        }
        $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
        $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
        }
        $this->p_correo_paciente = $Busca_temp['p_correo_paciente']; 
        $tmp_pos = strpos($this->p_correo_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_correo_paciente = substr($this->p_correo_paciente, 0, $tmp_pos);
        }
        $this->p_direccion_paciente = $Busca_temp['p_direccion_paciente']; 
        $tmp_pos = strpos($this->p_direccion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_direccion_paciente = substr($this->p_direccion_paciente, 0, $tmp_pos);
        }
        $this->p_barrio_paciente = $Busca_temp['p_barrio_paciente']; 
        $tmp_pos = strpos($this->p_barrio_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_barrio_paciente = substr($this->p_barrio_paciente, 0, $tmp_pos);
        }
        $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
        $tmp_pos = strpos($this->p_pais_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
        }
        $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
        $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
        }
        $this->p_genero_paciente = $Busca_temp['p_genero_paciente']; 
        $tmp_pos = strpos($this->p_genero_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_genero_paciente = substr($this->p_genero_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_nacimineto_paciente = $Busca_temp['p_fecha_nacimineto_paciente']; 
        $tmp_pos = strpos($this->p_fecha_nacimineto_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_nacimineto_paciente = substr($this->p_fecha_nacimineto_paciente, 0, $tmp_pos);
        }
        $this->p_edad_paciente = $Busca_temp['p_edad_paciente']; 
        $tmp_pos = strpos($this->p_edad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_edad_paciente = substr($this->p_edad_paciente, 0, $tmp_pos);
        }
        $this->p_edad_paciente_2 = $Busca_temp['p_edad_paciente_input_2']; 
        $this->p_acudiente_paciente = $Busca_temp['p_acudiente_paciente']; 
        $tmp_pos = strpos($this->p_acudiente_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_acudiente_paciente = substr($this->p_acudiente_paciente, 0, $tmp_pos);
        }
        $this->p_telefono_acudiente_paciente = $Busca_temp['p_telefono_acudiente_paciente']; 
        $tmp_pos = strpos($this->p_telefono_acudiente_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono_acudiente_paciente = substr($this->p_telefono_acudiente_paciente, 0, $tmp_pos);
        }
        $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
        $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
        }
        $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
        $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
        $tmp_pos = strpos($this->p_status_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
        }
        $this->p_id_ultima_gestion = $Busca_temp['p_id_ultima_gestion']; 
        $tmp_pos = strpos($this->p_id_ultima_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_ultima_gestion = substr($this->p_id_ultima_gestion, 0, $tmp_pos);
        }
        $this->p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
        $this->p_usuario_creacion = $Busca_temp['p_usuario_creacion']; 
        $tmp_pos = strpos($this->p_usuario_creacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_usuario_creacion = substr($this->p_usuario_creacion, 0, $tmp_pos);
        }
        $this->t_id_tratamiento = $Busca_temp['t_id_tratamiento']; 
        $tmp_pos = strpos($this->t_id_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_id_tratamiento = substr($this->t_id_tratamiento, 0, $tmp_pos);
        }
        $this->t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
        $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
        $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
        }
        $this->t_nombre_referencia = $Busca_temp['t_nombre_referencia']; 
        $tmp_pos = strpos($this->t_nombre_referencia, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_nombre_referencia = substr($this->t_nombre_referencia, 0, $tmp_pos);
        }
        $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
        $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
        }
        $this->t_tratamiento_previo = $Busca_temp['t_tratamiento_previo']; 
        $tmp_pos = strpos($this->t_tratamiento_previo, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_tratamiento_previo = substr($this->t_tratamiento_previo, 0, $tmp_pos);
        }
        $this->t_consentimiento_tratamiento = $Busca_temp['t_consentimiento_tratamiento']; 
        $tmp_pos = strpos($this->t_consentimiento_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_consentimiento_tratamiento = substr($this->t_consentimiento_tratamiento, 0, $tmp_pos);
        }
        $this->t_fecha_inicio_terapia_tratamiento = $Busca_temp['t_fecha_inicio_terapia_tratamiento']; 
        $tmp_pos = strpos($this->t_fecha_inicio_terapia_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_fecha_inicio_terapia_tratamiento = substr($this->t_fecha_inicio_terapia_tratamiento, 0, $tmp_pos);
        }
        $this->t_regimen_tratamiento = $Busca_temp['t_regimen_tratamiento']; 
        $tmp_pos = strpos($this->t_regimen_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_regimen_tratamiento = substr($this->t_regimen_tratamiento, 0, $tmp_pos);
        }
        $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
        $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
        }
        $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
        $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
        }
        $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
        $tmp_pos = strpos($this->t_punto_entrega, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
        }
        $this->t_fecha_ultima_reclamacion_tratamiento = $Busca_temp['t_fecha_ultima_reclamacion_tratamiento']; 
        $tmp_pos = strpos($this->t_fecha_ultima_reclamacion_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_fecha_ultima_reclamacion_tratamiento = substr($this->t_fecha_ultima_reclamacion_tratamiento, 0, $tmp_pos);
        }
        $this->t_otros_operadores_tratamiento = $Busca_temp['t_otros_operadores_tratamiento']; 
        $tmp_pos = strpos($this->t_otros_operadores_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_otros_operadores_tratamiento = substr($this->t_otros_operadores_tratamiento, 0, $tmp_pos);
        }
        $this->t_medios_adquisicion_tratamiento = $Busca_temp['t_medios_adquisicion_tratamiento']; 
        $tmp_pos = strpos($this->t_medios_adquisicion_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_medios_adquisicion_tratamiento = substr($this->t_medios_adquisicion_tratamiento, 0, $tmp_pos);
        }
        $this->t_ips_atiende_tratamiento = $Busca_temp['t_ips_atiende_tratamiento']; 
        $tmp_pos = strpos($this->t_ips_atiende_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_ips_atiende_tratamiento = substr($this->t_ips_atiende_tratamiento, 0, $tmp_pos);
        }
        $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
        $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
        }
        $this->t_especialidad_tratamiento = $Busca_temp['t_especialidad_tratamiento']; 
        $tmp_pos = strpos($this->t_especialidad_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_especialidad_tratamiento = substr($this->t_especialidad_tratamiento, 0, $tmp_pos);
        }
        $this->t_paramedico_tratamiento = $Busca_temp['t_paramedico_tratamiento']; 
        $tmp_pos = strpos($this->t_paramedico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_paramedico_tratamiento = substr($this->t_paramedico_tratamiento, 0, $tmp_pos);
        }
        $this->t_zona_atencion_paramedico_tratamiento = $Busca_temp['t_zona_atencion_paramedico_tratamiento']; 
        $tmp_pos = strpos($this->t_zona_atencion_paramedico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_zona_atencion_paramedico_tratamiento = substr($this->t_zona_atencion_paramedico_tratamiento, 0, $tmp_pos);
        }
        $this->t_ciudad_base_paramedico_tratamiento = $Busca_temp['t_ciudad_base_paramedico_tratamiento']; 
        $tmp_pos = strpos($this->t_ciudad_base_paramedico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_ciudad_base_paramedico_tratamiento = substr($this->t_ciudad_base_paramedico_tratamiento, 0, $tmp_pos);
        }
        $this->t_notas_adjuntos_tratamiento = $Busca_temp['t_notas_adjuntos_tratamiento']; 
        $tmp_pos = strpos($this->t_notas_adjuntos_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_notas_adjuntos_tratamiento = substr($this->t_notas_adjuntos_tratamiento, 0, $tmp_pos);
        }
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and p.ESTADO_PACIENTE = '$parms_det[1]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and p.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and p.IDENTIFICACION_PACIENTE = '$parms_det[6]' and p.NOMBRE_PACIENTE = '$parms_det[7]' and p.APELLIDO_PACIENTE = '$parms_det[8]' and p.TELEFONO_PACIENTE = '$parms_det[9]' and p.TELEFONO2_PACIENTE = '$parms_det[10]' and p.TELEFONO3_PACIENTE = '$parms_det[11]' and p.CORREO_PACIENTE = '$parms_det[12]' and p.DIRECCION_PACIENTE = '$parms_det[13]' and p.BARRIO_PACIENTE = '$parms_det[14]' and p.PAIS_PACIENTE = '$parms_det[15]' and p.CIUDAD_PACIENTE = '$parms_det[16]' and p.GENERO_PACIENTE = '$parms_det[17]' and p.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and p.EDAD_PACIENTE = $parms_det[19] and p.ACUDIENTE_PACIENTE = '$parms_det[20]' and p.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and p.CODIGO_XOFIGO = $parms_det[22] and p.STATUS_PACIENTE = '$parms_det[23]' and p.ID_ULTIMA_GESTION = $parms_det[24] and p.USUARIO_CREACION = '$parms_det[25]' and t.ID_TRATAMIENTO = $parms_det[26] and t.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and t.NOMBRE_REFERENCIA = '$parms_det[28]' and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and t.TRATAMIENTO_PREVIO = '$parms_det[30]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and t.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and t.REGIMEN_TRATAMIENTO = '$parms_det[33]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and t.PUNTO_ENTREGA = '$parms_det[36]' and t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and t.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and t.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and t.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and t.MEDICO_TRATAMIENTO = '$parms_det[41]' and t.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and t.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and t.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]'" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and p.ESTADO_PACIENTE = '$parms_det[1]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and p.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and p.IDENTIFICACION_PACIENTE = '$parms_det[6]' and p.NOMBRE_PACIENTE = '$parms_det[7]' and p.APELLIDO_PACIENTE = '$parms_det[8]' and p.TELEFONO_PACIENTE = '$parms_det[9]' and p.TELEFONO2_PACIENTE = '$parms_det[10]' and p.TELEFONO3_PACIENTE = '$parms_det[11]' and p.CORREO_PACIENTE = '$parms_det[12]' and p.DIRECCION_PACIENTE = '$parms_det[13]' and p.BARRIO_PACIENTE = '$parms_det[14]' and p.PAIS_PACIENTE = '$parms_det[15]' and p.CIUDAD_PACIENTE = '$parms_det[16]' and p.GENERO_PACIENTE = '$parms_det[17]' and p.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and p.EDAD_PACIENTE = $parms_det[19] and p.ACUDIENTE_PACIENTE = '$parms_det[20]' and p.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and p.CODIGO_XOFIGO = $parms_det[22] and p.STATUS_PACIENTE = '$parms_det[23]' and p.ID_ULTIMA_GESTION = $parms_det[24] and p.USUARIO_CREACION = '$parms_det[25]' and t.ID_TRATAMIENTO = $parms_det[26] and t.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and t.NOMBRE_REFERENCIA = '$parms_det[28]' and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and t.TRATAMIENTO_PREVIO = '$parms_det[30]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and t.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and t.REGIMEN_TRATAMIENTO = '$parms_det[33]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and t.PUNTO_ENTREGA = '$parms_det[36]' and t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and t.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and t.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and t.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and t.MEDICO_TRATAMIENTO = '$parms_det[41]' and t.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and t.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and t.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]'" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and p.ESTADO_PACIENTE = '$parms_det[1]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and p.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and p.IDENTIFICACION_PACIENTE = '$parms_det[6]' and p.NOMBRE_PACIENTE = '$parms_det[7]' and p.APELLIDO_PACIENTE = '$parms_det[8]' and p.TELEFONO_PACIENTE = '$parms_det[9]' and p.TELEFONO2_PACIENTE = '$parms_det[10]' and p.TELEFONO3_PACIENTE = '$parms_det[11]' and p.CORREO_PACIENTE = '$parms_det[12]' and p.DIRECCION_PACIENTE = '$parms_det[13]' and p.BARRIO_PACIENTE = '$parms_det[14]' and p.PAIS_PACIENTE = '$parms_det[15]' and p.CIUDAD_PACIENTE = '$parms_det[16]' and p.GENERO_PACIENTE = '$parms_det[17]' and p.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and p.EDAD_PACIENTE = $parms_det[19] and p.ACUDIENTE_PACIENTE = '$parms_det[20]' and p.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and p.CODIGO_XOFIGO = $parms_det[22] and p.STATUS_PACIENTE = '$parms_det[23]' and p.ID_ULTIMA_GESTION = $parms_det[24] and p.USUARIO_CREACION = '$parms_det[25]' and t.ID_TRATAMIENTO = $parms_det[26] and t.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and t.NOMBRE_REFERENCIA = '$parms_det[28]' and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and t.TRATAMIENTO_PREVIO = '$parms_det[30]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and t.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and t.REGIMEN_TRATAMIENTO = '$parms_det[33]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and t.PUNTO_ENTREGA = '$parms_det[36]' and t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and t.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and t.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and t.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and t.MEDICO_TRATAMIENTO = '$parms_det[41]' and t.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and t.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and t.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]'" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->p_id_paciente = $rs->fields[0] ;  
   $this->p_id_paciente = (string)$this->p_id_paciente;
   $this->p_estado_paciente = $rs->fields[1] ;  
   $this->p_fecha_activacion_paciente = $rs->fields[2] ;  
   $this->p_fecha_retiro_paciente = $rs->fields[3] ;  
   $this->p_motivo_retiro_paciente = $rs->fields[4] ;  
   $this->p_observacion_motivo_retiro_paciente = $rs->fields[5] ;  
   $this->p_identificacion_paciente = $rs->fields[6] ;  
   $this->p_nombre_paciente = $rs->fields[7] ;  
   $this->p_apellido_paciente = $rs->fields[8] ;  
   $this->p_telefono_paciente = $rs->fields[9] ;  
   $this->p_telefono2_paciente = $rs->fields[10] ;  
   $this->p_telefono3_paciente = $rs->fields[11] ;  
   $this->p_correo_paciente = $rs->fields[12] ;  
   $this->p_direccion_paciente = $rs->fields[13] ;  
   $this->p_barrio_paciente = $rs->fields[14] ;  
   $this->p_pais_paciente = $rs->fields[15] ;  
   $this->p_ciudad_paciente = $rs->fields[16] ;  
   $this->p_genero_paciente = $rs->fields[17] ;  
   $this->p_fecha_nacimineto_paciente = $rs->fields[18] ;  
   $this->p_edad_paciente = $rs->fields[19] ;  
   $this->p_edad_paciente = (string)$this->p_edad_paciente;
   $this->p_acudiente_paciente = $rs->fields[20] ;  
   $this->p_telefono_acudiente_paciente = $rs->fields[21] ;  
   $this->p_codigo_xofigo = $rs->fields[22] ;  
   $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
   $this->p_status_paciente = $rs->fields[23] ;  
   $this->p_id_ultima_gestion = $rs->fields[24] ;  
   $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
   $this->p_usuario_creacion = $rs->fields[25] ;  
   $this->t_id_tratamiento = $rs->fields[26] ;  
   $this->t_id_tratamiento = (string)$this->t_id_tratamiento;
   $this->t_producto_tratamiento = $rs->fields[27] ;  
   $this->t_nombre_referencia = $rs->fields[28] ;  
   $this->t_clasificacion_patologica_tratamiento = $rs->fields[29] ;  
   $this->t_tratamiento_previo = $rs->fields[30] ;  
   $this->t_consentimiento_tratamiento = $rs->fields[31] ;  
   $this->t_fecha_inicio_terapia_tratamiento = $rs->fields[32] ;  
   $this->t_regimen_tratamiento = $rs->fields[33] ;  
   $this->t_asegurador_tratamiento = $rs->fields[34] ;  
   $this->t_operador_logistico_tratamiento = $rs->fields[35] ;  
   $this->t_punto_entrega = $rs->fields[36] ;  
   $this->t_fecha_ultima_reclamacion_tratamiento = $rs->fields[37] ;  
   $this->t_otros_operadores_tratamiento = $rs->fields[38] ;  
   $this->t_medios_adquisicion_tratamiento = $rs->fields[39] ;  
   $this->t_ips_atiende_tratamiento = $rs->fields[40] ;  
   $this->t_medico_tratamiento = $rs->fields[41] ;  
   $this->t_especialidad_tratamiento = $rs->fields[42] ;  
   $this->t_paramedico_tratamiento = $rs->fields[43] ;  
   $this->t_zona_atencion_paramedico_tratamiento = $rs->fields[44] ;  
   $this->t_ciudad_base_paramedico_tratamiento = $rs->fields[45] ;  
   $this->t_notas_adjuntos_tratamiento = $rs->fields[46] ;  
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Listado Pacientes</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "listado_pacientes/listado_pacientes_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Listado Pacientes</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "listado_pacientes/listado_pacientes_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "listado_pacientes/listado_pacientes_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_retiro_paciente'])) ? $this->New_label['p_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_fecha_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_motivo_retiro_paciente'])) ? $this->New_label['p_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_observacion_motivo_retiro_paciente'])) ? $this->New_label['p_observacion_motivo_retiro_paciente'] : "OBSERVACION MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_observacion_motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_observacion_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_observacion_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_identificacion_paciente'])) ? $this->New_label['p_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_identificacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_identificacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_identificacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_nombre_paciente'])) ? $this->New_label['p_nombre_paciente'] : "NOMBRE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_nombre_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_nombre_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_nombre_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_apellido_paciente'])) ? $this->New_label['p_apellido_paciente'] : "APELLIDO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_apellido_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_apellido_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_apellido_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono_paciente'])) ? $this->New_label['p_telefono_paciente'] : "TELEFONO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_telefono_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono2_paciente'])) ? $this->New_label['p_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono2_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono2_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_telefono2_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono3_paciente'])) ? $this->New_label['p_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono3_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono3_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_telefono3_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_correo_paciente'])) ? $this->New_label['p_correo_paciente'] : "CORREO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_correo_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_correo_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_correo_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_direccion_paciente'])) ? $this->New_label['p_direccion_paciente'] : "DIRECCION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_direccion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_direccion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_direccion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_barrio_paciente'])) ? $this->New_label['p_barrio_paciente'] : "BARRIO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_barrio_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_barrio_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_barrio_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_pais_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_pais_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_pais_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_genero_paciente'])) ? $this->New_label['p_genero_paciente'] : "GENERO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_genero_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_genero_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_genero_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_nacimineto_paciente'])) ? $this->New_label['p_fecha_nacimineto_paciente'] : "FECHA NACIMINETO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_nacimineto_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_nacimineto_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_fecha_nacimineto_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_edad_paciente'])) ? $this->New_label['p_edad_paciente'] : "EDAD PACIENTE"; 
          $conteudo = trim(NM_encode_input(sc_strip_script($this->p_edad_paciente))); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
              nmgp_Form_Num_Val($conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_edad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_edad_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_acudiente_paciente'])) ? $this->New_label['p_acudiente_paciente'] : "ACUDIENTE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_acudiente_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_acudiente_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_acudiente_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono_acudiente_paciente'])) ? $this->New_label['p_telefono_acudiente_paciente'] : "TELEFONO ACUDIENTE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono_acudiente_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono_acudiente_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_telefono_acudiente_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->p_codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_status_paciente'])) ? $this->New_label['p_status_paciente'] : "STATUS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_status_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_status_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_status_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_ultima_gestion'])) ? $this->New_label['p_id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          $conteudo = trim(sc_strip_script($this->p_id_ultima_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_ultima_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_id_ultima_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_usuario_creacion'])) ? $this->New_label['p_usuario_creacion'] : "USUARIO CREACION"; 
          $conteudo = trim(sc_strip_script($this->p_usuario_creacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_usuario_creacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_usuario_creacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_id_tratamiento'])) ? $this->New_label['t_id_tratamiento'] : "ID TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_id_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_id_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_id_tratamiento_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->t_nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_clasificacion_patologica_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_clasificacion_patologica_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_clasificacion_patologica_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_tratamiento_previo'])) ? $this->New_label['t_tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          $conteudo = trim(sc_strip_script($this->t_tratamiento_previo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_tratamiento_previo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_tratamiento_previo_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_consentimiento_tratamiento'])) ? $this->New_label['t_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_consentimiento_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_consentimiento_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_consentimiento_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_fecha_inicio_terapia_tratamiento'])) ? $this->New_label['t_fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_fecha_inicio_terapia_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_fecha_inicio_terapia_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_fecha_inicio_terapia_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_regimen_tratamiento'])) ? $this->New_label['t_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_regimen_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_regimen_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_regimen_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->t_punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['t_fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_fecha_ultima_reclamacion_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_fecha_ultima_reclamacion_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_fecha_ultima_reclamacion_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_otros_operadores_tratamiento'])) ? $this->New_label['t_otros_operadores_tratamiento'] : "OTROS OPERADORES TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_otros_operadores_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_otros_operadores_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_otros_operadores_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_medios_adquisicion_tratamiento'])) ? $this->New_label['t_medios_adquisicion_tratamiento'] : "MEDIOS ADQUISICION TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_medios_adquisicion_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_medios_adquisicion_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_medios_adquisicion_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_ips_atiende_tratamiento'])) ? $this->New_label['t_ips_atiende_tratamiento'] : "IPS ATIENDE TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_ips_atiende_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_ips_atiende_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_ips_atiende_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_medico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_medico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_medico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_especialidad_tratamiento'])) ? $this->New_label['t_especialidad_tratamiento'] : "ESPECIALIDAD TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_especialidad_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_especialidad_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_especialidad_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_paramedico_tratamiento'])) ? $this->New_label['t_paramedico_tratamiento'] : "PARAMEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_paramedico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_paramedico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_paramedico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_zona_atencion_paramedico_tratamiento'])) ? $this->New_label['t_zona_atencion_paramedico_tratamiento'] : "ZONA ATENCION PARAMEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_zona_atencion_paramedico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_zona_atencion_paramedico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_zona_atencion_paramedico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_ciudad_base_paramedico_tratamiento'])) ? $this->New_label['t_ciudad_base_paramedico_tratamiento'] : "CIUDAD BASE PARAMEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_ciudad_base_paramedico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_ciudad_base_paramedico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_ciudad_base_paramedico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_notas_adjuntos_tratamiento'])) ? $this->New_label['t_notas_adjuntos_tratamiento'] : "NOTAS ADJUNTOS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_notas_adjuntos_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_notas_adjuntos_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_notas_adjuntos_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"listado_pacientes_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "listado_pacientes/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "listado_pacientes/listado_pacientes_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
