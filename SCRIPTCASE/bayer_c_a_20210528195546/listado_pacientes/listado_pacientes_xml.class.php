<?php

class listado_pacientes_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function listado_pacientes_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_listado_pacientes";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "listado_pacientes.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['listado_pacientes']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
          $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_observacion_motivo_retiro_paciente = $Busca_temp['p_observacion_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_observacion_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_observacion_motivo_retiro_paciente = substr($this->p_observacion_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
          $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
          }
          $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
          $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
          }
          $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
          $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
          }
          $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
          $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
          }
          $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
          $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
          }
          $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
          $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
          }
          $this->p_correo_paciente = $Busca_temp['p_correo_paciente']; 
          $tmp_pos = strpos($this->p_correo_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_correo_paciente = substr($this->p_correo_paciente, 0, $tmp_pos);
          }
          $this->p_direccion_paciente = $Busca_temp['p_direccion_paciente']; 
          $tmp_pos = strpos($this->p_direccion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_direccion_paciente = substr($this->p_direccion_paciente, 0, $tmp_pos);
          }
          $this->p_barrio_paciente = $Busca_temp['p_barrio_paciente']; 
          $tmp_pos = strpos($this->p_barrio_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_barrio_paciente = substr($this->p_barrio_paciente, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
          $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
          }
          $this->p_genero_paciente = $Busca_temp['p_genero_paciente']; 
          $tmp_pos = strpos($this->p_genero_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_genero_paciente = substr($this->p_genero_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_nacimineto_paciente = $Busca_temp['p_fecha_nacimineto_paciente']; 
          $tmp_pos = strpos($this->p_fecha_nacimineto_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_nacimineto_paciente = substr($this->p_fecha_nacimineto_paciente, 0, $tmp_pos);
          }
          $this->p_edad_paciente = $Busca_temp['p_edad_paciente']; 
          $tmp_pos = strpos($this->p_edad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_edad_paciente = substr($this->p_edad_paciente, 0, $tmp_pos);
          }
          $this->p_edad_paciente_2 = $Busca_temp['p_edad_paciente_input_2']; 
          $this->p_acudiente_paciente = $Busca_temp['p_acudiente_paciente']; 
          $tmp_pos = strpos($this->p_acudiente_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_acudiente_paciente = substr($this->p_acudiente_paciente, 0, $tmp_pos);
          }
          $this->p_telefono_acudiente_paciente = $Busca_temp['p_telefono_acudiente_paciente']; 
          $tmp_pos = strpos($this->p_telefono_acudiente_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_acudiente_paciente = substr($this->p_telefono_acudiente_paciente, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
          $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
          $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
          $tmp_pos = strpos($this->p_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
          }
          $this->p_id_ultima_gestion = $Busca_temp['p_id_ultima_gestion']; 
          $tmp_pos = strpos($this->p_id_ultima_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_ultima_gestion = substr($this->p_id_ultima_gestion, 0, $tmp_pos);
          }
          $this->p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
          $this->p_usuario_creacion = $Busca_temp['p_usuario_creacion']; 
          $tmp_pos = strpos($this->p_usuario_creacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_usuario_creacion = substr($this->p_usuario_creacion, 0, $tmp_pos);
          }
          $this->t_id_tratamiento = $Busca_temp['t_id_tratamiento']; 
          $tmp_pos = strpos($this->t_id_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_id_tratamiento = substr($this->t_id_tratamiento, 0, $tmp_pos);
          }
          $this->t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
          $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
          $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
          }
          $this->t_nombre_referencia = $Busca_temp['t_nombre_referencia']; 
          $tmp_pos = strpos($this->t_nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_nombre_referencia = substr($this->t_nombre_referencia, 0, $tmp_pos);
          }
          $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
          $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
          }
          $this->t_tratamiento_previo = $Busca_temp['t_tratamiento_previo']; 
          $tmp_pos = strpos($this->t_tratamiento_previo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_tratamiento_previo = substr($this->t_tratamiento_previo, 0, $tmp_pos);
          }
          $this->t_consentimiento_tratamiento = $Busca_temp['t_consentimiento_tratamiento']; 
          $tmp_pos = strpos($this->t_consentimiento_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_consentimiento_tratamiento = substr($this->t_consentimiento_tratamiento, 0, $tmp_pos);
          }
          $this->t_fecha_inicio_terapia_tratamiento = $Busca_temp['t_fecha_inicio_terapia_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_inicio_terapia_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_inicio_terapia_tratamiento = substr($this->t_fecha_inicio_terapia_tratamiento, 0, $tmp_pos);
          }
          $this->t_regimen_tratamiento = $Busca_temp['t_regimen_tratamiento']; 
          $tmp_pos = strpos($this->t_regimen_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_regimen_tratamiento = substr($this->t_regimen_tratamiento, 0, $tmp_pos);
          }
          $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
          $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
          $tmp_pos = strpos($this->t_punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
          }
          $this->t_fecha_ultima_reclamacion_tratamiento = $Busca_temp['t_fecha_ultima_reclamacion_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_ultima_reclamacion_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_ultima_reclamacion_tratamiento = substr($this->t_fecha_ultima_reclamacion_tratamiento, 0, $tmp_pos);
          }
          $this->t_otros_operadores_tratamiento = $Busca_temp['t_otros_operadores_tratamiento']; 
          $tmp_pos = strpos($this->t_otros_operadores_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_otros_operadores_tratamiento = substr($this->t_otros_operadores_tratamiento, 0, $tmp_pos);
          }
          $this->t_medios_adquisicion_tratamiento = $Busca_temp['t_medios_adquisicion_tratamiento']; 
          $tmp_pos = strpos($this->t_medios_adquisicion_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medios_adquisicion_tratamiento = substr($this->t_medios_adquisicion_tratamiento, 0, $tmp_pos);
          }
          $this->t_ips_atiende_tratamiento = $Busca_temp['t_ips_atiende_tratamiento']; 
          $tmp_pos = strpos($this->t_ips_atiende_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_ips_atiende_tratamiento = substr($this->t_ips_atiende_tratamiento, 0, $tmp_pos);
          }
          $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
          $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
          }
          $this->t_especialidad_tratamiento = $Busca_temp['t_especialidad_tratamiento']; 
          $tmp_pos = strpos($this->t_especialidad_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_especialidad_tratamiento = substr($this->t_especialidad_tratamiento, 0, $tmp_pos);
          }
          $this->t_paramedico_tratamiento = $Busca_temp['t_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_paramedico_tratamiento = substr($this->t_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_zona_atencion_paramedico_tratamiento = $Busca_temp['t_zona_atencion_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_zona_atencion_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_zona_atencion_paramedico_tratamiento = substr($this->t_zona_atencion_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_ciudad_base_paramedico_tratamiento = $Busca_temp['t_ciudad_base_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_ciudad_base_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_ciudad_base_paramedico_tratamiento = substr($this->t_ciudad_base_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->t_notas_adjuntos_tratamiento = $Busca_temp['t_notas_adjuntos_tratamiento']; 
          $tmp_pos = strpos($this->t_notas_adjuntos_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_notas_adjuntos_tratamiento = substr($this->t_notas_adjuntos_tratamiento, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.ACUDIENTE_PACIENTE as p_acudiente_paciente, p.TELEFONO_ACUDIENTE_PACIENTE as p_telefono_acudiente_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.STATUS_PACIENTE as p_status_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, p.USUARIO_CREACION as p_usuario_creacion, t.ID_TRATAMIENTO as t_id_tratamiento, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<listado_pacientes";
         $this->p_id_paciente = $rs->fields[0] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->p_estado_paciente = $rs->fields[1] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[2] ;  
         $this->p_fecha_retiro_paciente = $rs->fields[3] ;  
         $this->p_motivo_retiro_paciente = $rs->fields[4] ;  
         $this->p_observacion_motivo_retiro_paciente = $rs->fields[5] ;  
         $this->p_identificacion_paciente = $rs->fields[6] ;  
         $this->p_nombre_paciente = $rs->fields[7] ;  
         $this->p_apellido_paciente = $rs->fields[8] ;  
         $this->p_telefono_paciente = $rs->fields[9] ;  
         $this->p_telefono2_paciente = $rs->fields[10] ;  
         $this->p_telefono3_paciente = $rs->fields[11] ;  
         $this->p_correo_paciente = $rs->fields[12] ;  
         $this->p_direccion_paciente = $rs->fields[13] ;  
         $this->p_barrio_paciente = $rs->fields[14] ;  
         $this->p_pais_paciente = $rs->fields[15] ;  
         $this->p_ciudad_paciente = $rs->fields[16] ;  
         $this->p_genero_paciente = $rs->fields[17] ;  
         $this->p_fecha_nacimineto_paciente = $rs->fields[18] ;  
         $this->p_edad_paciente = $rs->fields[19] ;  
         $this->p_edad_paciente = (string)$this->p_edad_paciente;
         $this->p_acudiente_paciente = $rs->fields[20] ;  
         $this->p_telefono_acudiente_paciente = $rs->fields[21] ;  
         $this->p_codigo_xofigo = $rs->fields[22] ;  
         $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
         $this->p_status_paciente = $rs->fields[23] ;  
         $this->p_id_ultima_gestion = $rs->fields[24] ;  
         $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
         $this->p_usuario_creacion = $rs->fields[25] ;  
         $this->t_id_tratamiento = $rs->fields[26] ;  
         $this->t_id_tratamiento = (string)$this->t_id_tratamiento;
         $this->t_producto_tratamiento = $rs->fields[27] ;  
         $this->t_nombre_referencia = $rs->fields[28] ;  
         $this->t_clasificacion_patologica_tratamiento = $rs->fields[29] ;  
         $this->t_tratamiento_previo = $rs->fields[30] ;  
         $this->t_consentimiento_tratamiento = $rs->fields[31] ;  
         $this->t_fecha_inicio_terapia_tratamiento = $rs->fields[32] ;  
         $this->t_regimen_tratamiento = $rs->fields[33] ;  
         $this->t_asegurador_tratamiento = $rs->fields[34] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[35] ;  
         $this->t_punto_entrega = $rs->fields[36] ;  
         $this->t_fecha_ultima_reclamacion_tratamiento = $rs->fields[37] ;  
         $this->t_otros_operadores_tratamiento = $rs->fields[38] ;  
         $this->t_medios_adquisicion_tratamiento = $rs->fields[39] ;  
         $this->t_ips_atiende_tratamiento = $rs->fields[40] ;  
         $this->t_medico_tratamiento = $rs->fields[41] ;  
         $this->t_especialidad_tratamiento = $rs->fields[42] ;  
         $this->t_paramedico_tratamiento = $rs->fields[43] ;  
         $this->t_zona_atencion_paramedico_tratamiento = $rs->fields[44] ;  
         $this->t_ciudad_base_paramedico_tratamiento = $rs->fields[45] ;  
         $this->t_notas_adjuntos_tratamiento = $rs->fields[46] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_paciente =\"" . $this->trata_dados($this->p_id_paciente) . "\"";
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_estado_paciente))
         {
             $this->p_estado_paciente = sc_convert_encoding($this->p_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_estado_paciente =\"" . $this->trata_dados($this->p_estado_paciente) . "\"";
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_activacion_paciente =\"" . $this->trata_dados($this->p_fecha_activacion_paciente) . "\"";
   }
   //----- p_fecha_retiro_paciente
   function NM_export_p_fecha_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_retiro_paciente))
         {
             $this->p_fecha_retiro_paciente = sc_convert_encoding($this->p_fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_retiro_paciente =\"" . $this->trata_dados($this->p_fecha_retiro_paciente) . "\"";
   }
   //----- p_motivo_retiro_paciente
   function NM_export_p_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_motivo_retiro_paciente))
         {
             $this->p_motivo_retiro_paciente = sc_convert_encoding($this->p_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_motivo_retiro_paciente =\"" . $this->trata_dados($this->p_motivo_retiro_paciente) . "\"";
   }
   //----- p_observacion_motivo_retiro_paciente
   function NM_export_p_observacion_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_observacion_motivo_retiro_paciente))
         {
             $this->p_observacion_motivo_retiro_paciente = sc_convert_encoding($this->p_observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_observacion_motivo_retiro_paciente =\"" . $this->trata_dados($this->p_observacion_motivo_retiro_paciente) . "\"";
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_identificacion_paciente))
         {
             $this->p_identificacion_paciente = sc_convert_encoding($this->p_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_identificacion_paciente =\"" . $this->trata_dados($this->p_identificacion_paciente) . "\"";
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_nombre_paciente))
         {
             $this->p_nombre_paciente = sc_convert_encoding($this->p_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_nombre_paciente =\"" . $this->trata_dados($this->p_nombre_paciente) . "\"";
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_apellido_paciente))
         {
             $this->p_apellido_paciente = sc_convert_encoding($this->p_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_apellido_paciente =\"" . $this->trata_dados($this->p_apellido_paciente) . "\"";
   }
   //----- p_telefono_paciente
   function NM_export_p_telefono_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_telefono_paciente))
         {
             $this->p_telefono_paciente = sc_convert_encoding($this->p_telefono_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_telefono_paciente =\"" . $this->trata_dados($this->p_telefono_paciente) . "\"";
   }
   //----- p_telefono2_paciente
   function NM_export_p_telefono2_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_telefono2_paciente))
         {
             $this->p_telefono2_paciente = sc_convert_encoding($this->p_telefono2_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_telefono2_paciente =\"" . $this->trata_dados($this->p_telefono2_paciente) . "\"";
   }
   //----- p_telefono3_paciente
   function NM_export_p_telefono3_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_telefono3_paciente))
         {
             $this->p_telefono3_paciente = sc_convert_encoding($this->p_telefono3_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_telefono3_paciente =\"" . $this->trata_dados($this->p_telefono3_paciente) . "\"";
   }
   //----- p_correo_paciente
   function NM_export_p_correo_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_correo_paciente))
         {
             $this->p_correo_paciente = sc_convert_encoding($this->p_correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_correo_paciente =\"" . $this->trata_dados($this->p_correo_paciente) . "\"";
   }
   //----- p_direccion_paciente
   function NM_export_p_direccion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_direccion_paciente))
         {
             $this->p_direccion_paciente = sc_convert_encoding($this->p_direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_direccion_paciente =\"" . $this->trata_dados($this->p_direccion_paciente) . "\"";
   }
   //----- p_barrio_paciente
   function NM_export_p_barrio_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_barrio_paciente))
         {
             $this->p_barrio_paciente = sc_convert_encoding($this->p_barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_barrio_paciente =\"" . $this->trata_dados($this->p_barrio_paciente) . "\"";
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_pais_paciente =\"" . $this->trata_dados($this->p_pais_paciente) . "\"";
   }
   //----- p_ciudad_paciente
   function NM_export_p_ciudad_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_ciudad_paciente))
         {
             $this->p_ciudad_paciente = sc_convert_encoding($this->p_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_ciudad_paciente =\"" . $this->trata_dados($this->p_ciudad_paciente) . "\"";
   }
   //----- p_genero_paciente
   function NM_export_p_genero_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_genero_paciente))
         {
             $this->p_genero_paciente = sc_convert_encoding($this->p_genero_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_genero_paciente =\"" . $this->trata_dados($this->p_genero_paciente) . "\"";
   }
   //----- p_fecha_nacimineto_paciente
   function NM_export_p_fecha_nacimineto_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_nacimineto_paciente))
         {
             $this->p_fecha_nacimineto_paciente = sc_convert_encoding($this->p_fecha_nacimineto_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_nacimineto_paciente =\"" . $this->trata_dados($this->p_fecha_nacimineto_paciente) . "\"";
   }
   //----- p_edad_paciente
   function NM_export_p_edad_paciente()
   {
         nmgp_Form_Num_Val($this->p_edad_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_edad_paciente))
         {
             $this->p_edad_paciente = sc_convert_encoding($this->p_edad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_edad_paciente =\"" . $this->trata_dados($this->p_edad_paciente) . "\"";
   }
   //----- p_acudiente_paciente
   function NM_export_p_acudiente_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_acudiente_paciente))
         {
             $this->p_acudiente_paciente = sc_convert_encoding($this->p_acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_acudiente_paciente =\"" . $this->trata_dados($this->p_acudiente_paciente) . "\"";
   }
   //----- p_telefono_acudiente_paciente
   function NM_export_p_telefono_acudiente_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_telefono_acudiente_paciente))
         {
             $this->p_telefono_acudiente_paciente = sc_convert_encoding($this->p_telefono_acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_telefono_acudiente_paciente =\"" . $this->trata_dados($this->p_telefono_acudiente_paciente) . "\"";
   }
   //----- p_codigo_xofigo
   function NM_export_p_codigo_xofigo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_codigo_xofigo))
         {
             $this->p_codigo_xofigo = sc_convert_encoding($this->p_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_codigo_xofigo =\"" . $this->trata_dados($this->p_codigo_xofigo) . "\"";
   }
   //----- p_status_paciente
   function NM_export_p_status_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_status_paciente))
         {
             $this->p_status_paciente = sc_convert_encoding($this->p_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_status_paciente =\"" . $this->trata_dados($this->p_status_paciente) . "\"";
   }
   //----- p_id_ultima_gestion
   function NM_export_p_id_ultima_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_ultima_gestion))
         {
             $this->p_id_ultima_gestion = sc_convert_encoding($this->p_id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_ultima_gestion =\"" . $this->trata_dados($this->p_id_ultima_gestion) . "\"";
   }
   //----- p_usuario_creacion
   function NM_export_p_usuario_creacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_usuario_creacion))
         {
             $this->p_usuario_creacion = sc_convert_encoding($this->p_usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_usuario_creacion =\"" . $this->trata_dados($this->p_usuario_creacion) . "\"";
   }
   //----- t_id_tratamiento
   function NM_export_t_id_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_id_tratamiento))
         {
             $this->t_id_tratamiento = sc_convert_encoding($this->t_id_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_id_tratamiento =\"" . $this->trata_dados($this->t_id_tratamiento) . "\"";
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_producto_tratamiento))
         {
             $this->t_producto_tratamiento = sc_convert_encoding($this->t_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_producto_tratamiento =\"" . $this->trata_dados($this->t_producto_tratamiento) . "\"";
   }
   //----- t_nombre_referencia
   function NM_export_t_nombre_referencia()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_nombre_referencia))
         {
             $this->t_nombre_referencia = sc_convert_encoding($this->t_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_nombre_referencia =\"" . $this->trata_dados($this->t_nombre_referencia) . "\"";
   }
   //----- t_clasificacion_patologica_tratamiento
   function NM_export_t_clasificacion_patologica_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_clasificacion_patologica_tratamiento))
         {
             $this->t_clasificacion_patologica_tratamiento = sc_convert_encoding($this->t_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_clasificacion_patologica_tratamiento =\"" . $this->trata_dados($this->t_clasificacion_patologica_tratamiento) . "\"";
   }
   //----- t_tratamiento_previo
   function NM_export_t_tratamiento_previo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_tratamiento_previo))
         {
             $this->t_tratamiento_previo = sc_convert_encoding($this->t_tratamiento_previo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_tratamiento_previo =\"" . $this->trata_dados($this->t_tratamiento_previo) . "\"";
   }
   //----- t_consentimiento_tratamiento
   function NM_export_t_consentimiento_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_consentimiento_tratamiento))
         {
             $this->t_consentimiento_tratamiento = sc_convert_encoding($this->t_consentimiento_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_consentimiento_tratamiento =\"" . $this->trata_dados($this->t_consentimiento_tratamiento) . "\"";
   }
   //----- t_fecha_inicio_terapia_tratamiento
   function NM_export_t_fecha_inicio_terapia_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_fecha_inicio_terapia_tratamiento))
         {
             $this->t_fecha_inicio_terapia_tratamiento = sc_convert_encoding($this->t_fecha_inicio_terapia_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_fecha_inicio_terapia_tratamiento =\"" . $this->trata_dados($this->t_fecha_inicio_terapia_tratamiento) . "\"";
   }
   //----- t_regimen_tratamiento
   function NM_export_t_regimen_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_regimen_tratamiento))
         {
             $this->t_regimen_tratamiento = sc_convert_encoding($this->t_regimen_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_regimen_tratamiento =\"" . $this->trata_dados($this->t_regimen_tratamiento) . "\"";
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_asegurador_tratamiento))
         {
             $this->t_asegurador_tratamiento = sc_convert_encoding($this->t_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_asegurador_tratamiento =\"" . $this->trata_dados($this->t_asegurador_tratamiento) . "\"";
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_operador_logistico_tratamiento))
         {
             $this->t_operador_logistico_tratamiento = sc_convert_encoding($this->t_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_operador_logistico_tratamiento =\"" . $this->trata_dados($this->t_operador_logistico_tratamiento) . "\"";
   }
   //----- t_punto_entrega
   function NM_export_t_punto_entrega()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_punto_entrega))
         {
             $this->t_punto_entrega = sc_convert_encoding($this->t_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_punto_entrega =\"" . $this->trata_dados($this->t_punto_entrega) . "\"";
   }
   //----- t_fecha_ultima_reclamacion_tratamiento
   function NM_export_t_fecha_ultima_reclamacion_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_fecha_ultima_reclamacion_tratamiento))
         {
             $this->t_fecha_ultima_reclamacion_tratamiento = sc_convert_encoding($this->t_fecha_ultima_reclamacion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_fecha_ultima_reclamacion_tratamiento =\"" . $this->trata_dados($this->t_fecha_ultima_reclamacion_tratamiento) . "\"";
   }
   //----- t_otros_operadores_tratamiento
   function NM_export_t_otros_operadores_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_otros_operadores_tratamiento))
         {
             $this->t_otros_operadores_tratamiento = sc_convert_encoding($this->t_otros_operadores_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_otros_operadores_tratamiento =\"" . $this->trata_dados($this->t_otros_operadores_tratamiento) . "\"";
   }
   //----- t_medios_adquisicion_tratamiento
   function NM_export_t_medios_adquisicion_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_medios_adquisicion_tratamiento))
         {
             $this->t_medios_adquisicion_tratamiento = sc_convert_encoding($this->t_medios_adquisicion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_medios_adquisicion_tratamiento =\"" . $this->trata_dados($this->t_medios_adquisicion_tratamiento) . "\"";
   }
   //----- t_ips_atiende_tratamiento
   function NM_export_t_ips_atiende_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_ips_atiende_tratamiento))
         {
             $this->t_ips_atiende_tratamiento = sc_convert_encoding($this->t_ips_atiende_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_ips_atiende_tratamiento =\"" . $this->trata_dados($this->t_ips_atiende_tratamiento) . "\"";
   }
   //----- t_medico_tratamiento
   function NM_export_t_medico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_medico_tratamiento))
         {
             $this->t_medico_tratamiento = sc_convert_encoding($this->t_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_medico_tratamiento =\"" . $this->trata_dados($this->t_medico_tratamiento) . "\"";
   }
   //----- t_especialidad_tratamiento
   function NM_export_t_especialidad_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_especialidad_tratamiento))
         {
             $this->t_especialidad_tratamiento = sc_convert_encoding($this->t_especialidad_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_especialidad_tratamiento =\"" . $this->trata_dados($this->t_especialidad_tratamiento) . "\"";
   }
   //----- t_paramedico_tratamiento
   function NM_export_t_paramedico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_paramedico_tratamiento))
         {
             $this->t_paramedico_tratamiento = sc_convert_encoding($this->t_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_paramedico_tratamiento =\"" . $this->trata_dados($this->t_paramedico_tratamiento) . "\"";
   }
   //----- t_zona_atencion_paramedico_tratamiento
   function NM_export_t_zona_atencion_paramedico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_zona_atencion_paramedico_tratamiento))
         {
             $this->t_zona_atencion_paramedico_tratamiento = sc_convert_encoding($this->t_zona_atencion_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_zona_atencion_paramedico_tratamiento =\"" . $this->trata_dados($this->t_zona_atencion_paramedico_tratamiento) . "\"";
   }
   //----- t_ciudad_base_paramedico_tratamiento
   function NM_export_t_ciudad_base_paramedico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_ciudad_base_paramedico_tratamiento))
         {
             $this->t_ciudad_base_paramedico_tratamiento = sc_convert_encoding($this->t_ciudad_base_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_ciudad_base_paramedico_tratamiento =\"" . $this->trata_dados($this->t_ciudad_base_paramedico_tratamiento) . "\"";
   }
   //----- t_notas_adjuntos_tratamiento
   function NM_export_t_notas_adjuntos_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_notas_adjuntos_tratamiento))
         {
             $this->t_notas_adjuntos_tratamiento = sc_convert_encoding($this->t_notas_adjuntos_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_notas_adjuntos_tratamiento =\"" . $this->trata_dados($this->t_notas_adjuntos_tratamiento) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['listado_pacientes'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Listado Pacientes :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="listado_pacientes_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="listado_pacientes"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
