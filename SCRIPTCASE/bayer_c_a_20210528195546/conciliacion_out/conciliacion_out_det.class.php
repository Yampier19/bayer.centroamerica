<?php
//--- 
class conciliacion_out_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $g_fecha_comunicacion;
   var $p_nombre_paciente;
   var $p_apellido_paciente;
   var $p_identificacion_paciente;
   var $p_pais_paciente;
   var $t_producto_tratamiento;
   var $p_telefono_paciente;
   var $p_telefono2_paciente;
   var $p_telefono3_paciente;
   var $g_fecha_programada_gestion;
   var $g_descripcion_comunicacion_gestion;
   var $g_evento_adverso_gestion;
   var $g_tipo_evento_adverso;
   var $g_autor_gestion;
   var $g_numero_intentos_gestion;
   var $p_estado_paciente;
   var $g_tipo_llamada_gestion;
   var $p_id_paciente;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
        $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
        }
        $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
        $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
        }
        $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
        $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
        }
        $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
        $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
        }
        $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
        $tmp_pos = strpos($this->p_pais_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
        }
        $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
        $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
        }
        $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
        $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
        }
        $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
        $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
        }
        $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
        $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
        }
        $this->g_fecha_programada_gestion = $Busca_temp['g_fecha_programada_gestion']; 
        $tmp_pos = strpos($this->g_fecha_programada_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_programada_gestion = substr($this->g_fecha_programada_gestion, 0, $tmp_pos);
        }
        $this->g_descripcion_comunicacion_gestion = $Busca_temp['g_descripcion_comunicacion_gestion']; 
        $tmp_pos = strpos($this->g_descripcion_comunicacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_descripcion_comunicacion_gestion = substr($this->g_descripcion_comunicacion_gestion, 0, $tmp_pos);
        }
        $this->g_evento_adverso_gestion = $Busca_temp['g_evento_adverso_gestion']; 
        $tmp_pos = strpos($this->g_evento_adverso_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_evento_adverso_gestion = substr($this->g_evento_adverso_gestion, 0, $tmp_pos);
        }
        $this->g_tipo_evento_adverso = $Busca_temp['g_tipo_evento_adverso']; 
        $tmp_pos = strpos($this->g_tipo_evento_adverso, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_tipo_evento_adverso = substr($this->g_tipo_evento_adverso, 0, $tmp_pos);
        }
        $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
        $tmp_pos = strpos($this->g_autor_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
        }
        $this->g_numero_intentos_gestion = $Busca_temp['g_numero_intentos_gestion']; 
        $tmp_pos = strpos($this->g_numero_intentos_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_numero_intentos_gestion = substr($this->g_numero_intentos_gestion, 0, $tmp_pos);
        }
        $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
        $tmp_pos = strpos($this->p_estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
        }
        $this->g_tipo_llamada_gestion = $Busca_temp['g_tipo_llamada_gestion']; 
        $tmp_pos = strpos($this->g_tipo_llamada_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_tipo_llamada_gestion = substr($this->g_tipo_llamada_gestion, 0, $tmp_pos);
        }
        $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
        $tmp_pos = strpos($this->p_id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
        }
        $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, LOTOFILE(g.DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as g_descripcion_comunicacion_gestion, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  g.FECHA_COMUNICACION = '$parms_det[0]' and p.NOMBRE_PACIENTE = '$parms_det[1]' and p.APELLIDO_PACIENTE = '$parms_det[2]' and p.IDENTIFICACION_PACIENTE = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[5]' and p.TELEFONO_PACIENTE = '$parms_det[6]' and p.TELEFONO2_PACIENTE = '$parms_det[7]' and p.TELEFONO3_PACIENTE = '$parms_det[8]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[9]' and g.EVENTO_ADVERSO_GESTION = '$parms_det[10]' and g.TIPO_EVENTO_ADVERSO = '$parms_det[11]' and g.AUTOR_GESTION = '$parms_det[12]' and g.NUMERO_INTENTOS_GESTION = '$parms_det[13]' and p.ESTADO_PACIENTE = '$parms_det[14]' and g.TIPO_LLAMADA_GESTION = '$parms_det[15]' and p.ID_PACIENTE = $parms_det[16]" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  g.FECHA_COMUNICACION = '$parms_det[0]' and p.NOMBRE_PACIENTE = '$parms_det[1]' and p.APELLIDO_PACIENTE = '$parms_det[2]' and p.IDENTIFICACION_PACIENTE = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[5]' and p.TELEFONO_PACIENTE = '$parms_det[6]' and p.TELEFONO2_PACIENTE = '$parms_det[7]' and p.TELEFONO3_PACIENTE = '$parms_det[8]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[9]' and g.EVENTO_ADVERSO_GESTION = '$parms_det[10]' and g.TIPO_EVENTO_ADVERSO = '$parms_det[11]' and g.AUTOR_GESTION = '$parms_det[12]' and g.NUMERO_INTENTOS_GESTION = '$parms_det[13]' and p.ESTADO_PACIENTE = '$parms_det[14]' and g.TIPO_LLAMADA_GESTION = '$parms_det[15]' and p.ID_PACIENTE = $parms_det[16]" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  g.FECHA_COMUNICACION = '$parms_det[0]' and p.NOMBRE_PACIENTE = '$parms_det[1]' and p.APELLIDO_PACIENTE = '$parms_det[2]' and p.IDENTIFICACION_PACIENTE = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[5]' and p.TELEFONO_PACIENTE = '$parms_det[6]' and p.TELEFONO2_PACIENTE = '$parms_det[7]' and p.TELEFONO3_PACIENTE = '$parms_det[8]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[9]' and g.EVENTO_ADVERSO_GESTION = '$parms_det[10]' and g.TIPO_EVENTO_ADVERSO = '$parms_det[11]' and g.AUTOR_GESTION = '$parms_det[12]' and g.NUMERO_INTENTOS_GESTION = '$parms_det[13]' and p.ESTADO_PACIENTE = '$parms_det[14]' and g.TIPO_LLAMADA_GESTION = '$parms_det[15]' and p.ID_PACIENTE = $parms_det[16]" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->g_fecha_comunicacion = $rs->fields[0] ;  
   $this->p_nombre_paciente = $rs->fields[1] ;  
   $this->p_apellido_paciente = $rs->fields[2] ;  
   $this->p_identificacion_paciente = $rs->fields[3] ;  
   $this->p_pais_paciente = $rs->fields[4] ;  
   $this->t_producto_tratamiento = $rs->fields[5] ;  
   $this->p_telefono_paciente = $rs->fields[6] ;  
   $this->p_telefono2_paciente = $rs->fields[7] ;  
   $this->p_telefono3_paciente = $rs->fields[8] ;  
   $this->g_fecha_programada_gestion = $rs->fields[9] ;  
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
   { 
       $this->g_descripcion_comunicacion_gestion = "";  
       if (is_file($rs->fields[10])) 
       { 
           $this->g_descripcion_comunicacion_gestion = file_get_contents($rs->fields[10]);  
       } 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[10]) ;  
   } 
   else
   { 
       $this->g_descripcion_comunicacion_gestion = $rs->fields[10] ;  
   } 
   $this->g_evento_adverso_gestion = $rs->fields[11] ;  
   $this->g_tipo_evento_adverso = $rs->fields[12] ;  
   $this->g_autor_gestion = $rs->fields[13] ;  
   $this->g_numero_intentos_gestion = $rs->fields[14] ;  
   $this->p_estado_paciente = $rs->fields[15] ;  
   $this->g_tipo_llamada_gestion = $rs->fields[16] ;  
   $this->p_id_paciente = $rs->fields[17] ;  
   $this->p_id_paciente = (string)$this->p_id_paciente;
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   { 
       if (!empty($this->g_descripcion_comunicacion_gestion))
       { 
           $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($this->g_descripcion_comunicacion_gestion, false, true, "BLOB");
       }
   }
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Conciliacion out</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "conciliacion_out/conciliacion_out_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Conciliacion out</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "conciliacion_out/conciliacion_out_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "conciliacion_out/conciliacion_out_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_comunicacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_comunicacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_fecha_comunicacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_nombre_paciente'])) ? $this->New_label['p_nombre_paciente'] : "NOMBRE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_nombre_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_nombre_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_nombre_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_apellido_paciente'])) ? $this->New_label['p_apellido_paciente'] : "APELLIDO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_apellido_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_apellido_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_apellido_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_identificacion_paciente'])) ? $this->New_label['p_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_identificacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_identificacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_identificacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_pais_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_pais_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_pais_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono_paciente'])) ? $this->New_label['p_telefono_paciente'] : "TELEFONO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_telefono_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono2_paciente'])) ? $this->New_label['p_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono2_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono2_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_telefono2_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_telefono3_paciente'])) ? $this->New_label['p_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_telefono3_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_telefono3_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_telefono3_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_programada_gestion'])) ? $this->New_label['g_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_programada_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_programada_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_fecha_programada_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_descripcion_comunicacion_gestion'])) ? $this->New_label['g_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_descripcion_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else   
          { 
              $conteudo = nl2br($conteudo) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_descripcion_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_descripcion_comunicacion_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_evento_adverso_gestion'])) ? $this->New_label['g_evento_adverso_gestion'] : "EVENTO ADVERSO GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_evento_adverso_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_evento_adverso_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_evento_adverso_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_tipo_evento_adverso'])) ? $this->New_label['g_tipo_evento_adverso'] : "TIPO EVENTO ADVERSO"; 
          $conteudo = trim(sc_strip_script($this->g_tipo_evento_adverso)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_tipo_evento_adverso_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_tipo_evento_adverso_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_autor_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_autor_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_autor_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_numero_intentos_gestion'])) ? $this->New_label['g_numero_intentos_gestion'] : "NUMERO INTENTOS GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_numero_intentos_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_numero_intentos_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_numero_intentos_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_tipo_llamada_gestion'])) ? $this->New_label['g_tipo_llamada_gestion'] : "TIPO LLAMADA GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_tipo_llamada_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_tipo_llamada_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_tipo_llamada_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"conciliacion_out_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "conciliacion_out/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "conciliacion_out/conciliacion_out_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
