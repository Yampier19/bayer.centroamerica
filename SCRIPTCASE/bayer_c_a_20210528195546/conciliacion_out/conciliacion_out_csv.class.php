<?php

class conciliacion_out_csv
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Tit_doc;
   var $Delim_dados;
   var $Delim_line;
   var $Delim_col;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function conciliacion_out_csv()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_csv()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
     global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo     = "sc_csv";
      $this->Arquivo    .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo    .= "_conciliacion_out";
      $this->Arquivo    .= ".csv";
      $this->Tit_doc    = "conciliacion_out.csv";
      $this->Delim_dados = "\"";
      $this->Delim_col   = ";";
      $this->Delim_line  = "\r\n";
   }

   //----- 
   function grava_arquivo()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['conciliacion_out']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
          $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
          }
          $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
          $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
          }
          $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
          $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
          }
          $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
          $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
          $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
          }
          $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
          $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
          }
          $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
          $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
          }
          $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
          $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
          }
          $this->g_fecha_programada_gestion = $Busca_temp['g_fecha_programada_gestion']; 
          $tmp_pos = strpos($this->g_fecha_programada_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_programada_gestion = substr($this->g_fecha_programada_gestion, 0, $tmp_pos);
          }
          $this->g_descripcion_comunicacion_gestion = $Busca_temp['g_descripcion_comunicacion_gestion']; 
          $tmp_pos = strpos($this->g_descripcion_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_descripcion_comunicacion_gestion = substr($this->g_descripcion_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->g_evento_adverso_gestion = $Busca_temp['g_evento_adverso_gestion']; 
          $tmp_pos = strpos($this->g_evento_adverso_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_evento_adverso_gestion = substr($this->g_evento_adverso_gestion, 0, $tmp_pos);
          }
          $this->g_tipo_evento_adverso = $Busca_temp['g_tipo_evento_adverso']; 
          $tmp_pos = strpos($this->g_tipo_evento_adverso, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_tipo_evento_adverso = substr($this->g_tipo_evento_adverso, 0, $tmp_pos);
          }
          $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
          $tmp_pos = strpos($this->g_autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
          }
          $this->g_numero_intentos_gestion = $Busca_temp['g_numero_intentos_gestion']; 
          $tmp_pos = strpos($this->g_numero_intentos_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_numero_intentos_gestion = substr($this->g_numero_intentos_gestion, 0, $tmp_pos);
          }
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->g_tipo_llamada_gestion = $Busca_temp['g_tipo_llamada_gestion']; 
          $tmp_pos = strpos($this->g_tipo_llamada_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_tipo_llamada_gestion = substr($this->g_tipo_llamada_gestion, 0, $tmp_pos);
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, LOTOFILE(g.DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT g.FECHA_COMUNICACION as g_fecha_comunicacion, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.PAIS_PACIENTE as p_pais_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_1, g.EVENTO_ADVERSO_GESTION as g_evento_adverso_gestion, g.TIPO_EVENTO_ADVERSO as g_tipo_evento_adverso, g.AUTOR_GESTION as g_autor_gestion, g.NUMERO_INTENTOS_GESTION as g_numero_intentos_gestion, p.ESTADO_PACIENTE as p_estado_paciente, g.TIPO_LLAMADA_GESTION as g_tipo_llamada_gestion, p.ID_PACIENTE as p_id_paciente from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $csv_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      $this->NM_prim_col  = 0;
      $this->csv_registro = "";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "g_fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_nombre_paciente'])) ? $this->New_label['p_nombre_paciente'] : "NOMBRE PACIENTE"; 
          if ($Cada_col == "p_nombre_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_apellido_paciente'])) ? $this->New_label['p_apellido_paciente'] : "APELLIDO PACIENTE"; 
          if ($Cada_col == "p_apellido_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_identificacion_paciente'])) ? $this->New_label['p_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          if ($Cada_col == "p_identificacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          if ($Cada_col == "p_pais_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "t_producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_telefono_paciente'])) ? $this->New_label['p_telefono_paciente'] : "TELEFONO PACIENTE"; 
          if ($Cada_col == "p_telefono_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_telefono2_paciente'])) ? $this->New_label['p_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          if ($Cada_col == "p_telefono2_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_telefono3_paciente'])) ? $this->New_label['p_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          if ($Cada_col == "p_telefono3_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_fecha_programada_gestion'])) ? $this->New_label['g_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; 
          if ($Cada_col == "g_fecha_programada_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_descripcion_comunicacion_gestion'])) ? $this->New_label['g_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; 
          if ($Cada_col == "g_descripcion_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_evento_adverso_gestion'])) ? $this->New_label['g_evento_adverso_gestion'] : "EVENTO ADVERSO GESTION"; 
          if ($Cada_col == "g_evento_adverso_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_tipo_evento_adverso'])) ? $this->New_label['g_tipo_evento_adverso'] : "TIPO EVENTO ADVERSO"; 
          if ($Cada_col == "g_tipo_evento_adverso" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "g_autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_numero_intentos_gestion'])) ? $this->New_label['g_numero_intentos_gestion'] : "NUMERO INTENTOS GESTION"; 
          if ($Cada_col == "g_numero_intentos_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "p_estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['g_tipo_llamada_gestion'])) ? $this->New_label['g_tipo_llamada_gestion'] : "TIPO LLAMADA GESTION"; 
          if ($Cada_col == "g_tipo_llamada_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "p_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
      } 
      $this->csv_registro .= $this->Delim_line;
      fwrite($csv_f, $this->csv_registro);
      while (!$rs->EOF)
      {
         $this->csv_registro = "";
         $this->NM_prim_col  = 0;
         $this->g_fecha_comunicacion = $rs->fields[0] ;  
         $this->p_nombre_paciente = $rs->fields[1] ;  
         $this->p_apellido_paciente = $rs->fields[2] ;  
         $this->p_identificacion_paciente = $rs->fields[3] ;  
         $this->p_pais_paciente = $rs->fields[4] ;  
         $this->t_producto_tratamiento = $rs->fields[5] ;  
         $this->p_telefono_paciente = $rs->fields[6] ;  
         $this->p_telefono2_paciente = $rs->fields[7] ;  
         $this->p_telefono3_paciente = $rs->fields[8] ;  
         $this->g_fecha_programada_gestion = $rs->fields[9] ;  
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $this->g_descripcion_comunicacion_gestion = "";  
              if (is_file($rs_grid->fields[10])) 
              { 
                  $this->g_descripcion_comunicacion_gestion = file_get_contents($rs_grid->fields[10]);  
              } 
          } 
         elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
         { 
             $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[10]) ;  
         } 
         else
         { 
             $this->g_descripcion_comunicacion_gestion = $rs->fields[10] ;  
         } 
         $this->g_evento_adverso_gestion = $rs->fields[11] ;  
         $this->g_tipo_evento_adverso = $rs->fields[12] ;  
         $this->g_autor_gestion = $rs->fields[13] ;  
         $this->g_numero_intentos_gestion = $rs->fields[14] ;  
         $this->p_estado_paciente = $rs->fields[15] ;  
         $this->g_tipo_llamada_gestion = $rs->fields[16] ;  
         $this->p_id_paciente = $rs->fields[17] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->csv_registro .= $this->Delim_line;
         fwrite($csv_f, $this->csv_registro);
         $rs->MoveNext();
      }
      fclose($csv_f);

      $rs->Close();
   }
   //----- g_fecha_comunicacion
   function NM_export_g_fecha_comunicacion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_fecha_comunicacion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_nombre_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_apellido_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_identificacion_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_pais_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_producto_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono_paciente
   function NM_export_p_telefono_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono2_paciente
   function NM_export_p_telefono2_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono2_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono3_paciente
   function NM_export_p_telefono3_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono3_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_fecha_programada_gestion
   function NM_export_g_fecha_programada_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_fecha_programada_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_descripcion_comunicacion_gestion
   function NM_export_g_descripcion_comunicacion_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_descripcion_comunicacion_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_evento_adverso_gestion
   function NM_export_g_evento_adverso_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_evento_adverso_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_tipo_evento_adverso
   function NM_export_g_tipo_evento_adverso()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_tipo_evento_adverso);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_autor_gestion
   function NM_export_g_autor_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_autor_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_numero_intentos_gestion
   function NM_export_g_numero_intentos_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_numero_intentos_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_estado_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- g_tipo_llamada_gestion
   function NM_export_g_tipo_llamada_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->g_tipo_llamada_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_id_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out']['csv_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['conciliacion_out'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Conciliacion out :: CSV</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?>" GMT">
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate">
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0">
 <META http-equiv="Pragma" content="no-cache">
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">CSV</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="conciliacion_out_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="conciliacion_out"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
