<?php

class informe_reclamacion_pesq
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $cmp_formatado;
   var $nm_data;
   var $Campos_Mens_erro;

   var $comando;
   var $comando_sum;
   var $comando_filtro;
   var $comando_ini;
   var $comando_fim;
   var $NM_operador;
   var $NM_data_qp;
   var $NM_path_filter;
   var $NM_curr_fil;
   var $nm_location;
   var $nmgp_botoes = array();
   var $NM_fil_ant = array();

   /**
    * @access  public
    */
   function informe_reclamacion_pesq()
   {
   }

   /**
    * @access  public
    * @global  string  $bprocessa  
    */
   function monta_busca()
   {
      global $bprocessa;
      include("../_lib/css/" . $this->Ini->str_schema_filter . "_filter.php");
      $this->Ini->Str_btn_filter = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
      $this->Str_btn_filter_css  = trim($str_button) . "/" . trim($str_button) . ".css";
      include($this->Ini->path_btn . $this->Ini->Str_btn_filter);
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['path_libs_php'] = $this->Ini->path_lib_php;
      $this->Img_sep_filter = "/" . trim($str_toolbar_separator);
      $this->Block_img_col  = trim($str_block_col);
      $this->Block_img_exp  = trim($str_block_exp);
      $this->Bubble_tail    = trim($str_bubble_tail);
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_config_btn.php", "F", "nmButtonOutput"); 
      $this->init();
      if ($this->NM_ajax_flag)
      {
          ob_start();
          $this->Arr_result = array();
          $this->processa_ajax();
          $Temp = ob_get_clean();
          if ($Temp !== false && trim($Temp) != "")
          {
              $this->Arr_result['htmOutput'] = NM_charset_to_utf8($Temp);
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($this->Arr_result);
          if ($this->Db)
          {
              $this->Db->Close(); 
          }
          exit;
      }
      if (isset($bprocessa) && "pesq" == $bprocessa)
      {
         $this->processa_busca();
      }
      else
      {
         $this->monta_formulario();
      }
   }

   /**
    * @access  public
    */
   function monta_formulario()
   {
      $this->monta_html_ini();
      $this->monta_cabecalho();
      $this->monta_form();
      $this->monta_html_fim();
   }

   /**
    * @access  public
    */
   function init()
   {
      global $bprocessa;
      $_SESSION['scriptcase']['sc_tab_meses']['int'] = array(
                                  $this->Ini->Nm_lang['lang_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_mnth_june'],
                                  $this->Ini->Nm_lang['lang_mnth_july'],
                                  $this->Ini->Nm_lang['lang_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_meses']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_june'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_july'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_dias']['int'] = array(
                                  $this->Ini->Nm_lang['lang_days_sund'],
                                  $this->Ini->Nm_lang['lang_days_mond'],
                                  $this->Ini->Nm_lang['lang_days_tued'],
                                  $this->Ini->Nm_lang['lang_days_wend'],
                                  $this->Ini->Nm_lang['lang_days_thud'],
                                  $this->Ini->Nm_lang['lang_days_frid'],
                                  $this->Ini->Nm_lang['lang_days_satd']);
      $_SESSION['scriptcase']['sc_tab_dias']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_days_sund'],
                                  $this->Ini->Nm_lang['lang_shrt_days_mond'],
                                  $this->Ini->Nm_lang['lang_shrt_days_tued'],
                                  $this->Ini->Nm_lang['lang_shrt_days_wend'],
                                  $this->Ini->Nm_lang['lang_shrt_days_thud'],
                                  $this->Ini->Nm_lang['lang_shrt_days_frid'],
                                  $this->Ini->Nm_lang['lang_shrt_days_satd']);
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_functions.php", "", "") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_data.class.php", "C", "nm_data") ; 
      $this->nm_data = new nm_data("es");
      $pos_path = strrpos($this->Ini->path_prod, "/");
      $this->NM_path_filter = $this->Ini->root . substr($this->Ini->path_prod, 0, $pos_path) . "/conf/filters/";
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['opcao'] = "igual";
   }

   function processa_ajax()
   {
      global $NM_filters, $NM_filters_del, $nmgp_save_name, $nmgp_save_option, $NM_fields_refresh, $NM_parms_refresh, $Campo_bi, $Opc_bi, $NM_operador;
//-- ajax metodos ---
      if ($this->NM_ajax_opcao == "ajax_filter_save")
      {
          $this->salva_filtro();
          $this->NM_fil_ant = $this->gera_array_filtros();
          $Nome_filter = "";
          $Opt_filter  = "<option value=\"\"></option>\r\n";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              if ($_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Tipo_filter[1] = sc_convert_encoding($Tipo_filter[1], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  $Opt_filter .= "<option value=\"\">" . informe_reclamacion_pack_protect_string($Nome_filter) . "</option>\r\n";
              }
              $Opt_filter .= "<option value=\"" . informe_reclamacion_pack_protect_string($Tipo_filter[0]) . "\">.." . informe_reclamacion_pack_protect_string($Cada_filter) .  "</option>\r\n";
          }
          $Ajax_select  = "<SELECT id=\"sel_recup_filters_bot\" name=\"NM_filters_bot\" onChange=\"nm_submit_filter(this, 'bot')\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_bot", 'value' => $Ajax_select);
          $Ajax_select = "<SELECT id=\"sel_filters_del_bot\" name=\"NM_filters_del_bot\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_del_bot", 'value' => $Ajax_select);
      }

      if ($this->NM_ajax_opcao == "ajax_filter_delete")
      {
          $this->apaga_filtro();
          $this->NM_fil_ant = $this->gera_array_filtros();
          $Nome_filter = "";
          $Opt_filter  = "<option value=\"\"></option>\r\n";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              if ($_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Tipo_filter[1] = sc_convert_encoding($Tipo_filter[1], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter  = $Tipo_filter[1];
                  $Opt_filter .= "<option value=\"\">" .  informe_reclamacion_pack_protect_string($Nome_filter) . "</option>\r\n";
              }
              $Opt_filter .= "<option value=\"" . informe_reclamacion_pack_protect_string($Tipo_filter[0]) . "\">.." . informe_reclamacion_pack_protect_string($Cada_filter) .  "</option>\r\n";
          }
          $Ajax_select  = "<SELECT id=\"sel_recup_filters_bot\" name=\"NM_filters_bot\" onChange=\"nm_submit_filter(this, 'bot')\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_bot", 'value' => $Ajax_select);
          $Ajax_select = "<SELECT id=\"sel_filters_del_bot\" name=\"NM_filters_del_bot\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_del_bot", 'value' => $Ajax_select);
      }
      if ($this->NM_ajax_opcao == "ajax_filter_select")
      {
          $this->Arr_result = $this->recupera_filtro();
      }

      if ($this->NM_ajax_opcao == 'autocomp_p_id_paciente')
      {
          $p_id_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_id_paciente($p_id_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_logro_comunicacion_gestion')
      {
          $g_logro_comunicacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_logro_comunicacion_gestion($g_logro_comunicacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_fecha_comunicacion')
      {
          $g_fecha_comunicacion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_fecha_comunicacion($g_fecha_comunicacion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_autor_gestion')
      {
          $g_autor_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_autor_gestion($g_autor_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_pais_paciente')
      {
          $p_pais_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_pais_paciente($p_pais_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_ciudad_paciente')
      {
          $p_ciudad_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_ciudad_paciente($p_ciudad_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_estado_paciente')
      {
          $p_estado_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_estado_paciente($p_estado_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_fecha_activacion_paciente')
      {
          $p_fecha_activacion_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_fecha_activacion_paciente($p_fecha_activacion_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_fecha_retiro_paciente')
      {
          $p_fecha_retiro_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_fecha_retiro_paciente($p_fecha_retiro_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_motivo_retiro_paciente')
      {
          $p_motivo_retiro_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_motivo_retiro_paciente($p_motivo_retiro_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_consentimiento_tratamiento')
      {
          $t_consentimiento_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_consentimiento_tratamiento($t_consentimiento_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_codigo_xofigo')
      {
          $p_codigo_xofigo = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_codigo_xofigo($p_codigo_xofigo);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_clasificacion_patologica_tratamiento')
      {
          $t_clasificacion_patologica_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_clasificacion_patologica_tratamiento($t_clasificacion_patologica_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_producto_tratamiento')
      {
          $t_producto_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_producto_tratamiento($t_producto_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_nombre_referencia')
      {
          $t_nombre_referencia = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_nombre_referencia($t_nombre_referencia);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_dosis_tratamiento')
      {
          $t_dosis_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_dosis_tratamiento($t_dosis_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_numero_cajas')
      {
          $g_numero_cajas = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_numero_cajas($g_numero_cajas);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_asegurador_tratamiento')
      {
          $t_asegurador_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_asegurador_tratamiento($t_asegurador_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_operador_logistico_tratamiento')
      {
          $t_operador_logistico_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_operador_logistico_tratamiento($t_operador_logistico_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_punto_entrega')
      {
          $t_punto_entrega = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_punto_entrega($t_punto_entrega);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_regimen_tratamiento')
      {
          $t_regimen_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_regimen_tratamiento($t_regimen_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_medico_tratamiento')
      {
          $t_medico_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_medico_tratamiento($t_medico_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_tratamiento_previo')
      {
          $t_tratamiento_previo = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_tratamiento_previo($t_tratamiento_previo);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_reclamo_gestion')
      {
          $g_reclamo_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_reclamo_gestion($g_reclamo_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_fecha_reclamacion_gestion')
      {
          $g_fecha_reclamacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_fecha_reclamacion_gestion($g_fecha_reclamacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_causa_no_reclamacion_gestion')
      {
          $g_causa_no_reclamacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_causa_no_reclamacion_gestion($g_causa_no_reclamacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_g_fecha_programada_gestion')
      {
          $g_fecha_programada_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_g_fecha_programada_gestion($g_fecha_programada_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_p_id_ultima_gestion')
      {
          $p_id_ultima_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_p_id_ultima_gestion($p_id_ultima_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_id_tratamiento')
      {
          $t_id_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_id_tratamiento($t_id_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_t_id_paciente_fk')
      {
          $t_id_paciente_fk = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_t_id_paciente_fk($t_id_paciente_fk);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
   }
   function lookup_ajax_p_id_paciente($p_id_paciente)
   {
      $p_id_paciente = substr($this->Db->qstr($p_id_paciente), 1, -1);
            $p_id_paciente_look = substr($this->Db->qstr($p_id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where   CAST (p.ID_PACIENTE AS TEXT)  like '%" . $p_id_paciente . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where  p.ID_PACIENTE like '%" . $p_id_paciente . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_logro_comunicacion_gestion($g_logro_comunicacion_gestion)
   {
      $g_logro_comunicacion_gestion = substr($this->Db->qstr($g_logro_comunicacion_gestion), 1, -1);
            $g_logro_comunicacion_gestion_look = substr($this->Db->qstr($g_logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.LOGRO_COMUNICACION_GESTION from " . $this->Ini->nm_tabela . " where  g.LOGRO_COMUNICACION_GESTION like '%" . $g_logro_comunicacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_fecha_comunicacion($g_fecha_comunicacion)
   {
      $g_fecha_comunicacion = substr($this->Db->qstr($g_fecha_comunicacion), 1, -1);
            $g_fecha_comunicacion_look = substr($this->Db->qstr($g_fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_COMUNICACION from " . $this->Ini->nm_tabela . " where  g.FECHA_COMUNICACION like '%" . $g_fecha_comunicacion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_autor_gestion($g_autor_gestion)
   {
      $g_autor_gestion = substr($this->Db->qstr($g_autor_gestion), 1, -1);
            $g_autor_gestion_look = substr($this->Db->qstr($g_autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.AUTOR_GESTION from " . $this->Ini->nm_tabela . " where  g.AUTOR_GESTION like '%" . $g_autor_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_pais_paciente($p_pais_paciente)
   {
      $p_pais_paciente = substr($this->Db->qstr($p_pais_paciente), 1, -1);
            $p_pais_paciente_look = substr($this->Db->qstr($p_pais_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.PAIS_PACIENTE from " . $this->Ini->nm_tabela . " where  p.PAIS_PACIENTE like '%" . $p_pais_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_ciudad_paciente($p_ciudad_paciente)
   {
      $p_ciudad_paciente = substr($this->Db->qstr($p_ciudad_paciente), 1, -1);
            $p_ciudad_paciente_look = substr($this->Db->qstr($p_ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.CIUDAD_PACIENTE from " . $this->Ini->nm_tabela . " where  p.CIUDAD_PACIENTE like '%" . $p_ciudad_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_estado_paciente($p_estado_paciente)
   {
      $p_estado_paciente = substr($this->Db->qstr($p_estado_paciente), 1, -1);
            $p_estado_paciente_look = substr($this->Db->qstr($p_estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.ESTADO_PACIENTE from " . $this->Ini->nm_tabela . " where  p.ESTADO_PACIENTE like '%" . $p_estado_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_fecha_activacion_paciente($p_fecha_activacion_paciente)
   {
      $p_fecha_activacion_paciente = substr($this->Db->qstr($p_fecha_activacion_paciente), 1, -1);
            $p_fecha_activacion_paciente_look = substr($this->Db->qstr($p_fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_ACTIVACION_PACIENTE from " . $this->Ini->nm_tabela . " where  p.FECHA_ACTIVACION_PACIENTE like '%" . $p_fecha_activacion_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_fecha_retiro_paciente($p_fecha_retiro_paciente)
   {
      $p_fecha_retiro_paciente = substr($this->Db->qstr($p_fecha_retiro_paciente), 1, -1);
            $p_fecha_retiro_paciente_look = substr($this->Db->qstr($p_fecha_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where  p.FECHA_RETIRO_PACIENTE like '%" . $p_fecha_retiro_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_motivo_retiro_paciente($p_motivo_retiro_paciente)
   {
      $p_motivo_retiro_paciente = substr($this->Db->qstr($p_motivo_retiro_paciente), 1, -1);
            $p_motivo_retiro_paciente_look = substr($this->Db->qstr($p_motivo_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.MOTIVO_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where  p.MOTIVO_RETIRO_PACIENTE like '%" . $p_motivo_retiro_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_consentimiento_tratamiento($t_consentimiento_tratamiento)
   {
      $t_consentimiento_tratamiento = substr($this->Db->qstr($t_consentimiento_tratamiento), 1, -1);
            $t_consentimiento_tratamiento_look = substr($this->Db->qstr($t_consentimiento_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CONSENTIMIENTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.CONSENTIMIENTO_TRATAMIENTO like '%" . $t_consentimiento_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_codigo_xofigo($p_codigo_xofigo)
   {
      $p_codigo_xofigo = substr($this->Db->qstr($p_codigo_xofigo), 1, -1);
            $p_codigo_xofigo_look = substr($this->Db->qstr($p_codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where   CAST (p.CODIGO_XOFIGO AS TEXT)  like '%" . $p_codigo_xofigo . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where  p.CODIGO_XOFIGO like '%" . $p_codigo_xofigo . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_clasificacion_patologica_tratamiento($t_clasificacion_patologica_tratamiento)
   {
      $t_clasificacion_patologica_tratamiento = substr($this->Db->qstr($t_clasificacion_patologica_tratamiento), 1, -1);
            $t_clasificacion_patologica_tratamiento_look = substr($this->Db->qstr($t_clasificacion_patologica_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CLASIFICACION_PATOLOGICA_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.CLASIFICACION_PATOLOGICA_TRATAMIENTO like '%" . $t_clasificacion_patologica_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_producto_tratamiento($t_producto_tratamiento)
   {
      $t_producto_tratamiento = substr($this->Db->qstr($t_producto_tratamiento), 1, -1);
            $t_producto_tratamiento_look = substr($this->Db->qstr($t_producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PRODUCTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.PRODUCTO_TRATAMIENTO like '%" . $t_producto_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_nombre_referencia($t_nombre_referencia)
   {
      $t_nombre_referencia = substr($this->Db->qstr($t_nombre_referencia), 1, -1);
            $t_nombre_referencia_look = substr($this->Db->qstr($t_nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.NOMBRE_REFERENCIA from " . $this->Ini->nm_tabela . " where  t.NOMBRE_REFERENCIA like '%" . $t_nombre_referencia . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_dosis_tratamiento($t_dosis_tratamiento)
   {
      $t_dosis_tratamiento = substr($this->Db->qstr($t_dosis_tratamiento), 1, -1);
            $t_dosis_tratamiento_look = substr($this->Db->qstr($t_dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.DOSIS_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.DOSIS_TRATAMIENTO like '%" . $t_dosis_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_numero_cajas($g_numero_cajas)
   {
      $g_numero_cajas = substr($this->Db->qstr($g_numero_cajas), 1, -1);
            $g_numero_cajas_look = substr($this->Db->qstr($g_numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.NUMERO_CAJAS from " . $this->Ini->nm_tabela . " where  g.NUMERO_CAJAS like '%" . $g_numero_cajas . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_asegurador_tratamiento($t_asegurador_tratamiento)
   {
      $t_asegurador_tratamiento = substr($this->Db->qstr($t_asegurador_tratamiento), 1, -1);
            $t_asegurador_tratamiento_look = substr($this->Db->qstr($t_asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.ASEGURADOR_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.ASEGURADOR_TRATAMIENTO like '%" . $t_asegurador_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_operador_logistico_tratamiento($t_operador_logistico_tratamiento)
   {
      $t_operador_logistico_tratamiento = substr($this->Db->qstr($t_operador_logistico_tratamiento), 1, -1);
            $t_operador_logistico_tratamiento_look = substr($this->Db->qstr($t_operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.OPERADOR_LOGISTICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.OPERADOR_LOGISTICO_TRATAMIENTO like '%" . $t_operador_logistico_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_punto_entrega($t_punto_entrega)
   {
      $t_punto_entrega = substr($this->Db->qstr($t_punto_entrega), 1, -1);
            $t_punto_entrega_look = substr($this->Db->qstr($t_punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PUNTO_ENTREGA from " . $this->Ini->nm_tabela . " where  t.PUNTO_ENTREGA like '%" . $t_punto_entrega . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_regimen_tratamiento($t_regimen_tratamiento)
   {
      $t_regimen_tratamiento = substr($this->Db->qstr($t_regimen_tratamiento), 1, -1);
            $t_regimen_tratamiento_look = substr($this->Db->qstr($t_regimen_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.REGIMEN_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.REGIMEN_TRATAMIENTO like '%" . $t_regimen_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_medico_tratamiento($t_medico_tratamiento)
   {
      $t_medico_tratamiento = substr($this->Db->qstr($t_medico_tratamiento), 1, -1);
            $t_medico_tratamiento_look = substr($this->Db->qstr($t_medico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.MEDICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.MEDICO_TRATAMIENTO like '%" . $t_medico_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_tratamiento_previo($t_tratamiento_previo)
   {
      $t_tratamiento_previo = substr($this->Db->qstr($t_tratamiento_previo), 1, -1);
            $t_tratamiento_previo_look = substr($this->Db->qstr($t_tratamiento_previo), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.TRATAMIENTO_PREVIO from " . $this->Ini->nm_tabela . " where  t.TRATAMIENTO_PREVIO like '%" . $t_tratamiento_previo . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_reclamo_gestion($g_reclamo_gestion)
   {
      $g_reclamo_gestion = substr($this->Db->qstr($g_reclamo_gestion), 1, -1);
            $g_reclamo_gestion_look = substr($this->Db->qstr($g_reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.RECLAMO_GESTION from " . $this->Ini->nm_tabela . " where  g.RECLAMO_GESTION like '%" . $g_reclamo_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_fecha_reclamacion_gestion($g_fecha_reclamacion_gestion)
   {
      $g_fecha_reclamacion_gestion = substr($this->Db->qstr($g_fecha_reclamacion_gestion), 1, -1);
            $g_fecha_reclamacion_gestion_look = substr($this->Db->qstr($g_fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where  g.FECHA_RECLAMACION_GESTION like '%" . $g_fecha_reclamacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_causa_no_reclamacion_gestion($g_causa_no_reclamacion_gestion)
   {
      $g_causa_no_reclamacion_gestion = substr($this->Db->qstr($g_causa_no_reclamacion_gestion), 1, -1);
            $g_causa_no_reclamacion_gestion_look = substr($this->Db->qstr($g_causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.CAUSA_NO_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where  g.CAUSA_NO_RECLAMACION_GESTION like '%" . $g_causa_no_reclamacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_g_fecha_programada_gestion($g_fecha_programada_gestion)
   {
      $g_fecha_programada_gestion = substr($this->Db->qstr($g_fecha_programada_gestion), 1, -1);
            $g_fecha_programada_gestion_look = substr($this->Db->qstr($g_fecha_programada_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_PROGRAMADA_GESTION from " . $this->Ini->nm_tabela . " where  g.FECHA_PROGRAMADA_GESTION like '%" . $g_fecha_programada_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_p_id_ultima_gestion($p_id_ultima_gestion)
   {
      $p_id_ultima_gestion = substr($this->Db->qstr($p_id_ultima_gestion), 1, -1);
            $p_id_ultima_gestion_look = substr($this->Db->qstr($p_id_ultima_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where   CAST (p.ID_ULTIMA_GESTION AS TEXT)  like '%" . $p_id_ultima_gestion . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where  p.ID_ULTIMA_GESTION like '%" . $p_id_ultima_gestion . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_id_tratamiento($t_id_tratamiento)
   {
      $t_id_tratamiento = substr($this->Db->qstr($t_id_tratamiento), 1, -1);
            $t_id_tratamiento_look = substr($this->Db->qstr($t_id_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where   CAST (t.ID_TRATAMIENTO AS TEXT)  like '%" . $t_id_tratamiento . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where  t.ID_TRATAMIENTO like '%" . $t_id_tratamiento . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_t_id_paciente_fk($t_id_paciente_fk)
   {
      $t_id_paciente_fk = substr($this->Db->qstr($t_id_paciente_fk), 1, -1);
            $t_id_paciente_fk_look = substr($this->Db->qstr($t_id_paciente_fk), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where   CAST (t.ID_PACIENTE_FK AS TEXT)  like '%" . $t_id_paciente_fk . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where  t.ID_PACIENTE_FK like '%" . $t_id_paciente_fk . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   

   /**
    * @access  public
    */
   function processa_busca()
   {
      $this->inicializa_vars();
      $this->trata_campos();
      if (!empty($this->Campos_Mens_erro)) 
      {
          scriptcase_error_display($this->Campos_Mens_erro, ""); 
          $this->monta_formulario();
      }
      else
      {
          $this->finaliza_resultado();
      }
   }

   /**
    * @access  public
    */
   function and_or()
   {
      $posWhere = strpos(strtolower($this->comando), "where");
      if (FALSE === $posWhere)
      {
         $this->comando     .= " where ";
         $this->comando_sum .= " and ";
      }
      if ($this->comando_ini == "ini")
      {
          if (FALSE !== $posWhere)
          {
              $this->comando     .= " and ( ";
              $this->comando_sum .= " and ( ";
              $this->comando_fim  = " ) ";
          }
         $this->comando_ini  = "";
      }
      elseif ("or" == $this->NM_operador)
      {
         $this->comando        .= " or ";
         $this->comando_sum    .= " or ";
         $this->comando_filtro .= " or ";
      }
      else
      {
         $this->comando        .= " and ";
         $this->comando_sum    .= " and ";
         $this->comando_filtro .= " and ";
      }
   }

   /**
    * @access  public
    * @param  string  $nome  
    * @param  string  $condicao  
    * @param  mixed  $campo  
    * @param  mixed  $campo2  
    * @param  string  $nome_campo  
    * @param  string  $tp_campo  
    * @global  array  $nmgp_tab_label  
    */
   function monta_condicao($nome, $condicao, $campo, $campo2 = "", $nome_campo="", $tp_campo="")
   {
      global $nmgp_tab_label;
      $condicao   = strtoupper($condicao);
      $nm_aspas   = "'";
      $nm_aspas1  = "'";
      $Nm_numeric = array();
      $nm_esp_postgres = array();
      $nm_ini_lower = "";
      $nm_fim_lower = "";
      $Nm_numeric[] = "p_id_paciente";$Nm_numeric[] = "p_codigo_xofigo";$Nm_numeric[] = "p_id_ultima_gestion";$Nm_numeric[] = "t_id_tratamiento";$Nm_numeric[] = "t_id_paciente_fk";
      $campo_join = strtolower(str_replace(".", "_", $nome));
      if (in_array($campo_join, $Nm_numeric))
      {
          if ($condicao == "EP" || $condicao == "NE")
          {
              return;
          }
         if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['decimal_db'] == ".")
         {
            $nm_aspas  = "";
            $nm_aspas1 = "";
         }
         if ($condicao != "IN")
         {
            if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['decimal_db'] == ".")
            {
               $campo  = str_replace(",", ".", $campo);
               $campo2 = str_replace(",", ".", $campo2);
            }
            if ($campo == "")
            {
               $campo = 0;
            }
            if ($campo2 == "")
            {
               $campo2 = 0;
            }
         }
      }
      if ($campo == "" && $condicao != "NU" && $condicao != "NN" && $condicao != "EP" && $condicao != "NE")
      {
         return;
      }
      else
      {
         $tmp_pos = strpos($campo, "##@@");
         if ($tmp_pos === false)
         {
             $res_lookup = $campo;
         }
         else
         {
             $res_lookup = substr($campo, $tmp_pos + 4);
             $campo = substr($campo, 0, $tmp_pos);
             if ($campo == "" && $condicao != "NU" && $condicao != "NN" && $condicao != "EP" && $condicao != "NE")
             {
                 return;
             }
         }
         $tmp_pos = strpos($this->cmp_formatado[$nome_campo], "##@@");
         if ($tmp_pos !== false)
         {
             $this->cmp_formatado[$nome_campo] = substr($this->cmp_formatado[$nome_campo], $tmp_pos + 4);
         }
         $this->and_or();
         $campo  = substr($this->Db->qstr($campo), 1, -1);
         $campo2 = substr($this->Db->qstr($campo2), 1, -1);
         $nome_sum = "$nome";
         if ($tp_campo == "TIMESTAMP")
         {
             $tp_campo = "DATETIME";
         }
         if (in_array($campo_join, $Nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && ($condicao == "II" || $condicao == "QP" || $condicao == "NP"))
         {
             $nome     = "CAST ($nome AS TEXT)";
             $nome_sum = "CAST ($nome_sum AS TEXT)";
         }
         if (in_array($campo_join, $nm_esp_postgres) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
         {
             $nome     = "CAST ($nome AS TEXT)";
             $nome_sum = "CAST ($nome_sum AS TEXT)";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'YYYY-MM-DD hh24:mi:ss')";
             $nome_sum = "to_char ($nome_sum, 'YYYY-MM-DD hh24:mi:ss')";
         }
         elseif (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'YYYY-MM-DD')";
             $nome_sum = "to_char ($nome_sum, 'YYYY-MM-DD')";
         }
         elseif (substr($tp_campo, 0, 4) == "TIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'hh24:mi:ss')";
             $nome_sum = "to_char ($nome_sum, 'hh24:mi:ss')";
         }
         if (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase) && !$this->Date_part)
         {
             $nome     = "str_replace (convert(char(10),$nome,102), '.', '-') + ' ' + convert(char(8),$nome,20)";
             $nome_sum = "str_replace (convert(char(10),$nome_sum,102), '.', '-') + ' ' + convert(char(8),$nome_sum,20)";
         }
         if ($tp_campo == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) && !$this->Date_part)
         {
             $nome     = "convert(char(10),$nome,121)";
             $nome_sum = "convert(char(10),$nome_sum,121)";
         }
         if ($tp_campo == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) && !$this->Date_part)
         {
             $nome     = "convert(char(19),$nome,121)";
             $nome_sum = "convert(char(19),$nome_sum,121)";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle) && !$this->Date_part)
         {
             $nome     = "TO_DATE(TO_CHAR($nome, 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss')";
             $nome_sum = "TO_DATE(TO_CHAR($nome_sum, 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss')";
             $tp_campo = "DATETIME";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix) && !$this->Date_part)
         {
             $nome     = "EXTEND($nome, YEAR TO FRACTION)";
             $nome_sum = "EXTEND($nome_sum, YEAR TO FRACTION)";
         }
         elseif (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix) && !$this->Date_part)
         {
             $nome     = "EXTEND($nome, YEAR TO DAY)";
             $nome_sum = "EXTEND($nome_sum, YEAR TO DAY)";
         }
         switch ($condicao)
         {
            case "EQ":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " = " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " = " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower. " = " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "II":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " like '" . $campo . "%'";
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " like '" . $campo . "%'";
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " like '" . $campo . "%'";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_strt'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
             case "QP";     // 
             case "NP";     // 
                $concat = " " . $this->NM_operador . " ";
                if ($condicao == "QP")
                {
                    $op_all    = " like ";
                    $lang_like = $this->Ini->Nm_lang['lang_srch_like'];
                }
                else
                {
                    $op_all    = " not like ";
                    $lang_like = $this->Ini->Nm_lang['lang_srch_not_like'];
                }
               $NM_cond    = "";
               $NM_cmd     = "";
               $NM_cmd_sum = "";
               if (substr($tp_campo, 0, 4) == "DATE" && $this->Date_part)
               {
                   if ($this->NM_data_qp['ano'] != "____")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_year'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['ano'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%Y', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%Y', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(year from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(year from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('year' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('year' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(year from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(year from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'YYYY')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'YYYY')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(year, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(year, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "year(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "year(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['mes'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_mnth'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['mes'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%m', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%m', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(month from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(month from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('month' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('month' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(month from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(month from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'MM')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'MM')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(month, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(month, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "month(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "month(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['dia'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_days'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['dia'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%d', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%d', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(day from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(day from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('day' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('day' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(day from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(day from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'DD')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'DD')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(day, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(day, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "day(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "day(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                   }
               }
               if (strpos($tp_campo, "TIME") !== false && $this->Date_part)
               {
                   if ($this->NM_data_qp['hor'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_time'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['hor'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%H', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%H', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(hour from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(hour from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('hour' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('hour' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(hour from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(hour from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'HH24')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'HH24')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(hour, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(hour, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "hour(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "hour(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['min'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_mint'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['min'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%M', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%M', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(minute from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(minute from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('minute' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('minute' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(minute from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(minute from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'MI')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'MI')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(minute, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(minute, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "minute(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "minute(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['seg'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_scnd'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['seg'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%S', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%S', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(second from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(second from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('second' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('second' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(second from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(second from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'SS')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'SS')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(second, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(second, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "second(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "second(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                   }
               }
               if ($this->Date_part)
               {
                   if (!empty($NM_cmd))
                   {
                       $NM_cmd     = " (" . $NM_cmd . ")";
                       $NM_cmd_sum = " (" . $NM_cmd_sum . ")";
                       $this->comando        .= $NM_cmd;
                       $this->comando_sum    .= $NM_cmd_sum;
                       $this->comando_filtro .= $NM_cmd;
                       $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . ": " . $NM_cond . "##*@@";
                   }
               }
               else
               {
                   $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $lang_like . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
               }
            break;
            case "DF":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_diff'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "GT":     // 
               $this->comando        .= " $nome > " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum > " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome > " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_grtr'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "GE":     // 
               $this->comando        .= " $nome >= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum >= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome >= " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_grtr_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "LT":     // 
               $this->comando        .= " $nome < " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum < " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome < " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_less'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "LE":     // 
               $this->comando        .= " $nome <= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum <= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome <= " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_less_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "BW":     // 
               $this->comando        .= " $nome between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $this->comando_filtro .= " $nome between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_betw'] . " " . $this->cmp_formatado[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " " . $this->cmp_formatado[$nome_campo . "_input_2"] . "##*@@";
            break;
            case "IN":     // 
               $nm_sc_valores = explode(",", $campo);
               $cond_str = "";
               $nm_cond  = "";
               if (!empty($nm_sc_valores))
               {
                   foreach ($nm_sc_valores as $nm_sc_valor)
                   {
                      if (in_array($campo_join, $Nm_numeric) && substr_count($nm_sc_valor, ".") > 1)
                      {
                         $nm_sc_valor = str_replace(".", "", $nm_sc_valor);
                      }
                      if ("" != $cond_str)
                      {
                         $cond_str .= ",";
                         $nm_cond  .= " " . $this->Ini->Nm_lang['lang_srch_orr_cond'] . " ";
                      }
                      $cond_str .= $nm_aspas . $nm_sc_valor . $nm_aspas1;
                      $nm_cond  .= $nm_aspas . $nm_sc_valor . $nm_aspas1;
                   }
               }
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $cond_str . ")";
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " in (" . $cond_str . ")";
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $cond_str . ")";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_like'] . " " . $nm_cond . "##*@@";
            break;
            case "NU":     // 
               $this->comando        .= " $nome IS NULL ";
               $this->comando_sum    .= " $nome_sum IS NULL ";
               $this->comando_filtro .= " $nome IS NULL ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_null'] . "##*@@";
            break;
            case "NN":     // 
               $this->comando        .= " $nome IS NOT NULL ";
               $this->comando_sum    .= " $nome_sum IS NOT NULL ";
               $this->comando_filtro .= " $nome IS NOT NULL ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_nnul'] . "##*@@";
            break;
            case "EP":     // 
               $this->comando        .= " $nome = '' ";
               $this->comando_sum    .= " $nome_sum = '' ";
               $this->comando_filtro .= " $nome = '' ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_empty'] . "##*@@";
            break;
            case "NE":     // 
               $this->comando        .= " $nome <> '' ";
               $this->comando_sum    .= " $nome_sum <> '' ";
               $this->comando_filtro .= " $nome <> '' ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_nempty'] . "##*@@";
            break;
         }
      }
   }

   function nm_prep_date(&$val, $tp, $tsql, &$cond, $format_nd, $tp_nd)
   {
       $fill_dt = false;
       if ($tsql == "TIMESTAMP")
       {
           $tsql = "DATETIME";
       }
       $cond = strtoupper($cond);
       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) && $tp != "ND")
       {
           if ($cond == "EP")
           {
               $cond = "NU";
           }
           if ($cond == "NE")
           {
               $cond = "NN";
           }
       }
       if ($cond == "NU" || $cond == "NN" || $cond == "EP" || $cond == "NE")
       {
           $val    = array();
           $val[0] = "";
           return;
       }
       if ($cond != "II" && $cond != "QP" && $cond != "NP")
       {
           $fill_dt = true;
       }
       if ($fill_dt)
       {
           $val[0]['dia'] = (!empty($val[0]['dia']) && strlen($val[0]['dia']) == 1) ? "0" . $val[0]['dia'] : $val[0]['dia'];
           $val[0]['mes'] = (!empty($val[0]['mes']) && strlen($val[0]['mes']) == 1) ? "0" . $val[0]['mes'] : $val[0]['mes'];
           if ($tp == "DH")
           {
               $val[0]['hor'] = (!empty($val[0]['hor']) && strlen($val[0]['hor']) == 1) ? "0" . $val[0]['hor'] : $val[0]['hor'];
               $val[0]['min'] = (!empty($val[0]['min']) && strlen($val[0]['min']) == 1) ? "0" . $val[0]['min'] : $val[0]['min'];
               $val[0]['seg'] = (!empty($val[0]['seg']) && strlen($val[0]['seg']) == 1) ? "0" . $val[0]['seg'] : $val[0]['seg'];
           }
           if ($cond == "BW")
           {
               $val[1]['dia'] = (!empty($val[1]['dia']) && strlen($val[1]['dia']) == 1) ? "0" . $val[1]['dia'] : $val[1]['dia'];
               $val[1]['mes'] = (!empty($val[1]['mes']) && strlen($val[1]['mes']) == 1) ? "0" . $val[1]['mes'] : $val[1]['mes'];
               if ($tp == "DH")
               {
                   $val[1]['hor'] = (!empty($val[1]['hor']) && strlen($val[1]['hor']) == 1) ? "0" . $val[1]['hor'] : $val[1]['hor'];
                   $val[1]['min'] = (!empty($val[1]['min']) && strlen($val[1]['min']) == 1) ? "0" . $val[1]['min'] : $val[1]['min'];
                   $val[1]['seg'] = (!empty($val[1]['seg']) && strlen($val[1]['seg']) == 1) ? "0" . $val[1]['seg'] : $val[1]['seg'];
               }
           }
       }
       if ($cond == "BW")
       {
           $this->NM_data_1 = array();
           $this->NM_data_1['ano'] = (isset($val[0]['ano']) && !empty($val[0]['ano'])) ? $val[0]['ano'] : "____";
           $this->NM_data_1['mes'] = (isset($val[0]['mes']) && !empty($val[0]['mes'])) ? $val[0]['mes'] : "__";
           $this->NM_data_1['dia'] = (isset($val[0]['dia']) && !empty($val[0]['dia'])) ? $val[0]['dia'] : "__";
           $this->NM_data_1['hor'] = (isset($val[0]['hor']) && !empty($val[0]['hor'])) ? $val[0]['hor'] : "__";
           $this->NM_data_1['min'] = (isset($val[0]['min']) && !empty($val[0]['min'])) ? $val[0]['min'] : "__";
           $this->NM_data_1['seg'] = (isset($val[0]['seg']) && !empty($val[0]['seg'])) ? $val[0]['seg'] : "__";
           $this->data_menor($this->NM_data_1);
           $this->NM_data_2 = array();
           $this->NM_data_2['ano'] = (isset($val[1]['ano']) && !empty($val[1]['ano'])) ? $val[1]['ano'] : "____";
           $this->NM_data_2['mes'] = (isset($val[1]['mes']) && !empty($val[1]['mes'])) ? $val[1]['mes'] : "__";
           $this->NM_data_2['dia'] = (isset($val[1]['dia']) && !empty($val[1]['dia'])) ? $val[1]['dia'] : "__";
           $this->NM_data_2['hor'] = (isset($val[1]['hor']) && !empty($val[1]['hor'])) ? $val[1]['hor'] : "__";
           $this->NM_data_2['min'] = (isset($val[1]['min']) && !empty($val[1]['min'])) ? $val[1]['min'] : "__";
           $this->NM_data_2['seg'] = (isset($val[1]['seg']) && !empty($val[1]['seg'])) ? $val[1]['seg'] : "__";
           $this->data_maior($this->NM_data_2);
           $val = array();
           if ($tp == "ND")
           {
               $out_dt1 = $format_nd;
               $out_dt1 = str_replace("yyyy", $this->NM_data_1['ano'], $out_dt1);
               $out_dt1 = str_replace("mm",   $this->NM_data_1['mes'], $out_dt1);
               $out_dt1 = str_replace("dd",   $this->NM_data_1['dia'], $out_dt1);
               $out_dt1 = str_replace("hh",   "", $out_dt1);
               $out_dt1 = str_replace("ii",   "", $out_dt1);
               $out_dt1 = str_replace("ss",   "", $out_dt1);
               $out_dt2 = $format_nd;
               $out_dt2 = str_replace("yyyy", $this->NM_data_2['ano'], $out_dt2);
               $out_dt2 = str_replace("mm",   $this->NM_data_2['mes'], $out_dt2);
               $out_dt2 = str_replace("dd",   $this->NM_data_2['dia'], $out_dt2);
               $out_dt2 = str_replace("hh",   "", $out_dt2);
               $out_dt2 = str_replace("ii",   "", $out_dt2);
               $out_dt2 = str_replace("ss",   "", $out_dt2);
               $val[0] = $out_dt1;
               $val[1] = $out_dt2;
               return;
           }
           if ($tsql == "TIME")
           {
               $val[0] = $this->NM_data_1['hor'] . ":" . $this->NM_data_1['min'] . ":" . $this->NM_data_1['seg'];
               $val[1] = $this->NM_data_2['hor'] . ":" . $this->NM_data_2['min'] . ":" . $this->NM_data_2['seg'];
           }
           elseif (substr($tsql, 0, 4) == "DATE")
           {
               $val[0] = $this->NM_data_1['ano'] . "-" . $this->NM_data_1['mes'] . "-" . $this->NM_data_1['dia'];
               $val[1] = $this->NM_data_2['ano'] . "-" . $this->NM_data_2['mes'] . "-" . $this->NM_data_2['dia'];
               if (strpos($tsql, "TIME") !== false)
               {
                   $val[0] .= " " . $this->NM_data_1['hor'] . ":" . $this->NM_data_1['min'] . ":" . $this->NM_data_1['seg'];
                   $val[1] .= " " . $this->NM_data_2['hor'] . ":" . $this->NM_data_2['min'] . ":" . $this->NM_data_2['seg'];
               }
           }
           return;
       }
       $this->NM_data_qp = array();
       $this->NM_data_qp['ano'] = (isset($val[0]['ano']) && $val[0]['ano'] != "") ? $val[0]['ano'] : "____";
       $this->NM_data_qp['mes'] = (isset($val[0]['mes']) && $val[0]['mes'] != "") ? $val[0]['mes'] : "__";
       $this->NM_data_qp['dia'] = (isset($val[0]['dia']) && $val[0]['dia'] != "") ? $val[0]['dia'] : "__";
       $this->NM_data_qp['hor'] = (isset($val[0]['hor']) && $val[0]['hor'] != "") ? $val[0]['hor'] : "__";
       $this->NM_data_qp['min'] = (isset($val[0]['min']) && $val[0]['min'] != "") ? $val[0]['min'] : "__";
       $this->NM_data_qp['seg'] = (isset($val[0]['seg']) && $val[0]['seg'] != "") ? $val[0]['seg'] : "__";
       if ($tp != "ND" && ($cond == "LE" || $cond == "LT" || $cond == "GE" || $cond == "GT"))
       {
           $count_fill = 0;
           foreach ($this->NM_data_qp as $x => $tx)
           {
               if (substr($tx, 0, 2) != "__")
               {
                   $count_fill++;
               }
           }
           if ($count_fill > 1)
           {
               if ($cond == "LE" || $cond == "GT")
               {
                   $this->data_maior($this->NM_data_qp);
               }
               else
               {
                   $this->data_menor($this->NM_data_qp);
               }
               if ($tsql == "TIME")
               {
                   $val[0] = $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
               }
               elseif (substr($tsql, 0, 4) == "DATE")
               {
                   $val[0] = $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
                   if (strpos($tsql, "TIME") !== false)
                   {
                       $val[0] .= " " . $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
                   }
               }
               return;
           }
       }
       foreach ($this->NM_data_qp as $x => $tx)
       {
           if (substr($tx, 0, 2) == "__" && ($x == "dia" || $x == "mes" || $x == "ano"))
           {
               if (substr($tsql, 0, 4) == "DATE")
               {
                   $this->Date_part = true;
                   break;
               }
           }
           if (substr($tx, 0, 2) == "__" && ($x == "hor" || $x == "min" || $x == "seg"))
           {
               if (strpos($tsql, "TIME") !== false && ($tp == "DH" || ($tp == "DT" && $cond != "LE" && $cond != "LT" && $cond != "GE" && $cond != "GT")))
               {
                   $this->Date_part = true;
                   break;
               }
           }
       }
       if ($this->Date_part)
       {
           $this->Ini_date_part = "";
           $this->End_date_part = "";
           $this->Ini_date_char = "";
           $this->End_date_char = "";
           if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
           {
               $this->Ini_date_part = "'";
               $this->End_date_part = "'";
           }
           if ($tp != "ND")
           {
               if ($cond == "EQ")
               {
                   $this->Operador_date_part = " = ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_equl'];
               }
               elseif ($cond == "II")
               {
                   $this->Operador_date_part = " like ";
                   $this->Ini_date_part = "'";
                   $this->End_date_part = "%'";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_strt'];
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
               elseif ($cond == "DF")
               {
                   $this->Operador_date_part = " <> ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_diff'];
               }
               elseif ($cond == "GT")
               {
                   $this->Operador_date_part = " > ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['pesq_cond_maior'];
               }
               elseif ($cond == "GE")
               {
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_grtr_equl'];
                   $this->Operador_date_part = " >= ";
               }
               elseif ($cond == "LT")
               {
                   $this->Operador_date_part = " < ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_less'];
               }
               elseif ($cond == "LE")
               {
                   $this->Operador_date_part = " <= ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_less_equl'];
               }
               elseif ($cond == "NP")
               {
                   $this->Operador_date_part = " not like ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_diff'];
                   $this->Ini_date_part = "'%";
                   $this->End_date_part = "%'";
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
               else
               {
                   $this->Operador_date_part = " like ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_equl'];
                   $this->Ini_date_part = "'%";
                   $this->End_date_part = "%'";
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
           }
           if ($cond == "DF")
           {
               $cond = "NP";
           }
           if ($cond != "NP")
           {
               $cond = "QP";
           }
       }
       $val = array();
       if ($tp != "ND" && ($cond == "QP" || $cond == "NP"))
       {
           $val[0] = "";
           if (substr($tsql, 0, 4) == "DATE")
           {
               $val[0] .= $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
               if (strpos($tsql, "TIME") !== false)
               {
                   $val[0] .= " ";
               }
           }
           if (strpos($tsql, "TIME") !== false)
           {
               $val[0] .= $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
           }
           return;
       }
       if ($cond == "II" || $cond == "DF" || $cond == "EQ" || $cond == "LT" || $cond == "GE")
       {
           $this->data_menor($this->NM_data_qp);
       }
       else
       {
           $this->data_maior($this->NM_data_qp);
       }
       if ($tsql == "TIME")
       {
           $val[0] = $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
           return;
       }
       $format_sql = "";
       if (substr($tsql, 0, 4) == "DATE")
       {
           $format_sql .= $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
           if (strpos($tsql, "TIME") !== false)
           {
               $format_sql .= " ";
           }
       }
       if (strpos($tsql, "TIME") !== false)
       {
           $format_sql .=  $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
       }
       if ($tp != "ND")
       {
           $val[0] = $format_sql;
           return;
       }
       if ($tp == "ND")
       {
           $format_nd = str_replace("yyyy", $this->NM_data_qp['ano'], $format_nd);
           $format_nd = str_replace("mm",   $this->NM_data_qp['mes'], $format_nd);
           $format_nd = str_replace("dd",   $this->NM_data_qp['dia'], $format_nd);
           $format_nd = str_replace("hh",   $this->NM_data_qp['hor'], $format_nd);
           $format_nd = str_replace("ii",   $this->NM_data_qp['min'], $format_nd);
           $format_nd = str_replace("ss",   $this->NM_data_qp['seg'], $format_nd);
           $val[0] = $format_nd;
           return;
       }
   }
   function data_menor(&$data_arr)
   {
       $data_arr["ano"] = ("____" == $data_arr["ano"]) ? "0001" : $data_arr["ano"];
       $data_arr["mes"] = ("__" == $data_arr["mes"])   ? "01" : $data_arr["mes"];
       $data_arr["dia"] = ("__" == $data_arr["dia"])   ? "01" : $data_arr["dia"];
       $data_arr["hor"] = ("__" == $data_arr["hor"])   ? "00" : $data_arr["hor"];
       $data_arr["min"] = ("__" == $data_arr["min"])   ? "00" : $data_arr["min"];
       $data_arr["seg"] = ("__" == $data_arr["seg"])   ? "00" : $data_arr["seg"];
   }

   function data_maior(&$data_arr)
   {
       $data_arr["ano"] = ("____" == $data_arr["ano"]) ? "9999" : $data_arr["ano"];
       $data_arr["mes"] = ("__" == $data_arr["mes"])   ? "12" : $data_arr["mes"];
       $data_arr["hor"] = ("__" == $data_arr["hor"])   ? "23" : $data_arr["hor"];
       $data_arr["min"] = ("__" == $data_arr["min"])   ? "59" : $data_arr["min"];
       $data_arr["seg"] = ("__" == $data_arr["seg"])   ? "59" : $data_arr["seg"];
       if ("__" == $data_arr["dia"])
       {
           $data_arr["dia"] = "31";
           if ($data_arr["mes"] == "04" || $data_arr["mes"] == "06" || $data_arr["mes"] == "09" || $data_arr["mes"] == "11")
           {
               $data_arr["dia"] = 30;
           }
           elseif ($data_arr["mes"] == "02")
           { 
                if  ($data_arr["ano"] % 4 == 0)
                {
                     $data_arr["dia"] = 29;
                }
                else 
                {
                     $data_arr["dia"] = 28;
                }
           }
       }
   }

   /**
    * @access  public
    * @param  string  $nm_data_hora  
    */
   function limpa_dt_hor_pesq(&$nm_data_hora)
   {
      $nm_data_hora = str_replace("Y", "", $nm_data_hora); 
      $nm_data_hora = str_replace("M", "", $nm_data_hora); 
      $nm_data_hora = str_replace("D", "", $nm_data_hora); 
      $nm_data_hora = str_replace("H", "", $nm_data_hora); 
      $nm_data_hora = str_replace("I", "", $nm_data_hora); 
      $nm_data_hora = str_replace("S", "", $nm_data_hora); 
      $tmp_pos = strpos($nm_data_hora, "--");
      if ($tmp_pos !== FALSE)
      {
          $nm_data_hora = str_replace("--", "-", $nm_data_hora); 
      }
      $tmp_pos = strpos($nm_data_hora, "::");
      if ($tmp_pos !== FALSE)
      {
          $nm_data_hora = str_replace("::", ":", $nm_data_hora); 
      }
   }

   /**
    * @access  public
    */
   function retorna_pesq()
   {
      global $nm_apl_dependente;
   $NM_retorno = "./";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
 <TITLE>Informe Reclamaciones</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
</HEAD>
<BODY class="scGridPage">
<FORM style="display:none;" name="form_ok" method="POST" action="<?php echo $NM_retorno; ?>" target="_self">
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="pesq"> 
</FORM>
<SCRIPT type="text/javascript">
 document.form_ok.submit();
</SCRIPT>
</BODY>
</HTML>
<?php
}

   /**
    * @access  public
    */
   function monta_html_ini()
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
 <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox.css" type="text/css" media="screen" />
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_filter.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_filter<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_error.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_error<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Str_btn_filter_css ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_form.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_form<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->path_link ?>informe_reclamacion/informe_reclamacion_fil_<?php echo strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) ?>.css" />
</HEAD>
<BODY class="scFilterPage">
<?php echo $this->Ini->Ajax_result_set ?>
<SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_js . "/browserSniffer.js" ?>"></SCRIPT>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery/js/jquery.js"></script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/jquery/js/jquery-ui.js"></script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/malsup-blockui/jquery.blockUI.js"></script>
 <script type="text/javascript" src="../_lib/lib/js/jquery.scInput.js"></script>
 <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery/css/smoothness/jquery-ui.css" type="text/css" media="screen" />
 <script type="text/javascript">var sc_pathToTB = '<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/';</script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox-compressed.js"></script>
 <script type="text/javascript" src="informe_reclamacion_ajax_search.js"></script>
 <script type="text/javascript" src="informe_reclamacion_ajax.js"></script>
 <script type="text/javascript">
   var sc_ajaxBg = '<?php echo $this->Ini->Color_bg_ajax ?>';
   var sc_ajaxBordC = '<?php echo $this->Ini->Border_c_ajax ?>';
   var sc_ajaxBordS = '<?php echo $this->Ini->Border_s_ajax ?>';
   var sc_ajaxBordW = '<?php echo $this->Ini->Border_w_ajax ?>';
 </script>
<?php
$Cod_Btn = nmButtonOutput($this->arr_buttons, "berrm_clse", "nmAjaxHideDebug()", "nmAjaxHideDebug()", "", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<div id="id_debug_window" style="display: none; position: absolute; left: 50px; top: 50px"><table class="scFormMessageTable">
<tr><td class="scFormMessageTitle"><?php echo $Cod_Btn ?>&nbsp;&nbsp;Output</td></tr>
<tr><td class="scFormMessageMessage" style="padding: 0px; vertical-align: top"><div style="padding: 2px; height: 200px; width: 350px; overflow: auto" id="id_debug_text"></div></td></tr>
</table></div>
 <SCRIPT type="text/javascript">

<?php
if (is_file($this->Ini->root . $this->Ini->path_link . "_lib/js/tab_erro_" . $this->Ini->str_lang . ".js"))
{
    $Tb_err_js = file($this->Ini->root . $this->Ini->path_link . "_lib/js/tab_erro_" . $this->Ini->str_lang . ".js");
    foreach ($Tb_err_js as $Lines)
    {
        if (NM_is_utf8($Lines) && $_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Lines = sc_convert_encoding($Lines, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        echo $Lines;
    }
}
 if (NM_is_utf8($Lines) && $_SESSION['scriptcase']['charset'] != "UTF-8")
 {
    $Msg_Inval = sc_convert_encoding("Inv�lido", $_SESSION['scriptcase']['charset'], "UTF-8");
 }
?>
var SC_crit_inv = "<?php echo $Msg_Inval ?>";
var nmdg_Form = "F1";

 $(function() {

   SC_carga_evt_jquery();
   $('input:text.sc-js-input').listen();
 });
var NM_index = 0;
var NM_hidden = new Array();
var NM_IE = (navigator.userAgent.indexOf('MSIE') > -1) ? 1 : 0;
function NM_hitTest(o, l)
{
    function getOffset(o){
        for(var r = {l: o.offsetLeft, t: o.offsetTop, r: o.offsetWidth, b: o.offsetHeight};
            o = o.offsetParent; r.l += o.offsetLeft, r.t += o.offsetTop);
        return r.r += r.l, r.b += r.t, r;
    }
    for(var b, s, r = [], a = getOffset(o), j = isNaN(l.length), i = (j ? l = [l] : l).length; i;
        b = getOffset(l[--i]), (a.l == b.l || (a.l > b.l ? a.l <= b.r : b.l <= a.r))
        && (a.t == b.t || (a.t > b.t ? a.t <= b.b : b.t <= a.b)) && (r[r.length] = l[i]));
    return j ? !!r.length : r;
}
var tem_obj = false;
function NM_show_menu(nn)
{
    if (!NM_IE)
    {
         return;
    }
    x = document.getElementById(nn);
    x.style.display = "block";
    obj_sel = document.body;
    tem_obj = true;
    x.ieFix = NM_hitTest(x, obj_sel.getElementsByTagName("select"));
    for (i = 0; i <  x.ieFix.length; i++)
    {
      if (x.ieFix[i].style.visibility != "hidden")
      {
          x.ieFix[i].style.visibility = "hidden";
          NM_hidden[NM_index] = x.ieFix[i];
          NM_index++;
      }
    }
}
function NM_hide_menu()
{
    if (!NM_IE)
    {
         return;
    }
    obj_del = document.body;
    if (tem_obj && obj_del == obj_sel)
    {
        for(var i = NM_hidden.length; i; NM_hidden[--i].style.visibility = "visible");
    }
    NM_index = 0;
    NM_hidden = new Array();
}
 function nm_campos_between(nm_campo, nm_cond, nm_nome_obj)
 {
  if (nm_cond.value == "bw")
  {
   nm_campo.style.display = "";
  }
  else
  {
    if (nm_campo)
    {
      nm_campo.style.display = "none";
    }
  }
  if (document.getElementById('id_hide_' + nm_nome_obj))
  {
      if (nm_cond.value == "nu" || nm_cond.value == "nn" || nm_cond.value == "ep" || nm_cond.value == "ne")
      {
          document.getElementById('id_hide_' + nm_nome_obj).style.display = 'none';
      }
      else
      {
          document.getElementById('id_hide_' + nm_nome_obj).style.display = '';
      }
  }
 }
 function nm_save_form(pos)
 {
  if (pos == 'top' && document.F1.nmgp_save_name_top.value == '')
  {
      return;
  }
  if (pos == 'bot' && document.F1.nmgp_save_name_bot.value == '')
  {
      return;
  }
  var str_out = "";
  str_out += 'SC_p_id_paciente_cond#NMF#' + search_get_sel_txt('SC_p_id_paciente_cond') + '@NMF@';
  str_out += 'SC_p_id_paciente#NMF#' + search_get_text('SC_p_id_paciente') + '@NMF@';
  str_out += 'id_ac_p_id_paciente#NMF#' + search_get_text('id_ac_p_id_paciente') + '@NMF@';
  str_out += 'SC_p_id_paciente_input_2#NMF#' + search_get_text('SC_p_id_paciente_input_2') + '@NMF@';
  str_out += 'SC_g_logro_comunicacion_gestion_cond#NMF#' + search_get_sel_txt('SC_g_logro_comunicacion_gestion_cond') + '@NMF@';
  str_out += 'SC_g_logro_comunicacion_gestion#NMF#' + search_get_text('SC_g_logro_comunicacion_gestion') + '@NMF@';
  str_out += 'id_ac_g_logro_comunicacion_gestion#NMF#' + search_get_text('id_ac_g_logro_comunicacion_gestion') + '@NMF@';
  str_out += 'SC_g_fecha_comunicacion_cond#NMF#' + search_get_sel_txt('SC_g_fecha_comunicacion_cond') + '@NMF@';
  str_out += 'SC_g_fecha_comunicacion#NMF#' + search_get_text('SC_g_fecha_comunicacion') + '@NMF@';
  str_out += 'id_ac_g_fecha_comunicacion#NMF#' + search_get_text('id_ac_g_fecha_comunicacion') + '@NMF@';
  str_out += 'SC_g_autor_gestion_cond#NMF#' + search_get_sel_txt('SC_g_autor_gestion_cond') + '@NMF@';
  str_out += 'SC_g_autor_gestion#NMF#' + search_get_text('SC_g_autor_gestion') + '@NMF@';
  str_out += 'id_ac_g_autor_gestion#NMF#' + search_get_text('id_ac_g_autor_gestion') + '@NMF@';
  str_out += 'SC_p_pais_paciente_cond#NMF#' + search_get_sel_txt('SC_p_pais_paciente_cond') + '@NMF@';
  str_out += 'SC_p_pais_paciente#NMF#' + search_get_text('SC_p_pais_paciente') + '@NMF@';
  str_out += 'id_ac_p_pais_paciente#NMF#' + search_get_text('id_ac_p_pais_paciente') + '@NMF@';
  str_out += 'SC_p_ciudad_paciente_cond#NMF#' + search_get_sel_txt('SC_p_ciudad_paciente_cond') + '@NMF@';
  str_out += 'SC_p_ciudad_paciente#NMF#' + search_get_text('SC_p_ciudad_paciente') + '@NMF@';
  str_out += 'id_ac_p_ciudad_paciente#NMF#' + search_get_text('id_ac_p_ciudad_paciente') + '@NMF@';
  str_out += 'SC_p_estado_paciente_cond#NMF#' + search_get_sel_txt('SC_p_estado_paciente_cond') + '@NMF@';
  str_out += 'SC_p_estado_paciente#NMF#' + search_get_text('SC_p_estado_paciente') + '@NMF@';
  str_out += 'id_ac_p_estado_paciente#NMF#' + search_get_text('id_ac_p_estado_paciente') + '@NMF@';
  str_out += 'SC_p_fecha_activacion_paciente_cond#NMF#' + search_get_sel_txt('SC_p_fecha_activacion_paciente_cond') + '@NMF@';
  str_out += 'SC_p_fecha_activacion_paciente#NMF#' + search_get_text('SC_p_fecha_activacion_paciente') + '@NMF@';
  str_out += 'id_ac_p_fecha_activacion_paciente#NMF#' + search_get_text('id_ac_p_fecha_activacion_paciente') + '@NMF@';
  str_out += 'SC_p_fecha_retiro_paciente_cond#NMF#' + search_get_sel_txt('SC_p_fecha_retiro_paciente_cond') + '@NMF@';
  str_out += 'SC_p_fecha_retiro_paciente#NMF#' + search_get_text('SC_p_fecha_retiro_paciente') + '@NMF@';
  str_out += 'id_ac_p_fecha_retiro_paciente#NMF#' + search_get_text('id_ac_p_fecha_retiro_paciente') + '@NMF@';
  str_out += 'SC_p_motivo_retiro_paciente_cond#NMF#' + search_get_sel_txt('SC_p_motivo_retiro_paciente_cond') + '@NMF@';
  str_out += 'SC_p_motivo_retiro_paciente#NMF#' + search_get_text('SC_p_motivo_retiro_paciente') + '@NMF@';
  str_out += 'id_ac_p_motivo_retiro_paciente#NMF#' + search_get_text('id_ac_p_motivo_retiro_paciente') + '@NMF@';
  str_out += 'SC_t_consentimiento_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_consentimiento_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_consentimiento_tratamiento#NMF#' + search_get_text('SC_t_consentimiento_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_consentimiento_tratamiento#NMF#' + search_get_text('id_ac_t_consentimiento_tratamiento') + '@NMF@';
  str_out += 'SC_p_codigo_xofigo_cond#NMF#' + search_get_sel_txt('SC_p_codigo_xofigo_cond') + '@NMF@';
  str_out += 'SC_p_codigo_xofigo#NMF#' + search_get_text('SC_p_codigo_xofigo') + '@NMF@';
  str_out += 'id_ac_p_codigo_xofigo#NMF#' + search_get_text('id_ac_p_codigo_xofigo') + '@NMF@';
  str_out += 'SC_p_codigo_xofigo_input_2#NMF#' + search_get_text('SC_p_codigo_xofigo_input_2') + '@NMF@';
  str_out += 'SC_t_clasificacion_patologica_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_clasificacion_patologica_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_clasificacion_patologica_tratamiento#NMF#' + search_get_text('SC_t_clasificacion_patologica_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_clasificacion_patologica_tratamiento#NMF#' + search_get_text('id_ac_t_clasificacion_patologica_tratamiento') + '@NMF@';
  str_out += 'SC_t_producto_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_producto_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_producto_tratamiento#NMF#' + search_get_text('SC_t_producto_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_producto_tratamiento#NMF#' + search_get_text('id_ac_t_producto_tratamiento') + '@NMF@';
  str_out += 'SC_t_nombre_referencia_cond#NMF#' + search_get_sel_txt('SC_t_nombre_referencia_cond') + '@NMF@';
  str_out += 'SC_t_nombre_referencia#NMF#' + search_get_text('SC_t_nombre_referencia') + '@NMF@';
  str_out += 'id_ac_t_nombre_referencia#NMF#' + search_get_text('id_ac_t_nombre_referencia') + '@NMF@';
  str_out += 'SC_t_dosis_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_dosis_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_dosis_tratamiento#NMF#' + search_get_text('SC_t_dosis_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_dosis_tratamiento#NMF#' + search_get_text('id_ac_t_dosis_tratamiento') + '@NMF@';
  str_out += 'SC_g_numero_cajas_cond#NMF#' + search_get_sel_txt('SC_g_numero_cajas_cond') + '@NMF@';
  str_out += 'SC_g_numero_cajas#NMF#' + search_get_text('SC_g_numero_cajas') + '@NMF@';
  str_out += 'id_ac_g_numero_cajas#NMF#' + search_get_text('id_ac_g_numero_cajas') + '@NMF@';
  str_out += 'SC_t_asegurador_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_asegurador_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_asegurador_tratamiento#NMF#' + search_get_text('SC_t_asegurador_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_asegurador_tratamiento#NMF#' + search_get_text('id_ac_t_asegurador_tratamiento') + '@NMF@';
  str_out += 'SC_t_operador_logistico_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_operador_logistico_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_operador_logistico_tratamiento#NMF#' + search_get_text('SC_t_operador_logistico_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_operador_logistico_tratamiento#NMF#' + search_get_text('id_ac_t_operador_logistico_tratamiento') + '@NMF@';
  str_out += 'SC_t_punto_entrega_cond#NMF#' + search_get_sel_txt('SC_t_punto_entrega_cond') + '@NMF@';
  str_out += 'SC_t_punto_entrega#NMF#' + search_get_text('SC_t_punto_entrega') + '@NMF@';
  str_out += 'id_ac_t_punto_entrega#NMF#' + search_get_text('id_ac_t_punto_entrega') + '@NMF@';
  str_out += 'SC_t_regimen_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_regimen_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_regimen_tratamiento#NMF#' + search_get_text('SC_t_regimen_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_regimen_tratamiento#NMF#' + search_get_text('id_ac_t_regimen_tratamiento') + '@NMF@';
  str_out += 'SC_t_medico_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_medico_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_medico_tratamiento#NMF#' + search_get_text('SC_t_medico_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_medico_tratamiento#NMF#' + search_get_text('id_ac_t_medico_tratamiento') + '@NMF@';
  str_out += 'SC_t_tratamiento_previo_cond#NMF#' + search_get_sel_txt('SC_t_tratamiento_previo_cond') + '@NMF@';
  str_out += 'SC_t_tratamiento_previo#NMF#' + search_get_text('SC_t_tratamiento_previo') + '@NMF@';
  str_out += 'id_ac_t_tratamiento_previo#NMF#' + search_get_text('id_ac_t_tratamiento_previo') + '@NMF@';
  str_out += 'SC_g_reclamo_gestion_cond#NMF#' + search_get_sel_txt('SC_g_reclamo_gestion_cond') + '@NMF@';
  str_out += 'SC_g_reclamo_gestion#NMF#' + search_get_text('SC_g_reclamo_gestion') + '@NMF@';
  str_out += 'id_ac_g_reclamo_gestion#NMF#' + search_get_text('id_ac_g_reclamo_gestion') + '@NMF@';
  str_out += 'SC_g_fecha_reclamacion_gestion_cond#NMF#' + search_get_sel_txt('SC_g_fecha_reclamacion_gestion_cond') + '@NMF@';
  str_out += 'SC_g_fecha_reclamacion_gestion#NMF#' + search_get_text('SC_g_fecha_reclamacion_gestion') + '@NMF@';
  str_out += 'id_ac_g_fecha_reclamacion_gestion#NMF#' + search_get_text('id_ac_g_fecha_reclamacion_gestion') + '@NMF@';
  str_out += 'SC_g_causa_no_reclamacion_gestion_cond#NMF#' + search_get_sel_txt('SC_g_causa_no_reclamacion_gestion_cond') + '@NMF@';
  str_out += 'SC_g_causa_no_reclamacion_gestion#NMF#' + search_get_text('SC_g_causa_no_reclamacion_gestion') + '@NMF@';
  str_out += 'id_ac_g_causa_no_reclamacion_gestion#NMF#' + search_get_text('id_ac_g_causa_no_reclamacion_gestion') + '@NMF@';
  str_out += 'SC_g_descripcion_comunicacion_gestion_cond#NMF#' + search_get_sel_txt('SC_g_descripcion_comunicacion_gestion_cond') + '@NMF@';
  str_out += 'SC_g_descripcion_comunicacion_gestion#NMF#' + search_get_text('SC_g_descripcion_comunicacion_gestion') + '@NMF@';
  str_out += 'SC_g_fecha_programada_gestion_cond#NMF#' + search_get_sel_txt('SC_g_fecha_programada_gestion_cond') + '@NMF@';
  str_out += 'SC_g_fecha_programada_gestion#NMF#' + search_get_text('SC_g_fecha_programada_gestion') + '@NMF@';
  str_out += 'id_ac_g_fecha_programada_gestion#NMF#' + search_get_text('id_ac_g_fecha_programada_gestion') + '@NMF@';
  str_out += 'SC_p_id_ultima_gestion_cond#NMF#' + search_get_sel_txt('SC_p_id_ultima_gestion_cond') + '@NMF@';
  str_out += 'SC_p_id_ultima_gestion#NMF#' + search_get_text('SC_p_id_ultima_gestion') + '@NMF@';
  str_out += 'id_ac_p_id_ultima_gestion#NMF#' + search_get_text('id_ac_p_id_ultima_gestion') + '@NMF@';
  str_out += 'SC_p_id_ultima_gestion_input_2#NMF#' + search_get_text('SC_p_id_ultima_gestion_input_2') + '@NMF@';
  str_out += 'SC_t_id_tratamiento_cond#NMF#' + search_get_sel_txt('SC_t_id_tratamiento_cond') + '@NMF@';
  str_out += 'SC_t_id_tratamiento#NMF#' + search_get_text('SC_t_id_tratamiento') + '@NMF@';
  str_out += 'id_ac_t_id_tratamiento#NMF#' + search_get_text('id_ac_t_id_tratamiento') + '@NMF@';
  str_out += 'SC_t_id_tratamiento_input_2#NMF#' + search_get_text('SC_t_id_tratamiento_input_2') + '@NMF@';
  str_out += 'SC_t_id_paciente_fk_cond#NMF#' + search_get_sel_txt('SC_t_id_paciente_fk_cond') + '@NMF@';
  str_out += 'SC_t_id_paciente_fk#NMF#' + search_get_text('SC_t_id_paciente_fk') + '@NMF@';
  str_out += 'id_ac_t_id_paciente_fk#NMF#' + search_get_text('id_ac_t_id_paciente_fk') + '@NMF@';
  str_out += 'SC_t_id_paciente_fk_input_2#NMF#' + search_get_text('SC_t_id_paciente_fk_input_2') + '@NMF@';
  str_out += 'NM_operador#NMF#' + search_get_text('SC_NM_operador');
  str_out  = str_out.replace(/[+]/g, "__NM_PLUS__");
  var save_name = search_get_text('SC_nmgp_save_name_' + pos);
  var save_opt  = search_get_sel_txt('SC_nmgp_save_option_' + pos);
  ajax_save_filter(save_name, save_opt, str_out, pos);
 }
 function nm_submit_filter(obj_sel, pos)
 {
  index   = obj_sel.selectedIndex;
  if (obj_sel.options[index].value == "") 
  {
      return false;
  }
  ajax_select_filter(obj_sel.options[index].value);
 }
 function nm_submit_filter_del(pos)
 {
  if (pos == 'top')
  {
      obj_sel = document.F1.elements['NM_filters_del_top'];
  }
  if (pos == 'bot')
  {
      obj_sel = document.F1.elements['NM_filters_del_bot'];
  }
  index   = obj_sel.selectedIndex;
  if (index == -1 || obj_sel.options[index].value == "") 
  {
      return false;
  }
  parm = obj_sel.options[index].value;
  ajax_delete_filter(parm);
 }
 function search_get_select(obj_id)
 {
    var index = document.getElementById(obj_id).selectedIndex;
    if (index != -1) {
        return document.getElementById(obj_id).options[index].value;
    }
    else {
        return '';
    }
 }
 function search_get_selmult(obj_id)
 {
    var obj = document.getElementById(obj_id);
    var val = "_NM_array_";
    for (iSelect = 0; iSelect < obj.length; iSelect++)
    {
        if (obj[iSelect].selected)
        {
            val += "#NMARR#" + obj[iSelect].value;
        }
    }
    return val;
 }
 function search_get_Dselelect(obj_id)
 {
    var obj = document.getElementById(obj_id);
    var val = "_NM_array_";
    for (iSelect = 0; iSelect < obj.length; iSelect++)
    {
         val += "#NMARR#" + obj[iSelect].value;
    }
    return val;
 }
 function search_get_radio(obj_id)
 {
    var val  = "";
    if (document.getElementById(obj_id)) {
       var Nobj = document.getElementById(obj_id).name;
       var obj  = document.getElementsByName(Nobj);
       for (iRadio = 0; iRadio < obj.length; iRadio++) {
           if (obj[iRadio].checked) {
               val = obj[iRadio].value;
           }
       }
    }
    return val;
 }
 function search_get_checkbox(obj_id)
 {
    var val  = "_NM_array_";
    if (document.getElementById(obj_id)) {
       var Nobj = document.getElementById(obj_id).name;
       var obj  = document.getElementsByName(Nobj);
       if (!obj.length) {
           if (obj.checked) {
               val += "#NMARR#" + obj.value;
           }
       }
       else {
           for (iCheck = 0; iCheck < obj.length; iCheck++) {
               if (obj[iCheck].checked) {
                   val += "#NMARR#" + obj[iCheck].value;
               }
           }
       }
    }
    return val;
 }
 function search_get_text(obj_id)
 {
    var obj = document.getElementById(obj_id);
    return (obj) ? obj.value : '';
 }
 function search_get_sel_txt(obj_id)
 {
    var val = "";
    obj_part  = document.getElementById(obj_id);
    if (obj_part && obj_part.type.substr(0, 6) == 'select')
    {
        val = search_get_select(obj_id);
    }
    else
    {
        val = (obj_part) ? obj_part.value : '';
    }
    return val;
 }
 function search_get_html(obj_id)
 {
    var obj = document.getElementById(obj_id);
    return obj.innerHTML;
 }
function nm_open_popup(parms)
{
    NovaJanela = window.open (parms, '', 'resizable, scrollbars');
}
 </SCRIPT>
<script type="text/javascript">
 $(function() {
   $("#id_ac_p_id_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_id_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_id_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_id_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_logro_comunicacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_logro_comunicacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_logro_comunicacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_logro_comunicacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_fecha_comunicacion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_fecha_comunicacion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_fecha_comunicacion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_fecha_comunicacion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_autor_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_autor_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_autor_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_autor_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_pais_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_pais_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_pais_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_pais_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_ciudad_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_ciudad_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_ciudad_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_ciudad_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_estado_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_estado_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_estado_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_estado_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_fecha_activacion_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_fecha_activacion_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_fecha_activacion_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_fecha_activacion_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_fecha_retiro_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_fecha_retiro_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_fecha_retiro_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_fecha_retiro_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_motivo_retiro_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_motivo_retiro_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_motivo_retiro_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_motivo_retiro_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_consentimiento_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_consentimiento_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_consentimiento_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_consentimiento_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_codigo_xofigo").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_codigo_xofigo",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_codigo_xofigo").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_codigo_xofigo").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_clasificacion_patologica_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_clasificacion_patologica_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_clasificacion_patologica_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_clasificacion_patologica_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_producto_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_producto_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_producto_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_producto_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_nombre_referencia").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_nombre_referencia",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_nombre_referencia").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_nombre_referencia").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_dosis_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_dosis_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_dosis_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_dosis_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_numero_cajas").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_numero_cajas",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_numero_cajas").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_numero_cajas").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_asegurador_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_asegurador_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_asegurador_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_asegurador_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_operador_logistico_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_operador_logistico_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_operador_logistico_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_operador_logistico_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_punto_entrega").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_punto_entrega",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_punto_entrega").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_punto_entrega").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_regimen_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_regimen_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_regimen_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_regimen_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_medico_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_medico_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_medico_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_medico_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_tratamiento_previo").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_tratamiento_previo",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_tratamiento_previo").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_tratamiento_previo").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_reclamo_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_reclamo_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_reclamo_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_reclamo_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_fecha_reclamacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_fecha_reclamacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_fecha_reclamacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_fecha_reclamacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_causa_no_reclamacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_causa_no_reclamacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_causa_no_reclamacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_causa_no_reclamacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_g_fecha_programada_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_g_fecha_programada_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_g_fecha_programada_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_g_fecha_programada_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_p_id_ultima_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_p_id_ultima_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_p_id_ultima_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_p_id_ultima_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_id_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_id_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_id_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_id_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_t_id_paciente_fk").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_t_id_paciente_fk",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_t_id_paciente_fk").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_t_id_paciente_fk").val( $(this).val() );
       }
     }
   });
 });
</script>
 <FORM name="F1" action="./" method="post" target="_self"> 
 <INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
 <INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
 <INPUT type="hidden" name="nmgp_opcao" value="busca"> 
 <div id="idJSSpecChar" style="display:none;"></div>
 <div id="id_div_process" style="display: none; position: absolute"><table class="scFilterTable"><tr><td class="scFilterLabelOdd"><?php echo $this->Ini->Nm_lang['lang_othr_prcs']; ?>...</td></tr></table></div>
 <div id="id_fatal_error" class="scFilterFieldOdd" style="display:none; position: absolute"></div>
<TABLE id="main_table" align="center" valign="top" >
<tr>
<td>
<div class="scFilterBorder">
  <div id="id_div_process_block" style="display: none; margin: 10px; whitespace: nowrap"><span class="scFormProcess"><img border="0" src="<?php echo $this->Ini->path_icones ?>/scriptcase__NM__ajax_load.gif" align="absmiddle" />&nbsp;<?php echo $this->Ini->Nm_lang['lang_othr_prcs'] ?>...</span></div>
<table cellspacing=0 cellpadding=0 width='100%'>
<?php
   }

   /**
    * @access  public
    * @global  string  $bprocessa  
    */
   /**
    * @access  public
    */
   function monta_cabecalho()
   {
      $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
      $Lim   = strlen($Str_date);
      $Ult   = "";
      $Arr_D = array();
      for ($I = 0; $I < $Lim; $I++)
      {
          $Char = substr($Str_date, $I, 1);
          if ($Char != $Ult)
          {
              $Arr_D[] = $Char;
          }
          $Ult = $Char;
      }
      $Prim = true;
      $Str  = "";
      foreach ($Arr_D as $Cada_d)
      {
          $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
          $Str .= $Cada_d;
          $Prim = false;
      }
      $Str = str_replace("a", "Y", $Str);
      $Str = str_replace("y", "Y", $Str);
      $nm_data_fixa = date($Str); 
?>
 <TR align="center">
  <TD class="scFilterTableTd">
<style>
#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 
#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}
</style>

<div style="width: 100%">
 <div class="scFilterHeader" style="height:11px; display: block; border-width:0px; "></div>
 <div style="height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block">
 	<table style="width:100%; border-collapse:collapse; padding:0;">
    	<tr>
        	<td id="lin1_col1" class="scFilterHeaderFont"><span>Informe Reclamaciones</span></td>
            <td id="lin1_col2" class="scFilterHeaderFont"><span><?php echo $nm_data_fixa; ?></span></td>
        </tr>
    </table>		 
 </div>
</div>
  </TD>
 </TR>
<?php
   }

   /**
    * @access  public
    * @global  string  $nm_url_saida  $this->Ini->Nm_lang['pesq_global_nm_url_saida']
    * @global  integer  $nm_apl_dependente  $this->Ini->Nm_lang['pesq_global_nm_apl_dependente']
    * @global  string  $nmgp_parms  
    * @global  string  $bprocessa  $this->Ini->Nm_lang['pesq_global_bprocessa']
    */
   function monta_form()
   {
      global 
             $p_id_paciente_cond, $p_id_paciente, $p_id_paciente_input_2, $p_id_paciente_autocomp,
             $g_logro_comunicacion_gestion_cond, $g_logro_comunicacion_gestion, $g_logro_comunicacion_gestion_autocomp,
             $g_fecha_comunicacion_cond, $g_fecha_comunicacion, $g_fecha_comunicacion_autocomp,
             $g_autor_gestion_cond, $g_autor_gestion, $g_autor_gestion_autocomp,
             $p_pais_paciente_cond, $p_pais_paciente, $p_pais_paciente_autocomp,
             $p_ciudad_paciente_cond, $p_ciudad_paciente, $p_ciudad_paciente_autocomp,
             $p_estado_paciente_cond, $p_estado_paciente, $p_estado_paciente_autocomp,
             $p_fecha_activacion_paciente_cond, $p_fecha_activacion_paciente, $p_fecha_activacion_paciente_autocomp,
             $p_fecha_retiro_paciente_cond, $p_fecha_retiro_paciente, $p_fecha_retiro_paciente_autocomp,
             $p_motivo_retiro_paciente_cond, $p_motivo_retiro_paciente, $p_motivo_retiro_paciente_autocomp,
             $t_consentimiento_tratamiento_cond, $t_consentimiento_tratamiento, $t_consentimiento_tratamiento_autocomp,
             $p_codigo_xofigo_cond, $p_codigo_xofigo, $p_codigo_xofigo_input_2, $p_codigo_xofigo_autocomp,
             $t_clasificacion_patologica_tratamiento_cond, $t_clasificacion_patologica_tratamiento, $t_clasificacion_patologica_tratamiento_autocomp,
             $t_producto_tratamiento_cond, $t_producto_tratamiento, $t_producto_tratamiento_autocomp,
             $t_nombre_referencia_cond, $t_nombre_referencia, $t_nombre_referencia_autocomp,
             $t_dosis_tratamiento_cond, $t_dosis_tratamiento, $t_dosis_tratamiento_autocomp,
             $g_numero_cajas_cond, $g_numero_cajas, $g_numero_cajas_autocomp,
             $t_asegurador_tratamiento_cond, $t_asegurador_tratamiento, $t_asegurador_tratamiento_autocomp,
             $t_operador_logistico_tratamiento_cond, $t_operador_logistico_tratamiento, $t_operador_logistico_tratamiento_autocomp,
             $t_punto_entrega_cond, $t_punto_entrega, $t_punto_entrega_autocomp,
             $t_regimen_tratamiento_cond, $t_regimen_tratamiento, $t_regimen_tratamiento_autocomp,
             $t_medico_tratamiento_cond, $t_medico_tratamiento, $t_medico_tratamiento_autocomp,
             $t_tratamiento_previo_cond, $t_tratamiento_previo, $t_tratamiento_previo_autocomp,
             $g_reclamo_gestion_cond, $g_reclamo_gestion, $g_reclamo_gestion_autocomp,
             $g_fecha_reclamacion_gestion_cond, $g_fecha_reclamacion_gestion, $g_fecha_reclamacion_gestion_autocomp,
             $g_causa_no_reclamacion_gestion_cond, $g_causa_no_reclamacion_gestion, $g_causa_no_reclamacion_gestion_autocomp,
             $g_descripcion_comunicacion_gestion_cond, $g_descripcion_comunicacion_gestion,
             $g_fecha_programada_gestion_cond, $g_fecha_programada_gestion, $g_fecha_programada_gestion_autocomp,
             $p_id_ultima_gestion_cond, $p_id_ultima_gestion, $p_id_ultima_gestion_input_2, $p_id_ultima_gestion_autocomp,
             $t_id_tratamiento_cond, $t_id_tratamiento, $t_id_tratamiento_input_2, $t_id_tratamiento_autocomp,
             $t_id_paciente_fk_cond, $t_id_paciente_fk, $t_id_paciente_fk_input_2, $t_id_paciente_fk_autocomp,
             $nm_url_saida, $nm_apl_dependente, $nmgp_parms, $bprocessa, $nmgp_save_name, $NM_operador, $NM_filters, $nmgp_save_option, $NM_filters_del, $Script_BI;
      $Script_BI = "";
      $this->nmgp_botoes['clear'] = "on";
      $this->nmgp_botoes['save'] = "on";
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
          {
              $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_aba_iframe']))
      {
          foreach ($_SESSION['scriptcase']['sc_aba_iframe'] as $aba => $apls_aba)
          {
              if (in_array("informe_reclamacion", $apls_aba))
              {
                  $this->aba_iframe = true;
                  break;
              }
          }
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['iframe_menu'] && (!isset($_SESSION['scriptcase']['menu_mobile']) || empty($_SESSION['scriptcase']['menu_mobile'])))
      {
          $this->aba_iframe = true;
      }
      $nmgp_tab_label = "";
      $delimitador = "##@@";
      if (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']) && $bprocessa != "recarga" && $bprocessa != "save_form" && $bprocessa != "filter_save" && $bprocessa != "filter_delete")
      { 
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'] = NM_conv_charset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'], $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $p_id_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente']; 
          $p_id_paciente_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente_input_2']; 
          $p_id_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente_cond']; 
          $g_logro_comunicacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_logro_comunicacion_gestion']; 
          $g_logro_comunicacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_logro_comunicacion_gestion_cond']; 
          $g_fecha_comunicacion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_comunicacion']; 
          $g_fecha_comunicacion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_comunicacion_cond']; 
          $g_autor_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_autor_gestion']; 
          $g_autor_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_autor_gestion_cond']; 
          $p_pais_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_pais_paciente']; 
          $p_pais_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_pais_paciente_cond']; 
          $p_ciudad_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_ciudad_paciente']; 
          $p_ciudad_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_ciudad_paciente_cond']; 
          $p_estado_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_estado_paciente']; 
          $p_estado_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_estado_paciente_cond']; 
          $p_fecha_activacion_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_activacion_paciente']; 
          $p_fecha_activacion_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_activacion_paciente_cond']; 
          $p_fecha_retiro_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_retiro_paciente']; 
          $p_fecha_retiro_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_retiro_paciente_cond']; 
          $p_motivo_retiro_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_motivo_retiro_paciente']; 
          $p_motivo_retiro_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_motivo_retiro_paciente_cond']; 
          $t_consentimiento_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_consentimiento_tratamiento']; 
          $t_consentimiento_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_consentimiento_tratamiento_cond']; 
          $p_codigo_xofigo = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo']; 
          $p_codigo_xofigo_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo_input_2']; 
          $p_codigo_xofigo_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo_cond']; 
          $t_clasificacion_patologica_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_clasificacion_patologica_tratamiento']; 
          $t_clasificacion_patologica_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_clasificacion_patologica_tratamiento_cond']; 
          $t_producto_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_producto_tratamiento']; 
          $t_producto_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_producto_tratamiento_cond']; 
          $t_nombre_referencia = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_nombre_referencia']; 
          $t_nombre_referencia_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_nombre_referencia_cond']; 
          $t_dosis_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_dosis_tratamiento']; 
          $t_dosis_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_dosis_tratamiento_cond']; 
          $g_numero_cajas = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_numero_cajas']; 
          $g_numero_cajas_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_numero_cajas_cond']; 
          $t_asegurador_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_asegurador_tratamiento']; 
          $t_asegurador_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_asegurador_tratamiento_cond']; 
          $t_operador_logistico_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_operador_logistico_tratamiento']; 
          $t_operador_logistico_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_operador_logistico_tratamiento_cond']; 
          $t_punto_entrega = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_punto_entrega']; 
          $t_punto_entrega_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_punto_entrega_cond']; 
          $t_regimen_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_regimen_tratamiento']; 
          $t_regimen_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_regimen_tratamiento_cond']; 
          $t_medico_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_medico_tratamiento']; 
          $t_medico_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_medico_tratamiento_cond']; 
          $t_tratamiento_previo = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_tratamiento_previo']; 
          $t_tratamiento_previo_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_tratamiento_previo_cond']; 
          $g_reclamo_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_reclamo_gestion']; 
          $g_reclamo_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_reclamo_gestion_cond']; 
          $g_fecha_reclamacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_reclamacion_gestion']; 
          $g_fecha_reclamacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_reclamacion_gestion_cond']; 
          $g_causa_no_reclamacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_causa_no_reclamacion_gestion']; 
          $g_causa_no_reclamacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_causa_no_reclamacion_gestion_cond']; 
          $g_descripcion_comunicacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_descripcion_comunicacion_gestion']; 
          $g_descripcion_comunicacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_descripcion_comunicacion_gestion_cond']; 
          $g_fecha_programada_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_programada_gestion']; 
          $g_fecha_programada_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_programada_gestion_cond']; 
          $p_id_ultima_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion']; 
          $p_id_ultima_gestion_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion_input_2']; 
          $p_id_ultima_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion_cond']; 
          $t_id_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento']; 
          $t_id_tratamiento_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento_input_2']; 
          $t_id_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento_cond']; 
          $t_id_paciente_fk = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk']; 
          $t_id_paciente_fk_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk_input_2']; 
          $t_id_paciente_fk_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk_cond']; 
          $this->NM_operador = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['NM_operador']; 
      } 
      if (!isset($p_id_paciente_cond) || empty($p_id_paciente_cond))
      {
         $p_id_paciente_cond = "gt";
      }
      if (!isset($g_logro_comunicacion_gestion_cond) || empty($g_logro_comunicacion_gestion_cond))
      {
         $g_logro_comunicacion_gestion_cond = "qp";
      }
      if (!isset($g_fecha_comunicacion_cond) || empty($g_fecha_comunicacion_cond))
      {
         $g_fecha_comunicacion_cond = "qp";
      }
      if (!isset($g_autor_gestion_cond) || empty($g_autor_gestion_cond))
      {
         $g_autor_gestion_cond = "qp";
      }
      if (!isset($p_pais_paciente_cond) || empty($p_pais_paciente_cond))
      {
         $p_pais_paciente_cond = "qp";
      }
      if (!isset($p_ciudad_paciente_cond) || empty($p_ciudad_paciente_cond))
      {
         $p_ciudad_paciente_cond = "qp";
      }
      if (!isset($p_estado_paciente_cond) || empty($p_estado_paciente_cond))
      {
         $p_estado_paciente_cond = "qp";
      }
      if (!isset($p_fecha_activacion_paciente_cond) || empty($p_fecha_activacion_paciente_cond))
      {
         $p_fecha_activacion_paciente_cond = "qp";
      }
      if (!isset($p_fecha_retiro_paciente_cond) || empty($p_fecha_retiro_paciente_cond))
      {
         $p_fecha_retiro_paciente_cond = "qp";
      }
      if (!isset($p_motivo_retiro_paciente_cond) || empty($p_motivo_retiro_paciente_cond))
      {
         $p_motivo_retiro_paciente_cond = "qp";
      }
      if (!isset($t_consentimiento_tratamiento_cond) || empty($t_consentimiento_tratamiento_cond))
      {
         $t_consentimiento_tratamiento_cond = "qp";
      }
      if (!isset($p_codigo_xofigo_cond) || empty($p_codigo_xofigo_cond))
      {
         $p_codigo_xofigo_cond = "gt";
      }
      if (!isset($t_clasificacion_patologica_tratamiento_cond) || empty($t_clasificacion_patologica_tratamiento_cond))
      {
         $t_clasificacion_patologica_tratamiento_cond = "qp";
      }
      if (!isset($t_producto_tratamiento_cond) || empty($t_producto_tratamiento_cond))
      {
         $t_producto_tratamiento_cond = "qp";
      }
      if (!isset($t_nombre_referencia_cond) || empty($t_nombre_referencia_cond))
      {
         $t_nombre_referencia_cond = "qp";
      }
      if (!isset($t_dosis_tratamiento_cond) || empty($t_dosis_tratamiento_cond))
      {
         $t_dosis_tratamiento_cond = "qp";
      }
      if (!isset($g_numero_cajas_cond) || empty($g_numero_cajas_cond))
      {
         $g_numero_cajas_cond = "qp";
      }
      if (!isset($t_asegurador_tratamiento_cond) || empty($t_asegurador_tratamiento_cond))
      {
         $t_asegurador_tratamiento_cond = "qp";
      }
      if (!isset($t_operador_logistico_tratamiento_cond) || empty($t_operador_logistico_tratamiento_cond))
      {
         $t_operador_logistico_tratamiento_cond = "qp";
      }
      if (!isset($t_punto_entrega_cond) || empty($t_punto_entrega_cond))
      {
         $t_punto_entrega_cond = "qp";
      }
      if (!isset($t_regimen_tratamiento_cond) || empty($t_regimen_tratamiento_cond))
      {
         $t_regimen_tratamiento_cond = "qp";
      }
      if (!isset($t_medico_tratamiento_cond) || empty($t_medico_tratamiento_cond))
      {
         $t_medico_tratamiento_cond = "qp";
      }
      if (!isset($t_tratamiento_previo_cond) || empty($t_tratamiento_previo_cond))
      {
         $t_tratamiento_previo_cond = "qp";
      }
      if (!isset($g_reclamo_gestion_cond) || empty($g_reclamo_gestion_cond))
      {
         $g_reclamo_gestion_cond = "qp";
      }
      if (!isset($g_fecha_reclamacion_gestion_cond) || empty($g_fecha_reclamacion_gestion_cond))
      {
         $g_fecha_reclamacion_gestion_cond = "qp";
      }
      if (!isset($g_causa_no_reclamacion_gestion_cond) || empty($g_causa_no_reclamacion_gestion_cond))
      {
         $g_causa_no_reclamacion_gestion_cond = "qp";
      }
      if (!isset($g_descripcion_comunicacion_gestion_cond) || empty($g_descripcion_comunicacion_gestion_cond))
      {
         $g_descripcion_comunicacion_gestion_cond = "eq";
      }
      if (!isset($g_fecha_programada_gestion_cond) || empty($g_fecha_programada_gestion_cond))
      {
         $g_fecha_programada_gestion_cond = "qp";
      }
      if (!isset($p_id_ultima_gestion_cond) || empty($p_id_ultima_gestion_cond))
      {
         $p_id_ultima_gestion_cond = "gt";
      }
      if (!isset($t_id_tratamiento_cond) || empty($t_id_tratamiento_cond))
      {
         $t_id_tratamiento_cond = "gt";
      }
      if (!isset($t_id_paciente_fk_cond) || empty($t_id_paciente_fk_cond))
      {
         $t_id_paciente_fk_cond = "gt";
      }
      $display_aberto  = "style=display:";
      $display_fechado = "style=display:none";
      $opc_hide_input = array("nu","nn","ep","ne");
      $str_hide_p_id_paciente = (in_array($p_id_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_logro_comunicacion_gestion = (in_array($g_logro_comunicacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_fecha_comunicacion = (in_array($g_fecha_comunicacion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_autor_gestion = (in_array($g_autor_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_pais_paciente = (in_array($p_pais_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_ciudad_paciente = (in_array($p_ciudad_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_estado_paciente = (in_array($p_estado_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_fecha_activacion_paciente = (in_array($p_fecha_activacion_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_fecha_retiro_paciente = (in_array($p_fecha_retiro_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_motivo_retiro_paciente = (in_array($p_motivo_retiro_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_consentimiento_tratamiento = (in_array($t_consentimiento_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_codigo_xofigo = (in_array($p_codigo_xofigo_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_clasificacion_patologica_tratamiento = (in_array($t_clasificacion_patologica_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_producto_tratamiento = (in_array($t_producto_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_nombre_referencia = (in_array($t_nombre_referencia_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_dosis_tratamiento = (in_array($t_dosis_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_numero_cajas = (in_array($g_numero_cajas_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_asegurador_tratamiento = (in_array($t_asegurador_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_operador_logistico_tratamiento = (in_array($t_operador_logistico_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_punto_entrega = (in_array($t_punto_entrega_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_regimen_tratamiento = (in_array($t_regimen_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_medico_tratamiento = (in_array($t_medico_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_tratamiento_previo = (in_array($t_tratamiento_previo_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_reclamo_gestion = (in_array($g_reclamo_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_fecha_reclamacion_gestion = (in_array($g_fecha_reclamacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_causa_no_reclamacion_gestion = (in_array($g_causa_no_reclamacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_descripcion_comunicacion_gestion = (in_array($g_descripcion_comunicacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_g_fecha_programada_gestion = (in_array($g_fecha_programada_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_p_id_ultima_gestion = (in_array($p_id_ultima_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_id_tratamiento = (in_array($t_id_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_t_id_paciente_fk = (in_array($t_id_paciente_fk_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;

      $str_display_p_id_paciente = ('bw' == $p_id_paciente_cond) ? $display_aberto : $display_fechado;
      $str_display_p_codigo_xofigo = ('bw' == $p_codigo_xofigo_cond) ? $display_aberto : $display_fechado;
      $str_display_p_id_ultima_gestion = ('bw' == $p_id_ultima_gestion_cond) ? $display_aberto : $display_fechado;
      $str_display_t_id_tratamiento = ('bw' == $t_id_tratamiento_cond) ? $display_aberto : $display_fechado;
      $str_display_t_id_paciente_fk = ('bw' == $t_id_paciente_fk_cond) ? $display_aberto : $display_fechado;

      if (!isset($p_id_paciente) || $p_id_paciente == "")
      {
          $p_id_paciente = "";
      }
      if (isset($p_id_paciente) && !empty($p_id_paciente))
      {
         $tmp_pos = strpos($p_id_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_id_paciente = substr($p_id_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($g_logro_comunicacion_gestion) || $g_logro_comunicacion_gestion == "")
      {
          $g_logro_comunicacion_gestion = "";
      }
      if (isset($g_logro_comunicacion_gestion) && !empty($g_logro_comunicacion_gestion))
      {
         $tmp_pos = strpos($g_logro_comunicacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_logro_comunicacion_gestion = substr($g_logro_comunicacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($g_fecha_comunicacion) || $g_fecha_comunicacion == "")
      {
          $g_fecha_comunicacion = "";
      }
      if (isset($g_fecha_comunicacion) && !empty($g_fecha_comunicacion))
      {
         $tmp_pos = strpos($g_fecha_comunicacion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_fecha_comunicacion = substr($g_fecha_comunicacion, 0, $tmp_pos);
         }
      }
      if (!isset($g_autor_gestion) || $g_autor_gestion == "")
      {
          $g_autor_gestion = "";
      }
      if (isset($g_autor_gestion) && !empty($g_autor_gestion))
      {
         $tmp_pos = strpos($g_autor_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_autor_gestion = substr($g_autor_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($p_pais_paciente) || $p_pais_paciente == "")
      {
          $p_pais_paciente = "";
      }
      if (isset($p_pais_paciente) && !empty($p_pais_paciente))
      {
         $tmp_pos = strpos($p_pais_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_pais_paciente = substr($p_pais_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($p_ciudad_paciente) || $p_ciudad_paciente == "")
      {
          $p_ciudad_paciente = "";
      }
      if (isset($p_ciudad_paciente) && !empty($p_ciudad_paciente))
      {
         $tmp_pos = strpos($p_ciudad_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_ciudad_paciente = substr($p_ciudad_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($p_estado_paciente) || $p_estado_paciente == "")
      {
          $p_estado_paciente = "";
      }
      if (isset($p_estado_paciente) && !empty($p_estado_paciente))
      {
         $tmp_pos = strpos($p_estado_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_estado_paciente = substr($p_estado_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($p_fecha_activacion_paciente) || $p_fecha_activacion_paciente == "")
      {
          $p_fecha_activacion_paciente = "";
      }
      if (isset($p_fecha_activacion_paciente) && !empty($p_fecha_activacion_paciente))
      {
         $tmp_pos = strpos($p_fecha_activacion_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_fecha_activacion_paciente = substr($p_fecha_activacion_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($p_fecha_retiro_paciente) || $p_fecha_retiro_paciente == "")
      {
          $p_fecha_retiro_paciente = "";
      }
      if (isset($p_fecha_retiro_paciente) && !empty($p_fecha_retiro_paciente))
      {
         $tmp_pos = strpos($p_fecha_retiro_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_fecha_retiro_paciente = substr($p_fecha_retiro_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($p_motivo_retiro_paciente) || $p_motivo_retiro_paciente == "")
      {
          $p_motivo_retiro_paciente = "";
      }
      if (isset($p_motivo_retiro_paciente) && !empty($p_motivo_retiro_paciente))
      {
         $tmp_pos = strpos($p_motivo_retiro_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_motivo_retiro_paciente = substr($p_motivo_retiro_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($t_consentimiento_tratamiento) || $t_consentimiento_tratamiento == "")
      {
          $t_consentimiento_tratamiento = "";
      }
      if (isset($t_consentimiento_tratamiento) && !empty($t_consentimiento_tratamiento))
      {
         $tmp_pos = strpos($t_consentimiento_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_consentimiento_tratamiento = substr($t_consentimiento_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($p_codigo_xofigo) || $p_codigo_xofigo == "")
      {
          $p_codigo_xofigo = "";
      }
      if (isset($p_codigo_xofigo) && !empty($p_codigo_xofigo))
      {
         $tmp_pos = strpos($p_codigo_xofigo, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_codigo_xofigo = substr($p_codigo_xofigo, 0, $tmp_pos);
         }
      }
      if (!isset($t_clasificacion_patologica_tratamiento) || $t_clasificacion_patologica_tratamiento == "")
      {
          $t_clasificacion_patologica_tratamiento = "";
      }
      if (isset($t_clasificacion_patologica_tratamiento) && !empty($t_clasificacion_patologica_tratamiento))
      {
         $tmp_pos = strpos($t_clasificacion_patologica_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_clasificacion_patologica_tratamiento = substr($t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_producto_tratamiento) || $t_producto_tratamiento == "")
      {
          $t_producto_tratamiento = "";
      }
      if (isset($t_producto_tratamiento) && !empty($t_producto_tratamiento))
      {
         $tmp_pos = strpos($t_producto_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_producto_tratamiento = substr($t_producto_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_nombre_referencia) || $t_nombre_referencia == "")
      {
          $t_nombre_referencia = "";
      }
      if (isset($t_nombre_referencia) && !empty($t_nombre_referencia))
      {
         $tmp_pos = strpos($t_nombre_referencia, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_nombre_referencia = substr($t_nombre_referencia, 0, $tmp_pos);
         }
      }
      if (!isset($t_dosis_tratamiento) || $t_dosis_tratamiento == "")
      {
          $t_dosis_tratamiento = "";
      }
      if (isset($t_dosis_tratamiento) && !empty($t_dosis_tratamiento))
      {
         $tmp_pos = strpos($t_dosis_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_dosis_tratamiento = substr($t_dosis_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($g_numero_cajas) || $g_numero_cajas == "")
      {
          $g_numero_cajas = "";
      }
      if (isset($g_numero_cajas) && !empty($g_numero_cajas))
      {
         $tmp_pos = strpos($g_numero_cajas, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_numero_cajas = substr($g_numero_cajas, 0, $tmp_pos);
         }
      }
      if (!isset($t_asegurador_tratamiento) || $t_asegurador_tratamiento == "")
      {
          $t_asegurador_tratamiento = "";
      }
      if (isset($t_asegurador_tratamiento) && !empty($t_asegurador_tratamiento))
      {
         $tmp_pos = strpos($t_asegurador_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_asegurador_tratamiento = substr($t_asegurador_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_operador_logistico_tratamiento) || $t_operador_logistico_tratamiento == "")
      {
          $t_operador_logistico_tratamiento = "";
      }
      if (isset($t_operador_logistico_tratamiento) && !empty($t_operador_logistico_tratamiento))
      {
         $tmp_pos = strpos($t_operador_logistico_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_operador_logistico_tratamiento = substr($t_operador_logistico_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_punto_entrega) || $t_punto_entrega == "")
      {
          $t_punto_entrega = "";
      }
      if (isset($t_punto_entrega) && !empty($t_punto_entrega))
      {
         $tmp_pos = strpos($t_punto_entrega, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_punto_entrega = substr($t_punto_entrega, 0, $tmp_pos);
         }
      }
      if (!isset($t_regimen_tratamiento) || $t_regimen_tratamiento == "")
      {
          $t_regimen_tratamiento = "";
      }
      if (isset($t_regimen_tratamiento) && !empty($t_regimen_tratamiento))
      {
         $tmp_pos = strpos($t_regimen_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_regimen_tratamiento = substr($t_regimen_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_medico_tratamiento) || $t_medico_tratamiento == "")
      {
          $t_medico_tratamiento = "";
      }
      if (isset($t_medico_tratamiento) && !empty($t_medico_tratamiento))
      {
         $tmp_pos = strpos($t_medico_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_medico_tratamiento = substr($t_medico_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_tratamiento_previo) || $t_tratamiento_previo == "")
      {
          $t_tratamiento_previo = "";
      }
      if (isset($t_tratamiento_previo) && !empty($t_tratamiento_previo))
      {
         $tmp_pos = strpos($t_tratamiento_previo, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_tratamiento_previo = substr($t_tratamiento_previo, 0, $tmp_pos);
         }
      }
      if (!isset($g_reclamo_gestion) || $g_reclamo_gestion == "")
      {
          $g_reclamo_gestion = "";
      }
      if (isset($g_reclamo_gestion) && !empty($g_reclamo_gestion))
      {
         $tmp_pos = strpos($g_reclamo_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_reclamo_gestion = substr($g_reclamo_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($g_fecha_reclamacion_gestion) || $g_fecha_reclamacion_gestion == "")
      {
          $g_fecha_reclamacion_gestion = "";
      }
      if (isset($g_fecha_reclamacion_gestion) && !empty($g_fecha_reclamacion_gestion))
      {
         $tmp_pos = strpos($g_fecha_reclamacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_fecha_reclamacion_gestion = substr($g_fecha_reclamacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($g_causa_no_reclamacion_gestion) || $g_causa_no_reclamacion_gestion == "")
      {
          $g_causa_no_reclamacion_gestion = "";
      }
      if (isset($g_causa_no_reclamacion_gestion) && !empty($g_causa_no_reclamacion_gestion))
      {
         $tmp_pos = strpos($g_causa_no_reclamacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_causa_no_reclamacion_gestion = substr($g_causa_no_reclamacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($g_descripcion_comunicacion_gestion) || $g_descripcion_comunicacion_gestion == "")
      {
          $g_descripcion_comunicacion_gestion = "";
      }
      if (isset($g_descripcion_comunicacion_gestion) && !empty($g_descripcion_comunicacion_gestion))
      {
         $tmp_pos = strpos($g_descripcion_comunicacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_descripcion_comunicacion_gestion = substr($g_descripcion_comunicacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($g_fecha_programada_gestion) || $g_fecha_programada_gestion == "")
      {
          $g_fecha_programada_gestion = "";
      }
      if (isset($g_fecha_programada_gestion) && !empty($g_fecha_programada_gestion))
      {
         $tmp_pos = strpos($g_fecha_programada_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $g_fecha_programada_gestion = substr($g_fecha_programada_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($p_id_ultima_gestion) || $p_id_ultima_gestion == "")
      {
          $p_id_ultima_gestion = "";
      }
      if (isset($p_id_ultima_gestion) && !empty($p_id_ultima_gestion))
      {
         $tmp_pos = strpos($p_id_ultima_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $p_id_ultima_gestion = substr($p_id_ultima_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($t_id_tratamiento) || $t_id_tratamiento == "")
      {
          $t_id_tratamiento = "";
      }
      if (isset($t_id_tratamiento) && !empty($t_id_tratamiento))
      {
         $tmp_pos = strpos($t_id_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_id_tratamiento = substr($t_id_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($t_id_paciente_fk) || $t_id_paciente_fk == "")
      {
          $t_id_paciente_fk = "";
      }
      if (isset($t_id_paciente_fk) && !empty($t_id_paciente_fk))
      {
         $tmp_pos = strpos($t_id_paciente_fk, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $t_id_paciente_fk = substr($t_id_paciente_fk, 0, $tmp_pos);
         }
      }
?>
 <?php
     if ($_SESSION['scriptcase']['proc_mobile'])
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
<?php
   if (is_file("informe_reclamacion_help.txt"))
   {
      $Arq_WebHelp = file("informe_reclamacion_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_top" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_top').style.display = 'none'", "document.getElementById('Salvar_filters_top').style.display = 'none'", "Cancel_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_top" name="nmgp_save_name_top" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('top')", "nm_save_form('top')", "Save_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_top" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_top">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_top" name="NM_filters_del_top" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('top')", "nm_submit_filter_del('top')", "Exc_filtro_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
     else
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
<?php
   if (is_file("informe_reclamacion_help.txt"))
   {
      $Arq_WebHelp = file("informe_reclamacion_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_top" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_top').style.display = 'none'", "document.getElementById('Salvar_filters_top').style.display = 'none'", "Cancel_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_top" name="nmgp_save_name_top" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('top')", "nm_save_form('top')", "Save_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_top" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_top">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_top" name="NM_filters_del_top" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('top')", "nm_submit_filter_del('top')", "Exc_filtro_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
 ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <TABLE style="padding: 0px; spacing: 0px; border-width: 0px;" width="100%" height="100%">
   <TR valign="top" >
  <TD width="100%" height="">
   <TABLE class="scFilterTable" id="hidden_bloco_0" valign="top" width="100%" style="height: 100%;">
   <tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_p_id_paciente_cond" name="p_id_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_id_paciente'), this, 'p_id_paciente')">
       <OPTION value="gt" <?php if ("gt" == $p_id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $p_id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $p_id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_id_paciente" <?php echo $str_hide_p_id_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE";
 $nmgp_tab_label .= "p_id_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_id_paciente_look = substr($this->Db->qstr($p_id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_id_paciente))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where p.ID_PACIENTE = $p_id_paciente_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where p.ID_PACIENTE = $p_id_paciente_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$p_id_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_id_paciente];
      }
      else
      {
          $sAutocompValue = $p_id_paciente;
      }
?>
<INPUT  type="text" id="SC_p_id_paciente" name="p_id_paciente" value="<?php echo NM_encode_input($p_id_paciente) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_p_id_paciente" name="p_id_paciente_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_p_id_paciente"  <?php echo $str_display_p_id_paciente; ?> class="scFilterFieldFontOdd">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_p_id_paciente_input_2" name="p_id_paciente_input_2" value="<?php echo NM_encode_input($p_id_paciente_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['g_logro_comunicacion_gestion'])) ? $this->New_label['g_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_g_logro_comunicacion_gestion_cond" name="g_logro_comunicacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_logro_comunicacion_gestion'), this, 'g_logro_comunicacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_logro_comunicacion_gestion" <?php echo $str_hide_g_logro_comunicacion_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['g_logro_comunicacion_gestion'])) ? $this->New_label['g_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION";
 $nmgp_tab_label .= "g_logro_comunicacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_logro_comunicacion_gestion_look = substr($this->Db->qstr($g_logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.LOGRO_COMUNICACION_GESTION from " . $this->Ini->nm_tabela . " where g.LOGRO_COMUNICACION_GESTION = '$g_logro_comunicacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_logro_comunicacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_logro_comunicacion_gestion];
      }
      else
      {
          $sAutocompValue = $g_logro_comunicacion_gestion;
      }
?>
<INPUT  type="text" id="SC_g_logro_comunicacion_gestion" name="g_logro_comunicacion_gestion" value="<?php echo NM_encode_input($g_logro_comunicacion_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_g_logro_comunicacion_gestion" name="g_logro_comunicacion_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_g_fecha_comunicacion_cond" name="g_fecha_comunicacion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_fecha_comunicacion'), this, 'g_fecha_comunicacion')">
       <OPTION value="qp" <?php if ("qp" == $g_fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_fecha_comunicacion" <?php echo $str_hide_g_fecha_comunicacion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION";
 $nmgp_tab_label .= "g_fecha_comunicacion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_fecha_comunicacion_look = substr($this->Db->qstr($g_fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_COMUNICACION from " . $this->Ini->nm_tabela . " where g.FECHA_COMUNICACION = '$g_fecha_comunicacion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_fecha_comunicacion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_fecha_comunicacion];
      }
      else
      {
          $sAutocompValue = $g_fecha_comunicacion;
      }
?>
<INPUT  type="text" id="SC_g_fecha_comunicacion" name="g_fecha_comunicacion" value="<?php echo NM_encode_input($g_fecha_comunicacion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_g_fecha_comunicacion" name="g_fecha_comunicacion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_g_autor_gestion_cond" name="g_autor_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_autor_gestion'), this, 'g_autor_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_autor_gestion" <?php echo $str_hide_g_autor_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION";
 $nmgp_tab_label .= "g_autor_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_autor_gestion_look = substr($this->Db->qstr($g_autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.AUTOR_GESTION from " . $this->Ini->nm_tabela . " where g.AUTOR_GESTION = '$g_autor_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_autor_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_autor_gestion];
      }
      else
      {
          $sAutocompValue = $g_autor_gestion;
      }
?>
<INPUT  type="text" id="SC_g_autor_gestion" name="g_autor_gestion" value="<?php echo NM_encode_input($g_autor_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_g_autor_gestion" name="g_autor_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_p_pais_paciente_cond" name="p_pais_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_pais_paciente'), this, 'p_pais_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_pais_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_pais_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_pais_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_pais_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_pais_paciente" <?php echo $str_hide_p_pais_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE";
 $nmgp_tab_label .= "p_pais_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_pais_paciente_look = substr($this->Db->qstr($p_pais_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.PAIS_PACIENTE from " . $this->Ini->nm_tabela . " where p.PAIS_PACIENTE = '$p_pais_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_pais_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_pais_paciente];
      }
      else
      {
          $sAutocompValue = $p_pais_paciente;
      }
?>
<INPUT  type="text" id="SC_p_pais_paciente" name="p_pais_paciente" value="<?php echo NM_encode_input($p_pais_paciente) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_p_pais_paciente" name="p_pais_paciente_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_p_ciudad_paciente_cond" name="p_ciudad_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_ciudad_paciente'), this, 'p_ciudad_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_ciudad_paciente" <?php echo $str_hide_p_ciudad_paciente?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE";
 $nmgp_tab_label .= "p_ciudad_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_ciudad_paciente_look = substr($this->Db->qstr($p_ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.CIUDAD_PACIENTE from " . $this->Ini->nm_tabela . " where p.CIUDAD_PACIENTE = '$p_ciudad_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_ciudad_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_ciudad_paciente];
      }
      else
      {
          $sAutocompValue = $p_ciudad_paciente;
      }
?>
<INPUT  type="text" id="SC_p_ciudad_paciente" name="p_ciudad_paciente" value="<?php echo NM_encode_input($p_ciudad_paciente) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_p_ciudad_paciente" name="p_ciudad_paciente_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_p_estado_paciente_cond" name="p_estado_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_estado_paciente'), this, 'p_estado_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_estado_paciente" <?php echo $str_hide_p_estado_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE";
 $nmgp_tab_label .= "p_estado_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_estado_paciente_look = substr($this->Db->qstr($p_estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.ESTADO_PACIENTE from " . $this->Ini->nm_tabela . " where p.ESTADO_PACIENTE = '$p_estado_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_estado_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_estado_paciente];
      }
      else
      {
          $sAutocompValue = $p_estado_paciente;
      }
?>
<INPUT  type="text" id="SC_p_estado_paciente" name="p_estado_paciente" value="<?php echo NM_encode_input($p_estado_paciente) ?>" size=20 alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_p_estado_paciente" name="p_estado_paciente_autocomp" size="20" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_p_fecha_activacion_paciente_cond" name="p_fecha_activacion_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_fecha_activacion_paciente'), this, 'p_fecha_activacion_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_fecha_activacion_paciente" <?php echo $str_hide_p_fecha_activacion_paciente?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE";
 $nmgp_tab_label .= "p_fecha_activacion_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_fecha_activacion_paciente_look = substr($this->Db->qstr($p_fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_ACTIVACION_PACIENTE from " . $this->Ini->nm_tabela . " where p.FECHA_ACTIVACION_PACIENTE = '$p_fecha_activacion_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_fecha_activacion_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_fecha_activacion_paciente];
      }
      else
      {
          $sAutocompValue = $p_fecha_activacion_paciente;
      }
?>
<INPUT  type="text" id="SC_p_fecha_activacion_paciente" name="p_fecha_activacion_paciente" value="<?php echo NM_encode_input($p_fecha_activacion_paciente) ?>" size=10 alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_p_fecha_activacion_paciente" name="p_fecha_activacion_paciente_autocomp" size="10" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['p_fecha_retiro_paciente'])) ? $this->New_label['p_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_p_fecha_retiro_paciente_cond" name="p_fecha_retiro_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_fecha_retiro_paciente'), this, 'p_fecha_retiro_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_fecha_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_fecha_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_fecha_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_fecha_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_fecha_retiro_paciente" <?php echo $str_hide_p_fecha_retiro_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['p_fecha_retiro_paciente'])) ? $this->New_label['p_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE";
 $nmgp_tab_label .= "p_fecha_retiro_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_fecha_retiro_paciente_look = substr($this->Db->qstr($p_fecha_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where p.FECHA_RETIRO_PACIENTE = '$p_fecha_retiro_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_fecha_retiro_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_fecha_retiro_paciente];
      }
      else
      {
          $sAutocompValue = $p_fecha_retiro_paciente;
      }
?>
<INPUT  type="text" id="SC_p_fecha_retiro_paciente" name="p_fecha_retiro_paciente" value="<?php echo NM_encode_input($p_fecha_retiro_paciente) ?>" size=10 alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_p_fecha_retiro_paciente" name="p_fecha_retiro_paciente_autocomp" size="10" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['p_motivo_retiro_paciente'])) ? $this->New_label['p_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_p_motivo_retiro_paciente_cond" name="p_motivo_retiro_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_motivo_retiro_paciente'), this, 'p_motivo_retiro_paciente')">
       <OPTION value="qp" <?php if ("qp" == $p_motivo_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $p_motivo_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_motivo_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $p_motivo_retiro_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_motivo_retiro_paciente" <?php echo $str_hide_p_motivo_retiro_paciente?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['p_motivo_retiro_paciente'])) ? $this->New_label['p_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE";
 $nmgp_tab_label .= "p_motivo_retiro_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_motivo_retiro_paciente_look = substr($this->Db->qstr($p_motivo_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.MOTIVO_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where p.MOTIVO_RETIRO_PACIENTE = '$p_motivo_retiro_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$p_motivo_retiro_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_motivo_retiro_paciente];
      }
      else
      {
          $sAutocompValue = $p_motivo_retiro_paciente;
      }
?>
<INPUT  type="text" id="SC_p_motivo_retiro_paciente" name="p_motivo_retiro_paciente" value="<?php echo NM_encode_input($p_motivo_retiro_paciente) ?>" size=50 alt="{datatype: 'text', maxLength: 1500, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_p_motivo_retiro_paciente" name="p_motivo_retiro_paciente_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_consentimiento_tratamiento'])) ? $this->New_label['t_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_consentimiento_tratamiento_cond" name="t_consentimiento_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_consentimiento_tratamiento'), this, 't_consentimiento_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_consentimiento_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_consentimiento_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_consentimiento_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_consentimiento_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_consentimiento_tratamiento" <?php echo $str_hide_t_consentimiento_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_consentimiento_tratamiento'])) ? $this->New_label['t_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO";
 $nmgp_tab_label .= "t_consentimiento_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_consentimiento_tratamiento_look = substr($this->Db->qstr($t_consentimiento_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CONSENTIMIENTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.CONSENTIMIENTO_TRATAMIENTO = '$t_consentimiento_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_consentimiento_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_consentimiento_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_consentimiento_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_consentimiento_tratamiento" name="t_consentimiento_tratamiento" value="<?php echo NM_encode_input($t_consentimiento_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 60, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_consentimiento_tratamiento" name="t_consentimiento_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_p_codigo_xofigo_cond" name="p_codigo_xofigo_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_codigo_xofigo'), this, 'p_codigo_xofigo')">
       <OPTION value="gt" <?php if ("gt" == $p_codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $p_codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $p_codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_codigo_xofigo" <?php echo $str_hide_p_codigo_xofigo?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO";
 $nmgp_tab_label .= "p_codigo_xofigo?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_codigo_xofigo_look = substr($this->Db->qstr($p_codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_codigo_xofigo))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where p.CODIGO_XOFIGO = $p_codigo_xofigo_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where p.CODIGO_XOFIGO = $p_codigo_xofigo_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$p_codigo_xofigo]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_codigo_xofigo];
      }
      else
      {
          $sAutocompValue = $p_codigo_xofigo;
      }
?>
<INPUT  type="text" id="SC_p_codigo_xofigo" name="p_codigo_xofigo" value="<?php echo NM_encode_input($p_codigo_xofigo) ?>" size=4 alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_p_codigo_xofigo" name="p_codigo_xofigo_autocomp" size="4" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_p_codigo_xofigo"  <?php echo $str_display_p_codigo_xofigo; ?> class="scFilterFieldFontEven">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_p_codigo_xofigo_input_2" name="p_codigo_xofigo_input_2" value="<?php echo NM_encode_input($p_codigo_xofigo_input_2) ?>" size=4 alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectEven">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_clasificacion_patologica_tratamiento_cond" name="t_clasificacion_patologica_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_clasificacion_patologica_tratamiento'), this, 't_clasificacion_patologica_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_clasificacion_patologica_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_clasificacion_patologica_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_clasificacion_patologica_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_clasificacion_patologica_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_clasificacion_patologica_tratamiento" <?php echo $str_hide_t_clasificacion_patologica_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO";
 $nmgp_tab_label .= "t_clasificacion_patologica_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_clasificacion_patologica_tratamiento_look = substr($this->Db->qstr($t_clasificacion_patologica_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CLASIFICACION_PATOLOGICA_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$t_clasificacion_patologica_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_clasificacion_patologica_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_clasificacion_patologica_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_clasificacion_patologica_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_clasificacion_patologica_tratamiento" name="t_clasificacion_patologica_tratamiento" value="<?php echo NM_encode_input($t_clasificacion_patologica_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 200, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_clasificacion_patologica_tratamiento" name="t_clasificacion_patologica_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_producto_tratamiento_cond" name="t_producto_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_producto_tratamiento'), this, 't_producto_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_producto_tratamiento" <?php echo $str_hide_t_producto_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO";
 $nmgp_tab_label .= "t_producto_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_producto_tratamiento_look = substr($this->Db->qstr($t_producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PRODUCTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.PRODUCTO_TRATAMIENTO = '$t_producto_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_producto_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_producto_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_producto_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_producto_tratamiento" name="t_producto_tratamiento" value="<?php echo NM_encode_input($t_producto_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 80, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_producto_tratamiento" name="t_producto_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_nombre_referencia_cond" name="t_nombre_referencia_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_nombre_referencia'), this, 't_nombre_referencia')">
       <OPTION value="qp" <?php if ("qp" == $t_nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_nombre_referencia" <?php echo $str_hide_t_nombre_referencia?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA";
 $nmgp_tab_label .= "t_nombre_referencia?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_nombre_referencia_look = substr($this->Db->qstr($t_nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.NOMBRE_REFERENCIA from " . $this->Ini->nm_tabela . " where t.NOMBRE_REFERENCIA = '$t_nombre_referencia_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_nombre_referencia]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_nombre_referencia];
      }
      else
      {
          $sAutocompValue = $t_nombre_referencia;
      }
?>
<INPUT  type="text" id="SC_t_nombre_referencia" name="t_nombre_referencia" value="<?php echo NM_encode_input($t_nombre_referencia) ?>" size=50 alt="{datatype: 'text', maxLength: 80, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_nombre_referencia" name="t_nombre_referencia_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_dosis_tratamiento_cond" name="t_dosis_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_dosis_tratamiento'), this, 't_dosis_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_dosis_tratamiento" <?php echo $str_hide_t_dosis_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO";
 $nmgp_tab_label .= "t_dosis_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_dosis_tratamiento_look = substr($this->Db->qstr($t_dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.DOSIS_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.DOSIS_TRATAMIENTO = '$t_dosis_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_dosis_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_dosis_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_dosis_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_dosis_tratamiento" name="t_dosis_tratamiento" value="<?php echo NM_encode_input($t_dosis_tratamiento) ?>" size=25 alt="{datatype: 'text', maxLength: 25, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_dosis_tratamiento" name="t_dosis_tratamiento_autocomp" size="25" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 25, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['g_numero_cajas'])) ? $this->New_label['g_numero_cajas'] : "NUMERO CAJAS"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_g_numero_cajas_cond" name="g_numero_cajas_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_numero_cajas'), this, 'g_numero_cajas')">
       <OPTION value="qp" <?php if ("qp" == $g_numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_numero_cajas" <?php echo $str_hide_g_numero_cajas?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['g_numero_cajas'])) ? $this->New_label['g_numero_cajas'] : "NUMERO CAJAS";
 $nmgp_tab_label .= "g_numero_cajas?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_numero_cajas_look = substr($this->Db->qstr($g_numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.NUMERO_CAJAS from " . $this->Ini->nm_tabela . " where g.NUMERO_CAJAS = '$g_numero_cajas_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_numero_cajas]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_numero_cajas];
      }
      else
      {
          $sAutocompValue = $g_numero_cajas;
      }
?>
<INPUT  type="text" id="SC_g_numero_cajas" name="g_numero_cajas" value="<?php echo NM_encode_input($g_numero_cajas) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_g_numero_cajas" name="g_numero_cajas_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_asegurador_tratamiento_cond" name="t_asegurador_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_asegurador_tratamiento'), this, 't_asegurador_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_asegurador_tratamiento" <?php echo $str_hide_t_asegurador_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO";
 $nmgp_tab_label .= "t_asegurador_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_asegurador_tratamiento_look = substr($this->Db->qstr($t_asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.ASEGURADOR_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ASEGURADOR_TRATAMIENTO = '$t_asegurador_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_asegurador_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_asegurador_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_asegurador_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_asegurador_tratamiento" name="t_asegurador_tratamiento" value="<?php echo NM_encode_input($t_asegurador_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 200, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_asegurador_tratamiento" name="t_asegurador_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_operador_logistico_tratamiento_cond" name="t_operador_logistico_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_operador_logistico_tratamiento'), this, 't_operador_logistico_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_operador_logistico_tratamiento" <?php echo $str_hide_t_operador_logistico_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO";
 $nmgp_tab_label .= "t_operador_logistico_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_operador_logistico_tratamiento_look = substr($this->Db->qstr($t_operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.OPERADOR_LOGISTICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.OPERADOR_LOGISTICO_TRATAMIENTO = '$t_operador_logistico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_operador_logistico_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_operador_logistico_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_operador_logistico_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_operador_logistico_tratamiento" name="t_operador_logistico_tratamiento" value="<?php echo NM_encode_input($t_operador_logistico_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 60, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_operador_logistico_tratamiento" name="t_operador_logistico_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_punto_entrega_cond" name="t_punto_entrega_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_punto_entrega'), this, 't_punto_entrega')">
       <OPTION value="qp" <?php if ("qp" == $t_punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_punto_entrega" <?php echo $str_hide_t_punto_entrega?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA";
 $nmgp_tab_label .= "t_punto_entrega?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_punto_entrega_look = substr($this->Db->qstr($t_punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PUNTO_ENTREGA from " . $this->Ini->nm_tabela . " where t.PUNTO_ENTREGA = '$t_punto_entrega_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_punto_entrega]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_punto_entrega];
      }
      else
      {
          $sAutocompValue = $t_punto_entrega;
      }
?>
<INPUT  type="text" id="SC_t_punto_entrega" name="t_punto_entrega" value="<?php echo NM_encode_input($t_punto_entrega) ?>" size=50 alt="{datatype: 'text', maxLength: 200, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_punto_entrega" name="t_punto_entrega_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_regimen_tratamiento'])) ? $this->New_label['t_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_regimen_tratamiento_cond" name="t_regimen_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_regimen_tratamiento'), this, 't_regimen_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_regimen_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_regimen_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_regimen_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_regimen_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_regimen_tratamiento" <?php echo $str_hide_t_regimen_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_regimen_tratamiento'])) ? $this->New_label['t_regimen_tratamiento'] : "REGIMEN TRATAMIENTO";
 $nmgp_tab_label .= "t_regimen_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_regimen_tratamiento_look = substr($this->Db->qstr($t_regimen_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.REGIMEN_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.REGIMEN_TRATAMIENTO = '$t_regimen_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_regimen_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_regimen_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_regimen_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_regimen_tratamiento" name="t_regimen_tratamiento" value="<?php echo NM_encode_input($t_regimen_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 60, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_regimen_tratamiento" name="t_regimen_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_medico_tratamiento_cond" name="t_medico_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_medico_tratamiento'), this, 't_medico_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $t_medico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_medico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_medico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_medico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_medico_tratamiento" <?php echo $str_hide_t_medico_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO";
 $nmgp_tab_label .= "t_medico_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_medico_tratamiento_look = substr($this->Db->qstr($t_medico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.MEDICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.MEDICO_TRATAMIENTO = '$t_medico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_medico_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_medico_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_medico_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_medico_tratamiento" name="t_medico_tratamiento" value="<?php echo NM_encode_input($t_medico_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 60, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_medico_tratamiento" name="t_medico_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_tratamiento_previo'])) ? $this->New_label['t_tratamiento_previo'] : "TRATAMIENTO PREVIO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_tratamiento_previo_cond" name="t_tratamiento_previo_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_tratamiento_previo'), this, 't_tratamiento_previo')">
       <OPTION value="qp" <?php if ("qp" == $t_tratamiento_previo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $t_tratamiento_previo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_tratamiento_previo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $t_tratamiento_previo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_tratamiento_previo" <?php echo $str_hide_t_tratamiento_previo?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_tratamiento_previo'])) ? $this->New_label['t_tratamiento_previo'] : "TRATAMIENTO PREVIO";
 $nmgp_tab_label .= "t_tratamiento_previo?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_tratamiento_previo_look = substr($this->Db->qstr($t_tratamiento_previo), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.TRATAMIENTO_PREVIO from " . $this->Ini->nm_tabela . " where t.TRATAMIENTO_PREVIO = '$t_tratamiento_previo_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$t_tratamiento_previo]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_tratamiento_previo];
      }
      else
      {
          $sAutocompValue = $t_tratamiento_previo;
      }
?>
<INPUT  type="text" id="SC_t_tratamiento_previo" name="t_tratamiento_previo" value="<?php echo NM_encode_input($t_tratamiento_previo) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_tratamiento_previo" name="t_tratamiento_previo_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['g_reclamo_gestion'])) ? $this->New_label['g_reclamo_gestion'] : "RECLAMO GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_g_reclamo_gestion_cond" name="g_reclamo_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_reclamo_gestion'), this, 'g_reclamo_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_reclamo_gestion" <?php echo $str_hide_g_reclamo_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['g_reclamo_gestion'])) ? $this->New_label['g_reclamo_gestion'] : "RECLAMO GESTION";
 $nmgp_tab_label .= "g_reclamo_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_reclamo_gestion_look = substr($this->Db->qstr($g_reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.RECLAMO_GESTION from " . $this->Ini->nm_tabela . " where g.RECLAMO_GESTION = '$g_reclamo_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_reclamo_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_reclamo_gestion];
      }
      else
      {
          $sAutocompValue = $g_reclamo_gestion;
      }
?>
<INPUT  type="text" id="SC_g_reclamo_gestion" name="g_reclamo_gestion" value="<?php echo NM_encode_input($g_reclamo_gestion) ?>" size=50 alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_g_reclamo_gestion" name="g_reclamo_gestion_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['g_fecha_reclamacion_gestion'])) ? $this->New_label['g_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_g_fecha_reclamacion_gestion_cond" name="g_fecha_reclamacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_fecha_reclamacion_gestion'), this, 'g_fecha_reclamacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_fecha_reclamacion_gestion" <?php echo $str_hide_g_fecha_reclamacion_gestion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['g_fecha_reclamacion_gestion'])) ? $this->New_label['g_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION";
 $nmgp_tab_label .= "g_fecha_reclamacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_fecha_reclamacion_gestion_look = substr($this->Db->qstr($g_fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where g.FECHA_RECLAMACION_GESTION = '$g_fecha_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_fecha_reclamacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_fecha_reclamacion_gestion];
      }
      else
      {
          $sAutocompValue = $g_fecha_reclamacion_gestion;
      }
?>
<INPUT  type="text" id="SC_g_fecha_reclamacion_gestion" name="g_fecha_reclamacion_gestion" value="<?php echo NM_encode_input($g_fecha_reclamacion_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_g_fecha_reclamacion_gestion" name="g_fecha_reclamacion_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['g_causa_no_reclamacion_gestion'])) ? $this->New_label['g_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_g_causa_no_reclamacion_gestion_cond" name="g_causa_no_reclamacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_causa_no_reclamacion_gestion'), this, 'g_causa_no_reclamacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_causa_no_reclamacion_gestion" <?php echo $str_hide_g_causa_no_reclamacion_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['g_causa_no_reclamacion_gestion'])) ? $this->New_label['g_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION";
 $nmgp_tab_label .= "g_causa_no_reclamacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_causa_no_reclamacion_gestion_look = substr($this->Db->qstr($g_causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.CAUSA_NO_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where g.CAUSA_NO_RECLAMACION_GESTION = '$g_causa_no_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_causa_no_reclamacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_causa_no_reclamacion_gestion];
      }
      else
      {
          $sAutocompValue = $g_causa_no_reclamacion_gestion;
      }
?>
<INPUT  type="text" id="SC_g_causa_no_reclamacion_gestion" name="g_causa_no_reclamacion_gestion" value="<?php echo NM_encode_input($g_causa_no_reclamacion_gestion) ?>" size=50 alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_g_causa_no_reclamacion_gestion" name="g_causa_no_reclamacion_gestion_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['g_descripcion_comunicacion_gestion'])) ? $this->New_label['g_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_g_descripcion_comunicacion_gestion_cond" name="g_descripcion_comunicacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_descripcion_comunicacion_gestion'), this, 'g_descripcion_comunicacion_gestion')">
       <OPTION value="eq" <?php if ("eq" == $g_descripcion_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ii" <?php if ("ii" == $g_descripcion_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_stts_with'] ?></OPTION>
       <OPTION value="qp" <?php if ("qp" == $g_descripcion_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="df" <?php if ("df" == $g_descripcion_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_dife'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_descripcion_comunicacion_gestion" <?php echo $str_hide_g_descripcion_comunicacion_gestion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['g_descripcion_comunicacion_gestion'])) ? $this->New_label['g_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION";
 $nmgp_tab_label .= "g_descripcion_comunicacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<INPUT  type="text" id="SC_g_descripcion_comunicacion_gestion" name="g_descripcion_comunicacion_gestion" value="<?php echo NM_encode_input($g_descripcion_comunicacion_gestion) ?>" size="50" maxlength="32767" class="scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['g_fecha_programada_gestion'])) ? $this->New_label['g_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_g_fecha_programada_gestion_cond" name="g_fecha_programada_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_g_fecha_programada_gestion'), this, 'g_fecha_programada_gestion')">
       <OPTION value="qp" <?php if ("qp" == $g_fecha_programada_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $g_fecha_programada_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $g_fecha_programada_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $g_fecha_programada_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_g_fecha_programada_gestion" <?php echo $str_hide_g_fecha_programada_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['g_fecha_programada_gestion'])) ? $this->New_label['g_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION";
 $nmgp_tab_label .= "g_fecha_programada_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $g_fecha_programada_gestion_look = substr($this->Db->qstr($g_fecha_programada_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_PROGRAMADA_GESTION from " . $this->Ini->nm_tabela . " where g.FECHA_PROGRAMADA_GESTION = '$g_fecha_programada_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$g_fecha_programada_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$g_fecha_programada_gestion];
      }
      else
      {
          $sAutocompValue = $g_fecha_programada_gestion;
      }
?>
<INPUT  type="text" id="SC_g_fecha_programada_gestion" name="g_fecha_programada_gestion" value="<?php echo NM_encode_input($g_fecha_programada_gestion) ?>" size=20 alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_g_fecha_programada_gestion" name="g_fecha_programada_gestion_autocomp" size="20" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['p_id_ultima_gestion'])) ? $this->New_label['p_id_ultima_gestion'] : "ID ULTIMA GESTION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_p_id_ultima_gestion_cond" name="p_id_ultima_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_p_id_ultima_gestion'), this, 'p_id_ultima_gestion')">
       <OPTION value="gt" <?php if ("gt" == $p_id_ultima_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $p_id_ultima_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $p_id_ultima_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $p_id_ultima_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_p_id_ultima_gestion" <?php echo $str_hide_p_id_ultima_gestion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['p_id_ultima_gestion'])) ? $this->New_label['p_id_ultima_gestion'] : "ID ULTIMA GESTION";
 $nmgp_tab_label .= "p_id_ultima_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $p_id_ultima_gestion_look = substr($this->Db->qstr($p_id_ultima_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_id_ultima_gestion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where p.ID_ULTIMA_GESTION = $p_id_ultima_gestion_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where p.ID_ULTIMA_GESTION = $p_id_ultima_gestion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$p_id_ultima_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$p_id_ultima_gestion];
      }
      else
      {
          $sAutocompValue = $p_id_ultima_gestion;
      }
?>
<INPUT  type="text" id="SC_p_id_ultima_gestion" name="p_id_ultima_gestion" value="<?php echo NM_encode_input($p_id_ultima_gestion) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_p_id_ultima_gestion" name="p_id_ultima_gestion_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_p_id_ultima_gestion"  <?php echo $str_display_p_id_ultima_gestion; ?> class="scFilterFieldFontOdd">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_p_id_ultima_gestion_input_2" name="p_id_ultima_gestion_input_2" value="<?php echo NM_encode_input($p_id_ultima_gestion_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['t_id_tratamiento'])) ? $this->New_label['t_id_tratamiento'] : "ID TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_t_id_tratamiento_cond" name="t_id_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_id_tratamiento'), this, 't_id_tratamiento')">
       <OPTION value="gt" <?php if ("gt" == $t_id_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $t_id_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_id_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $t_id_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_id_tratamiento" <?php echo $str_hide_t_id_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['t_id_tratamiento'])) ? $this->New_label['t_id_tratamiento'] : "ID TRATAMIENTO";
 $nmgp_tab_label .= "t_id_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_id_tratamiento_look = substr($this->Db->qstr($t_id_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($t_id_tratamiento))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ID_TRATAMIENTO = $t_id_tratamiento_look"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ID_TRATAMIENTO = $t_id_tratamiento_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$t_id_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_id_tratamiento];
      }
      else
      {
          $sAutocompValue = $t_id_tratamiento;
      }
?>
<INPUT  type="text" id="SC_t_id_tratamiento" name="t_id_tratamiento" value="<?php echo NM_encode_input($t_id_tratamiento) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_t_id_tratamiento" name="t_id_tratamiento_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_t_id_tratamiento"  <?php echo $str_display_t_id_tratamiento; ?> class="scFilterFieldFontEven">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_t_id_tratamiento_input_2" name="t_id_tratamiento_input_2" value="<?php echo NM_encode_input($t_id_tratamiento_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectEven">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['t_id_paciente_fk'])) ? $this->New_label['t_id_paciente_fk'] : "ID PACIENTE FK"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_t_id_paciente_fk_cond" name="t_id_paciente_fk_cond" onChange="nm_campos_between(document.getElementById('id_vis_t_id_paciente_fk'), this, 't_id_paciente_fk')">
       <OPTION value="gt" <?php if ("gt" == $t_id_paciente_fk_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $t_id_paciente_fk_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $t_id_paciente_fk_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $t_id_paciente_fk_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_t_id_paciente_fk" <?php echo $str_hide_t_id_paciente_fk?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['t_id_paciente_fk'])) ? $this->New_label['t_id_paciente_fk'] : "ID PACIENTE FK";
 $nmgp_tab_label .= "t_id_paciente_fk?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $t_id_paciente_fk_look = substr($this->Db->qstr($t_id_paciente_fk), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($t_id_paciente_fk))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where t.ID_PACIENTE_FK = $t_id_paciente_fk_look"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where t.ID_PACIENTE_FK = $t_id_paciente_fk_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$t_id_paciente_fk]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$t_id_paciente_fk];
      }
      else
      {
          $sAutocompValue = $t_id_paciente_fk;
      }
?>
<INPUT  type="text" id="SC_t_id_paciente_fk" name="t_id_paciente_fk" value="<?php echo NM_encode_input($t_id_paciente_fk) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_t_id_paciente_fk" name="t_id_paciente_fk_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_t_id_paciente_fk"  <?php echo $str_display_t_id_paciente_fk; ?> class="scFilterFieldFontOdd">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_t_id_paciente_fk_input_2" name="t_id_paciente_fk_input_2" value="<?php echo NM_encode_input($t_id_paciente_fk_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr>
   </TABLE>
  </TD>
 </TR>
 </TABLE>
 </TD>
 </TR>
 <TR>
  <TD class="scFilterTableTd" align="center">
<INPUT type="hidden" id="SC_NM_operador" name="NM_operador" value="and">  </TD>
 </TR>
   <INPUT type="hidden" name="nmgp_tab_label" value="<?php echo NM_encode_input($nmgp_tab_label); ?>"> 
   <INPUT type="hidden" name="bprocessa" value="pesq"> 
 <?php
     if ($_SESSION['scriptcase']['proc_mobile'])
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
   <?php echo nmButtonOutput($this->arr_buttons, "bpesquisa", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "sc_b_pesq_bot", "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone'] . "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone_hint'] . "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   if ($this->nmgp_botoes['clear'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "blimpar", "limpa_form()", "limpa_form()", "limpa_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (!isset($this->nmgp_botoes['save']) || $this->nmgp_botoes['save'] == "on")
   {
       $this->NM_fil_ant = $this->gera_array_filtros();
?>
     <span id="idAjaxSelect_NM_filters_bot">
       <SELECT class="scFilterToolbar_obj" id="sel_recup_filters_bot" name="NM_filters_bot" onChange="nm_submit_filter(this, 'bot')" size="1">
           <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "           <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
           <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
       </SELECT>
     </span>
<?php
   }
?>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bedit_filter", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "Ativa_save_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (is_file("informe_reclamacion_help.txt"))
   {
      $Arq_WebHelp = file("informe_reclamacion_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start'] == 'filter' && $nm_apl_dependente != 1)
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bsair", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_bot" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "Cancel_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_bot" name="nmgp_save_name_bot" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('bot')", "nm_save_form('bot')", "Save_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_bot" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_bot">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_bot" name="NM_filters_del_bot" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('bot')", "nm_submit_filter_del('bot')", "Exc_filtro_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
     else
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
   <?php echo nmButtonOutput($this->arr_buttons, "bpesquisa", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "sc_b_pesq_bot", "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone'] . "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone_hint'] . "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   if ($this->nmgp_botoes['clear'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "blimpar", "limpa_form()", "limpa_form()", "limpa_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (!isset($this->nmgp_botoes['save']) || $this->nmgp_botoes['save'] == "on")
   {
       $this->NM_fil_ant = $this->gera_array_filtros();
?>
     <span id="idAjaxSelect_NM_filters_bot">
       <SELECT class="scFilterToolbar_obj" id="sel_recup_filters_bot" name="NM_filters_bot" onChange="nm_submit_filter(this, 'bot')" size="1">
           <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "           <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
           <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
       </SELECT>
     </span>
<?php
   }
?>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bedit_filter", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "Ativa_save_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (is_file("informe_reclamacion_help.txt"))
   {
      $Arq_WebHelp = file("informe_reclamacion_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start'] == 'filter' && $nm_apl_dependente != 1)
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bsair", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_bot" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "Cancel_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_bot" name="nmgp_save_name_bot" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('bot')", "nm_save_form('bot')", "Save_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_bot" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_bot">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_bot" name="NM_filters_del_bot" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('bot')", "nm_submit_filter_del('bot')", "Exc_filtro_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
 ?>
<?php
   }

   function monta_html_fim()
   {
       global $bprocessa, $nm_url_saida, $Script_BI;
?>

</TABLE>
   <INPUT type="hidden" name="form_condicao" value="3">
</FORM> 
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['start'] == 'filter')
   {
?>
   <FORM style="display:none;" name="form_cancel"  method="POST" action="<?php echo $nm_url_saida; ?>" target="_self"> 
<?php
   }
   else
   {
?>
   <FORM style="display:none;" name="form_cancel"  method="POST" action="./" target="_self"> 
<?php
   }
?>
   <INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
   <INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<?php
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['orig_pesq'] == "grid")
   {
       $Ret_cancel_pesq = "volta_grid";
   }
   else
   {
       $Ret_cancel_pesq = "resumo";
   }
?>
   <INPUT type="hidden" name="nmgp_opcao" value="<?php echo $Ret_cancel_pesq; ?>"> 
   </FORM> 
<SCRIPT type="text/javascript">
<?php
   if (empty($this->NM_fil_ant))
   {
?>
      document.getElementById('Apaga_filters_bot').style.display = 'none';
      document.getElementById('sel_recup_filters_bot').style.display = 'none';
<?php
   }
?>
 function nm_submit_form()
 {
    document.F1.submit();
 }
 function limpa_form()
 {
   document.F1.reset();
   if (document.F1.NM_filters)
   {
       document.F1.NM_filters.selectedIndex = -1;
   }
   document.getElementById('Salvar_filters_bot').style.display = 'none';
   nm_campos_between(document.getElementById('id_vis_p_id_paciente'), document.F1.p_id_paciente_cond, 'p_id_paciente');
   document.F1.p_id_paciente.value = "";
   document.F1.p_id_paciente_autocomp.value = "";
   document.F1.p_id_paciente_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_g_logro_comunicacion_gestion'), document.F1.g_logro_comunicacion_gestion_cond, 'g_logro_comunicacion_gestion');
   document.F1.g_logro_comunicacion_gestion.value = "";
   document.F1.g_logro_comunicacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_fecha_comunicacion'), document.F1.g_fecha_comunicacion_cond, 'g_fecha_comunicacion');
   document.F1.g_fecha_comunicacion.value = "";
   document.F1.g_fecha_comunicacion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_autor_gestion'), document.F1.g_autor_gestion_cond, 'g_autor_gestion');
   document.F1.g_autor_gestion.value = "";
   document.F1.g_autor_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_pais_paciente'), document.F1.p_pais_paciente_cond, 'p_pais_paciente');
   document.F1.p_pais_paciente.value = "";
   document.F1.p_pais_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_ciudad_paciente'), document.F1.p_ciudad_paciente_cond, 'p_ciudad_paciente');
   document.F1.p_ciudad_paciente.value = "";
   document.F1.p_ciudad_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_estado_paciente'), document.F1.p_estado_paciente_cond, 'p_estado_paciente');
   document.F1.p_estado_paciente.value = "";
   document.F1.p_estado_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_fecha_activacion_paciente'), document.F1.p_fecha_activacion_paciente_cond, 'p_fecha_activacion_paciente');
   document.F1.p_fecha_activacion_paciente.value = "";
   document.F1.p_fecha_activacion_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_fecha_retiro_paciente'), document.F1.p_fecha_retiro_paciente_cond, 'p_fecha_retiro_paciente');
   document.F1.p_fecha_retiro_paciente.value = "";
   document.F1.p_fecha_retiro_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_motivo_retiro_paciente'), document.F1.p_motivo_retiro_paciente_cond, 'p_motivo_retiro_paciente');
   document.F1.p_motivo_retiro_paciente.value = "";
   document.F1.p_motivo_retiro_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_consentimiento_tratamiento'), document.F1.t_consentimiento_tratamiento_cond, 't_consentimiento_tratamiento');
   document.F1.t_consentimiento_tratamiento.value = "";
   document.F1.t_consentimiento_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_codigo_xofigo'), document.F1.p_codigo_xofigo_cond, 'p_codigo_xofigo');
   document.F1.p_codigo_xofigo.value = "";
   document.F1.p_codigo_xofigo_autocomp.value = "";
   document.F1.p_codigo_xofigo_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_t_clasificacion_patologica_tratamiento'), document.F1.t_clasificacion_patologica_tratamiento_cond, 't_clasificacion_patologica_tratamiento');
   document.F1.t_clasificacion_patologica_tratamiento.value = "";
   document.F1.t_clasificacion_patologica_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_producto_tratamiento'), document.F1.t_producto_tratamiento_cond, 't_producto_tratamiento');
   document.F1.t_producto_tratamiento.value = "";
   document.F1.t_producto_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_nombre_referencia'), document.F1.t_nombre_referencia_cond, 't_nombre_referencia');
   document.F1.t_nombre_referencia.value = "";
   document.F1.t_nombre_referencia_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_dosis_tratamiento'), document.F1.t_dosis_tratamiento_cond, 't_dosis_tratamiento');
   document.F1.t_dosis_tratamiento.value = "";
   document.F1.t_dosis_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_numero_cajas'), document.F1.g_numero_cajas_cond, 'g_numero_cajas');
   document.F1.g_numero_cajas.value = "";
   document.F1.g_numero_cajas_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_asegurador_tratamiento'), document.F1.t_asegurador_tratamiento_cond, 't_asegurador_tratamiento');
   document.F1.t_asegurador_tratamiento.value = "";
   document.F1.t_asegurador_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_operador_logistico_tratamiento'), document.F1.t_operador_logistico_tratamiento_cond, 't_operador_logistico_tratamiento');
   document.F1.t_operador_logistico_tratamiento.value = "";
   document.F1.t_operador_logistico_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_punto_entrega'), document.F1.t_punto_entrega_cond, 't_punto_entrega');
   document.F1.t_punto_entrega.value = "";
   document.F1.t_punto_entrega_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_regimen_tratamiento'), document.F1.t_regimen_tratamiento_cond, 't_regimen_tratamiento');
   document.F1.t_regimen_tratamiento.value = "";
   document.F1.t_regimen_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_medico_tratamiento'), document.F1.t_medico_tratamiento_cond, 't_medico_tratamiento');
   document.F1.t_medico_tratamiento.value = "";
   document.F1.t_medico_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_t_tratamiento_previo'), document.F1.t_tratamiento_previo_cond, 't_tratamiento_previo');
   document.F1.t_tratamiento_previo.value = "";
   document.F1.t_tratamiento_previo_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_reclamo_gestion'), document.F1.g_reclamo_gestion_cond, 'g_reclamo_gestion');
   document.F1.g_reclamo_gestion.value = "";
   document.F1.g_reclamo_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_fecha_reclamacion_gestion'), document.F1.g_fecha_reclamacion_gestion_cond, 'g_fecha_reclamacion_gestion');
   document.F1.g_fecha_reclamacion_gestion.value = "";
   document.F1.g_fecha_reclamacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_causa_no_reclamacion_gestion'), document.F1.g_causa_no_reclamacion_gestion_cond, 'g_causa_no_reclamacion_gestion');
   document.F1.g_causa_no_reclamacion_gestion.value = "";
   document.F1.g_causa_no_reclamacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_g_descripcion_comunicacion_gestion'), document.F1.g_descripcion_comunicacion_gestion_cond, 'g_descripcion_comunicacion_gestion');
   document.F1.g_descripcion_comunicacion_gestion.value = "";
   nm_campos_between(document.getElementById('id_vis_g_fecha_programada_gestion'), document.F1.g_fecha_programada_gestion_cond, 'g_fecha_programada_gestion');
   document.F1.g_fecha_programada_gestion.value = "";
   document.F1.g_fecha_programada_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_p_id_ultima_gestion'), document.F1.p_id_ultima_gestion_cond, 'p_id_ultima_gestion');
   document.F1.p_id_ultima_gestion.value = "";
   document.F1.p_id_ultima_gestion_autocomp.value = "";
   document.F1.p_id_ultima_gestion_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_t_id_tratamiento'), document.F1.t_id_tratamiento_cond, 't_id_tratamiento');
   document.F1.t_id_tratamiento.value = "";
   document.F1.t_id_tratamiento_autocomp.value = "";
   document.F1.t_id_tratamiento_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_t_id_paciente_fk'), document.F1.t_id_paciente_fk_cond, 't_id_paciente_fk');
   document.F1.t_id_paciente_fk.value = "";
   document.F1.t_id_paciente_fk_autocomp.value = "";
   document.F1.t_id_paciente_fk_input_2.value = "";
 }
function nm_tabula(obj, tam, cond)
{
   if (obj.value.length == tam)
   {
       for (i=0; i < document.F1.elements.length;i++)
       {
            if (document.F1.elements[i].name == obj.name)
            {
                i++;
                campo = document.F1.elements[i].name;
                campo2 = campo.lastIndexOf('_input_2');
                if (document.F1.elements[i].type == 'text' && (campo2 == -1 || cond == 'bw'))
                {
                    eval('document.F1.' + campo + '.focus()');
                }
                break;
            }
       }
   }
}
 function SC_carga_evt_jquery()
 {
 }
</SCRIPT>
</BODY>
</HTML>
<?php
   }

   function gera_array_filtros()
   {
       $this->NM_fil_ant = array();
       $NM_patch   = "bayer_c_a/informe_reclamacion";
       if (is_dir($this->NM_path_filter . $NM_patch))
       {
           $NM_dir = @opendir($this->NM_path_filter . $NM_patch);
           while (FALSE !== ($NM_arq = @readdir($NM_dir)))
           {
             if (@is_file($this->NM_path_filter . $NM_patch . "/" . $NM_arq))
             {
                 $Sc_v6 = false;
                 $NMcmp_filter = file($this->NM_path_filter . $NM_patch . "/" . $NM_arq);
                 $NMcmp_filter = explode("@NMF@", $NMcmp_filter[0]);
                 if (substr($NMcmp_filter[0], 0, 6) == "SC_V6_" || substr($NMcmp_filter[0], 0, 6) == "SC_V8_")
                 {
                     $Name_filter = substr($NMcmp_filter[0], 6);
                     if (!empty($Name_filter))
                     {
                         $nmgp_save_name = str_replace('/', ' ', $Name_filter);
                         $nmgp_save_name = str_replace('\\', ' ', $nmgp_save_name);
                         $nmgp_save_name = str_replace('.', ' ', $nmgp_save_name);
                         $this->NM_fil_ant[$Name_filter][0] = $NM_patch . "/" . $nmgp_save_name;
                         $this->NM_fil_ant[$Name_filter][1] = "" . $this->Ini->Nm_lang['lang_srch_public'] . "";
                         $Sc_v6 = true;
                     }
                 }
                 if (!$Sc_v6)
                 {
                     $this->NM_fil_ant[$NM_arq][0] = $NM_patch . "/" . $NM_arq;
                     $this->NM_fil_ant[$NM_arq][1] = "" . $this->Ini->Nm_lang['lang_srch_public'] . "";
                 }
             }
           }
       }
       return $this->NM_fil_ant;
   }
   /**
    * @access  public
    * @param  string  $NM_operador  $this->Ini->Nm_lang['pesq_global_NM_operador']
    * @param  array  $nmgp_tab_label  
    */
   function inicializa_vars()
   {
      global $NM_operador, $nmgp_tab_label;

      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/");  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1);  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz;
      $this->Campos_Mens_erro = ""; 
      $this->nm_data = new nm_data("es");
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] = "";
      if (!empty($nmgp_tab_label))
      {
         $nm_tab_campos = explode("?@?", $nmgp_tab_label);
         $nmgp_tab_label = array();
         foreach ($nm_tab_campos as $cada_campo)
         {
             $parte_campo = explode("?#?", $cada_campo);
             $nmgp_tab_label[$parte_campo[0]] = $parte_campo[1];
         }
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig'] = "";
      }
      $this->comando        = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig'];
      $this->comando_sum    = "";
      $this->comando_filtro = "";
      $this->comando_ini    = "ini";
      $this->comando_fim    = "";
      $this->NM_operador    = (isset($NM_operador) && ("and" == strtolower($NM_operador) || "or" == strtolower($NM_operador))) ? $NM_operador : "and";
   }

   function salva_filtro()
   {
      global $NM_filters_save, $nmgp_save_name, $nmgp_save_option, $script_case_init;
          $NM_filters_save = str_replace("__NM_PLUS__", "+", $NM_filters_save);
          $NM_str_filter  = "SC_V8_" . $nmgp_save_name . "@NMF@";
          $nmgp_save_name = str_replace('/', ' ', $nmgp_save_name);
          $nmgp_save_name = str_replace('\\', ' ', $nmgp_save_name);
          $nmgp_save_name = str_replace('.', ' ', $nmgp_save_name);
          if (!NM_is_utf8($nmgp_save_name))
          {
              $nmgp_save_name = sc_convert_encoding($nmgp_save_name, "UTF-8", $_SESSION['scriptcase']['charset']);
          }
          $NM_str_filter  .= $NM_filters_save;
          $NM_patch = $this->NM_path_filter;
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $NM_patch .= "bayer_c_a/";
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $NM_patch .= "informe_reclamacion/";
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $Parms_usr  = "";
          $NM_filter = fopen ($NM_patch . $nmgp_save_name, 'w');
          if (!NM_is_utf8($NM_str_filter))
          {
              $NM_str_filter = sc_convert_encoding($NM_str_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
          }
          fwrite($NM_filter, $NM_str_filter);
          fclose($NM_filter);
   }
   function recupera_filtro()
   {
      global $NM_filters, $NM_operador, $script_case_init;
      $NM_patch = $this->NM_path_filter . "/" . $NM_filters;
      if (!is_file($NM_patch))
      {
          $NM_filters = sc_convert_encoding($NM_filters, "UTF-8", $_SESSION['scriptcase']['charset']);
          $NM_patch = $this->NM_path_filter . "/" . $NM_filters;
      }
      $return_fields = array();
      $tp_fields     = array();
      $tb_fields_esp = array();
      $tp_fields['SC_p_id_paciente_cond'] = 'cond';
      $tp_fields['SC_p_id_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_id_paciente'] = 'text_aut';
      $tp_fields['SC_p_id_paciente_input_2'] = 'text';
      $tp_fields['SC_g_logro_comunicacion_gestion_cond'] = 'cond';
      $tp_fields['SC_g_logro_comunicacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_logro_comunicacion_gestion'] = 'text_aut';
      $tp_fields['SC_g_fecha_comunicacion_cond'] = 'cond';
      $tp_fields['SC_g_fecha_comunicacion'] = 'text_aut';
      $tp_fields['id_ac_g_fecha_comunicacion'] = 'text_aut';
      $tp_fields['SC_g_autor_gestion_cond'] = 'cond';
      $tp_fields['SC_g_autor_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_autor_gestion'] = 'text_aut';
      $tp_fields['SC_p_pais_paciente_cond'] = 'cond';
      $tp_fields['SC_p_pais_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_pais_paciente'] = 'text_aut';
      $tp_fields['SC_p_ciudad_paciente_cond'] = 'cond';
      $tp_fields['SC_p_ciudad_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_ciudad_paciente'] = 'text_aut';
      $tp_fields['SC_p_estado_paciente_cond'] = 'cond';
      $tp_fields['SC_p_estado_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_estado_paciente'] = 'text_aut';
      $tp_fields['SC_p_fecha_activacion_paciente_cond'] = 'cond';
      $tp_fields['SC_p_fecha_activacion_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_fecha_activacion_paciente'] = 'text_aut';
      $tp_fields['SC_p_fecha_retiro_paciente_cond'] = 'cond';
      $tp_fields['SC_p_fecha_retiro_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_fecha_retiro_paciente'] = 'text_aut';
      $tp_fields['SC_p_motivo_retiro_paciente_cond'] = 'cond';
      $tp_fields['SC_p_motivo_retiro_paciente'] = 'text_aut';
      $tp_fields['id_ac_p_motivo_retiro_paciente'] = 'text_aut';
      $tp_fields['SC_t_consentimiento_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_consentimiento_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_consentimiento_tratamiento'] = 'text_aut';
      $tp_fields['SC_p_codigo_xofigo_cond'] = 'cond';
      $tp_fields['SC_p_codigo_xofigo'] = 'text_aut';
      $tp_fields['id_ac_p_codigo_xofigo'] = 'text_aut';
      $tp_fields['SC_p_codigo_xofigo_input_2'] = 'text';
      $tp_fields['SC_t_clasificacion_patologica_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_clasificacion_patologica_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_clasificacion_patologica_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_producto_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_producto_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_producto_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_nombre_referencia_cond'] = 'cond';
      $tp_fields['SC_t_nombre_referencia'] = 'text_aut';
      $tp_fields['id_ac_t_nombre_referencia'] = 'text_aut';
      $tp_fields['SC_t_dosis_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_dosis_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_dosis_tratamiento'] = 'text_aut';
      $tp_fields['SC_g_numero_cajas_cond'] = 'cond';
      $tp_fields['SC_g_numero_cajas'] = 'text_aut';
      $tp_fields['id_ac_g_numero_cajas'] = 'text_aut';
      $tp_fields['SC_t_asegurador_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_asegurador_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_asegurador_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_operador_logistico_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_operador_logistico_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_operador_logistico_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_punto_entrega_cond'] = 'cond';
      $tp_fields['SC_t_punto_entrega'] = 'text_aut';
      $tp_fields['id_ac_t_punto_entrega'] = 'text_aut';
      $tp_fields['SC_t_regimen_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_regimen_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_regimen_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_medico_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_medico_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_medico_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_tratamiento_previo_cond'] = 'cond';
      $tp_fields['SC_t_tratamiento_previo'] = 'text_aut';
      $tp_fields['id_ac_t_tratamiento_previo'] = 'text_aut';
      $tp_fields['SC_g_reclamo_gestion_cond'] = 'cond';
      $tp_fields['SC_g_reclamo_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_reclamo_gestion'] = 'text_aut';
      $tp_fields['SC_g_fecha_reclamacion_gestion_cond'] = 'cond';
      $tp_fields['SC_g_fecha_reclamacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_fecha_reclamacion_gestion'] = 'text_aut';
      $tp_fields['SC_g_causa_no_reclamacion_gestion_cond'] = 'cond';
      $tp_fields['SC_g_causa_no_reclamacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_causa_no_reclamacion_gestion'] = 'text_aut';
      $tp_fields['SC_g_descripcion_comunicacion_gestion_cond'] = 'cond';
      $tp_fields['SC_g_descripcion_comunicacion_gestion'] = 'text';
      $tp_fields['SC_g_fecha_programada_gestion_cond'] = 'cond';
      $tp_fields['SC_g_fecha_programada_gestion'] = 'text_aut';
      $tp_fields['id_ac_g_fecha_programada_gestion'] = 'text_aut';
      $tp_fields['SC_p_id_ultima_gestion_cond'] = 'cond';
      $tp_fields['SC_p_id_ultima_gestion'] = 'text_aut';
      $tp_fields['id_ac_p_id_ultima_gestion'] = 'text_aut';
      $tp_fields['SC_p_id_ultima_gestion_input_2'] = 'text';
      $tp_fields['SC_t_id_tratamiento_cond'] = 'cond';
      $tp_fields['SC_t_id_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_t_id_tratamiento'] = 'text_aut';
      $tp_fields['SC_t_id_tratamiento_input_2'] = 'text';
      $tp_fields['SC_t_id_paciente_fk_cond'] = 'cond';
      $tp_fields['SC_t_id_paciente_fk'] = 'text_aut';
      $tp_fields['id_ac_t_id_paciente_fk'] = 'text_aut';
      $tp_fields['SC_t_id_paciente_fk_input_2'] = 'text';
      if (is_file($NM_patch))
      {
          $SC_V8    = false;
          $NMfilter = file($NM_patch);
          $NMcmp_filter = explode("@NMF@", $NMfilter[0]);
          if (substr($NMcmp_filter[0], 0, 5) == "SC_V8")
          {
              $SC_V8 = true;
          }
          if (substr($NMcmp_filter[0], 0, 5) == "SC_V6" || substr($NMcmp_filter[0], 0, 5) == "SC_V8")
          {
              unset($NMcmp_filter[0]);
          }
          foreach ($NMcmp_filter as $Cada_cmp)
          {
              $Cada_cmp = explode("#NMF#", $Cada_cmp);
              if (isset($tb_fields_esp[$Cada_cmp[0]]))
              {
                  $Cada_cmp[0] = $tb_fields_esp[$Cada_cmp[0]];
              }
              if (!$SC_V8 && substr($Cada_cmp[0], 0, 11) != "div_ac_lab_" && substr($Cada_cmp[0], 0, 6) != "id_ac_" && substr($Cada_cmp[0], 0, 11) != "NM_operador")
              {
                  $Cada_cmp[0] = "SC_" . $Cada_cmp[0];
              }
              if (!isset($tp_fields[$Cada_cmp[0]]))
              {
                  continue;
              }
              $list   = array();
              $list_a = array();
              if (substr($Cada_cmp[1], 0, 10) == "_NM_array_")
              {
                  if (substr($Cada_cmp[1], 0, 17) == "_NM_array_#NMARR#")
                  {
                      $Sc_temp = explode("#NMARR#", substr($Cada_cmp[1], 17));
                      foreach ($Sc_temp as $Cada_val)
                      {
                          $list[]   = $Cada_val;
                          $tmp_pos  = strpos($Cada_val, "##@@");
                          $val_a    = ($tmp_pos !== false) ?  substr($Cada_val, $tmp_pos + 4) : $Cada_val;
                          $list_a[] = array('opt' => $Cada_val, 'value' => $val_a);
                      }
                  }
              }
              else
              {
                  $list[0] = $Cada_cmp[1];
              }
              if ($tp_fields[$Cada_cmp[0]] == 'dselect')
              {
                  $return_fields['set_dselect'][] = array('field' => $Cada_cmp[0], 'value' => $list_a);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'fil_order')
              {
                  $return_fields['set_fil_order'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'selmult')
              {
                  $return_fields['set_selmult'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'checkbox')
              {
                  $return_fields['set_checkbox'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              else
              {
                  if (!isset($list[0]))
                  {
                      $list[0] = "";
                  }
                  if ($tp_fields[$Cada_cmp[0]] == 'html')
                  {
                      $return_fields['set_html'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
                  elseif ($tp_fields[$Cada_cmp[0]] == 'radio')
                  {
                      $return_fields['set_radio'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
                  else
                  {
                      $return_fields['set_val'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
              }
          }
          $this->NM_curr_fil = $NM_filters;
      }
      return $return_fields;
   }
   function apaga_filtro()
   {
      global $NM_filters_del;
      if (isset($NM_filters_del) && !empty($NM_filters_del))
      { 
          $NM_patch = $this->NM_path_filter . "/" . $NM_filters_del;
          if (!is_file($NM_patch))
          {
              $NM_filters_del = sc_convert_encoding($NM_filters_del, "UTF-8", $_SESSION['scriptcase']['charset']);
              $NM_patch = $this->NM_path_filter . "/" . $NM_filters_del;
          }
          if (is_file($NM_patch))
          {
              @unlink($NM_patch);
          }
          if ($NM_filters_del == $this->NM_curr_fil)
          {
              $this->NM_curr_fil = "";
          }
      }
   }
   /**
    * @access  public
    */
   function trata_campos()
   {
      global $p_id_paciente_cond, $p_id_paciente, $p_id_paciente_input_2, $p_id_paciente_autocomp,
             $g_logro_comunicacion_gestion_cond, $g_logro_comunicacion_gestion, $g_logro_comunicacion_gestion_autocomp,
             $g_fecha_comunicacion_cond, $g_fecha_comunicacion, $g_fecha_comunicacion_autocomp,
             $g_autor_gestion_cond, $g_autor_gestion, $g_autor_gestion_autocomp,
             $p_pais_paciente_cond, $p_pais_paciente, $p_pais_paciente_autocomp,
             $p_ciudad_paciente_cond, $p_ciudad_paciente, $p_ciudad_paciente_autocomp,
             $p_estado_paciente_cond, $p_estado_paciente, $p_estado_paciente_autocomp,
             $p_fecha_activacion_paciente_cond, $p_fecha_activacion_paciente, $p_fecha_activacion_paciente_autocomp,
             $p_fecha_retiro_paciente_cond, $p_fecha_retiro_paciente, $p_fecha_retiro_paciente_autocomp,
             $p_motivo_retiro_paciente_cond, $p_motivo_retiro_paciente, $p_motivo_retiro_paciente_autocomp,
             $t_consentimiento_tratamiento_cond, $t_consentimiento_tratamiento, $t_consentimiento_tratamiento_autocomp,
             $p_codigo_xofigo_cond, $p_codigo_xofigo, $p_codigo_xofigo_input_2, $p_codigo_xofigo_autocomp,
             $t_clasificacion_patologica_tratamiento_cond, $t_clasificacion_patologica_tratamiento, $t_clasificacion_patologica_tratamiento_autocomp,
             $t_producto_tratamiento_cond, $t_producto_tratamiento, $t_producto_tratamiento_autocomp,
             $t_nombre_referencia_cond, $t_nombre_referencia, $t_nombre_referencia_autocomp,
             $t_dosis_tratamiento_cond, $t_dosis_tratamiento, $t_dosis_tratamiento_autocomp,
             $g_numero_cajas_cond, $g_numero_cajas, $g_numero_cajas_autocomp,
             $t_asegurador_tratamiento_cond, $t_asegurador_tratamiento, $t_asegurador_tratamiento_autocomp,
             $t_operador_logistico_tratamiento_cond, $t_operador_logistico_tratamiento, $t_operador_logistico_tratamiento_autocomp,
             $t_punto_entrega_cond, $t_punto_entrega, $t_punto_entrega_autocomp,
             $t_regimen_tratamiento_cond, $t_regimen_tratamiento, $t_regimen_tratamiento_autocomp,
             $t_medico_tratamiento_cond, $t_medico_tratamiento, $t_medico_tratamiento_autocomp,
             $t_tratamiento_previo_cond, $t_tratamiento_previo, $t_tratamiento_previo_autocomp,
             $g_reclamo_gestion_cond, $g_reclamo_gestion, $g_reclamo_gestion_autocomp,
             $g_fecha_reclamacion_gestion_cond, $g_fecha_reclamacion_gestion, $g_fecha_reclamacion_gestion_autocomp,
             $g_causa_no_reclamacion_gestion_cond, $g_causa_no_reclamacion_gestion, $g_causa_no_reclamacion_gestion_autocomp,
             $g_descripcion_comunicacion_gestion_cond, $g_descripcion_comunicacion_gestion,
             $g_fecha_programada_gestion_cond, $g_fecha_programada_gestion, $g_fecha_programada_gestion_autocomp,
             $p_id_ultima_gestion_cond, $p_id_ultima_gestion, $p_id_ultima_gestion_input_2, $p_id_ultima_gestion_autocomp,
             $t_id_tratamiento_cond, $t_id_tratamiento, $t_id_tratamiento_input_2, $t_id_tratamiento_autocomp,
             $t_id_paciente_fk_cond, $t_id_paciente_fk, $t_id_paciente_fk_input_2, $t_id_paciente_fk_autocomp, $nmgp_tab_label;

      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_limpa.php", "F", "nm_limpa_valor") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_conv_dados.php", "F", "nm_conv_limpa_dado") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
      if (!empty($p_id_paciente_autocomp) && empty($p_id_paciente))
      {
          $p_id_paciente = $p_id_paciente_autocomp;
      }
      if (!empty($g_logro_comunicacion_gestion_autocomp) && empty($g_logro_comunicacion_gestion))
      {
          $g_logro_comunicacion_gestion = $g_logro_comunicacion_gestion_autocomp;
      }
      if (!empty($g_fecha_comunicacion_autocomp) && empty($g_fecha_comunicacion))
      {
          $g_fecha_comunicacion = $g_fecha_comunicacion_autocomp;
      }
      if (!empty($g_autor_gestion_autocomp) && empty($g_autor_gestion))
      {
          $g_autor_gestion = $g_autor_gestion_autocomp;
      }
      if (!empty($p_pais_paciente_autocomp) && empty($p_pais_paciente))
      {
          $p_pais_paciente = $p_pais_paciente_autocomp;
      }
      if (!empty($p_ciudad_paciente_autocomp) && empty($p_ciudad_paciente))
      {
          $p_ciudad_paciente = $p_ciudad_paciente_autocomp;
      }
      if (!empty($p_estado_paciente_autocomp) && empty($p_estado_paciente))
      {
          $p_estado_paciente = $p_estado_paciente_autocomp;
      }
      if (!empty($p_fecha_activacion_paciente_autocomp) && empty($p_fecha_activacion_paciente))
      {
          $p_fecha_activacion_paciente = $p_fecha_activacion_paciente_autocomp;
      }
      if (!empty($p_fecha_retiro_paciente_autocomp) && empty($p_fecha_retiro_paciente))
      {
          $p_fecha_retiro_paciente = $p_fecha_retiro_paciente_autocomp;
      }
      if (!empty($p_motivo_retiro_paciente_autocomp) && empty($p_motivo_retiro_paciente))
      {
          $p_motivo_retiro_paciente = $p_motivo_retiro_paciente_autocomp;
      }
      if (!empty($t_consentimiento_tratamiento_autocomp) && empty($t_consentimiento_tratamiento))
      {
          $t_consentimiento_tratamiento = $t_consentimiento_tratamiento_autocomp;
      }
      if (!empty($p_codigo_xofigo_autocomp) && empty($p_codigo_xofigo))
      {
          $p_codigo_xofigo = $p_codigo_xofigo_autocomp;
      }
      if (!empty($t_clasificacion_patologica_tratamiento_autocomp) && empty($t_clasificacion_patologica_tratamiento))
      {
          $t_clasificacion_patologica_tratamiento = $t_clasificacion_patologica_tratamiento_autocomp;
      }
      if (!empty($t_producto_tratamiento_autocomp) && empty($t_producto_tratamiento))
      {
          $t_producto_tratamiento = $t_producto_tratamiento_autocomp;
      }
      if (!empty($t_nombre_referencia_autocomp) && empty($t_nombre_referencia))
      {
          $t_nombre_referencia = $t_nombre_referencia_autocomp;
      }
      if (!empty($t_dosis_tratamiento_autocomp) && empty($t_dosis_tratamiento))
      {
          $t_dosis_tratamiento = $t_dosis_tratamiento_autocomp;
      }
      if (!empty($g_numero_cajas_autocomp) && empty($g_numero_cajas))
      {
          $g_numero_cajas = $g_numero_cajas_autocomp;
      }
      if (!empty($t_asegurador_tratamiento_autocomp) && empty($t_asegurador_tratamiento))
      {
          $t_asegurador_tratamiento = $t_asegurador_tratamiento_autocomp;
      }
      if (!empty($t_operador_logistico_tratamiento_autocomp) && empty($t_operador_logistico_tratamiento))
      {
          $t_operador_logistico_tratamiento = $t_operador_logistico_tratamiento_autocomp;
      }
      if (!empty($t_punto_entrega_autocomp) && empty($t_punto_entrega))
      {
          $t_punto_entrega = $t_punto_entrega_autocomp;
      }
      if (!empty($t_regimen_tratamiento_autocomp) && empty($t_regimen_tratamiento))
      {
          $t_regimen_tratamiento = $t_regimen_tratamiento_autocomp;
      }
      if (!empty($t_medico_tratamiento_autocomp) && empty($t_medico_tratamiento))
      {
          $t_medico_tratamiento = $t_medico_tratamiento_autocomp;
      }
      if (!empty($t_tratamiento_previo_autocomp) && empty($t_tratamiento_previo))
      {
          $t_tratamiento_previo = $t_tratamiento_previo_autocomp;
      }
      if (!empty($g_reclamo_gestion_autocomp) && empty($g_reclamo_gestion))
      {
          $g_reclamo_gestion = $g_reclamo_gestion_autocomp;
      }
      if (!empty($g_fecha_reclamacion_gestion_autocomp) && empty($g_fecha_reclamacion_gestion))
      {
          $g_fecha_reclamacion_gestion = $g_fecha_reclamacion_gestion_autocomp;
      }
      if (!empty($g_causa_no_reclamacion_gestion_autocomp) && empty($g_causa_no_reclamacion_gestion))
      {
          $g_causa_no_reclamacion_gestion = $g_causa_no_reclamacion_gestion_autocomp;
      }
      if (!empty($g_fecha_programada_gestion_autocomp) && empty($g_fecha_programada_gestion))
      {
          $g_fecha_programada_gestion = $g_fecha_programada_gestion_autocomp;
      }
      if (!empty($p_id_ultima_gestion_autocomp) && empty($p_id_ultima_gestion))
      {
          $p_id_ultima_gestion = $p_id_ultima_gestion_autocomp;
      }
      if (!empty($t_id_tratamiento_autocomp) && empty($t_id_tratamiento))
      {
          $t_id_tratamiento = $t_id_tratamiento_autocomp;
      }
      if (!empty($t_id_paciente_fk_autocomp) && empty($t_id_paciente_fk))
      {
          $t_id_paciente_fk = $t_id_paciente_fk_autocomp;
      }
      $p_id_paciente_cond_salva = $p_id_paciente_cond; 
      if (!isset($p_id_paciente_input_2) || $p_id_paciente_input_2 == "")
      {
          $p_id_paciente_input_2 = $p_id_paciente;
      }
      $g_logro_comunicacion_gestion_cond_salva = $g_logro_comunicacion_gestion_cond; 
      if (!isset($g_logro_comunicacion_gestion_input_2) || $g_logro_comunicacion_gestion_input_2 == "")
      {
          $g_logro_comunicacion_gestion_input_2 = $g_logro_comunicacion_gestion;
      }
      $g_fecha_comunicacion_cond_salva = $g_fecha_comunicacion_cond; 
      if (!isset($g_fecha_comunicacion_input_2) || $g_fecha_comunicacion_input_2 == "")
      {
          $g_fecha_comunicacion_input_2 = $g_fecha_comunicacion;
      }
      $g_autor_gestion_cond_salva = $g_autor_gestion_cond; 
      if (!isset($g_autor_gestion_input_2) || $g_autor_gestion_input_2 == "")
      {
          $g_autor_gestion_input_2 = $g_autor_gestion;
      }
      $p_pais_paciente_cond_salva = $p_pais_paciente_cond; 
      if (!isset($p_pais_paciente_input_2) || $p_pais_paciente_input_2 == "")
      {
          $p_pais_paciente_input_2 = $p_pais_paciente;
      }
      $p_ciudad_paciente_cond_salva = $p_ciudad_paciente_cond; 
      if (!isset($p_ciudad_paciente_input_2) || $p_ciudad_paciente_input_2 == "")
      {
          $p_ciudad_paciente_input_2 = $p_ciudad_paciente;
      }
      $p_estado_paciente_cond_salva = $p_estado_paciente_cond; 
      if (!isset($p_estado_paciente_input_2) || $p_estado_paciente_input_2 == "")
      {
          $p_estado_paciente_input_2 = $p_estado_paciente;
      }
      $p_fecha_activacion_paciente_cond_salva = $p_fecha_activacion_paciente_cond; 
      if (!isset($p_fecha_activacion_paciente_input_2) || $p_fecha_activacion_paciente_input_2 == "")
      {
          $p_fecha_activacion_paciente_input_2 = $p_fecha_activacion_paciente;
      }
      $p_fecha_retiro_paciente_cond_salva = $p_fecha_retiro_paciente_cond; 
      if (!isset($p_fecha_retiro_paciente_input_2) || $p_fecha_retiro_paciente_input_2 == "")
      {
          $p_fecha_retiro_paciente_input_2 = $p_fecha_retiro_paciente;
      }
      $p_motivo_retiro_paciente_cond_salva = $p_motivo_retiro_paciente_cond; 
      if (!isset($p_motivo_retiro_paciente_input_2) || $p_motivo_retiro_paciente_input_2 == "")
      {
          $p_motivo_retiro_paciente_input_2 = $p_motivo_retiro_paciente;
      }
      $t_consentimiento_tratamiento_cond_salva = $t_consentimiento_tratamiento_cond; 
      if (!isset($t_consentimiento_tratamiento_input_2) || $t_consentimiento_tratamiento_input_2 == "")
      {
          $t_consentimiento_tratamiento_input_2 = $t_consentimiento_tratamiento;
      }
      $p_codigo_xofigo_cond_salva = $p_codigo_xofigo_cond; 
      if (!isset($p_codigo_xofigo_input_2) || $p_codigo_xofigo_input_2 == "")
      {
          $p_codigo_xofigo_input_2 = $p_codigo_xofigo;
      }
      $t_clasificacion_patologica_tratamiento_cond_salva = $t_clasificacion_patologica_tratamiento_cond; 
      if (!isset($t_clasificacion_patologica_tratamiento_input_2) || $t_clasificacion_patologica_tratamiento_input_2 == "")
      {
          $t_clasificacion_patologica_tratamiento_input_2 = $t_clasificacion_patologica_tratamiento;
      }
      $t_producto_tratamiento_cond_salva = $t_producto_tratamiento_cond; 
      if (!isset($t_producto_tratamiento_input_2) || $t_producto_tratamiento_input_2 == "")
      {
          $t_producto_tratamiento_input_2 = $t_producto_tratamiento;
      }
      $t_nombre_referencia_cond_salva = $t_nombre_referencia_cond; 
      if (!isset($t_nombre_referencia_input_2) || $t_nombre_referencia_input_2 == "")
      {
          $t_nombre_referencia_input_2 = $t_nombre_referencia;
      }
      $t_dosis_tratamiento_cond_salva = $t_dosis_tratamiento_cond; 
      if (!isset($t_dosis_tratamiento_input_2) || $t_dosis_tratamiento_input_2 == "")
      {
          $t_dosis_tratamiento_input_2 = $t_dosis_tratamiento;
      }
      $g_numero_cajas_cond_salva = $g_numero_cajas_cond; 
      if (!isset($g_numero_cajas_input_2) || $g_numero_cajas_input_2 == "")
      {
          $g_numero_cajas_input_2 = $g_numero_cajas;
      }
      $t_asegurador_tratamiento_cond_salva = $t_asegurador_tratamiento_cond; 
      if (!isset($t_asegurador_tratamiento_input_2) || $t_asegurador_tratamiento_input_2 == "")
      {
          $t_asegurador_tratamiento_input_2 = $t_asegurador_tratamiento;
      }
      $t_operador_logistico_tratamiento_cond_salva = $t_operador_logistico_tratamiento_cond; 
      if (!isset($t_operador_logistico_tratamiento_input_2) || $t_operador_logistico_tratamiento_input_2 == "")
      {
          $t_operador_logistico_tratamiento_input_2 = $t_operador_logistico_tratamiento;
      }
      $t_punto_entrega_cond_salva = $t_punto_entrega_cond; 
      if (!isset($t_punto_entrega_input_2) || $t_punto_entrega_input_2 == "")
      {
          $t_punto_entrega_input_2 = $t_punto_entrega;
      }
      $t_regimen_tratamiento_cond_salva = $t_regimen_tratamiento_cond; 
      if (!isset($t_regimen_tratamiento_input_2) || $t_regimen_tratamiento_input_2 == "")
      {
          $t_regimen_tratamiento_input_2 = $t_regimen_tratamiento;
      }
      $t_medico_tratamiento_cond_salva = $t_medico_tratamiento_cond; 
      if (!isset($t_medico_tratamiento_input_2) || $t_medico_tratamiento_input_2 == "")
      {
          $t_medico_tratamiento_input_2 = $t_medico_tratamiento;
      }
      $t_tratamiento_previo_cond_salva = $t_tratamiento_previo_cond; 
      if (!isset($t_tratamiento_previo_input_2) || $t_tratamiento_previo_input_2 == "")
      {
          $t_tratamiento_previo_input_2 = $t_tratamiento_previo;
      }
      $g_reclamo_gestion_cond_salva = $g_reclamo_gestion_cond; 
      if (!isset($g_reclamo_gestion_input_2) || $g_reclamo_gestion_input_2 == "")
      {
          $g_reclamo_gestion_input_2 = $g_reclamo_gestion;
      }
      $g_fecha_reclamacion_gestion_cond_salva = $g_fecha_reclamacion_gestion_cond; 
      if (!isset($g_fecha_reclamacion_gestion_input_2) || $g_fecha_reclamacion_gestion_input_2 == "")
      {
          $g_fecha_reclamacion_gestion_input_2 = $g_fecha_reclamacion_gestion;
      }
      $g_causa_no_reclamacion_gestion_cond_salva = $g_causa_no_reclamacion_gestion_cond; 
      if (!isset($g_causa_no_reclamacion_gestion_input_2) || $g_causa_no_reclamacion_gestion_input_2 == "")
      {
          $g_causa_no_reclamacion_gestion_input_2 = $g_causa_no_reclamacion_gestion;
      }
      $g_descripcion_comunicacion_gestion_cond_salva = $g_descripcion_comunicacion_gestion_cond; 
      if (!isset($g_descripcion_comunicacion_gestion_input_2) || $g_descripcion_comunicacion_gestion_input_2 == "")
      {
          $g_descripcion_comunicacion_gestion_input_2 = $g_descripcion_comunicacion_gestion;
      }
      $g_fecha_programada_gestion_cond_salva = $g_fecha_programada_gestion_cond; 
      if (!isset($g_fecha_programada_gestion_input_2) || $g_fecha_programada_gestion_input_2 == "")
      {
          $g_fecha_programada_gestion_input_2 = $g_fecha_programada_gestion;
      }
      $p_id_ultima_gestion_cond_salva = $p_id_ultima_gestion_cond; 
      if (!isset($p_id_ultima_gestion_input_2) || $p_id_ultima_gestion_input_2 == "")
      {
          $p_id_ultima_gestion_input_2 = $p_id_ultima_gestion;
      }
      $t_id_tratamiento_cond_salva = $t_id_tratamiento_cond; 
      if (!isset($t_id_tratamiento_input_2) || $t_id_tratamiento_input_2 == "")
      {
          $t_id_tratamiento_input_2 = $t_id_tratamiento;
      }
      $t_id_paciente_fk_cond_salva = $t_id_paciente_fk_cond; 
      if (!isset($t_id_paciente_fk_input_2) || $t_id_paciente_fk_input_2 == "")
      {
          $t_id_paciente_fk_input_2 = $t_id_paciente_fk;
      }
      if ($p_id_paciente_cond != "in")
      {
          nm_limpa_numero($p_id_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $p_id_paciente);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $p_id_paciente = implode(",", $Nm_sc_valores);
      }
      if ($p_id_paciente_cond != "in")
      {
          nm_limpa_numero($p_id_paciente_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($p_codigo_xofigo_cond != "in")
      {
          nm_limpa_numero($p_codigo_xofigo, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $p_codigo_xofigo);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $p_codigo_xofigo = implode(",", $Nm_sc_valores);
      }
      if ($p_codigo_xofigo_cond != "in")
      {
          nm_limpa_numero($p_codigo_xofigo_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($p_id_ultima_gestion_cond != "in")
      {
          nm_limpa_numero($p_id_ultima_gestion, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $p_id_ultima_gestion);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $p_id_ultima_gestion = implode(",", $Nm_sc_valores);
      }
      if ($p_id_ultima_gestion_cond != "in")
      {
          nm_limpa_numero($p_id_ultima_gestion_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($t_id_tratamiento_cond != "in")
      {
          nm_limpa_numero($t_id_tratamiento, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $t_id_tratamiento);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $t_id_tratamiento = implode(",", $Nm_sc_valores);
      }
      if ($t_id_tratamiento_cond != "in")
      {
          nm_limpa_numero($t_id_tratamiento_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($t_id_paciente_fk_cond != "in")
      {
          nm_limpa_numero($t_id_paciente_fk, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $t_id_paciente_fk);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $t_id_paciente_fk = implode(",", $Nm_sc_valores);
      }
      if ($t_id_paciente_fk_cond != "in")
      {
          nm_limpa_numero($t_id_paciente_fk_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']  = array(); 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente'] = $p_id_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente_input_2'] = $p_id_paciente_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_paciente_cond'] = $p_id_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_logro_comunicacion_gestion'] = $g_logro_comunicacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_logro_comunicacion_gestion_cond'] = $g_logro_comunicacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_comunicacion'] = $g_fecha_comunicacion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_comunicacion_cond'] = $g_fecha_comunicacion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_autor_gestion'] = $g_autor_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_autor_gestion_cond'] = $g_autor_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_pais_paciente'] = $p_pais_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_pais_paciente_cond'] = $p_pais_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_ciudad_paciente'] = $p_ciudad_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_ciudad_paciente_cond'] = $p_ciudad_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_estado_paciente'] = $p_estado_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_estado_paciente_cond'] = $p_estado_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_activacion_paciente'] = $p_fecha_activacion_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_activacion_paciente_cond'] = $p_fecha_activacion_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_retiro_paciente'] = $p_fecha_retiro_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_fecha_retiro_paciente_cond'] = $p_fecha_retiro_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_motivo_retiro_paciente'] = $p_motivo_retiro_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_motivo_retiro_paciente_cond'] = $p_motivo_retiro_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_consentimiento_tratamiento'] = $t_consentimiento_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_consentimiento_tratamiento_cond'] = $t_consentimiento_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo'] = $p_codigo_xofigo; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo_input_2'] = $p_codigo_xofigo_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_codigo_xofigo_cond'] = $p_codigo_xofigo_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_clasificacion_patologica_tratamiento'] = $t_clasificacion_patologica_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_clasificacion_patologica_tratamiento_cond'] = $t_clasificacion_patologica_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_producto_tratamiento'] = $t_producto_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_producto_tratamiento_cond'] = $t_producto_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_nombre_referencia'] = $t_nombre_referencia; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_nombre_referencia_cond'] = $t_nombre_referencia_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_dosis_tratamiento'] = $t_dosis_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_dosis_tratamiento_cond'] = $t_dosis_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_numero_cajas'] = $g_numero_cajas; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_numero_cajas_cond'] = $g_numero_cajas_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_asegurador_tratamiento'] = $t_asegurador_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_asegurador_tratamiento_cond'] = $t_asegurador_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_operador_logistico_tratamiento'] = $t_operador_logistico_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_operador_logistico_tratamiento_cond'] = $t_operador_logistico_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_punto_entrega'] = $t_punto_entrega; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_punto_entrega_cond'] = $t_punto_entrega_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_regimen_tratamiento'] = $t_regimen_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_regimen_tratamiento_cond'] = $t_regimen_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_medico_tratamiento'] = $t_medico_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_medico_tratamiento_cond'] = $t_medico_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_tratamiento_previo'] = $t_tratamiento_previo; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_tratamiento_previo_cond'] = $t_tratamiento_previo_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_reclamo_gestion'] = $g_reclamo_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_reclamo_gestion_cond'] = $g_reclamo_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_reclamacion_gestion'] = $g_fecha_reclamacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_reclamacion_gestion_cond'] = $g_fecha_reclamacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_causa_no_reclamacion_gestion'] = $g_causa_no_reclamacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_causa_no_reclamacion_gestion_cond'] = $g_causa_no_reclamacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_descripcion_comunicacion_gestion'] = $g_descripcion_comunicacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_descripcion_comunicacion_gestion_cond'] = $g_descripcion_comunicacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_programada_gestion'] = $g_fecha_programada_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['g_fecha_programada_gestion_cond'] = $g_fecha_programada_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion'] = $p_id_ultima_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion_input_2'] = $p_id_ultima_gestion_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['p_id_ultima_gestion_cond'] = $p_id_ultima_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento'] = $t_id_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento_input_2'] = $t_id_tratamiento_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_tratamiento_cond'] = $t_id_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk'] = $t_id_paciente_fk; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk_input_2'] = $t_id_paciente_fk_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['t_id_paciente_fk_cond'] = $t_id_paciente_fk_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']['NM_operador'] = $this->NM_operador; 
      if ($p_id_paciente_cond != "in" && $p_id_paciente_cond != "bw" && !empty($p_id_paciente) && !is_numeric($p_id_paciente))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE";
      }
      if ($p_id_paciente_cond == "bw" && ((!empty($p_id_paciente) && !is_numeric($p_id_paciente)) || (!empty($p_id_paciente_input_2) && !is_numeric($p_id_paciente_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE";
      }
      if ($p_codigo_xofigo_cond != "in" && $p_codigo_xofigo_cond != "bw" && !empty($p_codigo_xofigo) && !is_numeric($p_codigo_xofigo))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : CODIGO XOFIGO";
      }
      if ($p_codigo_xofigo_cond == "bw" && ((!empty($p_codigo_xofigo) && !is_numeric($p_codigo_xofigo)) || (!empty($p_codigo_xofigo_input_2) && !is_numeric($p_codigo_xofigo_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : CODIGO XOFIGO";
      }
      if ($p_id_ultima_gestion_cond != "in" && $p_id_ultima_gestion_cond != "bw" && !empty($p_id_ultima_gestion) && !is_numeric($p_id_ultima_gestion))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID ULTIMA GESTION";
      }
      if ($p_id_ultima_gestion_cond == "bw" && ((!empty($p_id_ultima_gestion) && !is_numeric($p_id_ultima_gestion)) || (!empty($p_id_ultima_gestion_input_2) && !is_numeric($p_id_ultima_gestion_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID ULTIMA GESTION";
      }
      if ($t_id_tratamiento_cond != "in" && $t_id_tratamiento_cond != "bw" && !empty($t_id_tratamiento) && !is_numeric($t_id_tratamiento))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID TRATAMIENTO";
      }
      if ($t_id_tratamiento_cond == "bw" && ((!empty($t_id_tratamiento) && !is_numeric($t_id_tratamiento)) || (!empty($t_id_tratamiento_input_2) && !is_numeric($t_id_tratamiento_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID TRATAMIENTO";
      }
      if ($t_id_paciente_fk_cond != "in" && $t_id_paciente_fk_cond != "bw" && !empty($t_id_paciente_fk) && !is_numeric($t_id_paciente_fk))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE FK";
      }
      if ($t_id_paciente_fk_cond == "bw" && ((!empty($t_id_paciente_fk) && !is_numeric($t_id_paciente_fk)) || (!empty($t_id_paciente_fk_input_2) && !is_numeric($t_id_paciente_fk_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE FK";
      }
      if (!empty($this->Campos_Mens_erro)) 
      {
          return;
      }
      $p_id_paciente_look = substr($this->Db->qstr($p_id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_id_paciente))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where p.ID_PACIENTE = $p_id_paciente_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_PACIENTE from " . $this->Ini->nm_tabela . " where p.ID_PACIENTE = $p_id_paciente_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_id_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_id_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_id_paciente'] = $p_id_paciente;
      }
      $Conteudo = $p_id_paciente_input_2;
      if (strtoupper($p_id_paciente_cond) != "II" && strtoupper($p_id_paciente_cond) != "QP" && strtoupper($p_id_paciente_cond) != "NP" && strtoupper($p_id_paciente_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['p_id_paciente_input_2'] = $Conteudo;
      $g_logro_comunicacion_gestion_look = substr($this->Db->qstr($g_logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.LOGRO_COMUNICACION_GESTION from " . $this->Ini->nm_tabela . " where g.LOGRO_COMUNICACION_GESTION = '$g_logro_comunicacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_logro_comunicacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_logro_comunicacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_logro_comunicacion_gestion'] = $g_logro_comunicacion_gestion;
      }
      $g_fecha_comunicacion_look = substr($this->Db->qstr($g_fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_COMUNICACION from " . $this->Ini->nm_tabela . " where g.FECHA_COMUNICACION = '$g_fecha_comunicacion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_comunicacion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_comunicacion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_fecha_comunicacion'] = $g_fecha_comunicacion;
      }
      $g_autor_gestion_look = substr($this->Db->qstr($g_autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.AUTOR_GESTION from " . $this->Ini->nm_tabela . " where g.AUTOR_GESTION = '$g_autor_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_autor_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_autor_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_autor_gestion'] = $g_autor_gestion;
      }
      $p_pais_paciente_look = substr($this->Db->qstr($p_pais_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.PAIS_PACIENTE from " . $this->Ini->nm_tabela . " where p.PAIS_PACIENTE = '$p_pais_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_pais_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_pais_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_pais_paciente'] = $p_pais_paciente;
      }
      $p_ciudad_paciente_look = substr($this->Db->qstr($p_ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.CIUDAD_PACIENTE from " . $this->Ini->nm_tabela . " where p.CIUDAD_PACIENTE = '$p_ciudad_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_ciudad_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_ciudad_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_ciudad_paciente'] = $p_ciudad_paciente;
      }
      $p_estado_paciente_look = substr($this->Db->qstr($p_estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.ESTADO_PACIENTE from " . $this->Ini->nm_tabela . " where p.ESTADO_PACIENTE = '$p_estado_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_estado_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_estado_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_estado_paciente'] = $p_estado_paciente;
      }
      $p_fecha_activacion_paciente_look = substr($this->Db->qstr($p_fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_ACTIVACION_PACIENTE from " . $this->Ini->nm_tabela . " where p.FECHA_ACTIVACION_PACIENTE = '$p_fecha_activacion_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_fecha_activacion_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_fecha_activacion_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_fecha_activacion_paciente'] = $p_fecha_activacion_paciente;
      }
      $p_fecha_retiro_paciente_look = substr($this->Db->qstr($p_fecha_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.FECHA_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where p.FECHA_RETIRO_PACIENTE = '$p_fecha_retiro_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_fecha_retiro_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_fecha_retiro_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_fecha_retiro_paciente'] = $p_fecha_retiro_paciente;
      }
      $p_motivo_retiro_paciente_look = substr($this->Db->qstr($p_motivo_retiro_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct p.MOTIVO_RETIRO_PACIENTE from " . $this->Ini->nm_tabela . " where p.MOTIVO_RETIRO_PACIENTE = '$p_motivo_retiro_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_motivo_retiro_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_motivo_retiro_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_motivo_retiro_paciente'] = $p_motivo_retiro_paciente;
      }
      $t_consentimiento_tratamiento_look = substr($this->Db->qstr($t_consentimiento_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CONSENTIMIENTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.CONSENTIMIENTO_TRATAMIENTO = '$t_consentimiento_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_consentimiento_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_consentimiento_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_consentimiento_tratamiento'] = $t_consentimiento_tratamiento;
      }
      $p_codigo_xofigo_look = substr($this->Db->qstr($p_codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_codigo_xofigo))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where p.CODIGO_XOFIGO = $p_codigo_xofigo_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.CODIGO_XOFIGO from " . $this->Ini->nm_tabela . " where p.CODIGO_XOFIGO = $p_codigo_xofigo_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_codigo_xofigo'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_codigo_xofigo'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_codigo_xofigo'] = $p_codigo_xofigo;
      }
      $Conteudo = $p_codigo_xofigo_input_2;
      if (strtoupper($p_codigo_xofigo_cond) != "II" && strtoupper($p_codigo_xofigo_cond) != "QP" && strtoupper($p_codigo_xofigo_cond) != "NP" && strtoupper($p_codigo_xofigo_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['p_codigo_xofigo_input_2'] = $Conteudo;
      $t_clasificacion_patologica_tratamiento_look = substr($this->Db->qstr($t_clasificacion_patologica_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.CLASIFICACION_PATOLOGICA_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$t_clasificacion_patologica_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_clasificacion_patologica_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_clasificacion_patologica_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_clasificacion_patologica_tratamiento'] = $t_clasificacion_patologica_tratamiento;
      }
      $t_producto_tratamiento_look = substr($this->Db->qstr($t_producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PRODUCTO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.PRODUCTO_TRATAMIENTO = '$t_producto_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_producto_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_producto_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_producto_tratamiento'] = $t_producto_tratamiento;
      }
      $t_nombre_referencia_look = substr($this->Db->qstr($t_nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.NOMBRE_REFERENCIA from " . $this->Ini->nm_tabela . " where t.NOMBRE_REFERENCIA = '$t_nombre_referencia_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_nombre_referencia'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_nombre_referencia'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_nombre_referencia'] = $t_nombre_referencia;
      }
      $t_dosis_tratamiento_look = substr($this->Db->qstr($t_dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.DOSIS_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.DOSIS_TRATAMIENTO = '$t_dosis_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_dosis_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_dosis_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_dosis_tratamiento'] = $t_dosis_tratamiento;
      }
      $g_numero_cajas_look = substr($this->Db->qstr($g_numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.NUMERO_CAJAS from " . $this->Ini->nm_tabela . " where g.NUMERO_CAJAS = '$g_numero_cajas_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_numero_cajas'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_numero_cajas'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_numero_cajas'] = $g_numero_cajas;
      }
      $t_asegurador_tratamiento_look = substr($this->Db->qstr($t_asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.ASEGURADOR_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ASEGURADOR_TRATAMIENTO = '$t_asegurador_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_asegurador_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_asegurador_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_asegurador_tratamiento'] = $t_asegurador_tratamiento;
      }
      $t_operador_logistico_tratamiento_look = substr($this->Db->qstr($t_operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.OPERADOR_LOGISTICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.OPERADOR_LOGISTICO_TRATAMIENTO = '$t_operador_logistico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_operador_logistico_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_operador_logistico_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_operador_logistico_tratamiento'] = $t_operador_logistico_tratamiento;
      }
      $t_punto_entrega_look = substr($this->Db->qstr($t_punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.PUNTO_ENTREGA from " . $this->Ini->nm_tabela . " where t.PUNTO_ENTREGA = '$t_punto_entrega_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_punto_entrega'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_punto_entrega'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_punto_entrega'] = $t_punto_entrega;
      }
      $t_regimen_tratamiento_look = substr($this->Db->qstr($t_regimen_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.REGIMEN_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.REGIMEN_TRATAMIENTO = '$t_regimen_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_regimen_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_regimen_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_regimen_tratamiento'] = $t_regimen_tratamiento;
      }
      $t_medico_tratamiento_look = substr($this->Db->qstr($t_medico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.MEDICO_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.MEDICO_TRATAMIENTO = '$t_medico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_medico_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_medico_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_medico_tratamiento'] = $t_medico_tratamiento;
      }
      $t_tratamiento_previo_look = substr($this->Db->qstr($t_tratamiento_previo), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct t.TRATAMIENTO_PREVIO from " . $this->Ini->nm_tabela . " where t.TRATAMIENTO_PREVIO = '$t_tratamiento_previo_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_tratamiento_previo'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_tratamiento_previo'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_tratamiento_previo'] = $t_tratamiento_previo;
      }
      $g_reclamo_gestion_look = substr($this->Db->qstr($g_reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.RECLAMO_GESTION from " . $this->Ini->nm_tabela . " where g.RECLAMO_GESTION = '$g_reclamo_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_reclamo_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_reclamo_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_reclamo_gestion'] = $g_reclamo_gestion;
      }
      $g_fecha_reclamacion_gestion_look = substr($this->Db->qstr($g_fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where g.FECHA_RECLAMACION_GESTION = '$g_fecha_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_reclamacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_reclamacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_fecha_reclamacion_gestion'] = $g_fecha_reclamacion_gestion;
      }
      $g_causa_no_reclamacion_gestion_look = substr($this->Db->qstr($g_causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.CAUSA_NO_RECLAMACION_GESTION from " . $this->Ini->nm_tabela . " where g.CAUSA_NO_RECLAMACION_GESTION = '$g_causa_no_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_causa_no_reclamacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_causa_no_reclamacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_causa_no_reclamacion_gestion'] = $g_causa_no_reclamacion_gestion;
      }
      $Conteudo = $g_descripcion_comunicacion_gestion;
      $this->cmp_formatado['g_descripcion_comunicacion_gestion'] = $Conteudo;
      $g_fecha_programada_gestion_look = substr($this->Db->qstr($g_fecha_programada_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct g.FECHA_PROGRAMADA_GESTION from " . $this->Ini->nm_tabela . " where g.FECHA_PROGRAMADA_GESTION = '$g_fecha_programada_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_programada_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['g_fecha_programada_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['g_fecha_programada_gestion'] = $g_fecha_programada_gestion;
      }
      $p_id_ultima_gestion_look = substr($this->Db->qstr($p_id_ultima_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($p_id_ultima_gestion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where p.ID_ULTIMA_GESTION = $p_id_ultima_gestion_look"; 
      }
      else
      {
          $nm_comando = "select distinct p.ID_ULTIMA_GESTION from " . $this->Ini->nm_tabela . " where p.ID_ULTIMA_GESTION = $p_id_ultima_gestion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_id_ultima_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['p_id_ultima_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['p_id_ultima_gestion'] = $p_id_ultima_gestion;
      }
      $Conteudo = $p_id_ultima_gestion_input_2;
      if (strtoupper($p_id_ultima_gestion_cond) != "II" && strtoupper($p_id_ultima_gestion_cond) != "QP" && strtoupper($p_id_ultima_gestion_cond) != "NP" && strtoupper($p_id_ultima_gestion_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['p_id_ultima_gestion_input_2'] = $Conteudo;
      $t_id_tratamiento_look = substr($this->Db->qstr($t_id_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($t_id_tratamiento))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ID_TRATAMIENTO = $t_id_tratamiento_look"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_TRATAMIENTO from " . $this->Ini->nm_tabela . " where t.ID_TRATAMIENTO = $t_id_tratamiento_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_id_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_id_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_id_tratamiento'] = $t_id_tratamiento;
      }
      $Conteudo = $t_id_tratamiento_input_2;
      if (strtoupper($t_id_tratamiento_cond) != "II" && strtoupper($t_id_tratamiento_cond) != "QP" && strtoupper($t_id_tratamiento_cond) != "NP" && strtoupper($t_id_tratamiento_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['t_id_tratamiento_input_2'] = $Conteudo;
      $t_id_paciente_fk_look = substr($this->Db->qstr($t_id_paciente_fk), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($t_id_paciente_fk))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where t.ID_PACIENTE_FK = $t_id_paciente_fk_look"; 
      }
      else
      {
          $nm_comando = "select distinct t.ID_PACIENTE_FK from " . $this->Ini->nm_tabela . " where t.ID_PACIENTE_FK = $t_id_paciente_fk_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_id_paciente_fk'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['t_id_paciente_fk'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['t_id_paciente_fk'] = $t_id_paciente_fk;
      }
      $Conteudo = $t_id_paciente_fk_input_2;
      if (strtoupper($t_id_paciente_fk_cond) != "II" && strtoupper($t_id_paciente_fk_cond) != "QP" && strtoupper($t_id_paciente_fk_cond) != "NP" && strtoupper($t_id_paciente_fk_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['t_id_paciente_fk_input_2'] = $Conteudo;

      //----- $p_id_paciente
      $this->Date_part = false;
      if (isset($p_id_paciente) || $p_id_paciente_cond == "nu" || $p_id_paciente_cond == "nn" || $p_id_paciente_cond == "ep" || $p_id_paciente_cond == "ne")
      {
         $this->monta_condicao("p.ID_PACIENTE", $p_id_paciente_cond, $p_id_paciente, $p_id_paciente_input_2, "p_id_paciente");
      }

      //----- $g_logro_comunicacion_gestion
      $this->Date_part = false;
      if (isset($g_logro_comunicacion_gestion) || $g_logro_comunicacion_gestion_cond == "nu" || $g_logro_comunicacion_gestion_cond == "nn" || $g_logro_comunicacion_gestion_cond == "ep" || $g_logro_comunicacion_gestion_cond == "ne")
      {
         $this->monta_condicao("g.LOGRO_COMUNICACION_GESTION", $g_logro_comunicacion_gestion_cond, $g_logro_comunicacion_gestion, "", "g_logro_comunicacion_gestion");
      }

      //----- $g_fecha_comunicacion
      $this->Date_part = false;
      if (isset($g_fecha_comunicacion) || $g_fecha_comunicacion_cond == "nu" || $g_fecha_comunicacion_cond == "nn" || $g_fecha_comunicacion_cond == "ep" || $g_fecha_comunicacion_cond == "ne")
      {
         $this->monta_condicao("g.FECHA_COMUNICACION", $g_fecha_comunicacion_cond, $g_fecha_comunicacion, "", "g_fecha_comunicacion");
      }

      //----- $g_autor_gestion
      $this->Date_part = false;
      if (isset($g_autor_gestion) || $g_autor_gestion_cond == "nu" || $g_autor_gestion_cond == "nn" || $g_autor_gestion_cond == "ep" || $g_autor_gestion_cond == "ne")
      {
         $this->monta_condicao("g.AUTOR_GESTION", $g_autor_gestion_cond, $g_autor_gestion, "", "g_autor_gestion");
      }

      //----- $p_pais_paciente
      $this->Date_part = false;
      if (isset($p_pais_paciente) || $p_pais_paciente_cond == "nu" || $p_pais_paciente_cond == "nn" || $p_pais_paciente_cond == "ep" || $p_pais_paciente_cond == "ne")
      {
         $this->monta_condicao("p.PAIS_PACIENTE", $p_pais_paciente_cond, $p_pais_paciente, "", "p_pais_paciente");
      }

      //----- $p_ciudad_paciente
      $this->Date_part = false;
      if (isset($p_ciudad_paciente) || $p_ciudad_paciente_cond == "nu" || $p_ciudad_paciente_cond == "nn" || $p_ciudad_paciente_cond == "ep" || $p_ciudad_paciente_cond == "ne")
      {
         $this->monta_condicao("p.CIUDAD_PACIENTE", $p_ciudad_paciente_cond, $p_ciudad_paciente, "", "p_ciudad_paciente");
      }

      //----- $p_estado_paciente
      $this->Date_part = false;
      if (isset($p_estado_paciente) || $p_estado_paciente_cond == "nu" || $p_estado_paciente_cond == "nn" || $p_estado_paciente_cond == "ep" || $p_estado_paciente_cond == "ne")
      {
         $this->monta_condicao("p.ESTADO_PACIENTE", $p_estado_paciente_cond, $p_estado_paciente, "", "p_estado_paciente");
      }

      //----- $p_fecha_activacion_paciente
      $this->Date_part = false;
      if (isset($p_fecha_activacion_paciente) || $p_fecha_activacion_paciente_cond == "nu" || $p_fecha_activacion_paciente_cond == "nn" || $p_fecha_activacion_paciente_cond == "ep" || $p_fecha_activacion_paciente_cond == "ne")
      {
         $this->monta_condicao("p.FECHA_ACTIVACION_PACIENTE", $p_fecha_activacion_paciente_cond, $p_fecha_activacion_paciente, "", "p_fecha_activacion_paciente");
      }

      //----- $p_fecha_retiro_paciente
      $this->Date_part = false;
      if (isset($p_fecha_retiro_paciente) || $p_fecha_retiro_paciente_cond == "nu" || $p_fecha_retiro_paciente_cond == "nn" || $p_fecha_retiro_paciente_cond == "ep" || $p_fecha_retiro_paciente_cond == "ne")
      {
         $this->monta_condicao("p.FECHA_RETIRO_PACIENTE", $p_fecha_retiro_paciente_cond, $p_fecha_retiro_paciente, "", "p_fecha_retiro_paciente");
      }

      //----- $p_motivo_retiro_paciente
      $this->Date_part = false;
      if (isset($p_motivo_retiro_paciente) || $p_motivo_retiro_paciente_cond == "nu" || $p_motivo_retiro_paciente_cond == "nn" || $p_motivo_retiro_paciente_cond == "ep" || $p_motivo_retiro_paciente_cond == "ne")
      {
         $this->monta_condicao("p.MOTIVO_RETIRO_PACIENTE", $p_motivo_retiro_paciente_cond, $p_motivo_retiro_paciente, "", "p_motivo_retiro_paciente");
      }

      //----- $t_consentimiento_tratamiento
      $this->Date_part = false;
      if (isset($t_consentimiento_tratamiento) || $t_consentimiento_tratamiento_cond == "nu" || $t_consentimiento_tratamiento_cond == "nn" || $t_consentimiento_tratamiento_cond == "ep" || $t_consentimiento_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.CONSENTIMIENTO_TRATAMIENTO", $t_consentimiento_tratamiento_cond, $t_consentimiento_tratamiento, "", "t_consentimiento_tratamiento");
      }

      //----- $p_codigo_xofigo
      $this->Date_part = false;
      if (isset($p_codigo_xofigo) || $p_codigo_xofigo_cond == "nu" || $p_codigo_xofigo_cond == "nn" || $p_codigo_xofigo_cond == "ep" || $p_codigo_xofigo_cond == "ne")
      {
         $this->monta_condicao("p.CODIGO_XOFIGO", $p_codigo_xofigo_cond, $p_codigo_xofigo, $p_codigo_xofigo_input_2, "p_codigo_xofigo");
      }

      //----- $t_clasificacion_patologica_tratamiento
      $this->Date_part = false;
      if (isset($t_clasificacion_patologica_tratamiento) || $t_clasificacion_patologica_tratamiento_cond == "nu" || $t_clasificacion_patologica_tratamiento_cond == "nn" || $t_clasificacion_patologica_tratamiento_cond == "ep" || $t_clasificacion_patologica_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.CLASIFICACION_PATOLOGICA_TRATAMIENTO", $t_clasificacion_patologica_tratamiento_cond, $t_clasificacion_patologica_tratamiento, "", "t_clasificacion_patologica_tratamiento");
      }

      //----- $t_producto_tratamiento
      $this->Date_part = false;
      if (isset($t_producto_tratamiento) || $t_producto_tratamiento_cond == "nu" || $t_producto_tratamiento_cond == "nn" || $t_producto_tratamiento_cond == "ep" || $t_producto_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.PRODUCTO_TRATAMIENTO", $t_producto_tratamiento_cond, $t_producto_tratamiento, "", "t_producto_tratamiento");
      }

      //----- $t_nombre_referencia
      $this->Date_part = false;
      if (isset($t_nombre_referencia) || $t_nombre_referencia_cond == "nu" || $t_nombre_referencia_cond == "nn" || $t_nombre_referencia_cond == "ep" || $t_nombre_referencia_cond == "ne")
      {
         $this->monta_condicao("t.NOMBRE_REFERENCIA", $t_nombre_referencia_cond, $t_nombre_referencia, "", "t_nombre_referencia");
      }

      //----- $t_dosis_tratamiento
      $this->Date_part = false;
      if (isset($t_dosis_tratamiento) || $t_dosis_tratamiento_cond == "nu" || $t_dosis_tratamiento_cond == "nn" || $t_dosis_tratamiento_cond == "ep" || $t_dosis_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.DOSIS_TRATAMIENTO", $t_dosis_tratamiento_cond, $t_dosis_tratamiento, "", "t_dosis_tratamiento");
      }

      //----- $g_numero_cajas
      $this->Date_part = false;
      if (isset($g_numero_cajas) || $g_numero_cajas_cond == "nu" || $g_numero_cajas_cond == "nn" || $g_numero_cajas_cond == "ep" || $g_numero_cajas_cond == "ne")
      {
         $this->monta_condicao("g.NUMERO_CAJAS", $g_numero_cajas_cond, $g_numero_cajas, "", "g_numero_cajas");
      }

      //----- $t_asegurador_tratamiento
      $this->Date_part = false;
      if (isset($t_asegurador_tratamiento) || $t_asegurador_tratamiento_cond == "nu" || $t_asegurador_tratamiento_cond == "nn" || $t_asegurador_tratamiento_cond == "ep" || $t_asegurador_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.ASEGURADOR_TRATAMIENTO", $t_asegurador_tratamiento_cond, $t_asegurador_tratamiento, "", "t_asegurador_tratamiento");
      }

      //----- $t_operador_logistico_tratamiento
      $this->Date_part = false;
      if (isset($t_operador_logistico_tratamiento) || $t_operador_logistico_tratamiento_cond == "nu" || $t_operador_logistico_tratamiento_cond == "nn" || $t_operador_logistico_tratamiento_cond == "ep" || $t_operador_logistico_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.OPERADOR_LOGISTICO_TRATAMIENTO", $t_operador_logistico_tratamiento_cond, $t_operador_logistico_tratamiento, "", "t_operador_logistico_tratamiento");
      }

      //----- $t_punto_entrega
      $this->Date_part = false;
      if (isset($t_punto_entrega) || $t_punto_entrega_cond == "nu" || $t_punto_entrega_cond == "nn" || $t_punto_entrega_cond == "ep" || $t_punto_entrega_cond == "ne")
      {
         $this->monta_condicao("t.PUNTO_ENTREGA", $t_punto_entrega_cond, $t_punto_entrega, "", "t_punto_entrega");
      }

      //----- $t_regimen_tratamiento
      $this->Date_part = false;
      if (isset($t_regimen_tratamiento) || $t_regimen_tratamiento_cond == "nu" || $t_regimen_tratamiento_cond == "nn" || $t_regimen_tratamiento_cond == "ep" || $t_regimen_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.REGIMEN_TRATAMIENTO", $t_regimen_tratamiento_cond, $t_regimen_tratamiento, "", "t_regimen_tratamiento");
      }

      //----- $t_medico_tratamiento
      $this->Date_part = false;
      if (isset($t_medico_tratamiento) || $t_medico_tratamiento_cond == "nu" || $t_medico_tratamiento_cond == "nn" || $t_medico_tratamiento_cond == "ep" || $t_medico_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.MEDICO_TRATAMIENTO", $t_medico_tratamiento_cond, $t_medico_tratamiento, "", "t_medico_tratamiento");
      }

      //----- $t_tratamiento_previo
      $this->Date_part = false;
      if (isset($t_tratamiento_previo) || $t_tratamiento_previo_cond == "nu" || $t_tratamiento_previo_cond == "nn" || $t_tratamiento_previo_cond == "ep" || $t_tratamiento_previo_cond == "ne")
      {
         $this->monta_condicao("t.TRATAMIENTO_PREVIO", $t_tratamiento_previo_cond, $t_tratamiento_previo, "", "t_tratamiento_previo");
      }

      //----- $g_reclamo_gestion
      $this->Date_part = false;
      if (isset($g_reclamo_gestion) || $g_reclamo_gestion_cond == "nu" || $g_reclamo_gestion_cond == "nn" || $g_reclamo_gestion_cond == "ep" || $g_reclamo_gestion_cond == "ne")
      {
         $this->monta_condicao("g.RECLAMO_GESTION", $g_reclamo_gestion_cond, $g_reclamo_gestion, "", "g_reclamo_gestion");
      }

      //----- $g_fecha_reclamacion_gestion
      $this->Date_part = false;
      if (isset($g_fecha_reclamacion_gestion) || $g_fecha_reclamacion_gestion_cond == "nu" || $g_fecha_reclamacion_gestion_cond == "nn" || $g_fecha_reclamacion_gestion_cond == "ep" || $g_fecha_reclamacion_gestion_cond == "ne")
      {
         $this->monta_condicao("g.FECHA_RECLAMACION_GESTION", $g_fecha_reclamacion_gestion_cond, $g_fecha_reclamacion_gestion, "", "g_fecha_reclamacion_gestion");
      }

      //----- $g_causa_no_reclamacion_gestion
      $this->Date_part = false;
      if (isset($g_causa_no_reclamacion_gestion) || $g_causa_no_reclamacion_gestion_cond == "nu" || $g_causa_no_reclamacion_gestion_cond == "nn" || $g_causa_no_reclamacion_gestion_cond == "ep" || $g_causa_no_reclamacion_gestion_cond == "ne")
      {
         $this->monta_condicao("g.CAUSA_NO_RECLAMACION_GESTION", $g_causa_no_reclamacion_gestion_cond, $g_causa_no_reclamacion_gestion, "", "g_causa_no_reclamacion_gestion");
      }

      //----- $g_descripcion_comunicacion_gestion
      $this->Date_part = false;
      if (isset($g_descripcion_comunicacion_gestion) || $g_descripcion_comunicacion_gestion_cond == "nu" || $g_descripcion_comunicacion_gestion_cond == "nn" || $g_descripcion_comunicacion_gestion_cond == "ep" || $g_descripcion_comunicacion_gestion_cond == "ne")
      {
         $this->monta_condicao("g.DESCRIPCION_COMUNICACION_GESTION", $g_descripcion_comunicacion_gestion_cond, $g_descripcion_comunicacion_gestion, "", "g_descripcion_comunicacion_gestion");
      }

      //----- $g_fecha_programada_gestion
      $this->Date_part = false;
      if (isset($g_fecha_programada_gestion) || $g_fecha_programada_gestion_cond == "nu" || $g_fecha_programada_gestion_cond == "nn" || $g_fecha_programada_gestion_cond == "ep" || $g_fecha_programada_gestion_cond == "ne")
      {
         $this->monta_condicao("g.FECHA_PROGRAMADA_GESTION", $g_fecha_programada_gestion_cond, $g_fecha_programada_gestion, "", "g_fecha_programada_gestion");
      }

      //----- $p_id_ultima_gestion
      $this->Date_part = false;
      if (isset($p_id_ultima_gestion) || $p_id_ultima_gestion_cond == "nu" || $p_id_ultima_gestion_cond == "nn" || $p_id_ultima_gestion_cond == "ep" || $p_id_ultima_gestion_cond == "ne")
      {
         $this->monta_condicao("p.ID_ULTIMA_GESTION", $p_id_ultima_gestion_cond, $p_id_ultima_gestion, $p_id_ultima_gestion_input_2, "p_id_ultima_gestion");
      }

      //----- $t_id_tratamiento
      $this->Date_part = false;
      if (isset($t_id_tratamiento) || $t_id_tratamiento_cond == "nu" || $t_id_tratamiento_cond == "nn" || $t_id_tratamiento_cond == "ep" || $t_id_tratamiento_cond == "ne")
      {
         $this->monta_condicao("t.ID_TRATAMIENTO", $t_id_tratamiento_cond, $t_id_tratamiento, $t_id_tratamiento_input_2, "t_id_tratamiento");
      }

      //----- $t_id_paciente_fk
      $this->Date_part = false;
      if (isset($t_id_paciente_fk) || $t_id_paciente_fk_cond == "nu" || $t_id_paciente_fk_cond == "nn" || $t_id_paciente_fk_cond == "ep" || $t_id_paciente_fk_cond == "ne")
      {
         $this->monta_condicao("t.ID_PACIENTE_FK", $t_id_paciente_fk_cond, $t_id_paciente_fk, $t_id_paciente_fk_input_2, "t_id_paciente_fk");
      }
   }

   /**
    * @access  public
    */
   function finaliza_resultado()
   {
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_fast'] = "";
      if ("" == $this->comando_filtro)
      {
          $this->comando = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']) && $_SESSION['scriptcase']['charset'] != "UTF-8")
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'] = NM_conv_charset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'], "UTF-8", $_SESSION['scriptcase']['charset']);
      }

      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_lookup']  = $this->comando_sum . $this->comando_fim;
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq']         = $this->comando . $this->comando_fim;
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['opcao']              = "pesq";
      if ("" == $this->comando_filtro)
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_filtro'] = "";
      }
      else
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_filtro'] = " (" . $this->comando_filtro . ")";
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq'] != $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_ant'])
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cond_pesq'] .= $this->NM_operador;
         $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['contr_array_resumo'] = "NAO";
         $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['contr_total_geral']  = "NAO";
         unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['tot_geral']);
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_ant'] = $this->comando . $this->comando_fim;
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['fast_search']);

      $this->retorna_pesq();
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}

?>
