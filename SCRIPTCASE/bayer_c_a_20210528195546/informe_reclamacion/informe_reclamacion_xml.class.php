<?php

class informe_reclamacion_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function informe_reclamacion_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_informe_reclamacion";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "informe_reclamacion.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->g_logro_comunicacion_gestion = $Busca_temp['g_logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->g_logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_logro_comunicacion_gestion = substr($this->g_logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
          $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
          }
          $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
          $tmp_pos = strpos($this->g_autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
          $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
          }
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
          $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->t_consentimiento_tratamiento = $Busca_temp['t_consentimiento_tratamiento']; 
          $tmp_pos = strpos($this->t_consentimiento_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_consentimiento_tratamiento = substr($this->t_consentimiento_tratamiento, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
          $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
          $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
          $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
          }
          $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
          $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
          }
          $this->t_nombre_referencia = $Busca_temp['t_nombre_referencia']; 
          $tmp_pos = strpos($this->t_nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_nombre_referencia = substr($this->t_nombre_referencia, 0, $tmp_pos);
          }
          $this->t_dosis_tratamiento = $Busca_temp['t_dosis_tratamiento']; 
          $tmp_pos = strpos($this->t_dosis_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_dosis_tratamiento = substr($this->t_dosis_tratamiento, 0, $tmp_pos);
          }
          $this->g_numero_cajas = $Busca_temp['g_numero_cajas']; 
          $tmp_pos = strpos($this->g_numero_cajas, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_numero_cajas = substr($this->g_numero_cajas, 0, $tmp_pos);
          }
          $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
          $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
          $tmp_pos = strpos($this->t_punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
          }
          $this->t_regimen_tratamiento = $Busca_temp['t_regimen_tratamiento']; 
          $tmp_pos = strpos($this->t_regimen_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_regimen_tratamiento = substr($this->t_regimen_tratamiento, 0, $tmp_pos);
          }
          $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
          $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
          }
          $this->t_tratamiento_previo = $Busca_temp['t_tratamiento_previo']; 
          $tmp_pos = strpos($this->t_tratamiento_previo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_tratamiento_previo = substr($this->t_tratamiento_previo, 0, $tmp_pos);
          }
          $this->g_reclamo_gestion = $Busca_temp['g_reclamo_gestion']; 
          $tmp_pos = strpos($this->g_reclamo_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_reclamo_gestion = substr($this->g_reclamo_gestion, 0, $tmp_pos);
          }
          $this->g_fecha_reclamacion_gestion = $Busca_temp['g_fecha_reclamacion_gestion']; 
          $tmp_pos = strpos($this->g_fecha_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_reclamacion_gestion = substr($this->g_fecha_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->g_causa_no_reclamacion_gestion = $Busca_temp['g_causa_no_reclamacion_gestion']; 
          $tmp_pos = strpos($this->g_causa_no_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_causa_no_reclamacion_gestion = substr($this->g_causa_no_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->g_descripcion_comunicacion_gestion = $Busca_temp['g_descripcion_comunicacion_gestion']; 
          $tmp_pos = strpos($this->g_descripcion_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_descripcion_comunicacion_gestion = substr($this->g_descripcion_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->g_fecha_programada_gestion = $Busca_temp['g_fecha_programada_gestion']; 
          $tmp_pos = strpos($this->g_fecha_programada_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_programada_gestion = substr($this->g_fecha_programada_gestion, 0, $tmp_pos);
          }
          $this->p_id_ultima_gestion = $Busca_temp['p_id_ultima_gestion']; 
          $tmp_pos = strpos($this->p_id_ultima_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_ultima_gestion = substr($this->p_id_ultima_gestion, 0, $tmp_pos);
          }
          $this->p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
          $this->t_id_tratamiento = $Busca_temp['t_id_tratamiento']; 
          $tmp_pos = strpos($this->t_id_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_id_tratamiento = substr($this->t_id_tratamiento, 0, $tmp_pos);
          }
          $this->t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
          $this->t_id_paciente_fk = $Busca_temp['t_id_paciente_fk']; 
          $tmp_pos = strpos($this->t_id_paciente_fk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_id_paciente_fk = substr($this->t_id_paciente_fk, 0, $tmp_pos);
          }
          $this->t_id_paciente_fk_2 = $Busca_temp['t_id_paciente_fk_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, LOTOFILE(g.DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<informe_reclamacion";
         $this->p_id_paciente = $rs->fields[0] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->g_logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->g_fecha_comunicacion = $rs->fields[2] ;  
         $this->g_autor_gestion = $rs->fields[3] ;  
         $this->p_pais_paciente = $rs->fields[4] ;  
         $this->p_ciudad_paciente = $rs->fields[5] ;  
         $this->p_estado_paciente = $rs->fields[6] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[7] ;  
         $this->p_fecha_retiro_paciente = $rs->fields[8] ;  
         $this->p_motivo_retiro_paciente = $rs->fields[9] ;  
         $this->t_consentimiento_tratamiento = $rs->fields[10] ;  
         $this->p_codigo_xofigo = $rs->fields[11] ;  
         $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
         $this->t_clasificacion_patologica_tratamiento = $rs->fields[12] ;  
         $this->t_producto_tratamiento = $rs->fields[13] ;  
         $this->t_nombre_referencia = $rs->fields[14] ;  
         $this->t_dosis_tratamiento = $rs->fields[15] ;  
         $this->g_numero_cajas = $rs->fields[16] ;  
         $this->t_asegurador_tratamiento = $rs->fields[17] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[18] ;  
         $this->t_punto_entrega = $rs->fields[19] ;  
         $this->t_regimen_tratamiento = $rs->fields[20] ;  
         $this->t_medico_tratamiento = $rs->fields[21] ;  
         $this->t_tratamiento_previo = $rs->fields[22] ;  
         $this->g_reclamo_gestion = $rs->fields[23] ;  
         $this->g_fecha_reclamacion_gestion = $rs->fields[24] ;  
         $this->g_causa_no_reclamacion_gestion = $rs->fields[25] ;  
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $this->g_descripcion_comunicacion_gestion = "";  
              if (is_file($rs_grid->fields[26])) 
              { 
                  $this->g_descripcion_comunicacion_gestion = file_get_contents($rs_grid->fields[26]);  
              } 
          } 
         elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
         { 
             $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[26]) ;  
         } 
         else
         { 
             $this->g_descripcion_comunicacion_gestion = $rs->fields[26] ;  
         } 
         $this->g_fecha_programada_gestion = $rs->fields[27] ;  
         $this->p_id_ultima_gestion = $rs->fields[28] ;  
         $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
         $this->t_id_tratamiento = $rs->fields[29] ;  
         $this->t_id_tratamiento = (string)$this->t_id_tratamiento;
         $this->t_id_paciente_fk = $rs->fields[30] ;  
         $this->t_id_paciente_fk = (string)$this->t_id_paciente_fk;
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_paciente =\"" . $this->trata_dados($this->p_id_paciente) . "\"";
   }
   //----- g_logro_comunicacion_gestion
   function NM_export_g_logro_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_logro_comunicacion_gestion))
         {
             $this->g_logro_comunicacion_gestion = sc_convert_encoding($this->g_logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_logro_comunicacion_gestion =\"" . $this->trata_dados($this->g_logro_comunicacion_gestion) . "\"";
   }
   //----- g_fecha_comunicacion
   function NM_export_g_fecha_comunicacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_fecha_comunicacion))
         {
             $this->g_fecha_comunicacion = sc_convert_encoding($this->g_fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_fecha_comunicacion =\"" . $this->trata_dados($this->g_fecha_comunicacion) . "\"";
   }
   //----- g_autor_gestion
   function NM_export_g_autor_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_autor_gestion))
         {
             $this->g_autor_gestion = sc_convert_encoding($this->g_autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_autor_gestion =\"" . $this->trata_dados($this->g_autor_gestion) . "\"";
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_pais_paciente =\"" . $this->trata_dados($this->p_pais_paciente) . "\"";
   }
   //----- p_ciudad_paciente
   function NM_export_p_ciudad_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_ciudad_paciente))
         {
             $this->p_ciudad_paciente = sc_convert_encoding($this->p_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_ciudad_paciente =\"" . $this->trata_dados($this->p_ciudad_paciente) . "\"";
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_estado_paciente))
         {
             $this->p_estado_paciente = sc_convert_encoding($this->p_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_estado_paciente =\"" . $this->trata_dados($this->p_estado_paciente) . "\"";
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_activacion_paciente =\"" . $this->trata_dados($this->p_fecha_activacion_paciente) . "\"";
   }
   //----- p_fecha_retiro_paciente
   function NM_export_p_fecha_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_fecha_retiro_paciente))
         {
             $this->p_fecha_retiro_paciente = sc_convert_encoding($this->p_fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_fecha_retiro_paciente =\"" . $this->trata_dados($this->p_fecha_retiro_paciente) . "\"";
   }
   //----- p_motivo_retiro_paciente
   function NM_export_p_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_motivo_retiro_paciente))
         {
             $this->p_motivo_retiro_paciente = sc_convert_encoding($this->p_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_motivo_retiro_paciente =\"" . $this->trata_dados($this->p_motivo_retiro_paciente) . "\"";
   }
   //----- t_consentimiento_tratamiento
   function NM_export_t_consentimiento_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_consentimiento_tratamiento))
         {
             $this->t_consentimiento_tratamiento = sc_convert_encoding($this->t_consentimiento_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_consentimiento_tratamiento =\"" . $this->trata_dados($this->t_consentimiento_tratamiento) . "\"";
   }
   //----- p_codigo_xofigo
   function NM_export_p_codigo_xofigo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_codigo_xofigo))
         {
             $this->p_codigo_xofigo = sc_convert_encoding($this->p_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_codigo_xofigo =\"" . $this->trata_dados($this->p_codigo_xofigo) . "\"";
   }
   //----- t_clasificacion_patologica_tratamiento
   function NM_export_t_clasificacion_patologica_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_clasificacion_patologica_tratamiento))
         {
             $this->t_clasificacion_patologica_tratamiento = sc_convert_encoding($this->t_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_clasificacion_patologica_tratamiento =\"" . $this->trata_dados($this->t_clasificacion_patologica_tratamiento) . "\"";
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_producto_tratamiento))
         {
             $this->t_producto_tratamiento = sc_convert_encoding($this->t_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_producto_tratamiento =\"" . $this->trata_dados($this->t_producto_tratamiento) . "\"";
   }
   //----- t_nombre_referencia
   function NM_export_t_nombre_referencia()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_nombre_referencia))
         {
             $this->t_nombre_referencia = sc_convert_encoding($this->t_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_nombre_referencia =\"" . $this->trata_dados($this->t_nombre_referencia) . "\"";
   }
   //----- t_dosis_tratamiento
   function NM_export_t_dosis_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_dosis_tratamiento))
         {
             $this->t_dosis_tratamiento = sc_convert_encoding($this->t_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_dosis_tratamiento =\"" . $this->trata_dados($this->t_dosis_tratamiento) . "\"";
   }
   //----- g_numero_cajas
   function NM_export_g_numero_cajas()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_numero_cajas))
         {
             $this->g_numero_cajas = sc_convert_encoding($this->g_numero_cajas, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_numero_cajas =\"" . $this->trata_dados($this->g_numero_cajas) . "\"";
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_asegurador_tratamiento))
         {
             $this->t_asegurador_tratamiento = sc_convert_encoding($this->t_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_asegurador_tratamiento =\"" . $this->trata_dados($this->t_asegurador_tratamiento) . "\"";
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_operador_logistico_tratamiento))
         {
             $this->t_operador_logistico_tratamiento = sc_convert_encoding($this->t_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_operador_logistico_tratamiento =\"" . $this->trata_dados($this->t_operador_logistico_tratamiento) . "\"";
   }
   //----- t_punto_entrega
   function NM_export_t_punto_entrega()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_punto_entrega))
         {
             $this->t_punto_entrega = sc_convert_encoding($this->t_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_punto_entrega =\"" . $this->trata_dados($this->t_punto_entrega) . "\"";
   }
   //----- t_regimen_tratamiento
   function NM_export_t_regimen_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_regimen_tratamiento))
         {
             $this->t_regimen_tratamiento = sc_convert_encoding($this->t_regimen_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_regimen_tratamiento =\"" . $this->trata_dados($this->t_regimen_tratamiento) . "\"";
   }
   //----- t_medico_tratamiento
   function NM_export_t_medico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_medico_tratamiento))
         {
             $this->t_medico_tratamiento = sc_convert_encoding($this->t_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_medico_tratamiento =\"" . $this->trata_dados($this->t_medico_tratamiento) . "\"";
   }
   //----- t_tratamiento_previo
   function NM_export_t_tratamiento_previo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_tratamiento_previo))
         {
             $this->t_tratamiento_previo = sc_convert_encoding($this->t_tratamiento_previo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_tratamiento_previo =\"" . $this->trata_dados($this->t_tratamiento_previo) . "\"";
   }
   //----- g_reclamo_gestion
   function NM_export_g_reclamo_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_reclamo_gestion))
         {
             $this->g_reclamo_gestion = sc_convert_encoding($this->g_reclamo_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_reclamo_gestion =\"" . $this->trata_dados($this->g_reclamo_gestion) . "\"";
   }
   //----- g_fecha_reclamacion_gestion
   function NM_export_g_fecha_reclamacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_fecha_reclamacion_gestion))
         {
             $this->g_fecha_reclamacion_gestion = sc_convert_encoding($this->g_fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_fecha_reclamacion_gestion =\"" . $this->trata_dados($this->g_fecha_reclamacion_gestion) . "\"";
   }
   //----- g_causa_no_reclamacion_gestion
   function NM_export_g_causa_no_reclamacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_causa_no_reclamacion_gestion))
         {
             $this->g_causa_no_reclamacion_gestion = sc_convert_encoding($this->g_causa_no_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_causa_no_reclamacion_gestion =\"" . $this->trata_dados($this->g_causa_no_reclamacion_gestion) . "\"";
   }
   //----- g_descripcion_comunicacion_gestion
   function NM_export_g_descripcion_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_descripcion_comunicacion_gestion))
         {
             $this->g_descripcion_comunicacion_gestion = sc_convert_encoding($this->g_descripcion_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_descripcion_comunicacion_gestion =\"" . $this->trata_dados($this->g_descripcion_comunicacion_gestion) . "\"";
   }
   //----- g_fecha_programada_gestion
   function NM_export_g_fecha_programada_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->g_fecha_programada_gestion))
         {
             $this->g_fecha_programada_gestion = sc_convert_encoding($this->g_fecha_programada_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " g_fecha_programada_gestion =\"" . $this->trata_dados($this->g_fecha_programada_gestion) . "\"";
   }
   //----- p_id_ultima_gestion
   function NM_export_p_id_ultima_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->p_id_ultima_gestion))
         {
             $this->p_id_ultima_gestion = sc_convert_encoding($this->p_id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " p_id_ultima_gestion =\"" . $this->trata_dados($this->p_id_ultima_gestion) . "\"";
   }
   //----- t_id_tratamiento
   function NM_export_t_id_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_id_tratamiento))
         {
             $this->t_id_tratamiento = sc_convert_encoding($this->t_id_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_id_tratamiento =\"" . $this->trata_dados($this->t_id_tratamiento) . "\"";
   }
   //----- t_id_paciente_fk
   function NM_export_t_id_paciente_fk()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->t_id_paciente_fk))
         {
             $this->t_id_paciente_fk = sc_convert_encoding($this->t_id_paciente_fk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " t_id_paciente_fk =\"" . $this->trata_dados($this->t_id_paciente_fk) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="informe_reclamacion_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="informe_reclamacion"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
