
// ---------- delete_filter
function ajax_delete_filter(parm)
{
    nmAjaxProcOn();
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_filter_delete&script_case_init=" + document.F1.script_case_init.value + "&script_case_session=" + document.F1.script_case_session.value + "&NM_filters_del=" + parm,
      success: function(json_del_fil) {
        var i, oResp;
        Tst_integrid = json_del_fil.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (json_del_fil);
            return;
        }
        eval("oResp = " + json_del_fil);
        if (oResp["setValue"]) {
          for (i = 0; i < oResp["setValue"].length; i++) {
               $("#" + oResp["setValue"][i]["field"]).html(oResp["setValue"][i]["value"]);
          }
        }
        nmAjaxProcOff();
      }
    });
}

// ---------- save_filter
function ajax_save_filter(save_name, save_opt, parm, pos)
{
    nmAjaxProcOn();
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_filter_save&script_case_init=" + document.F1.script_case_init.value + "&script_case_session=" + document.F1.script_case_session.value + "&nmgp_save_name=" + save_name + "&nmgp_save_option=" + save_opt + "&NM_filters_save=" + parm,
      success: function(json_save_fil) {
        var i, oResp;
        Tst_integrid = json_save_fil.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (json_save_fil);
            return;
        }
        eval("oResp = " + json_save_fil);
        if (oResp["setValue"]) {
          for (i = 0; i < oResp["setValue"].length; i++) {
               $("#" + oResp["setValue"][i]["field"]).html(oResp["setValue"][i]["value"]);
          }
        }
        if (oResp["htmOutput"]) {
            nmAjaxShowDebug(oResp);
         }
        document.getElementById('sel_recup_filters_' + pos).selectedIndex = -1;
        document.getElementById('sel_filters_del_' + pos).selectedIndex = -1;
        document.getElementById('SC_nmgp_save_name_' + pos).value = '';
        document.getElementById('Apaga_filters_' + pos).style.display = '';
        document.getElementById('sel_recup_filters_' + pos).style.display = '';
        document.getElementById('Salvar_filters_' + pos).style.display = 'none';
        nmAjaxProcOff();
      }
    });
}

// ---------- select_filter
var Table_sv_fil = new Array();
Table_sv_fil[0] = "p_id_paciente";
Table_sv_fil[1] = "g_logro_comunicacion_gestion";
Table_sv_fil[2] = "g_fecha_comunicacion";
Table_sv_fil[3] = "g_autor_gestion";
Table_sv_fil[4] = "p_pais_paciente";
Table_sv_fil[5] = "p_ciudad_paciente";
Table_sv_fil[6] = "p_estado_paciente";
Table_sv_fil[7] = "p_fecha_activacion_paciente";
Table_sv_fil[8] = "p_fecha_retiro_paciente";
Table_sv_fil[9] = "p_motivo_retiro_paciente";
Table_sv_fil[10] = "t_consentimiento_tratamiento";
Table_sv_fil[11] = "p_codigo_xofigo";
Table_sv_fil[12] = "t_clasificacion_patologica_tratamiento";
Table_sv_fil[13] = "t_producto_tratamiento";
Table_sv_fil[14] = "t_nombre_referencia";
Table_sv_fil[15] = "t_dosis_tratamiento";
Table_sv_fil[16] = "g_numero_cajas";
Table_sv_fil[17] = "t_asegurador_tratamiento";
Table_sv_fil[18] = "t_operador_logistico_tratamiento";
Table_sv_fil[19] = "t_punto_entrega";
Table_sv_fil[20] = "t_regimen_tratamiento";
Table_sv_fil[21] = "t_medico_tratamiento";
Table_sv_fil[22] = "t_tratamiento_previo";
Table_sv_fil[23] = "g_reclamo_gestion";
Table_sv_fil[24] = "g_fecha_reclamacion_gestion";
Table_sv_fil[25] = "g_causa_no_reclamacion_gestion";
Table_sv_fil[26] = "g_descripcion_comunicacion_gestion";
Table_sv_fil[27] = "g_fecha_programada_gestion";
Table_sv_fil[28] = "p_id_ultima_gestion";
Table_sv_fil[29] = "t_id_tratamiento";
Table_sv_fil[30] = "t_id_paciente_fk";
function ajax_select_filter(parm)
{
    nmAjaxProcOn();
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_filter_select&script_case_init=" + document.F1.script_case_init.value + "&script_case_session=" + document.F1.script_case_session.value + "&NM_filters=" + parm,
      success: function(json_sel_fil) {
        var i, oResp;
        Tst_integrid = json_sel_fil.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (json_sel_fil);
            return;
        }
        eval("oResp = " + json_sel_fil);
        if (oResp["set_val"]) {
          for (i = 0; i < oResp["set_val"].length; i++) {
               $("#" + oResp["set_val"][i]["field"]).val(oResp["set_val"][i]["value"]);
          }
        }
        if (oResp["set_html"]) {
          for (i = 0; i < oResp["set_html"].length; i++) {
               $("#" + oResp["set_html"][i]["field"]).html(oResp["set_html"][i]["value"]);
          }
        }
        if (oResp["set_radio"]) {
          for (i = 0; i < oResp["set_radio"].length; i++) {
               if ($("#" + oResp["set_radio"][i]["field"])) {
                   $("#" + oResp["set_radio"][i]["field"]).removeAttr('checked');
                   $('input[id="' + oResp["set_radio"][i]["field"] + '"][value="' + oResp["set_radio"][i]["value"] + '"]').prop('checked', true);
              }
          }
        }
        if (oResp["set_checkbox"]) {
          for (i = 0; i < oResp["set_checkbox"].length; i++) {
              var cmp_ck = oResp["set_checkbox"][i]["field"].substr(3) + "[]";
              if (document.F1.elements[cmp_ck]) {
                  var obj_check = document.F1.elements[cmp_ck];
                  if (obj_check.length == undefined) {
                      document.F1.elements[cmp_ck].checked = false;
                      for (y = 0; y < oResp["set_checkbox"][i]["value"].length; y++) {
                          if (document.F1.elements[cmp_ck].value == oResp["set_checkbox"][i]["value"][y]) {
                              document.F1.elements[cmp_ck].checked = true;
                          }
                      }
                  }
                  if (obj_check.length != undefined) {
                      for (x = 0; x < obj_check.length; x++) {
                          obj_check[x].checked = false;
                      }
                      for (x = 0; x < obj_check.length; x++) {
                          for (y = 0; y < oResp["set_checkbox"][i]["value"].length; y++) {
                              if (obj_check[x].value == oResp["set_checkbox"][i]["value"][y]) {
                                  obj_check[x].checked = true;
                              }
                          }
                      }
                  }
              }
          }
        }
        if (oResp["set_selmult"]) {
          for (i = 0; i < oResp["set_selmult"].length; i++) {
             var obj_sel = document.getElementById(oResp["set_selmult"][i]["field"]);
             for (x = 0; x < obj_sel.length; x++) {
                 if (obj_sel[x].selected) {
                    obj_sel[x].selected = false;
                 }
             }
             for (x = 0; x < obj_sel.length; x++) {
                 for (y = 0; y < oResp["set_selmult"][i]["value"].length; y++) {
                     if (obj_sel[x].value == oResp["set_selmult"][i]["value"][y]) {
                         obj_sel[x].selected = true;
                     }
                 }
             }
          }
        }
        if (oResp["set_dselect"]) {
          for (i = 0; i < oResp["set_dselect"].length; i++) {
              var obj_sel_orig = document.getElementById(oResp["set_dselect"][i]["field"] + "_orig");
              var obj_sel_dest = document.getElementById(oResp["set_dselect"][i]["field"] + "_dest");
              obj_sel_dest.length = 0
              for (x = 0; x < obj_sel_orig.length; x++) {
                     obj_sel_orig[x].disabled = false;
                     obj_sel_orig[x].style.color = "";
              }
              var ind = 0;
              for (y = 0; y < oResp["set_dselect"][i]["value"].length; y++) {
                 for (x = 0; x < obj_sel_orig.length; x++) {
                     if (obj_sel_orig[x].value == oResp["set_dselect"][i]["value"][y]["opt"]) {
                         obj_sel_orig[x].disabled = true;
                         obj_sel_orig[x].style.color = "#A0A0A0";
                         obj_sel_dest.options[ind] = new Option(oResp["set_dselect"][i]["value"][y]["value"], oResp["set_dselect"][i]["value"][y]["opt"]);
                         ind++;
                     }
                 }
             }
          }
        }
        if (oResp["set_fil_order"]) {
          for (i = 0; i < oResp["set_fil_order"].length; i++) {
              var obj_sel_orig = document.getElementById(oResp["set_fil_order"][i]["field"] + "_orig");
              var obj_sel_dest = document.getElementById(oResp["set_fil_order"][i]["field"] + "_dest");
              obj_sel_dest.length = 0
              for (x = 0; x < obj_sel_orig.length; x++) {
                     obj_sel_orig[x].disabled = false;
                     obj_sel_orig[x].style.color = "";
              }
              var ind = 0;
              for (y = 0; y < oResp["set_fil_order"][i]["value"].length; y++) {
                 for (x = 0; x < obj_sel_orig.length; x++) {
                     if (obj_sel_orig[x].value == oResp["set_fil_order"][i]["value"][y].substr(1)) {
                         obj_sel_orig[x].disabled = true;
                         obj_sel_orig[x].style.color = "#A0A0A0";
                         obj_sel_dest.options[ind] = new Option(oResp["set_fil_order"][i]["value"][y], oResp["set_fil_order"][i]["value"][y]);
                         ind++;
                     }
                 }
             }
          }
        }
        for (i = 0; i < Table_sv_fil.length; i++) {
           if (document.getElementById('id_vis_' + Table_sv_fil[i])) {
              if (search_get_sel_txt("SC_" + Table_sv_fil[i] + "_cond") == "bw") {
                 document.getElementById('id_vis_' + Table_sv_fil[i]).style.display ='';
              }
              else {
                 document.getElementById('id_vis_' + Table_sv_fil[i]).style.display ='none';
              }
           }
        }
        nmAjaxProcOff();
      }
    });
}
