<?php
//--- 
class informe_reclamacion_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $p_id_paciente;
   var $g_logro_comunicacion_gestion;
   var $g_fecha_comunicacion;
   var $g_autor_gestion;
   var $p_pais_paciente;
   var $p_ciudad_paciente;
   var $p_estado_paciente;
   var $p_fecha_activacion_paciente;
   var $p_fecha_retiro_paciente;
   var $p_motivo_retiro_paciente;
   var $t_consentimiento_tratamiento;
   var $p_codigo_xofigo;
   var $t_clasificacion_patologica_tratamiento;
   var $t_producto_tratamiento;
   var $t_nombre_referencia;
   var $t_dosis_tratamiento;
   var $g_numero_cajas;
   var $t_asegurador_tratamiento;
   var $t_operador_logistico_tratamiento;
   var $t_punto_entrega;
   var $t_regimen_tratamiento;
   var $t_medico_tratamiento;
   var $t_tratamiento_previo;
   var $g_reclamo_gestion;
   var $g_fecha_reclamacion_gestion;
   var $g_causa_no_reclamacion_gestion;
   var $g_descripcion_comunicacion_gestion;
   var $g_fecha_programada_gestion;
   var $p_id_ultima_gestion;
   var $t_id_tratamiento;
   var $t_id_paciente_fk;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['informe_reclamacion']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
        $tmp_pos = strpos($this->p_id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
        }
        $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
        $this->g_logro_comunicacion_gestion = $Busca_temp['g_logro_comunicacion_gestion']; 
        $tmp_pos = strpos($this->g_logro_comunicacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_logro_comunicacion_gestion = substr($this->g_logro_comunicacion_gestion, 0, $tmp_pos);
        }
        $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
        $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
        }
        $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
        $tmp_pos = strpos($this->g_autor_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
        }
        $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
        $tmp_pos = strpos($this->p_pais_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
        }
        $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
        $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
        }
        $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
        $tmp_pos = strpos($this->p_estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
        $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
        $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
        }
        $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
        $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
        }
        $this->t_consentimiento_tratamiento = $Busca_temp['t_consentimiento_tratamiento']; 
        $tmp_pos = strpos($this->t_consentimiento_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_consentimiento_tratamiento = substr($this->t_consentimiento_tratamiento, 0, $tmp_pos);
        }
        $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
        $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
        }
        $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
        $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
        $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
        }
        $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
        $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
        }
        $this->t_nombre_referencia = $Busca_temp['t_nombre_referencia']; 
        $tmp_pos = strpos($this->t_nombre_referencia, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_nombre_referencia = substr($this->t_nombre_referencia, 0, $tmp_pos);
        }
        $this->t_dosis_tratamiento = $Busca_temp['t_dosis_tratamiento']; 
        $tmp_pos = strpos($this->t_dosis_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_dosis_tratamiento = substr($this->t_dosis_tratamiento, 0, $tmp_pos);
        }
        $this->g_numero_cajas = $Busca_temp['g_numero_cajas']; 
        $tmp_pos = strpos($this->g_numero_cajas, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_numero_cajas = substr($this->g_numero_cajas, 0, $tmp_pos);
        }
        $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
        $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
        }
        $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
        $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
        }
        $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
        $tmp_pos = strpos($this->t_punto_entrega, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
        }
        $this->t_regimen_tratamiento = $Busca_temp['t_regimen_tratamiento']; 
        $tmp_pos = strpos($this->t_regimen_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_regimen_tratamiento = substr($this->t_regimen_tratamiento, 0, $tmp_pos);
        }
        $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
        $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
        }
        $this->t_tratamiento_previo = $Busca_temp['t_tratamiento_previo']; 
        $tmp_pos = strpos($this->t_tratamiento_previo, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_tratamiento_previo = substr($this->t_tratamiento_previo, 0, $tmp_pos);
        }
        $this->g_reclamo_gestion = $Busca_temp['g_reclamo_gestion']; 
        $tmp_pos = strpos($this->g_reclamo_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_reclamo_gestion = substr($this->g_reclamo_gestion, 0, $tmp_pos);
        }
        $this->g_fecha_reclamacion_gestion = $Busca_temp['g_fecha_reclamacion_gestion']; 
        $tmp_pos = strpos($this->g_fecha_reclamacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_reclamacion_gestion = substr($this->g_fecha_reclamacion_gestion, 0, $tmp_pos);
        }
        $this->g_causa_no_reclamacion_gestion = $Busca_temp['g_causa_no_reclamacion_gestion']; 
        $tmp_pos = strpos($this->g_causa_no_reclamacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_causa_no_reclamacion_gestion = substr($this->g_causa_no_reclamacion_gestion, 0, $tmp_pos);
        }
        $this->g_descripcion_comunicacion_gestion = $Busca_temp['g_descripcion_comunicacion_gestion']; 
        $tmp_pos = strpos($this->g_descripcion_comunicacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_descripcion_comunicacion_gestion = substr($this->g_descripcion_comunicacion_gestion, 0, $tmp_pos);
        }
        $this->g_fecha_programada_gestion = $Busca_temp['g_fecha_programada_gestion']; 
        $tmp_pos = strpos($this->g_fecha_programada_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_programada_gestion = substr($this->g_fecha_programada_gestion, 0, $tmp_pos);
        }
        $this->p_id_ultima_gestion = $Busca_temp['p_id_ultima_gestion']; 
        $tmp_pos = strpos($this->p_id_ultima_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_ultima_gestion = substr($this->p_id_ultima_gestion, 0, $tmp_pos);
        }
        $this->p_id_ultima_gestion_2 = $Busca_temp['p_id_ultima_gestion_input_2']; 
        $this->t_id_tratamiento = $Busca_temp['t_id_tratamiento']; 
        $tmp_pos = strpos($this->t_id_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_id_tratamiento = substr($this->t_id_tratamiento, 0, $tmp_pos);
        }
        $this->t_id_tratamiento_2 = $Busca_temp['t_id_tratamiento_input_2']; 
        $this->t_id_paciente_fk = $Busca_temp['t_id_paciente_fk']; 
        $tmp_pos = strpos($this->t_id_paciente_fk, "##@@");
        if ($tmp_pos !== false)
        {
            $this->t_id_paciente_fk = substr($this->t_id_paciente_fk, 0, $tmp_pos);
        }
        $this->t_id_paciente_fk_2 = $Busca_temp['t_id_paciente_fk_input_2']; 
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, LOTOFILE(g.DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as g_descripcion_comunicacion_gestion, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, p.CODIGO_XOFIGO as p_codigo_xofigo, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, t.PUNTO_ENTREGA as t_punto_entrega, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, g.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_3, g.FECHA_PROGRAMADA_GESTION as g_fecha_programada_gestion, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, t.ID_PACIENTE_FK as t_id_paciente_fk from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[7]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[8]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[9]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[10]' and p.CODIGO_XOFIGO = $parms_det[11] and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[12]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[13]' and t.NOMBRE_REFERENCIA = '$parms_det[14]' and t.DOSIS_TRATAMIENTO = '$parms_det[15]' and g.NUMERO_CAJAS = '$parms_det[16]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[17]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[18]' and t.PUNTO_ENTREGA = '$parms_det[19]' and t.REGIMEN_TRATAMIENTO = '$parms_det[20]' and t.MEDICO_TRATAMIENTO = '$parms_det[21]' and t.TRATAMIENTO_PREVIO = '$parms_det[22]' and g.RECLAMO_GESTION = '$parms_det[23]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[24]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[25]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[26]' and p.ID_ULTIMA_GESTION = $parms_det[27] and t.ID_TRATAMIENTO = $parms_det[28] and t.ID_PACIENTE_FK = $parms_det[29]" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[7]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[8]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[9]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[10]' and p.CODIGO_XOFIGO = $parms_det[11] and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[12]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[13]' and t.NOMBRE_REFERENCIA = '$parms_det[14]' and t.DOSIS_TRATAMIENTO = '$parms_det[15]' and g.NUMERO_CAJAS = '$parms_det[16]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[17]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[18]' and t.PUNTO_ENTREGA = '$parms_det[19]' and t.REGIMEN_TRATAMIENTO = '$parms_det[20]' and t.MEDICO_TRATAMIENTO = '$parms_det[21]' and t.TRATAMIENTO_PREVIO = '$parms_det[22]' and g.RECLAMO_GESTION = '$parms_det[23]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[24]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[25]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[26]' and p.ID_ULTIMA_GESTION = $parms_det[27] and t.ID_TRATAMIENTO = $parms_det[28] and t.ID_PACIENTE_FK = $parms_det[29]" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[7]' and p.FECHA_RETIRO_PACIENTE = '$parms_det[8]' and p.MOTIVO_RETIRO_PACIENTE = '$parms_det[9]' and t.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[10]' and p.CODIGO_XOFIGO = $parms_det[11] and t.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[12]' and t.PRODUCTO_TRATAMIENTO = '$parms_det[13]' and t.NOMBRE_REFERENCIA = '$parms_det[14]' and t.DOSIS_TRATAMIENTO = '$parms_det[15]' and g.NUMERO_CAJAS = '$parms_det[16]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[17]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[18]' and t.PUNTO_ENTREGA = '$parms_det[19]' and t.REGIMEN_TRATAMIENTO = '$parms_det[20]' and t.MEDICO_TRATAMIENTO = '$parms_det[21]' and t.TRATAMIENTO_PREVIO = '$parms_det[22]' and g.RECLAMO_GESTION = '$parms_det[23]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[24]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[25]' and g.FECHA_PROGRAMADA_GESTION = '$parms_det[26]' and p.ID_ULTIMA_GESTION = $parms_det[27] and t.ID_TRATAMIENTO = $parms_det[28] and t.ID_PACIENTE_FK = $parms_det[29]" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->p_id_paciente = $rs->fields[0] ;  
   $this->p_id_paciente = (string)$this->p_id_paciente;
   $this->g_logro_comunicacion_gestion = $rs->fields[1] ;  
   $this->g_fecha_comunicacion = $rs->fields[2] ;  
   $this->g_autor_gestion = $rs->fields[3] ;  
   $this->p_pais_paciente = $rs->fields[4] ;  
   $this->p_ciudad_paciente = $rs->fields[5] ;  
   $this->p_estado_paciente = $rs->fields[6] ;  
   $this->p_fecha_activacion_paciente = $rs->fields[7] ;  
   $this->p_fecha_retiro_paciente = $rs->fields[8] ;  
   $this->p_motivo_retiro_paciente = $rs->fields[9] ;  
   $this->t_consentimiento_tratamiento = $rs->fields[10] ;  
   $this->p_codigo_xofigo = $rs->fields[11] ;  
   $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
   $this->t_clasificacion_patologica_tratamiento = $rs->fields[12] ;  
   $this->t_producto_tratamiento = $rs->fields[13] ;  
   $this->t_nombre_referencia = $rs->fields[14] ;  
   $this->t_dosis_tratamiento = $rs->fields[15] ;  
   $this->g_numero_cajas = $rs->fields[16] ;  
   $this->t_asegurador_tratamiento = $rs->fields[17] ;  
   $this->t_operador_logistico_tratamiento = $rs->fields[18] ;  
   $this->t_punto_entrega = $rs->fields[19] ;  
   $this->t_regimen_tratamiento = $rs->fields[20] ;  
   $this->t_medico_tratamiento = $rs->fields[21] ;  
   $this->t_tratamiento_previo = $rs->fields[22] ;  
   $this->g_reclamo_gestion = $rs->fields[23] ;  
   $this->g_fecha_reclamacion_gestion = $rs->fields[24] ;  
   $this->g_causa_no_reclamacion_gestion = $rs->fields[25] ;  
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
   { 
       $this->g_descripcion_comunicacion_gestion = "";  
       if (is_file($rs->fields[26])) 
       { 
           $this->g_descripcion_comunicacion_gestion = file_get_contents($rs->fields[26]);  
       } 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[26]) ;  
   } 
   else
   { 
       $this->g_descripcion_comunicacion_gestion = $rs->fields[26] ;  
   } 
   $this->g_fecha_programada_gestion = $rs->fields[27] ;  
   $this->p_id_ultima_gestion = $rs->fields[28] ;  
   $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
   $this->t_id_tratamiento = $rs->fields[29] ;  
   $this->t_id_tratamiento = (string)$this->t_id_tratamiento;
   $this->t_id_paciente_fk = $rs->fields[30] ;  
   $this->t_id_paciente_fk = (string)$this->t_id_paciente_fk;
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   { 
       if (!empty($this->g_descripcion_comunicacion_gestion))
       { 
           $this->g_descripcion_comunicacion_gestion = $this->Db->BlobDecode($this->g_descripcion_comunicacion_gestion, false, true, "BLOB");
       }
   }
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Informe Reclamaciones</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "informe_reclamacion/informe_reclamacion_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Informe Reclamaciones</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "informe_reclamacion/informe_reclamacion_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "informe_reclamacion/informe_reclamacion_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_logro_comunicacion_gestion'])) ? $this->New_label['g_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_logro_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_logro_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_logro_comunicacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_comunicacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_comunicacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_fecha_comunicacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_autor_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_autor_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_autor_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_pais_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_pais_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_pais_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_retiro_paciente'])) ? $this->New_label['p_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_fecha_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_motivo_retiro_paciente'])) ? $this->New_label['p_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_consentimiento_tratamiento'])) ? $this->New_label['t_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_consentimiento_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_consentimiento_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_consentimiento_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->p_codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_clasificacion_patologica_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_clasificacion_patologica_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_clasificacion_patologica_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->t_nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_dosis_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_dosis_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_dosis_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_numero_cajas'])) ? $this->New_label['g_numero_cajas'] : "NUMERO CAJAS"; 
          $conteudo = trim(sc_strip_script($this->g_numero_cajas)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_numero_cajas_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_numero_cajas_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->t_punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_regimen_tratamiento'])) ? $this->New_label['t_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_regimen_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_regimen_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_regimen_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_medico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_medico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_medico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_tratamiento_previo'])) ? $this->New_label['t_tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          $conteudo = trim(sc_strip_script($this->t_tratamiento_previo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_tratamiento_previo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_tratamiento_previo_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_reclamo_gestion'])) ? $this->New_label['g_reclamo_gestion'] : "RECLAMO GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_reclamo_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_reclamo_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_reclamo_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_reclamacion_gestion'])) ? $this->New_label['g_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_fecha_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_causa_no_reclamacion_gestion'])) ? $this->New_label['g_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_causa_no_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_causa_no_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_causa_no_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_descripcion_comunicacion_gestion'])) ? $this->New_label['g_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_descripcion_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else   
          { 
              $conteudo = nl2br($conteudo) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_descripcion_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_descripcion_comunicacion_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_programada_gestion'])) ? $this->New_label['g_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_programada_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_programada_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_fecha_programada_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_ultima_gestion'])) ? $this->New_label['p_id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          $conteudo = trim(sc_strip_script($this->p_id_ultima_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_ultima_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_id_ultima_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_id_tratamiento'])) ? $this->New_label['t_id_tratamiento'] : "ID TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_id_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_id_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_id_tratamiento_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_id_paciente_fk'])) ? $this->New_label['t_id_paciente_fk'] : "ID PACIENTE FK"; 
          $conteudo = trim(sc_strip_script($this->t_id_paciente_fk)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_id_paciente_fk_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_id_paciente_fk_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['informe_reclamacion']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"informe_reclamacion_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "informe_reclamacion/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "informe_reclamacion/informe_reclamacion_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
