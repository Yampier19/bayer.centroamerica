<?php

class paciente_terapia_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function paciente_terapia_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_paciente_terapia";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "paciente_terapia.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['paciente_terapia']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['paciente_terapia']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['paciente_terapia']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->t_producto_tratamiento = $Busca_temp['t_producto_tratamiento']; 
          $tmp_pos = strpos($this->t_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_producto_tratamiento = substr($this->t_producto_tratamiento, 0, $tmp_pos);
          }
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
          $tmp_pos = strpos($this->p_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
          $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
          }
          $this->t_dosis_tratamiento = $Busca_temp['t_dosis_tratamiento']; 
          $tmp_pos = strpos($this->t_dosis_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_dosis_tratamiento = substr($this->t_dosis_tratamiento, 0, $tmp_pos);
          }
          $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
          $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
          }
          $this->p_motivo_retiro_paciente = $Busca_temp['p_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_motivo_retiro_paciente = substr($this->p_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_observacion_motivo_retiro_paciente = $Busca_temp['p_observacion_motivo_retiro_paciente']; 
          $tmp_pos = strpos($this->p_observacion_motivo_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_observacion_motivo_retiro_paciente = substr($this->p_observacion_motivo_retiro_paciente, 0, $tmp_pos);
          }
          $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
          $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
          }
          $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
          $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
          }
          $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
          $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
          }
          $this->p_telefono_paciente = $Busca_temp['p_telefono_paciente']; 
          $tmp_pos = strpos($this->p_telefono_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono_paciente = substr($this->p_telefono_paciente, 0, $tmp_pos);
          }
          $this->p_telefono2_paciente = $Busca_temp['p_telefono2_paciente']; 
          $tmp_pos = strpos($this->p_telefono2_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono2_paciente = substr($this->p_telefono2_paciente, 0, $tmp_pos);
          }
          $this->p_telefono3_paciente = $Busca_temp['p_telefono3_paciente']; 
          $tmp_pos = strpos($this->p_telefono3_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_telefono3_paciente = substr($this->p_telefono3_paciente, 0, $tmp_pos);
          }
          $this->p_correo_paciente = $Busca_temp['p_correo_paciente']; 
          $tmp_pos = strpos($this->p_correo_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_correo_paciente = substr($this->p_correo_paciente, 0, $tmp_pos);
          }
          $this->p_direccion_paciente = $Busca_temp['p_direccion_paciente']; 
          $tmp_pos = strpos($this->p_direccion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_direccion_paciente = substr($this->p_direccion_paciente, 0, $tmp_pos);
          }
          $this->p_barrio_paciente = $Busca_temp['p_barrio_paciente']; 
          $tmp_pos = strpos($this->p_barrio_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_barrio_paciente = substr($this->p_barrio_paciente, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
          $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
          }
          $this->t_zona_atencion_paramedico_tratamiento = $Busca_temp['t_zona_atencion_paramedico_tratamiento']; 
          $tmp_pos = strpos($this->t_zona_atencion_paramedico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_zona_atencion_paramedico_tratamiento = substr($this->t_zona_atencion_paramedico_tratamiento, 0, $tmp_pos);
          }
          $this->p_genero_paciente = $Busca_temp['p_genero_paciente']; 
          $tmp_pos = strpos($this->p_genero_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_genero_paciente = substr($this->p_genero_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_nacimineto_paciente = $Busca_temp['p_fecha_nacimineto_paciente']; 
          $tmp_pos = strpos($this->p_fecha_nacimineto_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_nacimineto_paciente = substr($this->p_fecha_nacimineto_paciente, 0, $tmp_pos);
          }
          $this->p_edad_paciente = $Busca_temp['p_edad_paciente']; 
          $tmp_pos = strpos($this->p_edad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_edad_paciente = substr($this->p_edad_paciente, 0, $tmp_pos);
          }
          $this->p_edad_paciente_2 = $Busca_temp['p_edad_paciente_input_2']; 
          $this->p_codigo_xofigo = $Busca_temp['p_codigo_xofigo']; 
          $tmp_pos = strpos($this->p_codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_codigo_xofigo = substr($this->p_codigo_xofigo, 0, $tmp_pos);
          }
          $this->p_codigo_xofigo_2 = $Busca_temp['p_codigo_xofigo_input_2']; 
          $this->p_usuario_creacion = $Busca_temp['p_usuario_creacion']; 
          $tmp_pos = strpos($this->p_usuario_creacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_usuario_creacion = substr($this->p_usuario_creacion, 0, $tmp_pos);
          }
          $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
          $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->t_punto_entrega = $Busca_temp['t_punto_entrega']; 
          $tmp_pos = strpos($this->t_punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_punto_entrega = substr($this->t_punto_entrega, 0, $tmp_pos);
          }
          $this->t_fecha_ultima_reclamacion_tratamiento = $Busca_temp['t_fecha_ultima_reclamacion_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_ultima_reclamacion_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_ultima_reclamacion_tratamiento = substr($this->t_fecha_ultima_reclamacion_tratamiento, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.BARRIO_PACIENTE as p_barrio_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, t.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.EDAD_PACIENTE as p_edad_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, p.USUARIO_CREACION as p_usuario_creacion, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.PUNTO_ENTREGA as t_punto_entrega, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "PAP"; 
          if ($Cada_col == "p_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "t_producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "p_estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_status_paciente'])) ? $this->New_label['p_status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "p_status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "p_fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          if ($Cada_col == "t_clasificacion_patologica_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "t_dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          if ($Cada_col == "t_medico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_motivo_retiro_paciente'])) ? $this->New_label['p_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "p_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_observacion_motivo_retiro_paciente'])) ? $this->New_label['p_observacion_motivo_retiro_paciente'] : "OBSERVACION MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "p_observacion_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_identificacion_paciente'])) ? $this->New_label['p_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          if ($Cada_col == "p_identificacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_nombre_paciente'])) ? $this->New_label['p_nombre_paciente'] : "NOMBRE PACIENTE"; 
          if ($Cada_col == "p_nombre_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_apellido_paciente'])) ? $this->New_label['p_apellido_paciente'] : "APELLIDO PACIENTE"; 
          if ($Cada_col == "p_apellido_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_telefono_paciente'])) ? $this->New_label['p_telefono_paciente'] : "TELEFONO PACIENTE"; 
          if ($Cada_col == "p_telefono_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_telefono2_paciente'])) ? $this->New_label['p_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          if ($Cada_col == "p_telefono2_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_telefono3_paciente'])) ? $this->New_label['p_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          if ($Cada_col == "p_telefono3_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_correo_paciente'])) ? $this->New_label['p_correo_paciente'] : "CORREO PACIENTE"; 
          if ($Cada_col == "p_correo_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_direccion_paciente'])) ? $this->New_label['p_direccion_paciente'] : "DIRECCION PACIENTE"; 
          if ($Cada_col == "p_direccion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_barrio_paciente'])) ? $this->New_label['p_barrio_paciente'] : "BARRIO PACIENTE"; 
          if ($Cada_col == "p_barrio_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          if ($Cada_col == "p_pais_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "p_ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_zona_atencion_paramedico_tratamiento'])) ? $this->New_label['t_zona_atencion_paramedico_tratamiento'] : "ZONA ATENCION PARAMEDICO TRATAMIENTO"; 
          if ($Cada_col == "t_zona_atencion_paramedico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_genero_paciente'])) ? $this->New_label['p_genero_paciente'] : "GENERO PACIENTE"; 
          if ($Cada_col == "p_genero_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_fecha_nacimineto_paciente'])) ? $this->New_label['p_fecha_nacimineto_paciente'] : "FECHA NACIMINETO PACIENTE"; 
          if ($Cada_col == "p_fecha_nacimineto_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_edad_paciente'])) ? $this->New_label['p_edad_paciente'] : "EDAD PACIENTE"; 
          if ($Cada_col == "p_edad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "p_codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_usuario_creacion'])) ? $this->New_label['p_usuario_creacion'] : "USUARIO CREACION"; 
          if ($Cada_col == "p_usuario_creacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "t_asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "t_operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "t_punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['t_fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          if ($Cada_col == "t_fecha_ultima_reclamacion_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->p_id_paciente = $rs->fields[0] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->t_producto_tratamiento = $rs->fields[1] ;  
         $this->p_estado_paciente = $rs->fields[2] ;  
         $this->p_status_paciente = $rs->fields[3] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[4] ;  
         $this->t_clasificacion_patologica_tratamiento = $rs->fields[5] ;  
         $this->t_dosis_tratamiento = $rs->fields[6] ;  
         $this->t_medico_tratamiento = $rs->fields[7] ;  
         $this->p_motivo_retiro_paciente = $rs->fields[8] ;  
         $this->p_observacion_motivo_retiro_paciente = $rs->fields[9] ;  
         $this->p_identificacion_paciente = $rs->fields[10] ;  
         $this->p_nombre_paciente = $rs->fields[11] ;  
         $this->p_apellido_paciente = $rs->fields[12] ;  
         $this->p_telefono_paciente = $rs->fields[13] ;  
         $this->p_telefono2_paciente = $rs->fields[14] ;  
         $this->p_telefono3_paciente = $rs->fields[15] ;  
         $this->p_correo_paciente = $rs->fields[16] ;  
         $this->p_direccion_paciente = $rs->fields[17] ;  
         $this->p_barrio_paciente = $rs->fields[18] ;  
         $this->p_pais_paciente = $rs->fields[19] ;  
         $this->p_ciudad_paciente = $rs->fields[20] ;  
         $this->t_zona_atencion_paramedico_tratamiento = $rs->fields[21] ;  
         $this->p_genero_paciente = $rs->fields[22] ;  
         $this->p_fecha_nacimineto_paciente = $rs->fields[23] ;  
         $this->p_edad_paciente = $rs->fields[24] ;  
         $this->p_edad_paciente = (string)$this->p_edad_paciente;
         $this->p_codigo_xofigo = $rs->fields[25] ;  
         $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
         $this->p_usuario_creacion = $rs->fields[26] ;  
         $this->t_asegurador_tratamiento = $rs->fields[27] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[28] ;  
         $this->t_punto_entrega = $rs->fields[29] ;  
         $this->t_fecha_ultima_reclamacion_tratamiento = $rs->fields[30] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         $this->p_id_paciente = html_entity_decode($this->p_id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_id_paciente = strip_tags($this->p_id_paciente);
         if (!NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_id_paciente = str_replace('<', '&lt;', $this->p_id_paciente);
         $this->p_id_paciente = str_replace('>', '&gt;', $this->p_id_paciente);
         $this->Texto_tag .= "<td>" . $this->p_id_paciente . "</td>\r\n";
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
         $this->t_producto_tratamiento = html_entity_decode($this->t_producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_producto_tratamiento = strip_tags($this->t_producto_tratamiento);
         if (!NM_is_utf8($this->t_producto_tratamiento))
         {
             $this->t_producto_tratamiento = sc_convert_encoding($this->t_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_producto_tratamiento = str_replace('<', '&lt;', $this->t_producto_tratamiento);
         $this->t_producto_tratamiento = str_replace('>', '&gt;', $this->t_producto_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_producto_tratamiento . "</td>\r\n";
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
         $this->p_estado_paciente = html_entity_decode($this->p_estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_estado_paciente = strip_tags($this->p_estado_paciente);
         if (!NM_is_utf8($this->p_estado_paciente))
         {
             $this->p_estado_paciente = sc_convert_encoding($this->p_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_estado_paciente = str_replace('<', '&lt;', $this->p_estado_paciente);
         $this->p_estado_paciente = str_replace('>', '&gt;', $this->p_estado_paciente);
         $this->Texto_tag .= "<td>" . $this->p_estado_paciente . "</td>\r\n";
   }
   //----- p_status_paciente
   function NM_export_p_status_paciente()
   {
         $this->p_status_paciente = html_entity_decode($this->p_status_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_status_paciente = strip_tags($this->p_status_paciente);
         if (!NM_is_utf8($this->p_status_paciente))
         {
             $this->p_status_paciente = sc_convert_encoding($this->p_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_status_paciente = str_replace('<', '&lt;', $this->p_status_paciente);
         $this->p_status_paciente = str_replace('>', '&gt;', $this->p_status_paciente);
         $this->Texto_tag .= "<td>" . $this->p_status_paciente . "</td>\r\n";
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         $this->p_fecha_activacion_paciente = html_entity_decode($this->p_fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_fecha_activacion_paciente = strip_tags($this->p_fecha_activacion_paciente);
         if (!NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_fecha_activacion_paciente = str_replace('<', '&lt;', $this->p_fecha_activacion_paciente);
         $this->p_fecha_activacion_paciente = str_replace('>', '&gt;', $this->p_fecha_activacion_paciente);
         $this->Texto_tag .= "<td>" . $this->p_fecha_activacion_paciente . "</td>\r\n";
   }
   //----- t_clasificacion_patologica_tratamiento
   function NM_export_t_clasificacion_patologica_tratamiento()
   {
         $this->t_clasificacion_patologica_tratamiento = html_entity_decode($this->t_clasificacion_patologica_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_clasificacion_patologica_tratamiento = strip_tags($this->t_clasificacion_patologica_tratamiento);
         if (!NM_is_utf8($this->t_clasificacion_patologica_tratamiento))
         {
             $this->t_clasificacion_patologica_tratamiento = sc_convert_encoding($this->t_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_clasificacion_patologica_tratamiento = str_replace('<', '&lt;', $this->t_clasificacion_patologica_tratamiento);
         $this->t_clasificacion_patologica_tratamiento = str_replace('>', '&gt;', $this->t_clasificacion_patologica_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_clasificacion_patologica_tratamiento . "</td>\r\n";
   }
   //----- t_dosis_tratamiento
   function NM_export_t_dosis_tratamiento()
   {
         $this->t_dosis_tratamiento = html_entity_decode($this->t_dosis_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_dosis_tratamiento = strip_tags($this->t_dosis_tratamiento);
         if (!NM_is_utf8($this->t_dosis_tratamiento))
         {
             $this->t_dosis_tratamiento = sc_convert_encoding($this->t_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_dosis_tratamiento = str_replace('<', '&lt;', $this->t_dosis_tratamiento);
         $this->t_dosis_tratamiento = str_replace('>', '&gt;', $this->t_dosis_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_dosis_tratamiento . "</td>\r\n";
   }
   //----- t_medico_tratamiento
   function NM_export_t_medico_tratamiento()
   {
         $this->t_medico_tratamiento = html_entity_decode($this->t_medico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_medico_tratamiento = strip_tags($this->t_medico_tratamiento);
         if (!NM_is_utf8($this->t_medico_tratamiento))
         {
             $this->t_medico_tratamiento = sc_convert_encoding($this->t_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_medico_tratamiento = str_replace('<', '&lt;', $this->t_medico_tratamiento);
         $this->t_medico_tratamiento = str_replace('>', '&gt;', $this->t_medico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_medico_tratamiento . "</td>\r\n";
   }
   //----- p_motivo_retiro_paciente
   function NM_export_p_motivo_retiro_paciente()
   {
         $this->p_motivo_retiro_paciente = html_entity_decode($this->p_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_motivo_retiro_paciente = strip_tags($this->p_motivo_retiro_paciente);
         if (!NM_is_utf8($this->p_motivo_retiro_paciente))
         {
             $this->p_motivo_retiro_paciente = sc_convert_encoding($this->p_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_motivo_retiro_paciente = str_replace('<', '&lt;', $this->p_motivo_retiro_paciente);
         $this->p_motivo_retiro_paciente = str_replace('>', '&gt;', $this->p_motivo_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->p_motivo_retiro_paciente . "</td>\r\n";
   }
   //----- p_observacion_motivo_retiro_paciente
   function NM_export_p_observacion_motivo_retiro_paciente()
   {
         $this->p_observacion_motivo_retiro_paciente = html_entity_decode($this->p_observacion_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_observacion_motivo_retiro_paciente = strip_tags($this->p_observacion_motivo_retiro_paciente);
         if (!NM_is_utf8($this->p_observacion_motivo_retiro_paciente))
         {
             $this->p_observacion_motivo_retiro_paciente = sc_convert_encoding($this->p_observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_observacion_motivo_retiro_paciente = str_replace('<', '&lt;', $this->p_observacion_motivo_retiro_paciente);
         $this->p_observacion_motivo_retiro_paciente = str_replace('>', '&gt;', $this->p_observacion_motivo_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->p_observacion_motivo_retiro_paciente . "</td>\r\n";
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
         $this->p_identificacion_paciente = html_entity_decode($this->p_identificacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_identificacion_paciente = strip_tags($this->p_identificacion_paciente);
         if (!NM_is_utf8($this->p_identificacion_paciente))
         {
             $this->p_identificacion_paciente = sc_convert_encoding($this->p_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_identificacion_paciente = str_replace('<', '&lt;', $this->p_identificacion_paciente);
         $this->p_identificacion_paciente = str_replace('>', '&gt;', $this->p_identificacion_paciente);
         $this->Texto_tag .= "<td>" . $this->p_identificacion_paciente . "</td>\r\n";
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
         $this->p_nombre_paciente = html_entity_decode($this->p_nombre_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_nombre_paciente = strip_tags($this->p_nombre_paciente);
         if (!NM_is_utf8($this->p_nombre_paciente))
         {
             $this->p_nombre_paciente = sc_convert_encoding($this->p_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_nombre_paciente = str_replace('<', '&lt;', $this->p_nombre_paciente);
         $this->p_nombre_paciente = str_replace('>', '&gt;', $this->p_nombre_paciente);
         $this->Texto_tag .= "<td>" . $this->p_nombre_paciente . "</td>\r\n";
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
         $this->p_apellido_paciente = html_entity_decode($this->p_apellido_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_apellido_paciente = strip_tags($this->p_apellido_paciente);
         if (!NM_is_utf8($this->p_apellido_paciente))
         {
             $this->p_apellido_paciente = sc_convert_encoding($this->p_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_apellido_paciente = str_replace('<', '&lt;', $this->p_apellido_paciente);
         $this->p_apellido_paciente = str_replace('>', '&gt;', $this->p_apellido_paciente);
         $this->Texto_tag .= "<td>" . $this->p_apellido_paciente . "</td>\r\n";
   }
   //----- p_telefono_paciente
   function NM_export_p_telefono_paciente()
   {
         $this->p_telefono_paciente = html_entity_decode($this->p_telefono_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_telefono_paciente = strip_tags($this->p_telefono_paciente);
         if (!NM_is_utf8($this->p_telefono_paciente))
         {
             $this->p_telefono_paciente = sc_convert_encoding($this->p_telefono_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_telefono_paciente = str_replace('<', '&lt;', $this->p_telefono_paciente);
         $this->p_telefono_paciente = str_replace('>', '&gt;', $this->p_telefono_paciente);
         $this->Texto_tag .= "<td>" . $this->p_telefono_paciente . "</td>\r\n";
   }
   //----- p_telefono2_paciente
   function NM_export_p_telefono2_paciente()
   {
         $this->p_telefono2_paciente = html_entity_decode($this->p_telefono2_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_telefono2_paciente = strip_tags($this->p_telefono2_paciente);
         if (!NM_is_utf8($this->p_telefono2_paciente))
         {
             $this->p_telefono2_paciente = sc_convert_encoding($this->p_telefono2_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_telefono2_paciente = str_replace('<', '&lt;', $this->p_telefono2_paciente);
         $this->p_telefono2_paciente = str_replace('>', '&gt;', $this->p_telefono2_paciente);
         $this->Texto_tag .= "<td>" . $this->p_telefono2_paciente . "</td>\r\n";
   }
   //----- p_telefono3_paciente
   function NM_export_p_telefono3_paciente()
   {
         $this->p_telefono3_paciente = html_entity_decode($this->p_telefono3_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_telefono3_paciente = strip_tags($this->p_telefono3_paciente);
         if (!NM_is_utf8($this->p_telefono3_paciente))
         {
             $this->p_telefono3_paciente = sc_convert_encoding($this->p_telefono3_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_telefono3_paciente = str_replace('<', '&lt;', $this->p_telefono3_paciente);
         $this->p_telefono3_paciente = str_replace('>', '&gt;', $this->p_telefono3_paciente);
         $this->Texto_tag .= "<td>" . $this->p_telefono3_paciente . "</td>\r\n";
   }
   //----- p_correo_paciente
   function NM_export_p_correo_paciente()
   {
         $this->p_correo_paciente = html_entity_decode($this->p_correo_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_correo_paciente = strip_tags($this->p_correo_paciente);
         if (!NM_is_utf8($this->p_correo_paciente))
         {
             $this->p_correo_paciente = sc_convert_encoding($this->p_correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_correo_paciente = str_replace('<', '&lt;', $this->p_correo_paciente);
         $this->p_correo_paciente = str_replace('>', '&gt;', $this->p_correo_paciente);
         $this->Texto_tag .= "<td>" . $this->p_correo_paciente . "</td>\r\n";
   }
   //----- p_direccion_paciente
   function NM_export_p_direccion_paciente()
   {
         $this->p_direccion_paciente = html_entity_decode($this->p_direccion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_direccion_paciente = strip_tags($this->p_direccion_paciente);
         if (!NM_is_utf8($this->p_direccion_paciente))
         {
             $this->p_direccion_paciente = sc_convert_encoding($this->p_direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_direccion_paciente = str_replace('<', '&lt;', $this->p_direccion_paciente);
         $this->p_direccion_paciente = str_replace('>', '&gt;', $this->p_direccion_paciente);
         $this->Texto_tag .= "<td>" . $this->p_direccion_paciente . "</td>\r\n";
   }
   //----- p_barrio_paciente
   function NM_export_p_barrio_paciente()
   {
         $this->p_barrio_paciente = html_entity_decode($this->p_barrio_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_barrio_paciente = strip_tags($this->p_barrio_paciente);
         if (!NM_is_utf8($this->p_barrio_paciente))
         {
             $this->p_barrio_paciente = sc_convert_encoding($this->p_barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_barrio_paciente = str_replace('<', '&lt;', $this->p_barrio_paciente);
         $this->p_barrio_paciente = str_replace('>', '&gt;', $this->p_barrio_paciente);
         $this->Texto_tag .= "<td>" . $this->p_barrio_paciente . "</td>\r\n";
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         $this->p_pais_paciente = html_entity_decode($this->p_pais_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_pais_paciente = strip_tags($this->p_pais_paciente);
         if (!NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_pais_paciente = str_replace('<', '&lt;', $this->p_pais_paciente);
         $this->p_pais_paciente = str_replace('>', '&gt;', $this->p_pais_paciente);
         $this->Texto_tag .= "<td>" . $this->p_pais_paciente . "</td>\r\n";
   }
   //----- p_ciudad_paciente
   function NM_export_p_ciudad_paciente()
   {
         $this->p_ciudad_paciente = html_entity_decode($this->p_ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_ciudad_paciente = strip_tags($this->p_ciudad_paciente);
         if (!NM_is_utf8($this->p_ciudad_paciente))
         {
             $this->p_ciudad_paciente = sc_convert_encoding($this->p_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_ciudad_paciente = str_replace('<', '&lt;', $this->p_ciudad_paciente);
         $this->p_ciudad_paciente = str_replace('>', '&gt;', $this->p_ciudad_paciente);
         $this->Texto_tag .= "<td>" . $this->p_ciudad_paciente . "</td>\r\n";
   }
   //----- t_zona_atencion_paramedico_tratamiento
   function NM_export_t_zona_atencion_paramedico_tratamiento()
   {
         $this->t_zona_atencion_paramedico_tratamiento = html_entity_decode($this->t_zona_atencion_paramedico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_zona_atencion_paramedico_tratamiento = strip_tags($this->t_zona_atencion_paramedico_tratamiento);
         if (!NM_is_utf8($this->t_zona_atencion_paramedico_tratamiento))
         {
             $this->t_zona_atencion_paramedico_tratamiento = sc_convert_encoding($this->t_zona_atencion_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_zona_atencion_paramedico_tratamiento = str_replace('<', '&lt;', $this->t_zona_atencion_paramedico_tratamiento);
         $this->t_zona_atencion_paramedico_tratamiento = str_replace('>', '&gt;', $this->t_zona_atencion_paramedico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_zona_atencion_paramedico_tratamiento . "</td>\r\n";
   }
   //----- p_genero_paciente
   function NM_export_p_genero_paciente()
   {
         $this->p_genero_paciente = html_entity_decode($this->p_genero_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_genero_paciente = strip_tags($this->p_genero_paciente);
         if (!NM_is_utf8($this->p_genero_paciente))
         {
             $this->p_genero_paciente = sc_convert_encoding($this->p_genero_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_genero_paciente = str_replace('<', '&lt;', $this->p_genero_paciente);
         $this->p_genero_paciente = str_replace('>', '&gt;', $this->p_genero_paciente);
         $this->Texto_tag .= "<td>" . $this->p_genero_paciente . "</td>\r\n";
   }
   //----- p_fecha_nacimineto_paciente
   function NM_export_p_fecha_nacimineto_paciente()
   {
         $this->p_fecha_nacimineto_paciente = html_entity_decode($this->p_fecha_nacimineto_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_fecha_nacimineto_paciente = strip_tags($this->p_fecha_nacimineto_paciente);
         if (!NM_is_utf8($this->p_fecha_nacimineto_paciente))
         {
             $this->p_fecha_nacimineto_paciente = sc_convert_encoding($this->p_fecha_nacimineto_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_fecha_nacimineto_paciente = str_replace('<', '&lt;', $this->p_fecha_nacimineto_paciente);
         $this->p_fecha_nacimineto_paciente = str_replace('>', '&gt;', $this->p_fecha_nacimineto_paciente);
         $this->Texto_tag .= "<td>" . $this->p_fecha_nacimineto_paciente . "</td>\r\n";
   }
   //----- p_edad_paciente
   function NM_export_p_edad_paciente()
   {
         nmgp_Form_Num_Val($this->p_edad_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->p_edad_paciente))
         {
             $this->p_edad_paciente = sc_convert_encoding($this->p_edad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_edad_paciente = str_replace('<', '&lt;', $this->p_edad_paciente);
         $this->p_edad_paciente = str_replace('>', '&gt;', $this->p_edad_paciente);
         $this->Texto_tag .= "<td>" . $this->p_edad_paciente . "</td>\r\n";
   }
   //----- p_codigo_xofigo
   function NM_export_p_codigo_xofigo()
   {
         nmgp_Form_Num_Val($this->p_codigo_xofigo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->p_codigo_xofigo))
         {
             $this->p_codigo_xofigo = sc_convert_encoding($this->p_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_codigo_xofigo = str_replace('<', '&lt;', $this->p_codigo_xofigo);
         $this->p_codigo_xofigo = str_replace('>', '&gt;', $this->p_codigo_xofigo);
         $this->Texto_tag .= "<td>" . $this->p_codigo_xofigo . "</td>\r\n";
   }
   //----- p_usuario_creacion
   function NM_export_p_usuario_creacion()
   {
         $this->p_usuario_creacion = html_entity_decode($this->p_usuario_creacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_usuario_creacion = strip_tags($this->p_usuario_creacion);
         if (!NM_is_utf8($this->p_usuario_creacion))
         {
             $this->p_usuario_creacion = sc_convert_encoding($this->p_usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_usuario_creacion = str_replace('<', '&lt;', $this->p_usuario_creacion);
         $this->p_usuario_creacion = str_replace('>', '&gt;', $this->p_usuario_creacion);
         $this->Texto_tag .= "<td>" . $this->p_usuario_creacion . "</td>\r\n";
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
         $this->t_asegurador_tratamiento = html_entity_decode($this->t_asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_asegurador_tratamiento = strip_tags($this->t_asegurador_tratamiento);
         if (!NM_is_utf8($this->t_asegurador_tratamiento))
         {
             $this->t_asegurador_tratamiento = sc_convert_encoding($this->t_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_asegurador_tratamiento = str_replace('<', '&lt;', $this->t_asegurador_tratamiento);
         $this->t_asegurador_tratamiento = str_replace('>', '&gt;', $this->t_asegurador_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_asegurador_tratamiento . "</td>\r\n";
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
         $this->t_operador_logistico_tratamiento = html_entity_decode($this->t_operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_operador_logistico_tratamiento = strip_tags($this->t_operador_logistico_tratamiento);
         if (!NM_is_utf8($this->t_operador_logistico_tratamiento))
         {
             $this->t_operador_logistico_tratamiento = sc_convert_encoding($this->t_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_operador_logistico_tratamiento = str_replace('<', '&lt;', $this->t_operador_logistico_tratamiento);
         $this->t_operador_logistico_tratamiento = str_replace('>', '&gt;', $this->t_operador_logistico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_operador_logistico_tratamiento . "</td>\r\n";
   }
   //----- t_punto_entrega
   function NM_export_t_punto_entrega()
   {
         $this->t_punto_entrega = html_entity_decode($this->t_punto_entrega, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_punto_entrega = strip_tags($this->t_punto_entrega);
         if (!NM_is_utf8($this->t_punto_entrega))
         {
             $this->t_punto_entrega = sc_convert_encoding($this->t_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_punto_entrega = str_replace('<', '&lt;', $this->t_punto_entrega);
         $this->t_punto_entrega = str_replace('>', '&gt;', $this->t_punto_entrega);
         $this->Texto_tag .= "<td>" . $this->t_punto_entrega . "</td>\r\n";
   }
   //----- t_fecha_ultima_reclamacion_tratamiento
   function NM_export_t_fecha_ultima_reclamacion_tratamiento()
   {
         $this->t_fecha_ultima_reclamacion_tratamiento = html_entity_decode($this->t_fecha_ultima_reclamacion_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_fecha_ultima_reclamacion_tratamiento = strip_tags($this->t_fecha_ultima_reclamacion_tratamiento);
         if (!NM_is_utf8($this->t_fecha_ultima_reclamacion_tratamiento))
         {
             $this->t_fecha_ultima_reclamacion_tratamiento = sc_convert_encoding($this->t_fecha_ultima_reclamacion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_fecha_ultima_reclamacion_tratamiento = str_replace('<', '&lt;', $this->t_fecha_ultima_reclamacion_tratamiento);
         $this->t_fecha_ultima_reclamacion_tratamiento = str_replace('>', '&gt;', $this->t_fecha_ultima_reclamacion_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_fecha_ultima_reclamacion_tratamiento . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['paciente_terapia'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>PACIENTE TERAPIA :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="paciente_terapia_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="paciente_terapia"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
