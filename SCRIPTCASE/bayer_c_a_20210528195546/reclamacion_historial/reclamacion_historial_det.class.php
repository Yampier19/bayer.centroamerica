<?php
//--- 
class reclamacion_historial_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $p_id_paciente;
   var $g_logro_comunicacion_gestion;
   var $g_fecha_comunicacion;
   var $g_autor_gestion;
   var $p_pais_paciente;
   var $p_ciudad_paciente;
   var $p_estado_paciente;
   var $p_status_paciente;
   var $p_fecha_activacion_paciente;
   var $p_codigo_xofigo;
   var $t_producto_tratamiento;
   var $t_nombre_referencia;
   var $t_dosis_tratamiento;
   var $g_numero_cajas;
   var $t_asegurador_tratamiento;
   var $t_operador_logistico_tratamiento;
   var $t_punto_entrega;
   var $g_reclamo_gestion;
   var $g_fecha_reclamacion_gestion;
   var $g_causa_no_reclamacion_gestion;
   var $hr_id_historial_reclamacion;
   var $hr_anio_historial_reclamacion;
   var $hr_mes1;
   var $hr_reclamo1;
   var $hr_fecha_reclamacion1;
   var $hr_motivo_no_reclamacion1;
   var $hr_mes2;
   var $hr_reclamo2;
   var $hr_fecha_reclamacion2;
   var $hr_motivo_no_reclamacion2;
   var $hr_mes3;
   var $hr_reclamo3;
   var $hr_fecha_reclamacion3;
   var $hr_motivo_no_reclamacion3;
   var $hr_mes4;
   var $hr_reclamo4;
   var $hr_fecha_reclamacion4;
   var $hr_motivo_no_reclamacion4;
   var $hr_mes5;
   var $hr_reclamo5;
   var $hr_fecha_reclamacion5;
   var $hr_motivo_no_reclamacion5;
   var $hr_mes6;
   var $hr_reclamo6;
   var $hr_fecha_reclamacion6;
   var $hr_motivo_no_reclamacion6;
   var $hr_mes7;
   var $hr_reclamo7;
   var $hr_fecha_reclamacion7;
   var $hr_motivo_no_reclamacion7;
   var $hr_mes8;
   var $hr_reclamo8;
   var $hr_fecha_reclamacion8;
   var $hr_motivo_no_reclamacion8;
   var $hr_mes9;
   var $hr_reclamo9;
   var $hr_fecha_reclamacion9;
   var $hr_motivo_no_reclamacion9;
   var $hr_mes10;
   var $hr_reclamo10;
   var $hr_fecha_reclamacion10;
   var $hr_motivo_no_reclamacion10;
   var $hr_mes11;
   var $hr_reclamo11;
   var $hr_fecha_reclamacion11;
   var $hr_motivo_no_reclamacion11;
   var $hr_mes12;
   var $hr_reclamo12;
   var $hr_fecha_reclamacion12;
   var $hr_motivo_no_reclamacion12;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
        $tmp_pos = strpos($this->p_id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
        }
        $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
        $this->g_logro_comunicacion_gestion = $Busca_temp['g_logro_comunicacion_gestion']; 
        $tmp_pos = strpos($this->g_logro_comunicacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_logro_comunicacion_gestion = substr($this->g_logro_comunicacion_gestion, 0, $tmp_pos);
        }
        $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
        $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
        }
        $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
        $tmp_pos = strpos($this->g_autor_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
        }
        $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
        $tmp_pos = strpos($this->p_pais_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
        }
        $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
        $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
        }
        $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
        $tmp_pos = strpos($this->p_estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
        }
        $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
        $tmp_pos = strpos($this->p_status_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
        }
        $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
        $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
        }
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.STATUS_PACIENTE = '$parms_det[7]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and p.CODIGO_XOFIGO = $parms_det[9] and t.PRODUCTO_TRATAMIENTO = '$parms_det[10]' and t.NOMBRE_REFERENCIA = '$parms_det[11]' and t.DOSIS_TRATAMIENTO = '$parms_det[12]' and g.NUMERO_CAJAS = '$parms_det[13]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and t.PUNTO_ENTREGA = '$parms_det[16]' and g.RECLAMO_GESTION = '$parms_det[17]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[18]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and hr.ID_HISTORIAL_RECLAMACION = $parms_det[20] and hr.ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and hr.MES1 = '$parms_det[22]' and hr.RECLAMO1 = '$parms_det[23]' and hr.FECHA_RECLAMACION1 = '$parms_det[24]' and hr.MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and hr.MES2 = '$parms_det[26]' and hr.RECLAMO2 = '$parms_det[27]' and hr.FECHA_RECLAMACION2 = '$parms_det[28]' and hr.MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and hr.MES3 = '$parms_det[30]' and hr.RECLAMO3 = '$parms_det[31]' and hr.FECHA_RECLAMACION3 = '$parms_det[32]' and hr.MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and hr.MES4 = '$parms_det[34]' and hr.RECLAMO4 = '$parms_det[35]' and hr.FECHA_RECLAMACION4 = '$parms_det[36]' and hr.MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and hr.MES5 = '$parms_det[38]' and hr.RECLAMO5 = '$parms_det[39]' and hr.FECHA_RECLAMACION5 = '$parms_det[40]' and hr.MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and hr.MES6 = '$parms_det[42]' and hr.RECLAMO6 = '$parms_det[43]' and hr.FECHA_RECLAMACION6 = '$parms_det[44]' and hr.MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and hr.MES7 = '$parms_det[46]' and hr.RECLAMO7 = '$parms_det[47]' and hr.FECHA_RECLAMACION7 = '$parms_det[48]' and hr.MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and hr.MES8 = '$parms_det[50]' and hr.RECLAMO8 = '$parms_det[51]' and hr.FECHA_RECLAMACION8 = '$parms_det[52]' and hr.MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and hr.MES9 = '$parms_det[54]' and hr.RECLAMO9 = '$parms_det[55]' and hr.FECHA_RECLAMACION9 = '$parms_det[56]' and hr.MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and hr.MES10 = '$parms_det[58]' and hr.RECLAMO10 = '$parms_det[59]' and hr.FECHA_RECLAMACION10 = '$parms_det[60]' and hr.MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and hr.MES11 = '$parms_det[62]' and hr.RECLAMO11 = '$parms_det[63]' and hr.FECHA_RECLAMACION11 = '$parms_det[64]' and hr.MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and hr.MES12 = '$parms_det[66]' and hr.RECLAMO12 = '$parms_det[67]' and hr.FECHA_RECLAMACION12 = '$parms_det[68]' and hr.MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.STATUS_PACIENTE = '$parms_det[7]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and p.CODIGO_XOFIGO = $parms_det[9] and t.PRODUCTO_TRATAMIENTO = '$parms_det[10]' and t.NOMBRE_REFERENCIA = '$parms_det[11]' and t.DOSIS_TRATAMIENTO = '$parms_det[12]' and g.NUMERO_CAJAS = '$parms_det[13]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and t.PUNTO_ENTREGA = '$parms_det[16]' and g.RECLAMO_GESTION = '$parms_det[17]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[18]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and hr.ID_HISTORIAL_RECLAMACION = $parms_det[20] and hr.ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and hr.MES1 = '$parms_det[22]' and hr.RECLAMO1 = '$parms_det[23]' and hr.FECHA_RECLAMACION1 = '$parms_det[24]' and hr.MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and hr.MES2 = '$parms_det[26]' and hr.RECLAMO2 = '$parms_det[27]' and hr.FECHA_RECLAMACION2 = '$parms_det[28]' and hr.MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and hr.MES3 = '$parms_det[30]' and hr.RECLAMO3 = '$parms_det[31]' and hr.FECHA_RECLAMACION3 = '$parms_det[32]' and hr.MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and hr.MES4 = '$parms_det[34]' and hr.RECLAMO4 = '$parms_det[35]' and hr.FECHA_RECLAMACION4 = '$parms_det[36]' and hr.MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and hr.MES5 = '$parms_det[38]' and hr.RECLAMO5 = '$parms_det[39]' and hr.FECHA_RECLAMACION5 = '$parms_det[40]' and hr.MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and hr.MES6 = '$parms_det[42]' and hr.RECLAMO6 = '$parms_det[43]' and hr.FECHA_RECLAMACION6 = '$parms_det[44]' and hr.MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and hr.MES7 = '$parms_det[46]' and hr.RECLAMO7 = '$parms_det[47]' and hr.FECHA_RECLAMACION7 = '$parms_det[48]' and hr.MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and hr.MES8 = '$parms_det[50]' and hr.RECLAMO8 = '$parms_det[51]' and hr.FECHA_RECLAMACION8 = '$parms_det[52]' and hr.MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and hr.MES9 = '$parms_det[54]' and hr.RECLAMO9 = '$parms_det[55]' and hr.FECHA_RECLAMACION9 = '$parms_det[56]' and hr.MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and hr.MES10 = '$parms_det[58]' and hr.RECLAMO10 = '$parms_det[59]' and hr.FECHA_RECLAMACION10 = '$parms_det[60]' and hr.MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and hr.MES11 = '$parms_det[62]' and hr.RECLAMO11 = '$parms_det[63]' and hr.FECHA_RECLAMACION11 = '$parms_det[64]' and hr.MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and hr.MES12 = '$parms_det[66]' and hr.RECLAMO12 = '$parms_det[67]' and hr.FECHA_RECLAMACION12 = '$parms_det[68]' and hr.MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  p.ID_PACIENTE = $parms_det[0] and g.LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and g.FECHA_COMUNICACION = '$parms_det[2]' and g.AUTOR_GESTION = '$parms_det[3]' and p.PAIS_PACIENTE = '$parms_det[4]' and p.CIUDAD_PACIENTE = '$parms_det[5]' and p.ESTADO_PACIENTE = '$parms_det[6]' and p.STATUS_PACIENTE = '$parms_det[7]' and p.FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and p.CODIGO_XOFIGO = $parms_det[9] and t.PRODUCTO_TRATAMIENTO = '$parms_det[10]' and t.NOMBRE_REFERENCIA = '$parms_det[11]' and t.DOSIS_TRATAMIENTO = '$parms_det[12]' and g.NUMERO_CAJAS = '$parms_det[13]' and t.ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and t.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and t.PUNTO_ENTREGA = '$parms_det[16]' and g.RECLAMO_GESTION = '$parms_det[17]' and g.FECHA_RECLAMACION_GESTION = '$parms_det[18]' and g.CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and hr.ID_HISTORIAL_RECLAMACION = $parms_det[20] and hr.ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and hr.MES1 = '$parms_det[22]' and hr.RECLAMO1 = '$parms_det[23]' and hr.FECHA_RECLAMACION1 = '$parms_det[24]' and hr.MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and hr.MES2 = '$parms_det[26]' and hr.RECLAMO2 = '$parms_det[27]' and hr.FECHA_RECLAMACION2 = '$parms_det[28]' and hr.MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and hr.MES3 = '$parms_det[30]' and hr.RECLAMO3 = '$parms_det[31]' and hr.FECHA_RECLAMACION3 = '$parms_det[32]' and hr.MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and hr.MES4 = '$parms_det[34]' and hr.RECLAMO4 = '$parms_det[35]' and hr.FECHA_RECLAMACION4 = '$parms_det[36]' and hr.MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and hr.MES5 = '$parms_det[38]' and hr.RECLAMO5 = '$parms_det[39]' and hr.FECHA_RECLAMACION5 = '$parms_det[40]' and hr.MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and hr.MES6 = '$parms_det[42]' and hr.RECLAMO6 = '$parms_det[43]' and hr.FECHA_RECLAMACION6 = '$parms_det[44]' and hr.MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and hr.MES7 = '$parms_det[46]' and hr.RECLAMO7 = '$parms_det[47]' and hr.FECHA_RECLAMACION7 = '$parms_det[48]' and hr.MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and hr.MES8 = '$parms_det[50]' and hr.RECLAMO8 = '$parms_det[51]' and hr.FECHA_RECLAMACION8 = '$parms_det[52]' and hr.MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and hr.MES9 = '$parms_det[54]' and hr.RECLAMO9 = '$parms_det[55]' and hr.FECHA_RECLAMACION9 = '$parms_det[56]' and hr.MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and hr.MES10 = '$parms_det[58]' and hr.RECLAMO10 = '$parms_det[59]' and hr.FECHA_RECLAMACION10 = '$parms_det[60]' and hr.MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and hr.MES11 = '$parms_det[62]' and hr.RECLAMO11 = '$parms_det[63]' and hr.FECHA_RECLAMACION11 = '$parms_det[64]' and hr.MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and hr.MES12 = '$parms_det[66]' and hr.RECLAMO12 = '$parms_det[67]' and hr.FECHA_RECLAMACION12 = '$parms_det[68]' and hr.MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->p_id_paciente = $rs->fields[0] ;  
   $this->p_id_paciente = (string)$this->p_id_paciente;
   $this->g_logro_comunicacion_gestion = $rs->fields[1] ;  
   $this->g_fecha_comunicacion = $rs->fields[2] ;  
   $this->g_autor_gestion = $rs->fields[3] ;  
   $this->p_pais_paciente = $rs->fields[4] ;  
   $this->p_ciudad_paciente = $rs->fields[5] ;  
   $this->p_estado_paciente = $rs->fields[6] ;  
   $this->p_status_paciente = $rs->fields[7] ;  
   $this->p_fecha_activacion_paciente = $rs->fields[8] ;  
   $this->p_codigo_xofigo = $rs->fields[9] ;  
   $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
   $this->t_producto_tratamiento = $rs->fields[10] ;  
   $this->t_nombre_referencia = $rs->fields[11] ;  
   $this->t_dosis_tratamiento = $rs->fields[12] ;  
   $this->g_numero_cajas = $rs->fields[13] ;  
   $this->t_asegurador_tratamiento = $rs->fields[14] ;  
   $this->t_operador_logistico_tratamiento = $rs->fields[15] ;  
   $this->t_punto_entrega = $rs->fields[16] ;  
   $this->g_reclamo_gestion = $rs->fields[17] ;  
   $this->g_fecha_reclamacion_gestion = $rs->fields[18] ;  
   $this->g_causa_no_reclamacion_gestion = $rs->fields[19] ;  
   $this->hr_id_historial_reclamacion = $rs->fields[20] ;  
   $this->hr_id_historial_reclamacion = (string)$this->hr_id_historial_reclamacion;
   $this->hr_anio_historial_reclamacion = $rs->fields[21] ;  
   $this->hr_anio_historial_reclamacion = (string)$this->hr_anio_historial_reclamacion;
   $this->hr_mes1 = $rs->fields[22] ;  
   $this->hr_reclamo1 = $rs->fields[23] ;  
   $this->hr_fecha_reclamacion1 = $rs->fields[24] ;  
   $this->hr_motivo_no_reclamacion1 = $rs->fields[25] ;  
   $this->hr_mes2 = $rs->fields[26] ;  
   $this->hr_reclamo2 = $rs->fields[27] ;  
   $this->hr_fecha_reclamacion2 = $rs->fields[28] ;  
   $this->hr_motivo_no_reclamacion2 = $rs->fields[29] ;  
   $this->hr_mes3 = $rs->fields[30] ;  
   $this->hr_reclamo3 = $rs->fields[31] ;  
   $this->hr_fecha_reclamacion3 = $rs->fields[32] ;  
   $this->hr_motivo_no_reclamacion3 = $rs->fields[33] ;  
   $this->hr_mes4 = $rs->fields[34] ;  
   $this->hr_reclamo4 = $rs->fields[35] ;  
   $this->hr_fecha_reclamacion4 = $rs->fields[36] ;  
   $this->hr_motivo_no_reclamacion4 = $rs->fields[37] ;  
   $this->hr_mes5 = $rs->fields[38] ;  
   $this->hr_reclamo5 = $rs->fields[39] ;  
   $this->hr_fecha_reclamacion5 = $rs->fields[40] ;  
   $this->hr_motivo_no_reclamacion5 = $rs->fields[41] ;  
   $this->hr_mes6 = $rs->fields[42] ;  
   $this->hr_reclamo6 = $rs->fields[43] ;  
   $this->hr_fecha_reclamacion6 = $rs->fields[44] ;  
   $this->hr_motivo_no_reclamacion6 = $rs->fields[45] ;  
   $this->hr_mes7 = $rs->fields[46] ;  
   $this->hr_reclamo7 = $rs->fields[47] ;  
   $this->hr_fecha_reclamacion7 = $rs->fields[48] ;  
   $this->hr_motivo_no_reclamacion7 = $rs->fields[49] ;  
   $this->hr_mes8 = $rs->fields[50] ;  
   $this->hr_reclamo8 = $rs->fields[51] ;  
   $this->hr_fecha_reclamacion8 = $rs->fields[52] ;  
   $this->hr_motivo_no_reclamacion8 = $rs->fields[53] ;  
   $this->hr_mes9 = $rs->fields[54] ;  
   $this->hr_reclamo9 = $rs->fields[55] ;  
   $this->hr_fecha_reclamacion9 = $rs->fields[56] ;  
   $this->hr_motivo_no_reclamacion9 = $rs->fields[57] ;  
   $this->hr_mes10 = $rs->fields[58] ;  
   $this->hr_reclamo10 = $rs->fields[59] ;  
   $this->hr_fecha_reclamacion10 = $rs->fields[60] ;  
   $this->hr_motivo_no_reclamacion10 = $rs->fields[61] ;  
   $this->hr_mes11 = $rs->fields[62] ;  
   $this->hr_reclamo11 = $rs->fields[63] ;  
   $this->hr_fecha_reclamacion11 = $rs->fields[64] ;  
   $this->hr_motivo_no_reclamacion11 = $rs->fields[65] ;  
   $this->hr_mes12 = $rs->fields[66] ;  
   $this->hr_reclamo12 = $rs->fields[67] ;  
   $this->hr_fecha_reclamacion12 = $rs->fields[68] ;  
   $this->hr_motivo_no_reclamacion12 = $rs->fields[69] ;  
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Informe Reclamaciones Historial</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "reclamacion_historial/reclamacion_historial_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Informe Reclamaciones Historial</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "reclamacion_historial/reclamacion_historial_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "reclamacion_historial/reclamacion_historial_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_logro_comunicacion_gestion'])) ? $this->New_label['g_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_logro_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_logro_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_logro_comunicacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_comunicacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_comunicacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_fecha_comunicacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_autor_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_autor_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_autor_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_pais_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_pais_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_pais_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_status_paciente'])) ? $this->New_label['p_status_paciente'] : "STATUS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_status_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_status_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_status_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->p_fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_p_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->p_codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_p_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_p_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->t_nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_dosis_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_dosis_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_dosis_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_numero_cajas'])) ? $this->New_label['g_numero_cajas'] : "NUMERO CAJAS"; 
          $conteudo = trim(sc_strip_script($this->g_numero_cajas)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_numero_cajas_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_numero_cajas_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->t_operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_t_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->t_punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_t_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_t_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_reclamo_gestion'])) ? $this->New_label['g_reclamo_gestion'] : "RECLAMO GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_reclamo_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_reclamo_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_reclamo_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_fecha_reclamacion_gestion'])) ? $this->New_label['g_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_fecha_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_fecha_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_g_fecha_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['g_causa_no_reclamacion_gestion'])) ? $this->New_label['g_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->g_causa_no_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_g_causa_no_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_g_causa_no_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_id_historial_reclamacion'])) ? $this->New_label['hr_id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          $conteudo = trim(sc_strip_script($this->hr_id_historial_reclamacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_id_historial_reclamacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_id_historial_reclamacion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_anio_historial_reclamacion'])) ? $this->New_label['hr_anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          $conteudo = trim(NM_encode_input(sc_strip_script($this->hr_anio_historial_reclamacion))); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
              nmgp_Form_Num_Val($conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_anio_historial_reclamacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_anio_historial_reclamacion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes1'])) ? $this->New_label['hr_mes1'] : "MES1"; 
          $conteudo = trim(sc_strip_script($this->hr_mes1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo1'])) ? $this->New_label['hr_reclamo1'] : "RECLAMO1"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion1'])) ? $this->New_label['hr_fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion1'])) ? $this->New_label['hr_motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes2'])) ? $this->New_label['hr_mes2'] : "MES2"; 
          $conteudo = trim(sc_strip_script($this->hr_mes2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo2'])) ? $this->New_label['hr_reclamo2'] : "RECLAMO2"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion2'])) ? $this->New_label['hr_fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion2'])) ? $this->New_label['hr_motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes3'])) ? $this->New_label['hr_mes3'] : "MES3"; 
          $conteudo = trim(sc_strip_script($this->hr_mes3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo3'])) ? $this->New_label['hr_reclamo3'] : "RECLAMO3"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion3'])) ? $this->New_label['hr_fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion3'])) ? $this->New_label['hr_motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes4'])) ? $this->New_label['hr_mes4'] : "MES4"; 
          $conteudo = trim(sc_strip_script($this->hr_mes4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo4'])) ? $this->New_label['hr_reclamo4'] : "RECLAMO4"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion4'])) ? $this->New_label['hr_fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion4'])) ? $this->New_label['hr_motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes5'])) ? $this->New_label['hr_mes5'] : "MES5"; 
          $conteudo = trim(sc_strip_script($this->hr_mes5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo5'])) ? $this->New_label['hr_reclamo5'] : "RECLAMO5"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion5'])) ? $this->New_label['hr_fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion5'])) ? $this->New_label['hr_motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes6'])) ? $this->New_label['hr_mes6'] : "MES6"; 
          $conteudo = trim(sc_strip_script($this->hr_mes6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo6'])) ? $this->New_label['hr_reclamo6'] : "RECLAMO6"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion6'])) ? $this->New_label['hr_fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion6'])) ? $this->New_label['hr_motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes7'])) ? $this->New_label['hr_mes7'] : "MES7"; 
          $conteudo = trim(sc_strip_script($this->hr_mes7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo7'])) ? $this->New_label['hr_reclamo7'] : "RECLAMO7"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion7'])) ? $this->New_label['hr_fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion7'])) ? $this->New_label['hr_motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes8'])) ? $this->New_label['hr_mes8'] : "MES8"; 
          $conteudo = trim(sc_strip_script($this->hr_mes8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo8'])) ? $this->New_label['hr_reclamo8'] : "RECLAMO8"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion8'])) ? $this->New_label['hr_fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion8'])) ? $this->New_label['hr_motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes9'])) ? $this->New_label['hr_mes9'] : "MES9"; 
          $conteudo = trim(sc_strip_script($this->hr_mes9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo9'])) ? $this->New_label['hr_reclamo9'] : "RECLAMO9"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion9'])) ? $this->New_label['hr_fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion9'])) ? $this->New_label['hr_motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes10'])) ? $this->New_label['hr_mes10'] : "MES10"; 
          $conteudo = trim(sc_strip_script($this->hr_mes10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo10'])) ? $this->New_label['hr_reclamo10'] : "RECLAMO10"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion10'])) ? $this->New_label['hr_fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion10'])) ? $this->New_label['hr_motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes11'])) ? $this->New_label['hr_mes11'] : "MES11"; 
          $conteudo = trim(sc_strip_script($this->hr_mes11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo11'])) ? $this->New_label['hr_reclamo11'] : "RECLAMO11"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion11'])) ? $this->New_label['hr_fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion11'])) ? $this->New_label['hr_motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_mes12'])) ? $this->New_label['hr_mes12'] : "MES12"; 
          $conteudo = trim(sc_strip_script($this->hr_mes12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_mes12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_mes12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_reclamo12'])) ? $this->New_label['hr_reclamo12'] : "RECLAMO12"; 
          $conteudo = trim(sc_strip_script($this->hr_reclamo12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_reclamo12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_reclamo12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion12'])) ? $this->New_label['hr_fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          $conteudo = trim(sc_strip_script($this->hr_fecha_reclamacion12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_fecha_reclamacion12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_hr_fecha_reclamacion12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion12'])) ? $this->New_label['hr_motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          $conteudo = trim(sc_strip_script($this->hr_motivo_no_reclamacion12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_hr_motivo_no_reclamacion12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_hr_motivo_no_reclamacion12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"reclamacion_historial_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "reclamacion_historial/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "reclamacion_historial/reclamacion_historial_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
