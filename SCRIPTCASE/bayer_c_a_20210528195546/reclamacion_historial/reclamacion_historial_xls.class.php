<?php

class reclamacion_historial_xls
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Xls_dados;
   var $Xls_workbook;
   var $Xls_col;
   var $Xls_row;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();
   var $Arquivo;
   var $Tit_doc;
   //---- 
   function reclamacion_historial_xls()
   {
   }

   //---- 
   function monta_xls()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $this->Xls_row = 1;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      set_include_path(get_include_path() . PATH_SEPARATOR . $this->Ini->path_third . '/phpexcel/');
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/IOFactory.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/Cell/AdvancedValueBinder.php';
      $orig_form_dt = strtoupper($_SESSION['scriptcase']['reg_conf']['date_format']);
      $this->SC_date_conf_region = "";
      for ($i = 0; $i < 8; $i++)
      {
          if ($i > 0 && substr($orig_form_dt, $i, 1) != substr($this->SC_date_conf_region, -1, 1)) {
              $this->SC_date_conf_region .= $_SESSION['scriptcase']['reg_conf']['date_sep'];
          }
          $this->SC_date_conf_region .= substr($orig_form_dt, $i, 1);
      }
      $this->Xls_col    = 0;
      $this->nm_data    = new nm_data("es");
      $this->Arquivo    = "sc_xls";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_reclamacion_historial";
      $this->Arquivo   .= ".xls";
      $this->Tit_doc    = "reclamacion_historial.xls";
      $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );;
      $this->Xls_dados = new PHPExcel();
      $this->Xls_dados->setActiveSheetIndex(0);
      $this->Nm_ActiveSheet = $this->Xls_dados->getActiveSheet();
      if ($_SESSION['scriptcase']['reg_conf']['css_dir'] == "RTL")
      {
          $this->Nm_ActiveSheet->setRightToLeft(true);
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['reclamacion_historial']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->g_logro_comunicacion_gestion = $Busca_temp['g_logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->g_logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_logro_comunicacion_gestion = substr($this->g_logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->g_fecha_comunicacion = $Busca_temp['g_fecha_comunicacion']; 
          $tmp_pos = strpos($this->g_fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_fecha_comunicacion = substr($this->g_fecha_comunicacion, 0, $tmp_pos);
          }
          $this->g_autor_gestion = $Busca_temp['g_autor_gestion']; 
          $tmp_pos = strpos($this->g_autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->g_autor_gestion = substr($this->g_autor_gestion, 0, $tmp_pos);
          }
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_ciudad_paciente = $Busca_temp['p_ciudad_paciente']; 
          $tmp_pos = strpos($this->p_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_ciudad_paciente = substr($this->p_ciudad_paciente, 0, $tmp_pos);
          }
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_status_paciente = $Busca_temp['p_status_paciente']; 
          $tmp_pos = strpos($this->p_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_status_paciente = substr($this->p_status_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_name']);
          $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, g.LOGRO_COMUNICACION_GESTION as g_logro_comunicacion_gestion, g.FECHA_COMUNICACION as g_fecha_comunicacion, g.AUTOR_GESTION as g_autor_gestion, p.PAIS_PACIENTE as p_pais_paciente, p.CIUDAD_PACIENTE as p_ciudad_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.STATUS_PACIENTE as p_status_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.CODIGO_XOFIGO as p_codigo_xofigo, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.DOSIS_TRATAMIENTO as t_dosis_tratamiento, g.NUMERO_CAJAS as g_numero_cajas, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, t.PUNTO_ENTREGA as t_punto_entrega, g.RECLAMO_GESTION as g_reclamo_gestion, g.FECHA_RECLAMACION_GESTION as g_fecha_reclamacion_gestion, g.CAUSA_NO_RECLAMACION_GESTION as g_causa_no_reclamacion_gestion, hr.ID_HISTORIAL_RECLAMACION as hr_id_historial_reclamacion, hr.ANIO_HISTORIAL_RECLAMACION as hr_anio_historial_reclamacion, hr.MES1 as hr_mes1, hr.RECLAMO1 as hr_reclamo1, hr.FECHA_RECLAMACION1 as hr_fecha_reclamacion1, hr.MOTIVO_NO_RECLAMACION1 as hr_motivo_no_reclamacion1, hr.MES2 as hr_mes2, hr.RECLAMO2 as hr_reclamo2, hr.FECHA_RECLAMACION2 as hr_fecha_reclamacion2, hr.MOTIVO_NO_RECLAMACION2 as hr_motivo_no_reclamacion2, hr.MES3 as hr_mes3, hr.RECLAMO3 as hr_reclamo3, hr.FECHA_RECLAMACION3 as hr_fecha_reclamacion3, hr.MOTIVO_NO_RECLAMACION3 as hr_motivo_no_reclamacion3, hr.MES4 as hr_mes4, hr.RECLAMO4 as hr_reclamo4, hr.FECHA_RECLAMACION4 as hr_fecha_reclamacion4, hr.MOTIVO_NO_RECLAMACION4 as hr_motivo_no_reclamacion4, hr.MES5 as hr_mes5, hr.RECLAMO5 as hr_reclamo5, hr.FECHA_RECLAMACION5 as hr_fecha_reclamacion5, hr.MOTIVO_NO_RECLAMACION5 as hr_motivo_no_reclamacion5, hr.MES6 as hr_mes6, hr.RECLAMO6 as hr_reclamo6, hr.FECHA_RECLAMACION6 as hr_fecha_reclamacion6, hr.MOTIVO_NO_RECLAMACION6 as hr_motivo_no_reclamacion6, hr.MES7 as hr_mes7, hr.RECLAMO7 as hr_reclamo7, hr.FECHA_RECLAMACION7 as hr_fecha_reclamacion7, hr.MOTIVO_NO_RECLAMACION7 as hr_motivo_no_reclamacion7, hr.MES8 as hr_mes8, hr.RECLAMO8 as hr_reclamo8, hr.FECHA_RECLAMACION8 as hr_fecha_reclamacion8, hr.MOTIVO_NO_RECLAMACION8 as hr_motivo_no_reclamacion8, hr.MES9 as hr_mes9, hr.RECLAMO9 as hr_reclamo9, hr.FECHA_RECLAMACION9 as hr_fecha_reclamacion9, hr.MOTIVO_NO_RECLAMACION9 as hr_motivo_no_reclamacion9, hr.MES10 as hr_mes10, hr.RECLAMO10 as hr_reclamo10, hr.FECHA_RECLAMACION10 as hr_fecha_reclamacion10, hr.MOTIVO_NO_RECLAMACION10 as hr_motivo_no_reclamacion10, hr.MES11 as hr_mes11, hr.RECLAMO11 as hr_reclamo11, hr.FECHA_RECLAMACION11 as hr_fecha_reclamacion11, hr.MOTIVO_NO_RECLAMACION11 as hr_motivo_no_reclamacion11, hr.MES12 as hr_mes12, hr.RECLAMO12 as hr_reclamo12, hr.FECHA_RECLAMACION12 as hr_fecha_reclamacion12, hr.MOTIVO_NO_RECLAMACION12 as hr_motivo_no_reclamacion12 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "p_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_logro_comunicacion_gestion'])) ? $this->New_label['g_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          if ($Cada_col == "g_logro_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_fecha_comunicacion'])) ? $this->New_label['g_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "g_fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_autor_gestion'])) ? $this->New_label['g_autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "g_autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          if ($Cada_col == "p_pais_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_ciudad_paciente'])) ? $this->New_label['p_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "p_ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_estado_paciente'])) ? $this->New_label['p_estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "p_estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_status_paciente'])) ? $this->New_label['p_status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "p_status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "p_fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['p_codigo_xofigo'])) ? $this->New_label['p_codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "p_codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_producto_tratamiento'])) ? $this->New_label['t_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "t_producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_nombre_referencia'])) ? $this->New_label['t_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "t_nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_dosis_tratamiento'])) ? $this->New_label['t_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "t_dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_numero_cajas'])) ? $this->New_label['g_numero_cajas'] : "NUMERO CAJAS"; 
          if ($Cada_col == "g_numero_cajas" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "t_asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "t_operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['t_punto_entrega'])) ? $this->New_label['t_punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "t_punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_reclamo_gestion'])) ? $this->New_label['g_reclamo_gestion'] : "RECLAMO GESTION"; 
          if ($Cada_col == "g_reclamo_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_fecha_reclamacion_gestion'])) ? $this->New_label['g_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          if ($Cada_col == "g_fecha_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['g_causa_no_reclamacion_gestion'])) ? $this->New_label['g_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          if ($Cada_col == "g_causa_no_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_id_historial_reclamacion'])) ? $this->New_label['hr_id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          if ($Cada_col == "hr_id_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_anio_historial_reclamacion'])) ? $this->New_label['hr_anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          if ($Cada_col == "hr_anio_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes1'])) ? $this->New_label['hr_mes1'] : "MES1"; 
          if ($Cada_col == "hr_mes1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo1'])) ? $this->New_label['hr_reclamo1'] : "RECLAMO1"; 
          if ($Cada_col == "hr_reclamo1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion1'])) ? $this->New_label['hr_fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          if ($Cada_col == "hr_fecha_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion1'])) ? $this->New_label['hr_motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          if ($Cada_col == "hr_motivo_no_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes2'])) ? $this->New_label['hr_mes2'] : "MES2"; 
          if ($Cada_col == "hr_mes2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo2'])) ? $this->New_label['hr_reclamo2'] : "RECLAMO2"; 
          if ($Cada_col == "hr_reclamo2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion2'])) ? $this->New_label['hr_fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          if ($Cada_col == "hr_fecha_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion2'])) ? $this->New_label['hr_motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          if ($Cada_col == "hr_motivo_no_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes3'])) ? $this->New_label['hr_mes3'] : "MES3"; 
          if ($Cada_col == "hr_mes3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo3'])) ? $this->New_label['hr_reclamo3'] : "RECLAMO3"; 
          if ($Cada_col == "hr_reclamo3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion3'])) ? $this->New_label['hr_fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          if ($Cada_col == "hr_fecha_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion3'])) ? $this->New_label['hr_motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          if ($Cada_col == "hr_motivo_no_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes4'])) ? $this->New_label['hr_mes4'] : "MES4"; 
          if ($Cada_col == "hr_mes4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo4'])) ? $this->New_label['hr_reclamo4'] : "RECLAMO4"; 
          if ($Cada_col == "hr_reclamo4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion4'])) ? $this->New_label['hr_fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          if ($Cada_col == "hr_fecha_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion4'])) ? $this->New_label['hr_motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          if ($Cada_col == "hr_motivo_no_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes5'])) ? $this->New_label['hr_mes5'] : "MES5"; 
          if ($Cada_col == "hr_mes5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo5'])) ? $this->New_label['hr_reclamo5'] : "RECLAMO5"; 
          if ($Cada_col == "hr_reclamo5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion5'])) ? $this->New_label['hr_fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          if ($Cada_col == "hr_fecha_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion5'])) ? $this->New_label['hr_motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          if ($Cada_col == "hr_motivo_no_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes6'])) ? $this->New_label['hr_mes6'] : "MES6"; 
          if ($Cada_col == "hr_mes6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo6'])) ? $this->New_label['hr_reclamo6'] : "RECLAMO6"; 
          if ($Cada_col == "hr_reclamo6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion6'])) ? $this->New_label['hr_fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          if ($Cada_col == "hr_fecha_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion6'])) ? $this->New_label['hr_motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          if ($Cada_col == "hr_motivo_no_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes7'])) ? $this->New_label['hr_mes7'] : "MES7"; 
          if ($Cada_col == "hr_mes7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo7'])) ? $this->New_label['hr_reclamo7'] : "RECLAMO7"; 
          if ($Cada_col == "hr_reclamo7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion7'])) ? $this->New_label['hr_fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          if ($Cada_col == "hr_fecha_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion7'])) ? $this->New_label['hr_motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          if ($Cada_col == "hr_motivo_no_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes8'])) ? $this->New_label['hr_mes8'] : "MES8"; 
          if ($Cada_col == "hr_mes8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo8'])) ? $this->New_label['hr_reclamo8'] : "RECLAMO8"; 
          if ($Cada_col == "hr_reclamo8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion8'])) ? $this->New_label['hr_fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          if ($Cada_col == "hr_fecha_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion8'])) ? $this->New_label['hr_motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          if ($Cada_col == "hr_motivo_no_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes9'])) ? $this->New_label['hr_mes9'] : "MES9"; 
          if ($Cada_col == "hr_mes9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo9'])) ? $this->New_label['hr_reclamo9'] : "RECLAMO9"; 
          if ($Cada_col == "hr_reclamo9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion9'])) ? $this->New_label['hr_fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          if ($Cada_col == "hr_fecha_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion9'])) ? $this->New_label['hr_motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          if ($Cada_col == "hr_motivo_no_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes10'])) ? $this->New_label['hr_mes10'] : "MES10"; 
          if ($Cada_col == "hr_mes10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo10'])) ? $this->New_label['hr_reclamo10'] : "RECLAMO10"; 
          if ($Cada_col == "hr_reclamo10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion10'])) ? $this->New_label['hr_fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          if ($Cada_col == "hr_fecha_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion10'])) ? $this->New_label['hr_motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          if ($Cada_col == "hr_motivo_no_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes11'])) ? $this->New_label['hr_mes11'] : "MES11"; 
          if ($Cada_col == "hr_mes11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo11'])) ? $this->New_label['hr_reclamo11'] : "RECLAMO11"; 
          if ($Cada_col == "hr_reclamo11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion11'])) ? $this->New_label['hr_fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          if ($Cada_col == "hr_fecha_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion11'])) ? $this->New_label['hr_motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          if ($Cada_col == "hr_motivo_no_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_mes12'])) ? $this->New_label['hr_mes12'] : "MES12"; 
          if ($Cada_col == "hr_mes12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_reclamo12'])) ? $this->New_label['hr_reclamo12'] : "RECLAMO12"; 
          if ($Cada_col == "hr_reclamo12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_fecha_reclamacion12'])) ? $this->New_label['hr_fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          if ($Cada_col == "hr_fecha_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['hr_motivo_no_reclamacion12'])) ? $this->New_label['hr_motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          if ($Cada_col == "hr_motivo_no_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
      } 
      while (!$rs->EOF)
      {
         $this->Xls_col = 0;
         $this->Xls_row++;
         $this->p_id_paciente = $rs->fields[0] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->g_logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->g_fecha_comunicacion = $rs->fields[2] ;  
         $this->g_autor_gestion = $rs->fields[3] ;  
         $this->p_pais_paciente = $rs->fields[4] ;  
         $this->p_ciudad_paciente = $rs->fields[5] ;  
         $this->p_estado_paciente = $rs->fields[6] ;  
         $this->p_status_paciente = $rs->fields[7] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[8] ;  
         $this->p_codigo_xofigo = $rs->fields[9] ;  
         $this->p_codigo_xofigo = (string)$this->p_codigo_xofigo;
         $this->t_producto_tratamiento = $rs->fields[10] ;  
         $this->t_nombre_referencia = $rs->fields[11] ;  
         $this->t_dosis_tratamiento = $rs->fields[12] ;  
         $this->g_numero_cajas = $rs->fields[13] ;  
         $this->t_asegurador_tratamiento = $rs->fields[14] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[15] ;  
         $this->t_punto_entrega = $rs->fields[16] ;  
         $this->g_reclamo_gestion = $rs->fields[17] ;  
         $this->g_fecha_reclamacion_gestion = $rs->fields[18] ;  
         $this->g_causa_no_reclamacion_gestion = $rs->fields[19] ;  
         $this->hr_id_historial_reclamacion = $rs->fields[20] ;  
         $this->hr_id_historial_reclamacion = (string)$this->hr_id_historial_reclamacion;
         $this->hr_anio_historial_reclamacion = $rs->fields[21] ;  
         $this->hr_anio_historial_reclamacion = (string)$this->hr_anio_historial_reclamacion;
         $this->hr_mes1 = $rs->fields[22] ;  
         $this->hr_reclamo1 = $rs->fields[23] ;  
         $this->hr_fecha_reclamacion1 = $rs->fields[24] ;  
         $this->hr_motivo_no_reclamacion1 = $rs->fields[25] ;  
         $this->hr_mes2 = $rs->fields[26] ;  
         $this->hr_reclamo2 = $rs->fields[27] ;  
         $this->hr_fecha_reclamacion2 = $rs->fields[28] ;  
         $this->hr_motivo_no_reclamacion2 = $rs->fields[29] ;  
         $this->hr_mes3 = $rs->fields[30] ;  
         $this->hr_reclamo3 = $rs->fields[31] ;  
         $this->hr_fecha_reclamacion3 = $rs->fields[32] ;  
         $this->hr_motivo_no_reclamacion3 = $rs->fields[33] ;  
         $this->hr_mes4 = $rs->fields[34] ;  
         $this->hr_reclamo4 = $rs->fields[35] ;  
         $this->hr_fecha_reclamacion4 = $rs->fields[36] ;  
         $this->hr_motivo_no_reclamacion4 = $rs->fields[37] ;  
         $this->hr_mes5 = $rs->fields[38] ;  
         $this->hr_reclamo5 = $rs->fields[39] ;  
         $this->hr_fecha_reclamacion5 = $rs->fields[40] ;  
         $this->hr_motivo_no_reclamacion5 = $rs->fields[41] ;  
         $this->hr_mes6 = $rs->fields[42] ;  
         $this->hr_reclamo6 = $rs->fields[43] ;  
         $this->hr_fecha_reclamacion6 = $rs->fields[44] ;  
         $this->hr_motivo_no_reclamacion6 = $rs->fields[45] ;  
         $this->hr_mes7 = $rs->fields[46] ;  
         $this->hr_reclamo7 = $rs->fields[47] ;  
         $this->hr_fecha_reclamacion7 = $rs->fields[48] ;  
         $this->hr_motivo_no_reclamacion7 = $rs->fields[49] ;  
         $this->hr_mes8 = $rs->fields[50] ;  
         $this->hr_reclamo8 = $rs->fields[51] ;  
         $this->hr_fecha_reclamacion8 = $rs->fields[52] ;  
         $this->hr_motivo_no_reclamacion8 = $rs->fields[53] ;  
         $this->hr_mes9 = $rs->fields[54] ;  
         $this->hr_reclamo9 = $rs->fields[55] ;  
         $this->hr_fecha_reclamacion9 = $rs->fields[56] ;  
         $this->hr_motivo_no_reclamacion9 = $rs->fields[57] ;  
         $this->hr_mes10 = $rs->fields[58] ;  
         $this->hr_reclamo10 = $rs->fields[59] ;  
         $this->hr_fecha_reclamacion10 = $rs->fields[60] ;  
         $this->hr_motivo_no_reclamacion10 = $rs->fields[61] ;  
         $this->hr_mes11 = $rs->fields[62] ;  
         $this->hr_reclamo11 = $rs->fields[63] ;  
         $this->hr_fecha_reclamacion11 = $rs->fields[64] ;  
         $this->hr_motivo_no_reclamacion11 = $rs->fields[65] ;  
         $this->hr_mes12 = $rs->fields[66] ;  
         $this->hr_reclamo12 = $rs->fields[67] ;  
         $this->hr_fecha_reclamacion12 = $rs->fields[68] ;  
         $this->hr_motivo_no_reclamacion12 = $rs->fields[69] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         if (isset($this->NM_Row_din[$this->Xls_row]))
         { 
             $this->Nm_ActiveSheet->getRowDimension($this->Xls_row)->setRowHeight($this->NM_Row_din[$this->Xls_row]);
         } 
         $rs->MoveNext();
      }
      if (isset($this->NM_Col_din))
      { 
          foreach ($this->NM_Col_din as $col => $width)
          { 
              $this->Nm_ActiveSheet->getColumnDimension($col)->setWidth($width / 5);
          } 
      } 
      $rs->Close();
      $objWriter = new PHPExcel_Writer_Excel5($this->Xls_dados);
      $objWriter->save($this->Xls_f);
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         $this->p_id_paciente = html_entity_decode($this->p_id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_id_paciente = strip_tags($this->p_id_paciente);
         if (!NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_id_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_logro_comunicacion_gestion
   function NM_export_g_logro_comunicacion_gestion()
   {
         $this->g_logro_comunicacion_gestion = html_entity_decode($this->g_logro_comunicacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_logro_comunicacion_gestion = strip_tags($this->g_logro_comunicacion_gestion);
         if (!NM_is_utf8($this->g_logro_comunicacion_gestion))
         {
             $this->g_logro_comunicacion_gestion = sc_convert_encoding($this->g_logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_logro_comunicacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_fecha_comunicacion
   function NM_export_g_fecha_comunicacion()
   {
         $this->g_fecha_comunicacion = html_entity_decode($this->g_fecha_comunicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_fecha_comunicacion = strip_tags($this->g_fecha_comunicacion);
         if (!NM_is_utf8($this->g_fecha_comunicacion))
         {
             $this->g_fecha_comunicacion = sc_convert_encoding($this->g_fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_fecha_comunicacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_autor_gestion
   function NM_export_g_autor_gestion()
   {
         $this->g_autor_gestion = html_entity_decode($this->g_autor_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_autor_gestion = strip_tags($this->g_autor_gestion);
         if (!NM_is_utf8($this->g_autor_gestion))
         {
             $this->g_autor_gestion = sc_convert_encoding($this->g_autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_autor_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         $this->p_pais_paciente = html_entity_decode($this->p_pais_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_pais_paciente = strip_tags($this->p_pais_paciente);
         if (!NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_pais_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_ciudad_paciente
   function NM_export_p_ciudad_paciente()
   {
         $this->p_ciudad_paciente = html_entity_decode($this->p_ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_ciudad_paciente = strip_tags($this->p_ciudad_paciente);
         if (!NM_is_utf8($this->p_ciudad_paciente))
         {
             $this->p_ciudad_paciente = sc_convert_encoding($this->p_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_ciudad_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
         $this->p_estado_paciente = html_entity_decode($this->p_estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_estado_paciente = strip_tags($this->p_estado_paciente);
         if (!NM_is_utf8($this->p_estado_paciente))
         {
             $this->p_estado_paciente = sc_convert_encoding($this->p_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_estado_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_status_paciente
   function NM_export_p_status_paciente()
   {
         $this->p_status_paciente = html_entity_decode($this->p_status_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_status_paciente = strip_tags($this->p_status_paciente);
         if (!NM_is_utf8($this->p_status_paciente))
         {
             $this->p_status_paciente = sc_convert_encoding($this->p_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_status_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         $this->p_fecha_activacion_paciente = html_entity_decode($this->p_fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_fecha_activacion_paciente = strip_tags($this->p_fecha_activacion_paciente);
         if (!NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_fecha_activacion_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- p_codigo_xofigo
   function NM_export_p_codigo_xofigo()
   {
         $this->p_codigo_xofigo = html_entity_decode($this->p_codigo_xofigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_codigo_xofigo = strip_tags($this->p_codigo_xofigo);
         if (!NM_is_utf8($this->p_codigo_xofigo))
         {
             $this->p_codigo_xofigo = sc_convert_encoding($this->p_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->p_codigo_xofigo, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
         $this->t_producto_tratamiento = html_entity_decode($this->t_producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_producto_tratamiento = strip_tags($this->t_producto_tratamiento);
         if (!NM_is_utf8($this->t_producto_tratamiento))
         {
             $this->t_producto_tratamiento = sc_convert_encoding($this->t_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_producto_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_nombre_referencia
   function NM_export_t_nombre_referencia()
   {
         $this->t_nombre_referencia = html_entity_decode($this->t_nombre_referencia, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_nombre_referencia = strip_tags($this->t_nombre_referencia);
         if (!NM_is_utf8($this->t_nombre_referencia))
         {
             $this->t_nombre_referencia = sc_convert_encoding($this->t_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_nombre_referencia, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_dosis_tratamiento
   function NM_export_t_dosis_tratamiento()
   {
         $this->t_dosis_tratamiento = html_entity_decode($this->t_dosis_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_dosis_tratamiento = strip_tags($this->t_dosis_tratamiento);
         if (!NM_is_utf8($this->t_dosis_tratamiento))
         {
             $this->t_dosis_tratamiento = sc_convert_encoding($this->t_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_dosis_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_numero_cajas
   function NM_export_g_numero_cajas()
   {
         $this->g_numero_cajas = html_entity_decode($this->g_numero_cajas, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_numero_cajas = strip_tags($this->g_numero_cajas);
         if (!NM_is_utf8($this->g_numero_cajas))
         {
             $this->g_numero_cajas = sc_convert_encoding($this->g_numero_cajas, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_numero_cajas, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
         $this->t_asegurador_tratamiento = html_entity_decode($this->t_asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_asegurador_tratamiento = strip_tags($this->t_asegurador_tratamiento);
         if (!NM_is_utf8($this->t_asegurador_tratamiento))
         {
             $this->t_asegurador_tratamiento = sc_convert_encoding($this->t_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_asegurador_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
         $this->t_operador_logistico_tratamiento = html_entity_decode($this->t_operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_operador_logistico_tratamiento = strip_tags($this->t_operador_logistico_tratamiento);
         if (!NM_is_utf8($this->t_operador_logistico_tratamiento))
         {
             $this->t_operador_logistico_tratamiento = sc_convert_encoding($this->t_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_operador_logistico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- t_punto_entrega
   function NM_export_t_punto_entrega()
   {
         $this->t_punto_entrega = html_entity_decode($this->t_punto_entrega, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_punto_entrega = strip_tags($this->t_punto_entrega);
         if (!NM_is_utf8($this->t_punto_entrega))
         {
             $this->t_punto_entrega = sc_convert_encoding($this->t_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->t_punto_entrega, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_reclamo_gestion
   function NM_export_g_reclamo_gestion()
   {
         $this->g_reclamo_gestion = html_entity_decode($this->g_reclamo_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_reclamo_gestion = strip_tags($this->g_reclamo_gestion);
         if (!NM_is_utf8($this->g_reclamo_gestion))
         {
             $this->g_reclamo_gestion = sc_convert_encoding($this->g_reclamo_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_reclamo_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_fecha_reclamacion_gestion
   function NM_export_g_fecha_reclamacion_gestion()
   {
         $this->g_fecha_reclamacion_gestion = html_entity_decode($this->g_fecha_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_fecha_reclamacion_gestion = strip_tags($this->g_fecha_reclamacion_gestion);
         if (!NM_is_utf8($this->g_fecha_reclamacion_gestion))
         {
             $this->g_fecha_reclamacion_gestion = sc_convert_encoding($this->g_fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_fecha_reclamacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- g_causa_no_reclamacion_gestion
   function NM_export_g_causa_no_reclamacion_gestion()
   {
         $this->g_causa_no_reclamacion_gestion = html_entity_decode($this->g_causa_no_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->g_causa_no_reclamacion_gestion = strip_tags($this->g_causa_no_reclamacion_gestion);
         if (!NM_is_utf8($this->g_causa_no_reclamacion_gestion))
         {
             $this->g_causa_no_reclamacion_gestion = sc_convert_encoding($this->g_causa_no_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->g_causa_no_reclamacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_id_historial_reclamacion
   function NM_export_hr_id_historial_reclamacion()
   {
         $this->hr_id_historial_reclamacion = html_entity_decode($this->hr_id_historial_reclamacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_id_historial_reclamacion = strip_tags($this->hr_id_historial_reclamacion);
         if (!NM_is_utf8($this->hr_id_historial_reclamacion))
         {
             $this->hr_id_historial_reclamacion = sc_convert_encoding($this->hr_id_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_id_historial_reclamacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_anio_historial_reclamacion
   function NM_export_hr_anio_historial_reclamacion()
   {
         if (!NM_is_utf8($this->hr_anio_historial_reclamacion))
         {
             $this->hr_anio_historial_reclamacion = sc_convert_encoding($this->hr_anio_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
         if (is_numeric($this->hr_anio_historial_reclamacion))
         {
             $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getNumberFormat()->setFormatCode('#,##0');
         }
         $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_anio_historial_reclamacion);
         $this->Xls_col++;
   }
   //----- hr_mes1
   function NM_export_hr_mes1()
   {
         $this->hr_mes1 = html_entity_decode($this->hr_mes1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes1 = strip_tags($this->hr_mes1);
         if (!NM_is_utf8($this->hr_mes1))
         {
             $this->hr_mes1 = sc_convert_encoding($this->hr_mes1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo1
   function NM_export_hr_reclamo1()
   {
         $this->hr_reclamo1 = html_entity_decode($this->hr_reclamo1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo1 = strip_tags($this->hr_reclamo1);
         if (!NM_is_utf8($this->hr_reclamo1))
         {
             $this->hr_reclamo1 = sc_convert_encoding($this->hr_reclamo1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion1
   function NM_export_hr_fecha_reclamacion1()
   {
         $this->hr_fecha_reclamacion1 = html_entity_decode($this->hr_fecha_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion1 = strip_tags($this->hr_fecha_reclamacion1);
         if (!NM_is_utf8($this->hr_fecha_reclamacion1))
         {
             $this->hr_fecha_reclamacion1 = sc_convert_encoding($this->hr_fecha_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion1
   function NM_export_hr_motivo_no_reclamacion1()
   {
         $this->hr_motivo_no_reclamacion1 = html_entity_decode($this->hr_motivo_no_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion1 = strip_tags($this->hr_motivo_no_reclamacion1);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion1))
         {
             $this->hr_motivo_no_reclamacion1 = sc_convert_encoding($this->hr_motivo_no_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes2
   function NM_export_hr_mes2()
   {
         $this->hr_mes2 = html_entity_decode($this->hr_mes2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes2 = strip_tags($this->hr_mes2);
         if (!NM_is_utf8($this->hr_mes2))
         {
             $this->hr_mes2 = sc_convert_encoding($this->hr_mes2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo2
   function NM_export_hr_reclamo2()
   {
         $this->hr_reclamo2 = html_entity_decode($this->hr_reclamo2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo2 = strip_tags($this->hr_reclamo2);
         if (!NM_is_utf8($this->hr_reclamo2))
         {
             $this->hr_reclamo2 = sc_convert_encoding($this->hr_reclamo2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion2
   function NM_export_hr_fecha_reclamacion2()
   {
         $this->hr_fecha_reclamacion2 = html_entity_decode($this->hr_fecha_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion2 = strip_tags($this->hr_fecha_reclamacion2);
         if (!NM_is_utf8($this->hr_fecha_reclamacion2))
         {
             $this->hr_fecha_reclamacion2 = sc_convert_encoding($this->hr_fecha_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion2
   function NM_export_hr_motivo_no_reclamacion2()
   {
         $this->hr_motivo_no_reclamacion2 = html_entity_decode($this->hr_motivo_no_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion2 = strip_tags($this->hr_motivo_no_reclamacion2);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion2))
         {
             $this->hr_motivo_no_reclamacion2 = sc_convert_encoding($this->hr_motivo_no_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes3
   function NM_export_hr_mes3()
   {
         $this->hr_mes3 = html_entity_decode($this->hr_mes3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes3 = strip_tags($this->hr_mes3);
         if (!NM_is_utf8($this->hr_mes3))
         {
             $this->hr_mes3 = sc_convert_encoding($this->hr_mes3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo3
   function NM_export_hr_reclamo3()
   {
         $this->hr_reclamo3 = html_entity_decode($this->hr_reclamo3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo3 = strip_tags($this->hr_reclamo3);
         if (!NM_is_utf8($this->hr_reclamo3))
         {
             $this->hr_reclamo3 = sc_convert_encoding($this->hr_reclamo3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion3
   function NM_export_hr_fecha_reclamacion3()
   {
         $this->hr_fecha_reclamacion3 = html_entity_decode($this->hr_fecha_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion3 = strip_tags($this->hr_fecha_reclamacion3);
         if (!NM_is_utf8($this->hr_fecha_reclamacion3))
         {
             $this->hr_fecha_reclamacion3 = sc_convert_encoding($this->hr_fecha_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion3
   function NM_export_hr_motivo_no_reclamacion3()
   {
         $this->hr_motivo_no_reclamacion3 = html_entity_decode($this->hr_motivo_no_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion3 = strip_tags($this->hr_motivo_no_reclamacion3);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion3))
         {
             $this->hr_motivo_no_reclamacion3 = sc_convert_encoding($this->hr_motivo_no_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes4
   function NM_export_hr_mes4()
   {
         $this->hr_mes4 = html_entity_decode($this->hr_mes4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes4 = strip_tags($this->hr_mes4);
         if (!NM_is_utf8($this->hr_mes4))
         {
             $this->hr_mes4 = sc_convert_encoding($this->hr_mes4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo4
   function NM_export_hr_reclamo4()
   {
         $this->hr_reclamo4 = html_entity_decode($this->hr_reclamo4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo4 = strip_tags($this->hr_reclamo4);
         if (!NM_is_utf8($this->hr_reclamo4))
         {
             $this->hr_reclamo4 = sc_convert_encoding($this->hr_reclamo4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion4
   function NM_export_hr_fecha_reclamacion4()
   {
         $this->hr_fecha_reclamacion4 = html_entity_decode($this->hr_fecha_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion4 = strip_tags($this->hr_fecha_reclamacion4);
         if (!NM_is_utf8($this->hr_fecha_reclamacion4))
         {
             $this->hr_fecha_reclamacion4 = sc_convert_encoding($this->hr_fecha_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion4
   function NM_export_hr_motivo_no_reclamacion4()
   {
         $this->hr_motivo_no_reclamacion4 = html_entity_decode($this->hr_motivo_no_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion4 = strip_tags($this->hr_motivo_no_reclamacion4);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion4))
         {
             $this->hr_motivo_no_reclamacion4 = sc_convert_encoding($this->hr_motivo_no_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes5
   function NM_export_hr_mes5()
   {
         $this->hr_mes5 = html_entity_decode($this->hr_mes5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes5 = strip_tags($this->hr_mes5);
         if (!NM_is_utf8($this->hr_mes5))
         {
             $this->hr_mes5 = sc_convert_encoding($this->hr_mes5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo5
   function NM_export_hr_reclamo5()
   {
         $this->hr_reclamo5 = html_entity_decode($this->hr_reclamo5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo5 = strip_tags($this->hr_reclamo5);
         if (!NM_is_utf8($this->hr_reclamo5))
         {
             $this->hr_reclamo5 = sc_convert_encoding($this->hr_reclamo5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion5
   function NM_export_hr_fecha_reclamacion5()
   {
         $this->hr_fecha_reclamacion5 = html_entity_decode($this->hr_fecha_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion5 = strip_tags($this->hr_fecha_reclamacion5);
         if (!NM_is_utf8($this->hr_fecha_reclamacion5))
         {
             $this->hr_fecha_reclamacion5 = sc_convert_encoding($this->hr_fecha_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion5
   function NM_export_hr_motivo_no_reclamacion5()
   {
         $this->hr_motivo_no_reclamacion5 = html_entity_decode($this->hr_motivo_no_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion5 = strip_tags($this->hr_motivo_no_reclamacion5);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion5))
         {
             $this->hr_motivo_no_reclamacion5 = sc_convert_encoding($this->hr_motivo_no_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes6
   function NM_export_hr_mes6()
   {
         $this->hr_mes6 = html_entity_decode($this->hr_mes6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes6 = strip_tags($this->hr_mes6);
         if (!NM_is_utf8($this->hr_mes6))
         {
             $this->hr_mes6 = sc_convert_encoding($this->hr_mes6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo6
   function NM_export_hr_reclamo6()
   {
         $this->hr_reclamo6 = html_entity_decode($this->hr_reclamo6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo6 = strip_tags($this->hr_reclamo6);
         if (!NM_is_utf8($this->hr_reclamo6))
         {
             $this->hr_reclamo6 = sc_convert_encoding($this->hr_reclamo6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion6
   function NM_export_hr_fecha_reclamacion6()
   {
         $this->hr_fecha_reclamacion6 = html_entity_decode($this->hr_fecha_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion6 = strip_tags($this->hr_fecha_reclamacion6);
         if (!NM_is_utf8($this->hr_fecha_reclamacion6))
         {
             $this->hr_fecha_reclamacion6 = sc_convert_encoding($this->hr_fecha_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion6
   function NM_export_hr_motivo_no_reclamacion6()
   {
         $this->hr_motivo_no_reclamacion6 = html_entity_decode($this->hr_motivo_no_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion6 = strip_tags($this->hr_motivo_no_reclamacion6);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion6))
         {
             $this->hr_motivo_no_reclamacion6 = sc_convert_encoding($this->hr_motivo_no_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes7
   function NM_export_hr_mes7()
   {
         $this->hr_mes7 = html_entity_decode($this->hr_mes7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes7 = strip_tags($this->hr_mes7);
         if (!NM_is_utf8($this->hr_mes7))
         {
             $this->hr_mes7 = sc_convert_encoding($this->hr_mes7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo7
   function NM_export_hr_reclamo7()
   {
         $this->hr_reclamo7 = html_entity_decode($this->hr_reclamo7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo7 = strip_tags($this->hr_reclamo7);
         if (!NM_is_utf8($this->hr_reclamo7))
         {
             $this->hr_reclamo7 = sc_convert_encoding($this->hr_reclamo7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion7
   function NM_export_hr_fecha_reclamacion7()
   {
         $this->hr_fecha_reclamacion7 = html_entity_decode($this->hr_fecha_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion7 = strip_tags($this->hr_fecha_reclamacion7);
         if (!NM_is_utf8($this->hr_fecha_reclamacion7))
         {
             $this->hr_fecha_reclamacion7 = sc_convert_encoding($this->hr_fecha_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion7
   function NM_export_hr_motivo_no_reclamacion7()
   {
         $this->hr_motivo_no_reclamacion7 = html_entity_decode($this->hr_motivo_no_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion7 = strip_tags($this->hr_motivo_no_reclamacion7);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion7))
         {
             $this->hr_motivo_no_reclamacion7 = sc_convert_encoding($this->hr_motivo_no_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes8
   function NM_export_hr_mes8()
   {
         $this->hr_mes8 = html_entity_decode($this->hr_mes8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes8 = strip_tags($this->hr_mes8);
         if (!NM_is_utf8($this->hr_mes8))
         {
             $this->hr_mes8 = sc_convert_encoding($this->hr_mes8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo8
   function NM_export_hr_reclamo8()
   {
         $this->hr_reclamo8 = html_entity_decode($this->hr_reclamo8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo8 = strip_tags($this->hr_reclamo8);
         if (!NM_is_utf8($this->hr_reclamo8))
         {
             $this->hr_reclamo8 = sc_convert_encoding($this->hr_reclamo8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion8
   function NM_export_hr_fecha_reclamacion8()
   {
         $this->hr_fecha_reclamacion8 = html_entity_decode($this->hr_fecha_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion8 = strip_tags($this->hr_fecha_reclamacion8);
         if (!NM_is_utf8($this->hr_fecha_reclamacion8))
         {
             $this->hr_fecha_reclamacion8 = sc_convert_encoding($this->hr_fecha_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion8
   function NM_export_hr_motivo_no_reclamacion8()
   {
         $this->hr_motivo_no_reclamacion8 = html_entity_decode($this->hr_motivo_no_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion8 = strip_tags($this->hr_motivo_no_reclamacion8);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion8))
         {
             $this->hr_motivo_no_reclamacion8 = sc_convert_encoding($this->hr_motivo_no_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes9
   function NM_export_hr_mes9()
   {
         $this->hr_mes9 = html_entity_decode($this->hr_mes9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes9 = strip_tags($this->hr_mes9);
         if (!NM_is_utf8($this->hr_mes9))
         {
             $this->hr_mes9 = sc_convert_encoding($this->hr_mes9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo9
   function NM_export_hr_reclamo9()
   {
         $this->hr_reclamo9 = html_entity_decode($this->hr_reclamo9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo9 = strip_tags($this->hr_reclamo9);
         if (!NM_is_utf8($this->hr_reclamo9))
         {
             $this->hr_reclamo9 = sc_convert_encoding($this->hr_reclamo9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion9
   function NM_export_hr_fecha_reclamacion9()
   {
         $this->hr_fecha_reclamacion9 = html_entity_decode($this->hr_fecha_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion9 = strip_tags($this->hr_fecha_reclamacion9);
         if (!NM_is_utf8($this->hr_fecha_reclamacion9))
         {
             $this->hr_fecha_reclamacion9 = sc_convert_encoding($this->hr_fecha_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion9
   function NM_export_hr_motivo_no_reclamacion9()
   {
         $this->hr_motivo_no_reclamacion9 = html_entity_decode($this->hr_motivo_no_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion9 = strip_tags($this->hr_motivo_no_reclamacion9);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion9))
         {
             $this->hr_motivo_no_reclamacion9 = sc_convert_encoding($this->hr_motivo_no_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes10
   function NM_export_hr_mes10()
   {
         $this->hr_mes10 = html_entity_decode($this->hr_mes10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes10 = strip_tags($this->hr_mes10);
         if (!NM_is_utf8($this->hr_mes10))
         {
             $this->hr_mes10 = sc_convert_encoding($this->hr_mes10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo10
   function NM_export_hr_reclamo10()
   {
         $this->hr_reclamo10 = html_entity_decode($this->hr_reclamo10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo10 = strip_tags($this->hr_reclamo10);
         if (!NM_is_utf8($this->hr_reclamo10))
         {
             $this->hr_reclamo10 = sc_convert_encoding($this->hr_reclamo10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion10
   function NM_export_hr_fecha_reclamacion10()
   {
         $this->hr_fecha_reclamacion10 = html_entity_decode($this->hr_fecha_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion10 = strip_tags($this->hr_fecha_reclamacion10);
         if (!NM_is_utf8($this->hr_fecha_reclamacion10))
         {
             $this->hr_fecha_reclamacion10 = sc_convert_encoding($this->hr_fecha_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion10
   function NM_export_hr_motivo_no_reclamacion10()
   {
         $this->hr_motivo_no_reclamacion10 = html_entity_decode($this->hr_motivo_no_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion10 = strip_tags($this->hr_motivo_no_reclamacion10);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion10))
         {
             $this->hr_motivo_no_reclamacion10 = sc_convert_encoding($this->hr_motivo_no_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes11
   function NM_export_hr_mes11()
   {
         $this->hr_mes11 = html_entity_decode($this->hr_mes11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes11 = strip_tags($this->hr_mes11);
         if (!NM_is_utf8($this->hr_mes11))
         {
             $this->hr_mes11 = sc_convert_encoding($this->hr_mes11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo11
   function NM_export_hr_reclamo11()
   {
         $this->hr_reclamo11 = html_entity_decode($this->hr_reclamo11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo11 = strip_tags($this->hr_reclamo11);
         if (!NM_is_utf8($this->hr_reclamo11))
         {
             $this->hr_reclamo11 = sc_convert_encoding($this->hr_reclamo11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion11
   function NM_export_hr_fecha_reclamacion11()
   {
         $this->hr_fecha_reclamacion11 = html_entity_decode($this->hr_fecha_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion11 = strip_tags($this->hr_fecha_reclamacion11);
         if (!NM_is_utf8($this->hr_fecha_reclamacion11))
         {
             $this->hr_fecha_reclamacion11 = sc_convert_encoding($this->hr_fecha_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion11
   function NM_export_hr_motivo_no_reclamacion11()
   {
         $this->hr_motivo_no_reclamacion11 = html_entity_decode($this->hr_motivo_no_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion11 = strip_tags($this->hr_motivo_no_reclamacion11);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion11))
         {
             $this->hr_motivo_no_reclamacion11 = sc_convert_encoding($this->hr_motivo_no_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_mes12
   function NM_export_hr_mes12()
   {
         $this->hr_mes12 = html_entity_decode($this->hr_mes12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_mes12 = strip_tags($this->hr_mes12);
         if (!NM_is_utf8($this->hr_mes12))
         {
             $this->hr_mes12 = sc_convert_encoding($this->hr_mes12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_mes12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_reclamo12
   function NM_export_hr_reclamo12()
   {
         $this->hr_reclamo12 = html_entity_decode($this->hr_reclamo12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_reclamo12 = strip_tags($this->hr_reclamo12);
         if (!NM_is_utf8($this->hr_reclamo12))
         {
             $this->hr_reclamo12 = sc_convert_encoding($this->hr_reclamo12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_reclamo12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_fecha_reclamacion12
   function NM_export_hr_fecha_reclamacion12()
   {
         $this->hr_fecha_reclamacion12 = html_entity_decode($this->hr_fecha_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_fecha_reclamacion12 = strip_tags($this->hr_fecha_reclamacion12);
         if (!NM_is_utf8($this->hr_fecha_reclamacion12))
         {
             $this->hr_fecha_reclamacion12 = sc_convert_encoding($this->hr_fecha_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_fecha_reclamacion12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- hr_motivo_no_reclamacion12
   function NM_export_hr_motivo_no_reclamacion12()
   {
         $this->hr_motivo_no_reclamacion12 = html_entity_decode($this->hr_motivo_no_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->hr_motivo_no_reclamacion12 = strip_tags($this->hr_motivo_no_reclamacion12);
         if (!NM_is_utf8($this->hr_motivo_no_reclamacion12))
         {
             $this->hr_motivo_no_reclamacion12 = sc_convert_encoding($this->hr_motivo_no_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->hr_motivo_no_reclamacion12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }

   function calc_cell($col)
   {
       $arr_alfa = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
       $val_ret = "";
       $result = $col + 1;
       while ($result > 26)
       {
           $cel      = $result % 26;
           $result   = $result / 26;
           if ($cel == 0)
           {
               $cel    = 26;
               $result--;
           }
           $val_ret = $arr_alfa[$cel] . $val_ret;
       }
       $val_ret = $arr_alfa[$result] . $val_ret;
       return $val_ret;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_file']);
      if (is_file($this->Xls_f))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial']['xls_file'] = $this->Xls_f;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['reclamacion_historial'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones Historial :: Excel</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XLS</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="reclamacion_historial_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="reclamacion_historial"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
