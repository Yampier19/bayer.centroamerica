<?php

class informe_educacion_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function informe_educacion_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_informe_educacion";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "informe_educacion.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['informe_educacion']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['informe_educacion']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['informe_educacion']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->e_id_edu = $Busca_temp['e_id_edu']; 
          $tmp_pos = strpos($this->e_id_edu, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_id_edu = substr($this->e_id_edu, 0, $tmp_pos);
          }
          $this->e_id_edu_2 = $Busca_temp['e_id_edu_input_2']; 
          $this->e_user = $Busca_temp['e_user']; 
          $tmp_pos = strpos($this->e_user, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_user = substr($this->e_user, 0, $tmp_pos);
          }
          $this->e_id_paci_fk = $Busca_temp['e_id_paci_fk']; 
          $tmp_pos = strpos($this->e_id_paci_fk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_id_paci_fk = substr($this->e_id_paci_fk, 0, $tmp_pos);
          }
          $this->e_id_paci_fk_2 = $Busca_temp['e_id_paci_fk_input_2']; 
          $this->e_se_brindo_edu = $Busca_temp['e_se_brindo_edu']; 
          $tmp_pos = strpos($this->e_se_brindo_edu, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_se_brindo_edu = substr($this->e_se_brindo_edu, 0, $tmp_pos);
          }
          $this->e_tema_si_edu = $Busca_temp['e_tema_si_edu']; 
          $tmp_pos = strpos($this->e_tema_si_edu, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_tema_si_edu = substr($this->e_tema_si_edu, 0, $tmp_pos);
          }
          $this->e_fecha_si_edu = $Busca_temp['e_fecha_si_edu']; 
          $tmp_pos = strpos($this->e_fecha_si_edu, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_fecha_si_edu = substr($this->e_fecha_si_edu, 0, $tmp_pos);
          }
          $this->e_fecha_si_edu_2 = $Busca_temp['e_fecha_si_edu_input_2']; 
          $this->e_motivo_no_edu = $Busca_temp['e_motivo_no_edu']; 
          $tmp_pos = strpos($this->e_motivo_no_edu, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_motivo_no_edu = substr($this->e_motivo_no_edu, 0, $tmp_pos);
          }
          $this->e_fecha_registro = $Busca_temp['e_fecha_registro']; 
          $tmp_pos = strpos($this->e_fecha_registro, "##@@");
          if ($tmp_pos !== false)
          {
              $this->e_fecha_registro = substr($this->e_fecha_registro, 0, $tmp_pos);
          }
          $this->e_fecha_registro_2 = $Busca_temp['e_fecha_registro_input_2']; 
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->p_pais_paciente = $Busca_temp['p_pais_paciente']; 
          $tmp_pos = strpos($this->p_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_pais_paciente = substr($this->p_pais_paciente, 0, $tmp_pos);
          }
          $this->p_identificacion_paciente = $Busca_temp['p_identificacion_paciente']; 
          $tmp_pos = strpos($this->p_identificacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_identificacion_paciente = substr($this->p_identificacion_paciente, 0, $tmp_pos);
          }
          $this->p_nombre_paciente = $Busca_temp['p_nombre_paciente']; 
          $tmp_pos = strpos($this->p_nombre_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_nombre_paciente = substr($this->p_nombre_paciente, 0, $tmp_pos);
          }
          $this->p_apellido_paciente = $Busca_temp['p_apellido_paciente']; 
          $tmp_pos = strpos($this->p_apellido_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_apellido_paciente = substr($this->p_apellido_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, str_replace (convert(char(10),e.FECHA_SI_EDU,102), '.', '-') + ' ' + convert(char(8),e.FECHA_SI_EDU,20) as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, str_replace (convert(char(10),e.FECHA_REGISTRO,102), '.', '-') + ' ' + convert(char(8),e.FECHA_REGISTRO,20) as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, e.FECHA_SI_EDU as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, e.FECHA_REGISTRO as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, convert(char(23),e.FECHA_SI_EDU,121) as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, convert(char(23),e.FECHA_REGISTRO,121) as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, e.FECHA_SI_EDU as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, e.FECHA_REGISTRO as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, EXTEND(e.FECHA_SI_EDU, YEAR TO DAY) as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, EXTEND(e.FECHA_REGISTRO, YEAR TO FRACTION) as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT e.ID_EDU as e_id_edu, e.USER as e_user, e.ID_PACI_FK as e_id_paci_fk, e.SE_BRINDO_EDU as e_se_brindo_edu, e.TEMA_SI_EDU as e_tema_si_edu, e.FECHA_SI_EDU as e_fecha_si_edu, e.MOTIVO_NO_EDU as e_motivo_no_edu, e.FECHA_REGISTRO as e_fecha_registro, p.ID_PACIENTE as p_id_paciente, p.PAIS_PACIENTE as p_pais_paciente, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['e_id_edu'])) ? $this->New_label['e_id_edu'] : "ID EDU"; 
          if ($Cada_col == "e_id_edu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_user'])) ? $this->New_label['e_user'] : "USER"; 
          if ($Cada_col == "e_user" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_id_paci_fk'])) ? $this->New_label['e_id_paci_fk'] : "ID PACI FK"; 
          if ($Cada_col == "e_id_paci_fk" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_se_brindo_edu'])) ? $this->New_label['e_se_brindo_edu'] : "SE BRINDO EDU"; 
          if ($Cada_col == "e_se_brindo_edu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_tema_si_edu'])) ? $this->New_label['e_tema_si_edu'] : "TEMA SI EDU"; 
          if ($Cada_col == "e_tema_si_edu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_fecha_si_edu'])) ? $this->New_label['e_fecha_si_edu'] : "FECHA SI EDU"; 
          if ($Cada_col == "e_fecha_si_edu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_motivo_no_edu'])) ? $this->New_label['e_motivo_no_edu'] : "MOTIVO NO EDU"; 
          if ($Cada_col == "e_motivo_no_edu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['e_fecha_registro'])) ? $this->New_label['e_fecha_registro'] : "FECHA REGISTRO"; 
          if ($Cada_col == "e_fecha_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_id_paciente'])) ? $this->New_label['p_id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "p_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_pais_paciente'])) ? $this->New_label['p_pais_paciente'] : "PAIS PACIENTE"; 
          if ($Cada_col == "p_pais_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_identificacion_paciente'])) ? $this->New_label['p_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          if ($Cada_col == "p_identificacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_nombre_paciente'])) ? $this->New_label['p_nombre_paciente'] : "NOMBRE PACIENTE"; 
          if ($Cada_col == "p_nombre_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_apellido_paciente'])) ? $this->New_label['p_apellido_paciente'] : "APELLIDO PACIENTE"; 
          if ($Cada_col == "p_apellido_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->e_id_edu = $rs->fields[0] ;  
         $this->e_id_edu = (string)$this->e_id_edu;
         $this->e_user = $rs->fields[1] ;  
         $this->e_id_paci_fk = $rs->fields[2] ;  
         $this->e_id_paci_fk = (string)$this->e_id_paci_fk;
         $this->e_se_brindo_edu = $rs->fields[3] ;  
         $this->e_tema_si_edu = $rs->fields[4] ;  
         $this->e_fecha_si_edu = $rs->fields[5] ;  
         $this->e_motivo_no_edu = $rs->fields[6] ;  
         $this->e_fecha_registro = $rs->fields[7] ;  
         $this->p_id_paciente = $rs->fields[8] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->p_pais_paciente = $rs->fields[9] ;  
         $this->p_identificacion_paciente = $rs->fields[10] ;  
         $this->p_nombre_paciente = $rs->fields[11] ;  
         $this->p_apellido_paciente = $rs->fields[12] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- e_id_edu
   function NM_export_e_id_edu()
   {
         $this->e_id_edu = html_entity_decode($this->e_id_edu, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_id_edu = strip_tags($this->e_id_edu);
         if (!NM_is_utf8($this->e_id_edu))
         {
             $this->e_id_edu = sc_convert_encoding($this->e_id_edu, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_id_edu = str_replace('<', '&lt;', $this->e_id_edu);
         $this->e_id_edu = str_replace('>', '&gt;', $this->e_id_edu);
         $this->Texto_tag .= "<td>" . $this->e_id_edu . "</td>\r\n";
   }
   //----- e_user
   function NM_export_e_user()
   {
         $this->e_user = html_entity_decode($this->e_user, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_user = strip_tags($this->e_user);
         if (!NM_is_utf8($this->e_user))
         {
             $this->e_user = sc_convert_encoding($this->e_user, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_user = str_replace('<', '&lt;', $this->e_user);
         $this->e_user = str_replace('>', '&gt;', $this->e_user);
         $this->Texto_tag .= "<td>" . $this->e_user . "</td>\r\n";
   }
   //----- e_id_paci_fk
   function NM_export_e_id_paci_fk()
   {
         $this->e_id_paci_fk = html_entity_decode($this->e_id_paci_fk, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_id_paci_fk = strip_tags($this->e_id_paci_fk);
         if (!NM_is_utf8($this->e_id_paci_fk))
         {
             $this->e_id_paci_fk = sc_convert_encoding($this->e_id_paci_fk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_id_paci_fk = str_replace('<', '&lt;', $this->e_id_paci_fk);
         $this->e_id_paci_fk = str_replace('>', '&gt;', $this->e_id_paci_fk);
         $this->Texto_tag .= "<td>" . $this->e_id_paci_fk . "</td>\r\n";
   }
   //----- e_se_brindo_edu
   function NM_export_e_se_brindo_edu()
   {
         $this->e_se_brindo_edu = html_entity_decode($this->e_se_brindo_edu, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_se_brindo_edu = strip_tags($this->e_se_brindo_edu);
         if (!NM_is_utf8($this->e_se_brindo_edu))
         {
             $this->e_se_brindo_edu = sc_convert_encoding($this->e_se_brindo_edu, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_se_brindo_edu = str_replace('<', '&lt;', $this->e_se_brindo_edu);
         $this->e_se_brindo_edu = str_replace('>', '&gt;', $this->e_se_brindo_edu);
         $this->Texto_tag .= "<td>" . $this->e_se_brindo_edu . "</td>\r\n";
   }
   //----- e_tema_si_edu
   function NM_export_e_tema_si_edu()
   {
         $this->e_tema_si_edu = html_entity_decode($this->e_tema_si_edu, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_tema_si_edu = strip_tags($this->e_tema_si_edu);
         if (!NM_is_utf8($this->e_tema_si_edu))
         {
             $this->e_tema_si_edu = sc_convert_encoding($this->e_tema_si_edu, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_tema_si_edu = str_replace('<', '&lt;', $this->e_tema_si_edu);
         $this->e_tema_si_edu = str_replace('>', '&gt;', $this->e_tema_si_edu);
         $this->Texto_tag .= "<td>" . $this->e_tema_si_edu . "</td>\r\n";
   }
   //----- e_fecha_si_edu
   function NM_export_e_fecha_si_edu()
   {
         $conteudo_x = $this->e_fecha_si_edu;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->e_fecha_si_edu, "YYYY-MM-DD");
             $this->e_fecha_si_edu = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->e_fecha_si_edu))
         {
             $this->e_fecha_si_edu = sc_convert_encoding($this->e_fecha_si_edu, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_fecha_si_edu = str_replace('<', '&lt;', $this->e_fecha_si_edu);
         $this->e_fecha_si_edu = str_replace('>', '&gt;', $this->e_fecha_si_edu);
         $this->Texto_tag .= "<td>" . $this->e_fecha_si_edu . "</td>\r\n";
   }
   //----- e_motivo_no_edu
   function NM_export_e_motivo_no_edu()
   {
         $this->e_motivo_no_edu = html_entity_decode($this->e_motivo_no_edu, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->e_motivo_no_edu = strip_tags($this->e_motivo_no_edu);
         if (!NM_is_utf8($this->e_motivo_no_edu))
         {
             $this->e_motivo_no_edu = sc_convert_encoding($this->e_motivo_no_edu, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_motivo_no_edu = str_replace('<', '&lt;', $this->e_motivo_no_edu);
         $this->e_motivo_no_edu = str_replace('>', '&gt;', $this->e_motivo_no_edu);
         $this->Texto_tag .= "<td>" . $this->e_motivo_no_edu . "</td>\r\n";
   }
   //----- e_fecha_registro
   function NM_export_e_fecha_registro()
   {
         if (substr($this->e_fecha_registro, 10, 1) == "-") 
         { 
             $this->e_fecha_registro = substr($this->e_fecha_registro, 0, 10) . " " . substr($this->e_fecha_registro, 11);
         } 
         if (substr($this->e_fecha_registro, 13, 1) == ".") 
         { 
            $this->e_fecha_registro = substr($this->e_fecha_registro, 0, 13) . ":" . substr($this->e_fecha_registro, 14, 2) . ":" . substr($this->e_fecha_registro, 17);
         } 
         $conteudo_x = $this->e_fecha_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD HH:II:SS");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->e_fecha_registro, "YYYY-MM-DD HH:II:SS");
             $this->e_fecha_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->e_fecha_registro))
         {
             $this->e_fecha_registro = sc_convert_encoding($this->e_fecha_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->e_fecha_registro = str_replace('<', '&lt;', $this->e_fecha_registro);
         $this->e_fecha_registro = str_replace('>', '&gt;', $this->e_fecha_registro);
         $this->Texto_tag .= "<td>" . $this->e_fecha_registro . "</td>\r\n";
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
         $this->p_id_paciente = html_entity_decode($this->p_id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_id_paciente = strip_tags($this->p_id_paciente);
         if (!NM_is_utf8($this->p_id_paciente))
         {
             $this->p_id_paciente = sc_convert_encoding($this->p_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_id_paciente = str_replace('<', '&lt;', $this->p_id_paciente);
         $this->p_id_paciente = str_replace('>', '&gt;', $this->p_id_paciente);
         $this->Texto_tag .= "<td>" . $this->p_id_paciente . "</td>\r\n";
   }
   //----- p_pais_paciente
   function NM_export_p_pais_paciente()
   {
         $this->p_pais_paciente = html_entity_decode($this->p_pais_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_pais_paciente = strip_tags($this->p_pais_paciente);
         if (!NM_is_utf8($this->p_pais_paciente))
         {
             $this->p_pais_paciente = sc_convert_encoding($this->p_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_pais_paciente = str_replace('<', '&lt;', $this->p_pais_paciente);
         $this->p_pais_paciente = str_replace('>', '&gt;', $this->p_pais_paciente);
         $this->Texto_tag .= "<td>" . $this->p_pais_paciente . "</td>\r\n";
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
         $this->p_identificacion_paciente = html_entity_decode($this->p_identificacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_identificacion_paciente = strip_tags($this->p_identificacion_paciente);
         if (!NM_is_utf8($this->p_identificacion_paciente))
         {
             $this->p_identificacion_paciente = sc_convert_encoding($this->p_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_identificacion_paciente = str_replace('<', '&lt;', $this->p_identificacion_paciente);
         $this->p_identificacion_paciente = str_replace('>', '&gt;', $this->p_identificacion_paciente);
         $this->Texto_tag .= "<td>" . $this->p_identificacion_paciente . "</td>\r\n";
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
         $this->p_nombre_paciente = html_entity_decode($this->p_nombre_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_nombre_paciente = strip_tags($this->p_nombre_paciente);
         if (!NM_is_utf8($this->p_nombre_paciente))
         {
             $this->p_nombre_paciente = sc_convert_encoding($this->p_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_nombre_paciente = str_replace('<', '&lt;', $this->p_nombre_paciente);
         $this->p_nombre_paciente = str_replace('>', '&gt;', $this->p_nombre_paciente);
         $this->Texto_tag .= "<td>" . $this->p_nombre_paciente . "</td>\r\n";
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
         $this->p_apellido_paciente = html_entity_decode($this->p_apellido_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_apellido_paciente = strip_tags($this->p_apellido_paciente);
         if (!NM_is_utf8($this->p_apellido_paciente))
         {
             $this->p_apellido_paciente = sc_convert_encoding($this->p_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_apellido_paciente = str_replace('<', '&lt;', $this->p_apellido_paciente);
         $this->p_apellido_paciente = str_replace('>', '&gt;', $this->p_apellido_paciente);
         $this->Texto_tag .= "<td>" . $this->p_apellido_paciente . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['informe_educacion'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Educacion :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="informe_educacion_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="informe_educacion"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
