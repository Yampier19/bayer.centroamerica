<?php

class grid_bayer_paciente_tratamiento_csv
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Tit_doc;
   var $Delim_dados;
   var $Delim_line;
   var $Delim_col;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_bayer_paciente_tratamiento_csv()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_csv()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
     global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo     = "sc_csv";
      $this->Arquivo    .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo    .= "_grid_bayer_paciente_tratamiento";
      $this->Arquivo    .= ".csv";
      $this->Tit_doc    = "grid_bayer_paciente_tratamiento.csv";
      $this->Delim_dados = "\"";
      $this->Delim_col   = ";";
      $this->Delim_line  = "\r\n";
   }

   //----- 
   function grava_arquivo()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_paciente_tratamiento']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->p_id_paciente = $Busca_temp['p_id_paciente']; 
          $tmp_pos = strpos($this->p_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_id_paciente = substr($this->p_id_paciente, 0, $tmp_pos);
          }
          $this->p_id_paciente_2 = $Busca_temp['p_id_paciente_input_2']; 
          $this->p_estado_paciente = $Busca_temp['p_estado_paciente']; 
          $tmp_pos = strpos($this->p_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_estado_paciente = substr($this->p_estado_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->p_fecha_retiro_paciente = $Busca_temp['p_fecha_retiro_paciente']; 
          $tmp_pos = strpos($this->p_fecha_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_retiro_paciente = substr($this->p_fecha_retiro_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT p.ID_PACIENTE as p_id_paciente, p.ESTADO_PACIENTE as p_estado_paciente, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, p.FECHA_RETIRO_PACIENTE as p_fecha_retiro_paciente, p.MOTIVO_RETIRO_PACIENTE as p_motivo_retiro_paciente, p.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, p.IDENTIFICACION_PACIENTE as p_identificacion_paciente, p.NOMBRE_PACIENTE as p_nombre_paciente, p.APELLIDO_PACIENTE as p_apellido_paciente, p.TELEFONO_PACIENTE as p_telefono_paciente, p.TELEFONO2_PACIENTE as p_telefono2_paciente, p.TELEFONO3_PACIENTE as p_telefono3_paciente, p.CORREO_PACIENTE as p_correo_paciente, p.DIRECCION_PACIENTE as p_direccion_paciente, p.GENERO_PACIENTE as p_genero_paciente, p.FECHA_NACIMINETO_PACIENTE as p_fecha_nacimineto_paciente, p.ID_ULTIMA_GESTION as p_id_ultima_gestion, t.ID_TRATAMIENTO as t_id_tratamiento, p.USUARIO_CREACION as p_usuario_creacion, t.PRODUCTO_TRATAMIENTO as t_producto_tratamiento, t.NOMBRE_REFERENCIA as t_nombre_referencia, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, t.TRATAMIENTO_PREVIO as t_tratamiento_previo, t.CONSENTIMIENTO_TRATAMIENTO as t_consentimiento_tratamiento, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, t.REGIMEN_TRATAMIENTO as t_regimen_tratamiento, t.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, t.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, t.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, t.OTROS_OPERADORES_TRATAMIENTO as t_otros_operadores_tratamiento, t.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_6, t.IPS_ATIENDE_TRATAMIENTO as t_ips_atiende_tratamiento, t.MEDICO_TRATAMIENTO as t_medico_tratamiento, t.ESPECIALIDAD_TRATAMIENTO as t_especialidad_tratamiento, t.PARAMEDICO_TRATAMIENTO as t_paramedico_tratamiento, t.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_7, t.NOTAS_ADJUNTOS_TRATAMIENTO as t_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $csv_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      while (!$rs->EOF)
      {
         $this->csv_registro = "";
         $this->NM_prim_col  = 0;
         $this->p_id_paciente = $rs->fields[0] ;  
         $this->p_id_paciente = (string)$this->p_id_paciente;
         $this->p_estado_paciente = $rs->fields[1] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[2] ;  
         $this->p_fecha_retiro_paciente = $rs->fields[3] ;  
         $this->p_motivo_retiro_paciente = $rs->fields[4] ;  
         $this->p_observacion_motivo_retiro_paciente = $rs->fields[5] ;  
         $this->p_identificacion_paciente = $rs->fields[6] ;  
         $this->p_nombre_paciente = $rs->fields[7] ;  
         $this->p_apellido_paciente = $rs->fields[8] ;  
         $this->p_telefono_paciente = $rs->fields[9] ;  
         $this->p_telefono2_paciente = $rs->fields[10] ;  
         $this->p_telefono3_paciente = $rs->fields[11] ;  
         $this->p_correo_paciente = $rs->fields[12] ;  
         $this->p_direccion_paciente = $rs->fields[13] ;  
         $this->p_genero_paciente = $rs->fields[14] ;  
         $this->p_fecha_nacimineto_paciente = $rs->fields[15] ;  
         $this->p_id_ultima_gestion = $rs->fields[16] ;  
         $this->p_id_ultima_gestion = (string)$this->p_id_ultima_gestion;
         $this->t_id_tratamiento = $rs->fields[17] ;  
         $this->t_id_tratamiento = (string)$this->t_id_tratamiento;
         $this->p_usuario_creacion = $rs->fields[18] ;  
         $this->t_producto_tratamiento = $rs->fields[19] ;  
         $this->t_nombre_referencia = $rs->fields[20] ;  
         $this->t_clasificacion_patologica_tratamiento = $rs->fields[21] ;  
         $this->t_tratamiento_previo = $rs->fields[22] ;  
         $this->t_consentimiento_tratamiento = $rs->fields[23] ;  
         $this->t_fecha_inicio_terapia_tratamiento = $rs->fields[24] ;  
         $this->t_regimen_tratamiento = $rs->fields[25] ;  
         $this->t_asegurador_tratamiento = $rs->fields[26] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[27] ;  
         $this->t_fecha_ultima_reclamacion_tratamiento = $rs->fields[28] ;  
         $this->t_otros_operadores_tratamiento = $rs->fields[29] ;  
         $this->t_medios_adquisicion_tratamiento = $rs->fields[30] ;  
         $this->t_ips_atiende_tratamiento = $rs->fields[31] ;  
         $this->t_medico_tratamiento = $rs->fields[32] ;  
         $this->t_especialidad_tratamiento = $rs->fields[33] ;  
         $this->t_paramedico_tratamiento = $rs->fields[34] ;  
         $this->t_ciudad_base_paramedico_tratamiento = $rs->fields[35] ;  
         $this->t_notas_adjuntos_tratamiento = $rs->fields[36] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->csv_registro .= $this->Delim_line;
         fwrite($csv_f, $this->csv_registro);
         $rs->MoveNext();
      }
      fclose($csv_f);

      $rs->Close();
   }
   //----- p_id_paciente
   function NM_export_p_id_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_id_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_estado_paciente
   function NM_export_p_estado_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_estado_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_fecha_activacion_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_fecha_retiro_paciente
   function NM_export_p_fecha_retiro_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_fecha_retiro_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_motivo_retiro_paciente
   function NM_export_p_motivo_retiro_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_motivo_retiro_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_observacion_motivo_retiro_paciente
   function NM_export_p_observacion_motivo_retiro_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_observacion_motivo_retiro_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_identificacion_paciente
   function NM_export_p_identificacion_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_identificacion_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_nombre_paciente
   function NM_export_p_nombre_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_nombre_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_apellido_paciente
   function NM_export_p_apellido_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_apellido_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono_paciente
   function NM_export_p_telefono_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono2_paciente
   function NM_export_p_telefono2_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono2_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_telefono3_paciente
   function NM_export_p_telefono3_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_telefono3_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_correo_paciente
   function NM_export_p_correo_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_correo_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_direccion_paciente
   function NM_export_p_direccion_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_direccion_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_genero_paciente
   function NM_export_p_genero_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_genero_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_fecha_nacimineto_paciente
   function NM_export_p_fecha_nacimineto_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_fecha_nacimineto_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_id_ultima_gestion
   function NM_export_p_id_ultima_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_id_ultima_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_id_tratamiento
   function NM_export_t_id_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_id_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- p_usuario_creacion
   function NM_export_p_usuario_creacion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->p_usuario_creacion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_producto_tratamiento
   function NM_export_t_producto_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_producto_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_nombre_referencia
   function NM_export_t_nombre_referencia()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_nombre_referencia);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_clasificacion_patologica_tratamiento
   function NM_export_t_clasificacion_patologica_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_clasificacion_patologica_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_tratamiento_previo
   function NM_export_t_tratamiento_previo()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_tratamiento_previo);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_consentimiento_tratamiento
   function NM_export_t_consentimiento_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_consentimiento_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_fecha_inicio_terapia_tratamiento
   function NM_export_t_fecha_inicio_terapia_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_fecha_inicio_terapia_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_regimen_tratamiento
   function NM_export_t_regimen_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_regimen_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_asegurador_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_operador_logistico_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_fecha_ultima_reclamacion_tratamiento
   function NM_export_t_fecha_ultima_reclamacion_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_fecha_ultima_reclamacion_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_otros_operadores_tratamiento
   function NM_export_t_otros_operadores_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_otros_operadores_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_medios_adquisicion_tratamiento
   function NM_export_t_medios_adquisicion_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_medios_adquisicion_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_ips_atiende_tratamiento
   function NM_export_t_ips_atiende_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_ips_atiende_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_medico_tratamiento
   function NM_export_t_medico_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_medico_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_especialidad_tratamiento
   function NM_export_t_especialidad_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_especialidad_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_paramedico_tratamiento
   function NM_export_t_paramedico_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_paramedico_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_ciudad_base_paramedico_tratamiento
   function NM_export_t_ciudad_base_paramedico_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_ciudad_base_paramedico_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- t_notas_adjuntos_tratamiento
   function NM_export_t_notas_adjuntos_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->t_notas_adjuntos_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento']['csv_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_paciente_tratamiento'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Paciente Tratamiento  :: CSV</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?>" GMT">
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate">
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0">
 <META http-equiv="Pragma" content="no-cache">
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">CSV</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_bayer_paciente_tratamiento_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_bayer_paciente_tratamiento"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
