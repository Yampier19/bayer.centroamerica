<?php

class Reclamacion_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function Reclamacion_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_Reclamacion";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "Reclamacion.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Reclamacion']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Reclamacion']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Reclamacion']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->bp_id_paciente = $Busca_temp['bp_id_paciente']; 
          $tmp_pos = strpos($this->bp_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_id_paciente = substr($this->bp_id_paciente, 0, $tmp_pos);
          }
          $this->bp_id_paciente_2 = $Busca_temp['bp_id_paciente_input_2']; 
          $this->bp_pais_paciente = $Busca_temp['bp_pais_paciente']; 
          $tmp_pos = strpos($this->bp_pais_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_pais_paciente = substr($this->bp_pais_paciente, 0, $tmp_pos);
          }
          $this->bp_ciudad_paciente = $Busca_temp['bp_ciudad_paciente']; 
          $tmp_pos = strpos($this->bp_ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_ciudad_paciente = substr($this->bp_ciudad_paciente, 0, $tmp_pos);
          }
          $this->bp_estado_paciente = $Busca_temp['bp_estado_paciente']; 
          $tmp_pos = strpos($this->bp_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_estado_paciente = substr($this->bp_estado_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, bg.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, bg.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, bg.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, bg.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, LOTOFILE(bg.DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.PAIS_PACIENTE as bp_pais_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.CODIGO_XOFIGO as bp_codigo_xofigo_1, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion_1, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_1, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_2, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_3, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_4, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.ID_PACIENTE_FK as bt_id_paciente_fk, bg.ID_GESTION as bg_id_gestion, bg.LOGRO_COMUNICACION_GESTION as bg_logro_comunicacion_gestion, bg.CAUSA_NO_RECLAMACION_GESTION as cmp_maior_30_5, bg.RECLAMO_GESTION as bg_reclamo_gestion, bg.FECHA_RECLAMACION_GESTION as bg_fecha_reclamacion_gestion, bg.FECHA_COMUNICACION as bg_fecha_comunicacion, bg.FECHA_PROGRAMADA_GESTION as bg_fecha_programada_gestion, bg.AUTOR_GESTION as bg_autor_gestion, bg.DESCRIPCION_COMUNICACION_GESTION as cmp_maior_30_6, bg.NUMERO_CAJAS as bg_numero_cajas from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['bp_id_paciente'])) ? $this->New_label['bp_id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "bp_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_pais_paciente'])) ? $this->New_label['bp_pais_paciente'] : "PAIS PACIENTE"; 
          if ($Cada_col == "bp_pais_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_ciudad_paciente'])) ? $this->New_label['bp_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "bp_ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_estado_paciente'])) ? $this->New_label['bp_estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "bp_estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_status_paciente'])) ? $this->New_label['bp_status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "bp_status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_fecha_activacion_paciente'])) ? $this->New_label['bp_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "bp_fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_fecha_retiro_paciente'])) ? $this->New_label['bp_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          if ($Cada_col == "bp_fecha_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_motivo_retiro_paciente'])) ? $this->New_label['bp_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "bp_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_codigo_xofigo'])) ? $this->New_label['bp_codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "bp_codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_id_ultima_gestion'])) ? $this->New_label['bp_id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          if ($Cada_col == "bp_id_ultima_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_codigo_xofigo_1'])) ? $this->New_label['bp_codigo_xofigo_1'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "bp_codigo_xofigo_1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bp_id_ultima_gestion_1'])) ? $this->New_label['bp_id_ultima_gestion_1'] : "ID ULTIMA GESTION"; 
          if ($Cada_col == "bp_id_ultima_gestion_1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_id_tratamiento'])) ? $this->New_label['bt_id_tratamiento'] : "ID TRATAMIENTO"; 
          if ($Cada_col == "bt_id_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_producto_tratamiento'])) ? $this->New_label['bt_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "bt_producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_nombre_referencia'])) ? $this->New_label['bt_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "bt_nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_dosis_tratamiento'])) ? $this->New_label['bt_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "bt_dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_fecha_inicio_terapia_tratamiento'])) ? $this->New_label['bt_fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          if ($Cada_col == "bt_fecha_inicio_terapia_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_asegurador_tratamiento'])) ? $this->New_label['bt_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "bt_asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_operador_logistico_tratamiento'])) ? $this->New_label['bt_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "bt_operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_punto_entrega'])) ? $this->New_label['bt_punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "bt_punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_ips_atiende_tratamiento'])) ? $this->New_label['bt_ips_atiende_tratamiento'] : "IPS ATIENDE TRATAMIENTO"; 
          if ($Cada_col == "bt_ips_atiende_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_regimen_tratamiento'])) ? $this->New_label['bt_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          if ($Cada_col == "bt_regimen_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_medico_tratamiento'])) ? $this->New_label['bt_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          if ($Cada_col == "bt_medico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['bt_fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          if ($Cada_col == "bt_fecha_ultima_reclamacion_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_clasificacion_patologica_tratamiento'])) ? $this->New_label['bt_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          if ($Cada_col == "bt_clasificacion_patologica_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_tratamiento_previo'])) ? $this->New_label['bt_tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          if ($Cada_col == "bt_tratamiento_previo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bt_id_paciente_fk'])) ? $this->New_label['bt_id_paciente_fk'] : "ID PACIENTE FK"; 
          if ($Cada_col == "bt_id_paciente_fk" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_id_gestion'])) ? $this->New_label['bg_id_gestion'] : "ID GESTION"; 
          if ($Cada_col == "bg_id_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_logro_comunicacion_gestion'])) ? $this->New_label['bg_logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          if ($Cada_col == "bg_logro_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_causa_no_reclamacion_gestion'])) ? $this->New_label['bg_causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          if ($Cada_col == "bg_causa_no_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_reclamo_gestion'])) ? $this->New_label['bg_reclamo_gestion'] : "RECLAMO GESTION"; 
          if ($Cada_col == "bg_reclamo_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_fecha_reclamacion_gestion'])) ? $this->New_label['bg_fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          if ($Cada_col == "bg_fecha_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_fecha_comunicacion'])) ? $this->New_label['bg_fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "bg_fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_fecha_programada_gestion'])) ? $this->New_label['bg_fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; 
          if ($Cada_col == "bg_fecha_programada_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_autor_gestion'])) ? $this->New_label['bg_autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "bg_autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_descripcion_comunicacion_gestion'])) ? $this->New_label['bg_descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; 
          if ($Cada_col == "bg_descripcion_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bg_numero_cajas'])) ? $this->New_label['bg_numero_cajas'] : "NUMERO CAJAS"; 
          if ($Cada_col == "bg_numero_cajas" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->bp_id_paciente = $rs->fields[0] ;  
         $this->bp_id_paciente = (string)$this->bp_id_paciente;
         $this->bp_pais_paciente = $rs->fields[1] ;  
         $this->bp_ciudad_paciente = $rs->fields[2] ;  
         $this->bp_estado_paciente = $rs->fields[3] ;  
         $this->bp_status_paciente = $rs->fields[4] ;  
         $this->bp_fecha_activacion_paciente = $rs->fields[5] ;  
         $this->bp_fecha_retiro_paciente = $rs->fields[6] ;  
         $this->bp_motivo_retiro_paciente = $rs->fields[7] ;  
         $this->bp_codigo_xofigo = $rs->fields[8] ;  
         $this->bp_codigo_xofigo = (string)$this->bp_codigo_xofigo;
         $this->bp_id_ultima_gestion = $rs->fields[9] ;  
         $this->bp_id_ultima_gestion = (string)$this->bp_id_ultima_gestion;
         $this->bp_codigo_xofigo_1 = $rs->fields[10] ;  
         $this->bp_codigo_xofigo_1 = (string)$this->bp_codigo_xofigo_1;
         $this->bp_id_ultima_gestion_1 = $rs->fields[11] ;  
         $this->bp_id_ultima_gestion_1 = (string)$this->bp_id_ultima_gestion_1;
         $this->bt_id_tratamiento = $rs->fields[12] ;  
         $this->bt_id_tratamiento = (string)$this->bt_id_tratamiento;
         $this->bt_producto_tratamiento = $rs->fields[13] ;  
         $this->bt_nombre_referencia = $rs->fields[14] ;  
         $this->bt_dosis_tratamiento = $rs->fields[15] ;  
         $this->bt_fecha_inicio_terapia_tratamiento = $rs->fields[16] ;  
         $this->bt_asegurador_tratamiento = $rs->fields[17] ;  
         $this->bt_operador_logistico_tratamiento = $rs->fields[18] ;  
         $this->bt_punto_entrega = $rs->fields[19] ;  
         $this->bt_ips_atiende_tratamiento = $rs->fields[20] ;  
         $this->bt_regimen_tratamiento = $rs->fields[21] ;  
         $this->bt_medico_tratamiento = $rs->fields[22] ;  
         $this->bt_fecha_ultima_reclamacion_tratamiento = $rs->fields[23] ;  
         $this->bt_clasificacion_patologica_tratamiento = $rs->fields[24] ;  
         $this->bt_tratamiento_previo = $rs->fields[25] ;  
         $this->bt_id_paciente_fk = $rs->fields[26] ;  
         $this->bt_id_paciente_fk = (string)$this->bt_id_paciente_fk;
         $this->bg_id_gestion = $rs->fields[27] ;  
         $this->bg_id_gestion = (string)$this->bg_id_gestion;
         $this->bg_logro_comunicacion_gestion = $rs->fields[28] ;  
         $this->bg_causa_no_reclamacion_gestion = $rs->fields[29] ;  
         $this->bg_reclamo_gestion = $rs->fields[30] ;  
         $this->bg_fecha_reclamacion_gestion = $rs->fields[31] ;  
         $this->bg_fecha_comunicacion = $rs->fields[32] ;  
         $this->bg_fecha_programada_gestion = $rs->fields[33] ;  
         $this->bg_autor_gestion = $rs->fields[34] ;  
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $this->bg_descripcion_comunicacion_gestion = "";  
              if (is_file($rs_grid->fields[35])) 
              { 
                  $this->bg_descripcion_comunicacion_gestion = file_get_contents($rs_grid->fields[35]);  
              } 
          } 
         elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
         { 
             $this->bg_descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[35]) ;  
         } 
         else
         { 
             $this->bg_descripcion_comunicacion_gestion = $rs->fields[35] ;  
         } 
         $this->bg_numero_cajas = $rs->fields[36] ;  
         $this->bg_descripcion_comunicacion_gestion = "";
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- bp_id_paciente
   function NM_export_bp_id_paciente()
   {
         $this->bp_id_paciente = html_entity_decode($this->bp_id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_id_paciente = strip_tags($this->bp_id_paciente);
         if (!NM_is_utf8($this->bp_id_paciente))
         {
             $this->bp_id_paciente = sc_convert_encoding($this->bp_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_id_paciente = str_replace('<', '&lt;', $this->bp_id_paciente);
         $this->bp_id_paciente = str_replace('>', '&gt;', $this->bp_id_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_id_paciente . "</td>\r\n";
   }
   //----- bp_pais_paciente
   function NM_export_bp_pais_paciente()
   {
         $this->bp_pais_paciente = html_entity_decode($this->bp_pais_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_pais_paciente = strip_tags($this->bp_pais_paciente);
         if (!NM_is_utf8($this->bp_pais_paciente))
         {
             $this->bp_pais_paciente = sc_convert_encoding($this->bp_pais_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_pais_paciente = str_replace('<', '&lt;', $this->bp_pais_paciente);
         $this->bp_pais_paciente = str_replace('>', '&gt;', $this->bp_pais_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_pais_paciente . "</td>\r\n";
   }
   //----- bp_ciudad_paciente
   function NM_export_bp_ciudad_paciente()
   {
         $this->bp_ciudad_paciente = html_entity_decode($this->bp_ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_ciudad_paciente = strip_tags($this->bp_ciudad_paciente);
         if (!NM_is_utf8($this->bp_ciudad_paciente))
         {
             $this->bp_ciudad_paciente = sc_convert_encoding($this->bp_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_ciudad_paciente = str_replace('<', '&lt;', $this->bp_ciudad_paciente);
         $this->bp_ciudad_paciente = str_replace('>', '&gt;', $this->bp_ciudad_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_ciudad_paciente . "</td>\r\n";
   }
   //----- bp_estado_paciente
   function NM_export_bp_estado_paciente()
   {
         $this->bp_estado_paciente = html_entity_decode($this->bp_estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_estado_paciente = strip_tags($this->bp_estado_paciente);
         if (!NM_is_utf8($this->bp_estado_paciente))
         {
             $this->bp_estado_paciente = sc_convert_encoding($this->bp_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_estado_paciente = str_replace('<', '&lt;', $this->bp_estado_paciente);
         $this->bp_estado_paciente = str_replace('>', '&gt;', $this->bp_estado_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_estado_paciente . "</td>\r\n";
   }
   //----- bp_status_paciente
   function NM_export_bp_status_paciente()
   {
         $this->bp_status_paciente = html_entity_decode($this->bp_status_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_status_paciente = strip_tags($this->bp_status_paciente);
         if (!NM_is_utf8($this->bp_status_paciente))
         {
             $this->bp_status_paciente = sc_convert_encoding($this->bp_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_status_paciente = str_replace('<', '&lt;', $this->bp_status_paciente);
         $this->bp_status_paciente = str_replace('>', '&gt;', $this->bp_status_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_status_paciente . "</td>\r\n";
   }
   //----- bp_fecha_activacion_paciente
   function NM_export_bp_fecha_activacion_paciente()
   {
         $this->bp_fecha_activacion_paciente = html_entity_decode($this->bp_fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_fecha_activacion_paciente = strip_tags($this->bp_fecha_activacion_paciente);
         if (!NM_is_utf8($this->bp_fecha_activacion_paciente))
         {
             $this->bp_fecha_activacion_paciente = sc_convert_encoding($this->bp_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_fecha_activacion_paciente = str_replace('<', '&lt;', $this->bp_fecha_activacion_paciente);
         $this->bp_fecha_activacion_paciente = str_replace('>', '&gt;', $this->bp_fecha_activacion_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_fecha_activacion_paciente . "</td>\r\n";
   }
   //----- bp_fecha_retiro_paciente
   function NM_export_bp_fecha_retiro_paciente()
   {
         $this->bp_fecha_retiro_paciente = html_entity_decode($this->bp_fecha_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_fecha_retiro_paciente = strip_tags($this->bp_fecha_retiro_paciente);
         if (!NM_is_utf8($this->bp_fecha_retiro_paciente))
         {
             $this->bp_fecha_retiro_paciente = sc_convert_encoding($this->bp_fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_fecha_retiro_paciente = str_replace('<', '&lt;', $this->bp_fecha_retiro_paciente);
         $this->bp_fecha_retiro_paciente = str_replace('>', '&gt;', $this->bp_fecha_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_fecha_retiro_paciente . "</td>\r\n";
   }
   //----- bp_motivo_retiro_paciente
   function NM_export_bp_motivo_retiro_paciente()
   {
         $this->bp_motivo_retiro_paciente = html_entity_decode($this->bp_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_motivo_retiro_paciente = strip_tags($this->bp_motivo_retiro_paciente);
         if (!NM_is_utf8($this->bp_motivo_retiro_paciente))
         {
             $this->bp_motivo_retiro_paciente = sc_convert_encoding($this->bp_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_motivo_retiro_paciente = str_replace('<', '&lt;', $this->bp_motivo_retiro_paciente);
         $this->bp_motivo_retiro_paciente = str_replace('>', '&gt;', $this->bp_motivo_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->bp_motivo_retiro_paciente . "</td>\r\n";
   }
   //----- bp_codigo_xofigo
   function NM_export_bp_codigo_xofigo()
   {
         nmgp_Form_Num_Val($this->bp_codigo_xofigo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->bp_codigo_xofigo))
         {
             $this->bp_codigo_xofigo = sc_convert_encoding($this->bp_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_codigo_xofigo = str_replace('<', '&lt;', $this->bp_codigo_xofigo);
         $this->bp_codigo_xofigo = str_replace('>', '&gt;', $this->bp_codigo_xofigo);
         $this->Texto_tag .= "<td>" . $this->bp_codigo_xofigo . "</td>\r\n";
   }
   //----- bp_id_ultima_gestion
   function NM_export_bp_id_ultima_gestion()
   {
         $this->bp_id_ultima_gestion = html_entity_decode($this->bp_id_ultima_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_id_ultima_gestion = strip_tags($this->bp_id_ultima_gestion);
         if (!NM_is_utf8($this->bp_id_ultima_gestion))
         {
             $this->bp_id_ultima_gestion = sc_convert_encoding($this->bp_id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_id_ultima_gestion = str_replace('<', '&lt;', $this->bp_id_ultima_gestion);
         $this->bp_id_ultima_gestion = str_replace('>', '&gt;', $this->bp_id_ultima_gestion);
         $this->Texto_tag .= "<td>" . $this->bp_id_ultima_gestion . "</td>\r\n";
   }
   //----- bp_codigo_xofigo_1
   function NM_export_bp_codigo_xofigo_1()
   {
         nmgp_Form_Num_Val($this->bp_codigo_xofigo_1, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->bp_codigo_xofigo_1))
         {
             $this->bp_codigo_xofigo_1 = sc_convert_encoding($this->bp_codigo_xofigo_1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_codigo_xofigo_1 = str_replace('<', '&lt;', $this->bp_codigo_xofigo_1);
         $this->bp_codigo_xofigo_1 = str_replace('>', '&gt;', $this->bp_codigo_xofigo_1);
         $this->Texto_tag .= "<td>" . $this->bp_codigo_xofigo_1 . "</td>\r\n";
   }
   //----- bp_id_ultima_gestion_1
   function NM_export_bp_id_ultima_gestion_1()
   {
         $this->bp_id_ultima_gestion_1 = html_entity_decode($this->bp_id_ultima_gestion_1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_id_ultima_gestion_1 = strip_tags($this->bp_id_ultima_gestion_1);
         if (!NM_is_utf8($this->bp_id_ultima_gestion_1))
         {
             $this->bp_id_ultima_gestion_1 = sc_convert_encoding($this->bp_id_ultima_gestion_1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bp_id_ultima_gestion_1 = str_replace('<', '&lt;', $this->bp_id_ultima_gestion_1);
         $this->bp_id_ultima_gestion_1 = str_replace('>', '&gt;', $this->bp_id_ultima_gestion_1);
         $this->Texto_tag .= "<td>" . $this->bp_id_ultima_gestion_1 . "</td>\r\n";
   }
   //----- bt_id_tratamiento
   function NM_export_bt_id_tratamiento()
   {
         $this->bt_id_tratamiento = html_entity_decode($this->bt_id_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_id_tratamiento = strip_tags($this->bt_id_tratamiento);
         if (!NM_is_utf8($this->bt_id_tratamiento))
         {
             $this->bt_id_tratamiento = sc_convert_encoding($this->bt_id_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_id_tratamiento = str_replace('<', '&lt;', $this->bt_id_tratamiento);
         $this->bt_id_tratamiento = str_replace('>', '&gt;', $this->bt_id_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_id_tratamiento . "</td>\r\n";
   }
   //----- bt_producto_tratamiento
   function NM_export_bt_producto_tratamiento()
   {
         $this->bt_producto_tratamiento = html_entity_decode($this->bt_producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_producto_tratamiento = strip_tags($this->bt_producto_tratamiento);
         if (!NM_is_utf8($this->bt_producto_tratamiento))
         {
             $this->bt_producto_tratamiento = sc_convert_encoding($this->bt_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_producto_tratamiento = str_replace('<', '&lt;', $this->bt_producto_tratamiento);
         $this->bt_producto_tratamiento = str_replace('>', '&gt;', $this->bt_producto_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_producto_tratamiento . "</td>\r\n";
   }
   //----- bt_nombre_referencia
   function NM_export_bt_nombre_referencia()
   {
         $this->bt_nombre_referencia = html_entity_decode($this->bt_nombre_referencia, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_nombre_referencia = strip_tags($this->bt_nombre_referencia);
         if (!NM_is_utf8($this->bt_nombre_referencia))
         {
             $this->bt_nombre_referencia = sc_convert_encoding($this->bt_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_nombre_referencia = str_replace('<', '&lt;', $this->bt_nombre_referencia);
         $this->bt_nombre_referencia = str_replace('>', '&gt;', $this->bt_nombre_referencia);
         $this->Texto_tag .= "<td>" . $this->bt_nombre_referencia . "</td>\r\n";
   }
   //----- bt_dosis_tratamiento
   function NM_export_bt_dosis_tratamiento()
   {
         $this->bt_dosis_tratamiento = html_entity_decode($this->bt_dosis_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_dosis_tratamiento = strip_tags($this->bt_dosis_tratamiento);
         if (!NM_is_utf8($this->bt_dosis_tratamiento))
         {
             $this->bt_dosis_tratamiento = sc_convert_encoding($this->bt_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_dosis_tratamiento = str_replace('<', '&lt;', $this->bt_dosis_tratamiento);
         $this->bt_dosis_tratamiento = str_replace('>', '&gt;', $this->bt_dosis_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_dosis_tratamiento . "</td>\r\n";
   }
   //----- bt_fecha_inicio_terapia_tratamiento
   function NM_export_bt_fecha_inicio_terapia_tratamiento()
   {
         $this->bt_fecha_inicio_terapia_tratamiento = html_entity_decode($this->bt_fecha_inicio_terapia_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_fecha_inicio_terapia_tratamiento = strip_tags($this->bt_fecha_inicio_terapia_tratamiento);
         if (!NM_is_utf8($this->bt_fecha_inicio_terapia_tratamiento))
         {
             $this->bt_fecha_inicio_terapia_tratamiento = sc_convert_encoding($this->bt_fecha_inicio_terapia_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_fecha_inicio_terapia_tratamiento = str_replace('<', '&lt;', $this->bt_fecha_inicio_terapia_tratamiento);
         $this->bt_fecha_inicio_terapia_tratamiento = str_replace('>', '&gt;', $this->bt_fecha_inicio_terapia_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_fecha_inicio_terapia_tratamiento . "</td>\r\n";
   }
   //----- bt_asegurador_tratamiento
   function NM_export_bt_asegurador_tratamiento()
   {
         $this->bt_asegurador_tratamiento = html_entity_decode($this->bt_asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_asegurador_tratamiento = strip_tags($this->bt_asegurador_tratamiento);
         if (!NM_is_utf8($this->bt_asegurador_tratamiento))
         {
             $this->bt_asegurador_tratamiento = sc_convert_encoding($this->bt_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_asegurador_tratamiento = str_replace('<', '&lt;', $this->bt_asegurador_tratamiento);
         $this->bt_asegurador_tratamiento = str_replace('>', '&gt;', $this->bt_asegurador_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_asegurador_tratamiento . "</td>\r\n";
   }
   //----- bt_operador_logistico_tratamiento
   function NM_export_bt_operador_logistico_tratamiento()
   {
         $this->bt_operador_logistico_tratamiento = html_entity_decode($this->bt_operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_operador_logistico_tratamiento = strip_tags($this->bt_operador_logistico_tratamiento);
         if (!NM_is_utf8($this->bt_operador_logistico_tratamiento))
         {
             $this->bt_operador_logistico_tratamiento = sc_convert_encoding($this->bt_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_operador_logistico_tratamiento = str_replace('<', '&lt;', $this->bt_operador_logistico_tratamiento);
         $this->bt_operador_logistico_tratamiento = str_replace('>', '&gt;', $this->bt_operador_logistico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_operador_logistico_tratamiento . "</td>\r\n";
   }
   //----- bt_punto_entrega
   function NM_export_bt_punto_entrega()
   {
         $this->bt_punto_entrega = html_entity_decode($this->bt_punto_entrega, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_punto_entrega = strip_tags($this->bt_punto_entrega);
         if (!NM_is_utf8($this->bt_punto_entrega))
         {
             $this->bt_punto_entrega = sc_convert_encoding($this->bt_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_punto_entrega = str_replace('<', '&lt;', $this->bt_punto_entrega);
         $this->bt_punto_entrega = str_replace('>', '&gt;', $this->bt_punto_entrega);
         $this->Texto_tag .= "<td>" . $this->bt_punto_entrega . "</td>\r\n";
   }
   //----- bt_ips_atiende_tratamiento
   function NM_export_bt_ips_atiende_tratamiento()
   {
         $this->bt_ips_atiende_tratamiento = html_entity_decode($this->bt_ips_atiende_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_ips_atiende_tratamiento = strip_tags($this->bt_ips_atiende_tratamiento);
         if (!NM_is_utf8($this->bt_ips_atiende_tratamiento))
         {
             $this->bt_ips_atiende_tratamiento = sc_convert_encoding($this->bt_ips_atiende_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_ips_atiende_tratamiento = str_replace('<', '&lt;', $this->bt_ips_atiende_tratamiento);
         $this->bt_ips_atiende_tratamiento = str_replace('>', '&gt;', $this->bt_ips_atiende_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_ips_atiende_tratamiento . "</td>\r\n";
   }
   //----- bt_regimen_tratamiento
   function NM_export_bt_regimen_tratamiento()
   {
         $this->bt_regimen_tratamiento = html_entity_decode($this->bt_regimen_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_regimen_tratamiento = strip_tags($this->bt_regimen_tratamiento);
         if (!NM_is_utf8($this->bt_regimen_tratamiento))
         {
             $this->bt_regimen_tratamiento = sc_convert_encoding($this->bt_regimen_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_regimen_tratamiento = str_replace('<', '&lt;', $this->bt_regimen_tratamiento);
         $this->bt_regimen_tratamiento = str_replace('>', '&gt;', $this->bt_regimen_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_regimen_tratamiento . "</td>\r\n";
   }
   //----- bt_medico_tratamiento
   function NM_export_bt_medico_tratamiento()
   {
         $this->bt_medico_tratamiento = html_entity_decode($this->bt_medico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_medico_tratamiento = strip_tags($this->bt_medico_tratamiento);
         if (!NM_is_utf8($this->bt_medico_tratamiento))
         {
             $this->bt_medico_tratamiento = sc_convert_encoding($this->bt_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_medico_tratamiento = str_replace('<', '&lt;', $this->bt_medico_tratamiento);
         $this->bt_medico_tratamiento = str_replace('>', '&gt;', $this->bt_medico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_medico_tratamiento . "</td>\r\n";
   }
   //----- bt_fecha_ultima_reclamacion_tratamiento
   function NM_export_bt_fecha_ultima_reclamacion_tratamiento()
   {
         $this->bt_fecha_ultima_reclamacion_tratamiento = html_entity_decode($this->bt_fecha_ultima_reclamacion_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_fecha_ultima_reclamacion_tratamiento = strip_tags($this->bt_fecha_ultima_reclamacion_tratamiento);
         if (!NM_is_utf8($this->bt_fecha_ultima_reclamacion_tratamiento))
         {
             $this->bt_fecha_ultima_reclamacion_tratamiento = sc_convert_encoding($this->bt_fecha_ultima_reclamacion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_fecha_ultima_reclamacion_tratamiento = str_replace('<', '&lt;', $this->bt_fecha_ultima_reclamacion_tratamiento);
         $this->bt_fecha_ultima_reclamacion_tratamiento = str_replace('>', '&gt;', $this->bt_fecha_ultima_reclamacion_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_fecha_ultima_reclamacion_tratamiento . "</td>\r\n";
   }
   //----- bt_clasificacion_patologica_tratamiento
   function NM_export_bt_clasificacion_patologica_tratamiento()
   {
         $this->bt_clasificacion_patologica_tratamiento = html_entity_decode($this->bt_clasificacion_patologica_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_clasificacion_patologica_tratamiento = strip_tags($this->bt_clasificacion_patologica_tratamiento);
         if (!NM_is_utf8($this->bt_clasificacion_patologica_tratamiento))
         {
             $this->bt_clasificacion_patologica_tratamiento = sc_convert_encoding($this->bt_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_clasificacion_patologica_tratamiento = str_replace('<', '&lt;', $this->bt_clasificacion_patologica_tratamiento);
         $this->bt_clasificacion_patologica_tratamiento = str_replace('>', '&gt;', $this->bt_clasificacion_patologica_tratamiento);
         $this->Texto_tag .= "<td>" . $this->bt_clasificacion_patologica_tratamiento . "</td>\r\n";
   }
   //----- bt_tratamiento_previo
   function NM_export_bt_tratamiento_previo()
   {
         $this->bt_tratamiento_previo = html_entity_decode($this->bt_tratamiento_previo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_tratamiento_previo = strip_tags($this->bt_tratamiento_previo);
         if (!NM_is_utf8($this->bt_tratamiento_previo))
         {
             $this->bt_tratamiento_previo = sc_convert_encoding($this->bt_tratamiento_previo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_tratamiento_previo = str_replace('<', '&lt;', $this->bt_tratamiento_previo);
         $this->bt_tratamiento_previo = str_replace('>', '&gt;', $this->bt_tratamiento_previo);
         $this->Texto_tag .= "<td>" . $this->bt_tratamiento_previo . "</td>\r\n";
   }
   //----- bt_id_paciente_fk
   function NM_export_bt_id_paciente_fk()
   {
         $this->bt_id_paciente_fk = html_entity_decode($this->bt_id_paciente_fk, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_id_paciente_fk = strip_tags($this->bt_id_paciente_fk);
         if (!NM_is_utf8($this->bt_id_paciente_fk))
         {
             $this->bt_id_paciente_fk = sc_convert_encoding($this->bt_id_paciente_fk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bt_id_paciente_fk = str_replace('<', '&lt;', $this->bt_id_paciente_fk);
         $this->bt_id_paciente_fk = str_replace('>', '&gt;', $this->bt_id_paciente_fk);
         $this->Texto_tag .= "<td>" . $this->bt_id_paciente_fk . "</td>\r\n";
   }
   //----- bg_id_gestion
   function NM_export_bg_id_gestion()
   {
         $this->bg_id_gestion = html_entity_decode($this->bg_id_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_id_gestion = strip_tags($this->bg_id_gestion);
         if (!NM_is_utf8($this->bg_id_gestion))
         {
             $this->bg_id_gestion = sc_convert_encoding($this->bg_id_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_id_gestion = str_replace('<', '&lt;', $this->bg_id_gestion);
         $this->bg_id_gestion = str_replace('>', '&gt;', $this->bg_id_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_id_gestion . "</td>\r\n";
   }
   //----- bg_logro_comunicacion_gestion
   function NM_export_bg_logro_comunicacion_gestion()
   {
         $this->bg_logro_comunicacion_gestion = html_entity_decode($this->bg_logro_comunicacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_logro_comunicacion_gestion = strip_tags($this->bg_logro_comunicacion_gestion);
         if (!NM_is_utf8($this->bg_logro_comunicacion_gestion))
         {
             $this->bg_logro_comunicacion_gestion = sc_convert_encoding($this->bg_logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_logro_comunicacion_gestion = str_replace('<', '&lt;', $this->bg_logro_comunicacion_gestion);
         $this->bg_logro_comunicacion_gestion = str_replace('>', '&gt;', $this->bg_logro_comunicacion_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_logro_comunicacion_gestion . "</td>\r\n";
   }
   //----- bg_causa_no_reclamacion_gestion
   function NM_export_bg_causa_no_reclamacion_gestion()
   {
         $this->bg_causa_no_reclamacion_gestion = html_entity_decode($this->bg_causa_no_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_causa_no_reclamacion_gestion = strip_tags($this->bg_causa_no_reclamacion_gestion);
         if (!NM_is_utf8($this->bg_causa_no_reclamacion_gestion))
         {
             $this->bg_causa_no_reclamacion_gestion = sc_convert_encoding($this->bg_causa_no_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_causa_no_reclamacion_gestion = str_replace('<', '&lt;', $this->bg_causa_no_reclamacion_gestion);
         $this->bg_causa_no_reclamacion_gestion = str_replace('>', '&gt;', $this->bg_causa_no_reclamacion_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_causa_no_reclamacion_gestion . "</td>\r\n";
   }
   //----- bg_reclamo_gestion
   function NM_export_bg_reclamo_gestion()
   {
         $this->bg_reclamo_gestion = html_entity_decode($this->bg_reclamo_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_reclamo_gestion = strip_tags($this->bg_reclamo_gestion);
         if (!NM_is_utf8($this->bg_reclamo_gestion))
         {
             $this->bg_reclamo_gestion = sc_convert_encoding($this->bg_reclamo_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_reclamo_gestion = str_replace('<', '&lt;', $this->bg_reclamo_gestion);
         $this->bg_reclamo_gestion = str_replace('>', '&gt;', $this->bg_reclamo_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_reclamo_gestion . "</td>\r\n";
   }
   //----- bg_fecha_reclamacion_gestion
   function NM_export_bg_fecha_reclamacion_gestion()
   {
         $this->bg_fecha_reclamacion_gestion = html_entity_decode($this->bg_fecha_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_fecha_reclamacion_gestion = strip_tags($this->bg_fecha_reclamacion_gestion);
         if (!NM_is_utf8($this->bg_fecha_reclamacion_gestion))
         {
             $this->bg_fecha_reclamacion_gestion = sc_convert_encoding($this->bg_fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_fecha_reclamacion_gestion = str_replace('<', '&lt;', $this->bg_fecha_reclamacion_gestion);
         $this->bg_fecha_reclamacion_gestion = str_replace('>', '&gt;', $this->bg_fecha_reclamacion_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_fecha_reclamacion_gestion . "</td>\r\n";
   }
   //----- bg_fecha_comunicacion
   function NM_export_bg_fecha_comunicacion()
   {
         $this->bg_fecha_comunicacion = html_entity_decode($this->bg_fecha_comunicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_fecha_comunicacion = strip_tags($this->bg_fecha_comunicacion);
         if (!NM_is_utf8($this->bg_fecha_comunicacion))
         {
             $this->bg_fecha_comunicacion = sc_convert_encoding($this->bg_fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_fecha_comunicacion = str_replace('<', '&lt;', $this->bg_fecha_comunicacion);
         $this->bg_fecha_comunicacion = str_replace('>', '&gt;', $this->bg_fecha_comunicacion);
         $this->Texto_tag .= "<td>" . $this->bg_fecha_comunicacion . "</td>\r\n";
   }
   //----- bg_fecha_programada_gestion
   function NM_export_bg_fecha_programada_gestion()
   {
         $this->bg_fecha_programada_gestion = html_entity_decode($this->bg_fecha_programada_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_fecha_programada_gestion = strip_tags($this->bg_fecha_programada_gestion);
         if (!NM_is_utf8($this->bg_fecha_programada_gestion))
         {
             $this->bg_fecha_programada_gestion = sc_convert_encoding($this->bg_fecha_programada_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_fecha_programada_gestion = str_replace('<', '&lt;', $this->bg_fecha_programada_gestion);
         $this->bg_fecha_programada_gestion = str_replace('>', '&gt;', $this->bg_fecha_programada_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_fecha_programada_gestion . "</td>\r\n";
   }
   //----- bg_autor_gestion
   function NM_export_bg_autor_gestion()
   {
         $this->bg_autor_gestion = html_entity_decode($this->bg_autor_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_autor_gestion = strip_tags($this->bg_autor_gestion);
         if (!NM_is_utf8($this->bg_autor_gestion))
         {
             $this->bg_autor_gestion = sc_convert_encoding($this->bg_autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_autor_gestion = str_replace('<', '&lt;', $this->bg_autor_gestion);
         $this->bg_autor_gestion = str_replace('>', '&gt;', $this->bg_autor_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_autor_gestion . "</td>\r\n";
   }
   //----- bg_descripcion_comunicacion_gestion
   function NM_export_bg_descripcion_comunicacion_gestion()
   {
         if (!NM_is_utf8($this->bg_descripcion_comunicacion_gestion))
         {
             $this->bg_descripcion_comunicacion_gestion = sc_convert_encoding($this->bg_descripcion_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_descripcion_comunicacion_gestion = str_replace('<', '&lt;', $this->bg_descripcion_comunicacion_gestion);
         $this->bg_descripcion_comunicacion_gestion = str_replace('>', '&gt;', $this->bg_descripcion_comunicacion_gestion);
         $this->Texto_tag .= "<td>" . $this->bg_descripcion_comunicacion_gestion . "</td>\r\n";
   }
   //----- bg_numero_cajas
   function NM_export_bg_numero_cajas()
   {
         $this->bg_numero_cajas = html_entity_decode($this->bg_numero_cajas, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bg_numero_cajas = strip_tags($this->bg_numero_cajas);
         if (!NM_is_utf8($this->bg_numero_cajas))
         {
             $this->bg_numero_cajas = sc_convert_encoding($this->bg_numero_cajas, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->bg_numero_cajas = str_replace('<', '&lt;', $this->bg_numero_cajas);
         $this->bg_numero_cajas = str_replace('>', '&gt;', $this->bg_numero_cajas);
         $this->Texto_tag .= "<td>" . $this->bg_numero_cajas . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Reclamacion'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Reclamación :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Reclamacion_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Reclamacion"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
